(function () {
  if ($('.js-forgottenForm').length > 0 && $('.forgottenForm').length > 0 && !$('.js-forgottenForm').hasClass('-open'))
    $('.js-forgottenForm').on('click', function () {
      $(this).addClass('-open');
      $('.forgottenForm').slideDown(400)
    })
})();