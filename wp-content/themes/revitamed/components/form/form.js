var Form = {
  init: function (target) {
    this.target = target;

    this.catchDOM();
    this.setCheckboxSetting();
    this.setEvents();
    return this
  },
  catchDOM: function () {
    this.$el = $(this.target);
    this.$group = this.$el.find('[data-group]');
    this.$item = this.$el.find('[data-item]');
    this.$button = this.$el.find('.form__button.-pay');

    return this
  },
  setCheckboxSetting: function () {
    this.$group.each(this.setGroupSetting.bind(this));

    return this
  },
  setEvents : function(){
      this.$button.on('click',function(){
          var valid = true;
          var $form = $(this).closest('form');
          $form.find('input:required').each(function () {
             if($(this).val()=='' || !$(this).is(':checked'))
               valid = false;
          });
          if(valid)
          {
              return window.confirm($(this).data('accept').toString());
          }
      })
  },
  setGroupSetting: function (i, e) {
    $(e).find(this.$item).on('click', this.checkboxClickEvent.bind(this));

    return this
  },
  checkboxClickEvent: function (e) {
    var self = $(e.currentTarget);
    if (self.is(':checked')) {
      self.closest(this.$group).find(this.$item).not(self).prop("checked", false).trigger('change');
    }
    return this
  }
};

$(document).ready(function () {

    var form = new Object(Form);
    var diagnostics = new Object(Form);
    var partialSurvey = new Object(Form);
    if($('.form').length > 0)
        form.init('.form');
    if($('.individualDiagnostics').length > 0)
        diagnostics.init('.individualDiagnostics');
    if($('.formPartialSurvey').length > 0)
        partialSurvey.init('.formPartialSurvey');

});
