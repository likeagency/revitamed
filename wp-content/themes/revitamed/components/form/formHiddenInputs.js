var formHiddenInputs = new (function () {
    "use strict";
    var $form, $checkbox;

    $(document).ready(function () {
        init();
    });

    function init() {
        $form = $('.form');

        if ($form.length > 0) {
            $checkbox = $form.find('.form__checkbox.-other');
            $checkbox.change(function () {

                if ($(this).is(':checked')) {
                    $form.find('textarea[name="user_other_purposes"]').show();
                }
                else {
                    $form.find('textarea[name="user_other_purposes"]').hide();
                }
            });
        }
    }


});