(function(){
    var $checkYes = $('#checkbox_62'), $checkNo = $('#checkbox_63'), $periodRelated = $('.-periodRelated');

    var callback = (function () {
        if($checkYes.is(':checked') || $checkNo.is(':checked'))
        {
            if($checkYes.is(':checked'))
                $periodRelated.show();
            else
                $periodRelated.hide();
        }
    });

    $checkYes.on('change',callback);
    $checkNo.on('change',callback);
    callback();

})();