var dietCreator = (function () {
  var $el, $select, $start, $stop, dateFormat = "dd/mm/yy";
  var datePickerSettings = {
    inline: true,
    showOtherMonths: true,
    dayNamesMin: ['Nie', 'Pon', 'Wt', 'Śr', 'Czw', 'Pt', 'So'],
    monthNames: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
    dateFormat: "dd-mm-yy"
    };

  function init() {
    catchDOM();
    createCustomSelect();

    createDatePicker();
    bindEvents();
  }

  function catchDOM() {
    $el = $('.dietCreator');
    $select = $el.find('select');
    $start = $el.find('.-dateStart');
    $stop = $el.find('.-dateStop');
  }

  function createCustomSelect() {
    $select.selectric();
  }

  function createDatePicker() {
    var today = new Date();
    $start.datepicker(datePickerSettings).datepicker("option", "defaultDate", today);
    $stop.datepicker(datePickerSettings).datepicker("option", "minDate", today);
  }

  function bindEvents() {
    $start.on('change', function () {
      $stop.datepicker("option", "minDate", getDate(this));
    });

    $stop.on('change', function () {
      $start.datepicker("option", "maxDate", getDate(this));
    })
  }

  function getDate(element) {
    var dateFormat = "dd-mm-yy";
    var date;
    try {
      date = $.datepicker.parseDate(dateFormat, element.value);
    } catch (error) {
      date = null;
    }

    return date;
  }

  init();
});

$(document).ready(function () {
  dietCreator();
});