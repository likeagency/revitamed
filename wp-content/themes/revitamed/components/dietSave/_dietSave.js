var dietSave = new (function () {
  "use strict";
  var $saveDiet, $form, $navigation;

  $(document).ready(function () {
    init();
  });

  function init() {
    catchDOM();
    if ($saveDiet.length > 0 && $form.length > 0)
      bindEvents();
  }

  function catchDOM() {
    $navigation = $('[data-diet-navigation]');
    $saveDiet = $navigation.find('[data-diet-save]');
    $form = $('[data-diet-form]');
  }

  function bindEvents() {
    $saveDiet.on('click', function (e) {
      e.preventDefault();
      dietFormSubmit('saveCurrentDiet', function (response) {
        $navigation.prepend('<p class="dietPicker__alert -success"> <i class="fa fa-check-circle"></i> Dieta została zapisana pomyslnie.</p>');
        setTimeout(function () {
          $navigation.find('.dietPicker__alert').remove();
        }, 2000)
      });
    });
  }

  function dietFormSubmit(action, callback) {
    $.ajax({
      url: ajaxurl + '?action=' + action,
      method: 'post',
      data: $form.serialize(),
      success: function (response) {
        callback(response);
      }
    });
  }


});