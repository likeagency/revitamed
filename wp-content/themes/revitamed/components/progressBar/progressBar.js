var ProgressBar = {
  init: function (target) {
    this.target = target;
    this.allDays = 0;
    this.daysFromStart = 0;
    this.howLongAccess = 0;
    this.catchDOM();
    this.getValues();
    this.howDays();
    this.howDaysFromStart();
    this.setBarWidth();
    this.access();
    this.setAccessPosition();
    return this
  },
  catchDOM: function () {
    this.$el = $(this.target);
    this.$yourLvl = this.$el.find('.progressBar__label.-yourLvl');
    this.$access = this.$el.find('.progressBar__label.-access');
    this.$register = this.$el.find('.progressBar__date.-register');
    this.$end = this.$el.find('.progressBar__date.-end');
    this.$bar = this.$el.find('.progressBar__progress');
    return this
  },
  getValues: function () {
    this.registerDate = this.$register.text();
    this.endDate = this.$end.text();
  },
  howDays: function () {
    this.allDays = this.daydiff(this.parseDate(this.registerDate), this.parseDate(this.endDate));
  },
  howDaysFromStart: function () {
    this.daysFromStart = this.daydiff(this.parseDate(this.registerDate), this.parseDate(this.$yourLvl.data('today')));
  },
  parseDate: function (str) {
    var mdy = str.split('.');
    return new Date(mdy[2], mdy[1], mdy[0] - 1);
  },
  daydiff: function (first, second) {
    return Math.round((second - first) / (1000 * 60 * 60 * 24));
  },
  setBarWidth: function () {
    var width = Math.floor((this.daysFromStart / this.allDays) * 100);
    if(width.toString() >= 0 && width.toString() <= 100){
      this.$bar.width(width.toString() + '%');
      this.$yourLvl.css('left',width.toString() + '%');
      this.checkPosition(width)
    }
    else{
      this.$yourLvl.hide()
    }
  },
  access: function () {
    this.howLongAccess = this.daydiff(this.parseDate(this.registerDate), this.parseDate(this.$access.data('access')));
  },
  setAccessPosition: function () {
    var position = Math.floor((this.howLongAccess / this.allDays) * 100);
    if(position.toString() >= 0 && position.toString() <= 100) {
      this.$access.css('left', position.toString() + '%');
      this.checkPosition(position)
    }
    else{
      this.$access.hide()
    }
  },
  checkPosition: function (position) {
    if(position < 15 || position > 70){
      $('.progressBar__label.-begin').css('top','50px');
      $('.progressBar__label.-end').css('top','50px');
      $('.progressBar__dates').css('top','23px');
      $('.progressBar__line').css('top','0');
      $('.progressBar__labels.-bottom').css('margin','0').find('.progressBar__label').css('padding','0')
    }
  }
};

$(document).ready(function () {
  if ($('.calendarTable').length > 0) {
    var progressBar = new Object(ProgressBar);
    progressBar.init('.progressBar');
  }
});