(function(){
    var $messageNavigation,$messageNavigationItems,$messages,url;
    function init(){
        $messageNavigationItems = $messageNavigation.find('.message-navigation__item');
        $messages = $('.message');
        url = $messageNavigation.data('url');
        if(url[url.length-1]!=='/')
            url += '/';
        bindEvents();
        filterMessages(null);
    }

    function bindEvents(){
        $messageNavigationItems.on('click',function(){
            var $activeItem = $messageNavigation.find('.-active');
            $activeItem.removeClass('-active');
            $(this).addClass('-active');
            filterMessages($(this).data('id'));
        })
    }

    function filterMessages(id){
        if(id === null)
        {
            var $activeItem = $messageNavigation.find('.-active');
            if($activeItem.length > 0)
            {
                id = $activeItem.data('id');
            }
            else
            {
                id = $messageNavigationItems.data('id');
            }
        }
        window.history.pushState('page2', 'Title', url + id + '/');
        $messages.hide();
        $('.message[data-userid="'+id+'"]').show(400);
    }

    $(document).ready(function(){
        $messageNavigation = $('.message-navigation');
        if($messageNavigation.length > 0)
        init();
    })
})();