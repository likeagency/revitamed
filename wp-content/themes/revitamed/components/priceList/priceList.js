var paymentPrepare = function () {
    "use strict";
    var spinner = '<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>',$purchaseButton,buttonText;
    function init() {
        catchDOM();
        bindEvents();

    }

    function catchDOM()
    {
        $purchaseButton = $('.js-buy-diet');
        buttonText= $purchaseButton.html()
    }

    function bindEvents()
    {
        $purchaseButton.on('click',function(e){
            e.preventDefault();
            if($(this).hasClass('is-ajax'))
                return;
            $(this).toggleClass('is-ajax');

            var $form = $(this).closest('form'),that = this,dataForm = $form.serializeArray();
            dataForm.push( { name: 'ID', value: $(this).data('id')});
            $(that).html(spinner);

            $.ajax({
                    url: ajaxurl + '?action=getSessionTsPayU',
                    type: 'POST',
                    cache: false,
                    data: dataForm,
                    success: function(response){
                        $(that).html(buttonText);
                        $(that).toggleClass('is-ajax');
                        try{
                            var jsonResponse = JSON.parse(response);
                            if(jsonResponse.hasOwnProperty('ts'))
                                $form.find('input[name="ts"]').val(jsonResponse.ts);
                            if(jsonResponse.hasOwnProperty('sig'))
                                $form.find('input[name="sig"]').val(jsonResponse.sig);
                            if(jsonResponse.hasOwnProperty('session_id'))
                                $form.find('input[name="session_id"]').val(jsonResponse.session_id);
                            $form.submit();
                        }
                        catch(e)
                        {

                        }
                    },
                error: function(e)
                {
                    $(that).html(buttonText);
                    $(that).toggleClass('is-ajax');
                }
                });
        })


    }
    $(document).ready(function(){
        init();
    })
}();