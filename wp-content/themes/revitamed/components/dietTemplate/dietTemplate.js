(function(){
    var $delete,$box,$modal;

    $(document).ready(function () {
        init();

    });

    function init () {

        catchDOM();
        bindEvents();
    }
    function catchDOM() {
        $box = $('.dietTemplate__container');
        $delete = $box.find('.delete_item');
        $modal = $('.modal');

    }
    function bindEvents() {
        $delete.on('click',function(){
            $modal.html('<div class="template__text">Czy chcesz na pewno usunąć tą dietę?' + '<br><br>' + '<div class="clickYes btn" data-id="'+$(this).data('id')+'">Tak</div>' + '&nbsp;' + '<div class="clickNo btn">Nie</div></div>');
            $modal.modal();
        });

        $modal.on('click','.clickYes',function(){
            deleteItem($(this).data('id'));
            $.modal.close();
        });
        $modal.on('click','.clickNo',function(){
            $.modal.close();
        });
    }

    function deleteItem(id) {
        $.ajax({
            url: ajaxurl + '?action=removeTemplateDiet',
            method: 'post',
            data: {id:id}
        });
        $box.find('[data-id="'+id+'"]').parent().remove();
    }
})();