/**
 * Dodaje dla grup checkboxów require. Jeżeli zastanawiasz się dlaczego nie został użyty radio input zamiast tego, to pytaj klienta.
 */
(function () {
    var $checkboxes;
    $(document).ready(function () {
        init();
    });
    function init() {
        catchDOM();
        bindEvents();
        $checkboxes.each(function () {
            $(this).trigger('change');
        })
    }

    function catchDOM() {
        $checkboxes = $('[data-required]');
    }

    function bindEvents() {
        $checkboxes.on('change',function () {
            if($(this).is(':checked'))
            {
                $('[data-required="'+$(this).data('required')+'"]').attr('required',false);
            }
            else
            {
                if($('[data-required="'+$(this).data('required')+'"]:checked').length === 0)
                {
                    $('[data-required="'+$(this).data('required')+'"]').attr('required',true);
                }
            }
        })
    }


})();