(function () {
    "use strict";
    var $block,$inputs,$age;
    $(document).ready(function () {
        init();
    });

    function init() {
        $block = $('.survey-1');
        if($block.length > 0) {
            catchDOM();
            bindEvents();
            changeTypeUser();
        }

    }

    function changeTypeUser(){
        var user = getTypeUser();
        console.log(user);
        if(user !== null)
        {
            $inputs.each(function(){
                if($(this).data('type').toString().contains(user))
                    showItemInput(this);
                else
                    hideItemInput(this);
            });
        }
    }

    function getTypeUser(){

        var age = $age.val();
        var isFemale = $('#checkbox_woman').is(':checked');

        if(age < 16 && age !== '')
            return 'd';
        else if(isFemale)
            return 'k';
        else
            return 'm';
    }
    function hideItemInput(item) {
            var $item = $(item);
            $item.hide();
            item.setAttribute('name', null);
            item.removeAttribute('required');
            $item.parent().hide();
    }
    function showItemInput(item) {
            var $item = $(item);
            $item.show();
            item.setAttribute('required', '');
            item.setAttribute('name', $item.data('name'));
            $item.parent().show();
    }

    function catchDOM() {
        $inputs = $block.find('[data-type]');
        $age = $block.find('input[name="user_age"]');
    }

    function bindEvents() {
        $age.on('change',changeTypeUser);
        $('#checkbox_woman').on('change',changeTypeUser);
        $('#checkbox_men').on('change',changeTypeUser);

    }


})();
if (typeof String.prototype.contains === 'undefined')
{
    String.prototype.contains = function(it) { return this.indexOf(it) !== -1; };
}
