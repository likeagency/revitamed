var callbackModifyUpload = (function () {
    "use strict";
    var $block,$yes,$no,$pseudo_no;
    $(document).ready(function () {
        init();
    });

    function init() {
        $block = $('.modal-survey');
        if($block.length > 0) {
            catchDOM();
            bindEvents();
        }

    }
    function catchDOM() {
        $yes = $block.find('.yes');
        $no = $block.find('.no');
        $pseudo_no = $block.find('.pseudo-no');
    }

    function bindEvents() {
        $yes.on('click',function () {
            document.querySelector('.modal__step2').style.display='block';
            document.querySelector('.modal__step1').style.display='none';
        });
        $pseudo_no.on('click',function () {
           $no.trigger('click');
        })

    }
    return function (){
        var enableButton = false;
        $('.modal__step2').find('.fileUploader__uploaded').each(function(){
            enableButton = enableButton || (this.childNodes && this.childNodes.length > 0);

        });

        $('.modal-survey').find('.pseudo-no').attr('disabled',!enableButton);
    };
})();
callbackModifyUpload();