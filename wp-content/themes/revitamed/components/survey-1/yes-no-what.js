/**
 * Zapewnia funkcjonalność dodatkowego inputa tekstowego dla pary checkboxów Tak/Nie w wypadku wybrania jednego z nich
 */
(function () {
    "use strict";
    var $inputs,$checkboxes;
    $(document).ready(function () {
        init();
    });

    function init() {
            catchDOM();
            bindEvents();
            start();

    }
    
    function start() {
        $checkboxes.each(function () {
            $(this).trigger('change');
        })
    }

    function catchDOM() {
        $inputs = $('[data-show]');
        $checkboxes = $inputs.find('[data-toggle]')
    }

    function bindEvents() {
        $checkboxes.on('change',function () {
            var $msg = $(this).closest('[data-show]').find('.-hideOnNo');
           if($(this).is(':checked'))
           {
               $msg.show();
           }
           else
           {
               $msg.hide();
           }

            $(this).closest('[data-show]').find('[data-show]').find('[data-toggle]').trigger('change');

        });
    }


})();