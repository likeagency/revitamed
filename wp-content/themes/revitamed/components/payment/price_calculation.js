var payment = function () {
    "use strict";
    var $price,$paymentForm,triggerForm=false;
    function init() {
        catchDOM();
        if($price.length > 0)
            bindEvents();
            $price.trigger('change')
    }

    function catchDOM()
    {
        $price = $('input[data-price]');
    }

    function bindEvents()
    {
        $price.on('change',function(){
            var price = 0.0;
            $('input[data-price]:checked').each(function () {
                price+= $(this).data('price');
            });
            $('#overallPrice').html(price.toString() + ' zł');
        })
    }

    $(document).ready(function(){
        init();
    })

}();