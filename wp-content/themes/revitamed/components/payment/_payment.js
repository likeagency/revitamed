var payment = function () {
    "use strict";
    var $form,$paymentForm,triggerForm=false,$paymentInputs;
    function init() {
        catchDOM();
        if($form.length > 0)
        bindEvents();
    }

    function catchDOM()
    {
        $form = $('.js-payment-form-acc');
        $paymentForm = $('.js-actual-payment-form');
        $paymentInputs = $form.find('.payment__input[name="id"]')
    }

    function bindEvents()
    {
        $form.on('submit',function (e) {
            if(!triggerForm)
                e.preventDefault();
            else
                return;
            if($(this).hasClass('is-ajax'))
                return;
            else
                $(this).toggleClass('is-ajax');

            var dataForm = $paymentForm.serializeArray(), thatForm = this;
            dataForm.push( { name: 'ID', value: $(this).find('input[name="id"]:checked').val()});
            dataForm.push( { name: 'ID_2', value: $(this).find('input[name="id_2"]:checked').val()});
            $.ajax({
                url: ajaxurl + '?action=getSessionTsPayU',
                type: 'POST',
                cache: false,
                data: dataForm,
                success: function(response){
                    $(thatForm).toggleClass('is-ajax');
                    try{
                        var jsonResponse = JSON.parse(response);
                        if(jsonResponse.hasOwnProperty('ts'))
                            $paymentForm.find('input[name="ts"]').val(jsonResponse.ts);
                        if(jsonResponse.hasOwnProperty('sig'))
                            $paymentForm.find('input[name="sig"]').val(jsonResponse.sig);
                        if(jsonResponse.hasOwnProperty('session_id'))
                            $paymentForm.find('input[name="session_id"]').val(jsonResponse.session_id);
                        triggerForm = true;
                       $form.submit();
                    }
                    catch(e)
                    {

                    }
                },
                error: function(e)
                {
                    $(thatForm).toggleClass('is-ajax');
                }
            });

        })
        $paymentInputs.on('change',function () {
            if($(this).is(':checked') && $(this).data('price')!='0')
            $('.payment__content').show();
        })
    }

    $(document).ready(function(){
        init();
    })

}();