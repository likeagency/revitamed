var contact = new(function () {
  var $contact;
  var $map;

  function init() {
    $contact = $('.contact');

    if(isElement($contact)) {
      catchDOM();
      bindEvents();

      initApi();
    }
  }

  function catchDOM() {
    $map = $contact.find('.contact__map');
  }

  function bindEvents() {
  }

  function isElement(item) {
    return item.length > 0
  }

  function initApi() {
    var key = $map.data('key');
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?key=' + key + '&callback=contact.initMap';
    document.body.appendChild(script);
  }

  function initMap() {
    var lat = $map.data(lat).lat;
    var lng = $map.data(lng).lng;
    var zoom = $map.data(zoom).zoom;
    var point = new google.maps.LatLng(lat, lng)
    var map = new google.maps.Map(document.getElementById('contactMap'), {
      zoom: zoom,
      center: point,
      mapTypeId: 'roadmap'
    });

    var marker = new google.maps.Marker({
      position: point,
      map: map
    });
  }

  $(document).ready(function () {
    init();
  });

  return {
    initMap: initMap
  }
})();
