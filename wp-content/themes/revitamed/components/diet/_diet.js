var diet = (function () {
  var $el, $item, $sort;

  function init() {

    catchDOM();
    if ($el.length > 0) {
      bindEvents();
      sort();
    }
  }

  function catchDOM() {
    $el = $('.diet');
    $item = $el.find('.diet__item');
    $sort = $el.find('.diet__sortItem');
  }

  function bindEvents() {
    $sort.on('click', clickEvent)
  }

  function sort(){
      $item.sort(function(b, a) {

          return parseInt($(b).data('id')) - parseInt($(a).data('id'));
      }).each(function() {
          var elem = $(this);
          elem.remove();
          $(elem).appendTo('.diet');
      });
  }

  function clickEvent(e) {
    var self = $(e.currentTarget);

    if (self.data('diet') !== '') {
      $item.each(function () {
        if ( self.data('diet').indexOf($(this).data('status'))===-1 ) {
          $(this).hide();
        }
        else
        {
          $(this).show();
        }
      })
    }
    else
    {
        $item.each(function () {$(this).show();});
    }
  }

  init();
});


$(document).ready(function () {
  diet();
});