var Print = {
  init: function (target) {
    this.target = target;

    this.catchDOM();
    this.bindEvents();
    this.makeHtml();
    return this
  },
  catchDOM: function () {
    this.$el = $(this.target);
    this.$calendar = $('.diet-calendar');
    return true
  },
  bindEvents: function () {
    this.$el.on('click', this.print.bind(this));
  },
  print: function () {
    $('.shopingList__list').addClass('-notPrint');
    $('.calendar__title').removeClass('-notPrint');
    $('.calendarTable').removeClass('-notPrint');
    window.print();
  },
  makeHtml: function () {

      $.get(ajaxurl + '?action=generate_calendar_user',function (data) {
          $(".calendarTable__content > tbody").append(data.html)
      });

  }
};

// (function () {
//   if ($('.button.-print').length > 0) {
//     var print = new Object(Print);
//     print.init('.button.-print');
//   }
// })();