var DietPicker = {
    products: [],

    init: function (target) {
        this.target = target;
        this.data = {};
        this.catchDOM();
        this.bindEvents();
        this.createAutocomplete();
        this.createData();

        return this;
    },
    catchDOM: function () {
        this.$el = $(this.target);
        this.$menu = this.$el.find('[data-menu]');
        this.$header = this.$el.find('[data-header]');
        this.$product = this.$el.find('[data-product]');
        this.$creator = this.$el.find('.dietPicker-form__add');
        this.$productCreatorName = this.$el.find('[data-product_creator_name]');

    },
    bindEvents: function () {
        this.$header.on('click', this.toggleMenu.bind(this));
        this.$creator.on('click', this.createProducts.bind(this));

    },
    toggleMenu: function (e) {
        var self = $(e.currentTarget),
            parent = self.closest(this.$menu);
        parent.hasClass('-active') ? parent.removeClass('-active') : parent.addClass('-active');
    },

    createAutocomplete: function () {
        var that = this;
        this.$productCreatorName.autocomplete({
            source: that.$el.data('autocomplete').map(function (e) {
                return {"value": e[1], "label": e[1], "ID": e[0]};
            })
        });
        this.$productCreatorName.each(function(){
            $(this).autocomplete('widget').addClass('fixedHeight');
        });
        this.$productCreatorName.on("autocompleteselect", function (event, ui) {
            $(event.target).closest('.dietPicker-form').find('.dietPicker-form__add').data("id", ui.item.ID);
            $.ajax({
                type: "GET",
                url: ajaxurl,
                cache: true,
                data: {action: 'get_recipe', id: ui.item.ID},
                success: (function (data) {
                    this.data[ui.item.ID] = data;
                }).bind(that)
            });
        });
    },
    createData: function () {
        var that = this;
        this.$menu.each(function () {
            that.products.push({
                name: $(this).data('menu'),
                main: [],
                optional: []
            })
        });
    },

    checkProductName: function (name) {
        for (var i = 0; i < this.$el.data('autocomplete').length; i++) {
            if (this.$el.data('autocomplete')[i][0] === name) {
                return true
            }
        }

        return false
    },

    createProducts: function (event) {
        var $that = $(event.target);


        var id = $that.closest('.dietPicker-form').find('input[type="hidden"]').val();
        if($that.data('id')==0 && id==0)
            return;


        if(id===0 || id==='0')
        {

        }
        else
            if(!confirm('Czy na pewno chcesz zmienić danie? Skasujemy wprowadzone ilości.'))
                return;

        $that.closest('.dietPicker-form').find('input[type="hidden"]').val($that.data('id'));
        var $box =$that.closest('.dietPicker-form');
        var data = this.data[$that.data('id')];
        if (!data)
            $.ajax({
                type: "GET",
                url: ajaxurl,
                cache: true,
                data: {action: 'get_recipe', id: $that.data('id')},
                success: (function (data) {
                    this.data[$that.data('id')] = data;
                    this.renderIng(this.data[$that.data('id')],$box,$that.closest('.dietPicker-form').find('.dietPicker-form__ingredients'));
                }).bind(this)
            });
        else {
            this.renderIng(data,$box,$that.closest('.dietPicker-form').find('.dietPicker-form__ingredients'));
        }


    },

    renderIng: function(data,$box,$container){
        var html = '';
        for (var i = 0; i < data.length; i++)
            html += '<div class="dietPicker-form__ingredient">\n' +
                '<input name="meal['+$box.data('day')+']['+$box.data('meal')+'][ing][' + data[i].ing + ']" ' +
                'value="' + data[i].amount_show + '"> ' + data[i].unit + ' <strong>' + data[i].name + '</strong></div>' ;
        $container.html(html);
    }

};


$(document).ready(function () {
    var diet = '.js-dietPicker',
        dietObj;
    ($(diet).length > 0) ? (dietObj = new Object(DietPicker), dietObj.init(diet)) : '';
    // $('form').on('submit',function(e){
    //    e.preventDefault();
    //    console.log($(this).serializeArray());
    // });
});