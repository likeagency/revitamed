var diagrams = new (function () {
  var $chart1, $chart2, weight, target;


  $(document).ready(function () {
    initChart1();
    initChart2();
  });

  function initChart1 () {
    $chart1 = $('.chart.graph1');
    if($chart1.length > 0) {
      chart1();
    }
  }

  function initChart2() {
    $chart2 = $('.chart.graph2');
    if($chart2.length > 0) {
      chart2();
    }
  }

  function chart1() {
    getWeight();

    CanvasJS.addColorSet("chart1",
      [//colorSet Array
        "#e20c18",
        "#289f58"
      ]);
    var low = weight[0], max = weight[0];
    var options = {
      animationEnabled: true,
      interactivityEnabled: false,
      colorSet: "chart1",
      data: [
        {
          type: "spline", //change it to line, area, column, pie, etc,
          dataPoints: []
        },
        {
          type: "spline", //change it to line, area, column, pie, etc,
          dataPoints: []
        }
      ],
      axisX:{
        tickLength: 0
      },
      axisY:{
          interval: 5,
      },

    };
    if(weight != "" && weight != null){
      $.each(weight, function (i) {
        low =  Math.min(low,weight[i]);
        max =  Math.max(max,parseInt(weight[i]));
        options.data[0].dataPoints.push({ x: 10 + 5 * i, y: parseInt(weight[i]), label: " " })
      });
    }
    if(target != "" && target != null){
        low =  Math.min(low,target);
        max =  Math.max(max,parseInt(target));
      options.data[1].dataPoints.push({ x: 10, y: target },{ x: 45, y: target })
    }
      low -= 1;
      max += 1;
      options.axisY.minimum = Math.floor(low/10)*10;
      options.axisY.maximum = Math.ceil (max/10)*10;
      options.axisY.interval = Math.max(10,(options.axisY.maximum - options.axisY.minimum)/4);
    $chart1.find('.chart__canvas').CanvasJSChart(options);
  }

  function getWeight() {
    weight = $chart1.data('weight');
    target = $chart1.data('target')
  }

  function chart2() {

    CanvasJS.addColorSet("chart2",
      [//colorSet Array
        "#a4c842",
        "#e20c18",
        "#289f58"
      ]);
      var low = 999, max = 0;
    //Better to construct options first and then pass it as a parameter
    var options = {
      animationEnabled: true,
      interactivityEnabled: false,
      colorSet: "chart2",
      data: [
        {
          type: "spline", //change it to line, area, column, pie, etc
          dataPoints: []
        },
        {
          type: "spline",
          dataPoints: []
        },
        {
          type: "spline", //change it to line, area, column, pie, etc
          dataPoints: []
        }
      ],

      axisX:{
        tickLength: 0
      },
      axisY:{
        tickLength: 0,
        interval: 10
      }
    };
    if($chart2.data('waist') != "" && $chart2.data('waist') != null) {
      $.each($chart2.data('waist'), function (i) {
          low =  Math.min(low, parseInt($chart2.data('waist')[i]));
          max =  Math.max(max, parseInt($chart2.data('waist')[i]));
        options.data[0].dataPoints.push({x: 10 + 5 * i, y: parseInt($chart2.data('waist')[i]), label: " "})
      });
    }
    if($chart2.data('stomach') != "" && $chart2.data('stomach') != null) {
      $.each($chart2.data('stomach'), function (i) {
          low =  Math.min(low, parseInt($chart2.data('stomach')[i]));
          max =  Math.max(max, parseInt($chart2.data('stomach')[i]));
        options.data[1].dataPoints.push({x: 10 + 5 * i, y: parseInt($chart2.data('stomach')[i]), label: " "})
      });
    }
    if($chart2.data('hips') != "" && $chart2.data('hips') != null) {
      $.each($chart2.data('hips'), function (i) {
          low =  Math.min(low, parseInt($chart2.data('hips')[i]));
          max =  Math.max(max, parseInt($chart2.data('hips')[i]));
        options.data[2].dataPoints.push({x: 10 + 5 * i, y: parseInt($chart2.data('hips')[i]), label: " "})
      });
    }
    low -= 1;
    max += 1;
      options.axisY.minimum = Math.floor(low/10)*10;
      options.axisY.maximum = Math.ceil (max/10)*10;
      options.axisY.interval = Math.max(10,(options.axisY.maximum - options.axisY.minimum)/4);
    $chart2.find('.chart__canvas').CanvasJSChart(options);
  }



})();


