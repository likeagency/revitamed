var DietAdditional = {
  products: [],

  init: function (target) {
    this.target = target;

    this.catchDOM();
    this.bindEvents();

    this.createAutocomplete();
    this.loadData();

    return this
  },
  catchDOM: function () {
    this.$el = $(this.target);
    this.$product = this.$el.find('[data-product]');
    this.fieldProduct = this.$el.data('field_name');

    this.$creator = this.$el.find('[data-product_creator]');
    this.$create = this.$el.find('[data-product_creator_create]');
    this.$productCreatorName = this.$el.find('[data-product_creator_name]');
    this.$productList = this.$el.find('[data-product_list]');
  },
  bindEvents: function () {
    this.$create.on('click', this.createProduct.bind(this));
    this.$el.on('click', '[data-productRemove]', this.removeProduct.bind(this));
  },
  createAutocomplete: function () {
    var that = this;
    this.$productCreatorName.autocomplete({
      source: that.$el.data('autocomplete').map(function(e){return e[0];})
    });
    this.$productCreatorName.each(function(){
      $(this).autocomplete('widget').addClass('fixedHeight');
    });
  },
  loadData: function () {
    var that = this;
    if (this.$product.length > 0) {
      this.$product.each(function () {
        that.setProduct($(this).data('name'))
      })
    }
  },
  createProduct: function (e) {
    var self = $(e.currentTarget),
      parent = self.closest(this.$creator),
      productName = parent.find(this.$productCreatorName);

    parent.find('.dietAdditional__alert').remove();
    if (productName.val()) {
      if (this.checkProductName(productName.val())) {
        if (!this.isExistProduct(productName.val())) {
          this.setProduct(productName.val());
          this.generateProducts();
          productName.val('');
        } else {
          productName.val('');
          parent.prepend('<p class="dietAdditional__alert -warning"> <i class="fa fa-exclamation-circle"></i> Produkt jest już dodany.</p>');
        }
      } else {
        productName.val('');
        parent.prepend('<p class="dietAdditional__alert -warning"> <i class="fa fa-exclamation-circle"></i> Nazwa produktu jest niepoprawna.</p>');
      }
    } else {
      parent.prepend('<p class="dietAdditional__alert"> <i class="fa fa-exclamation-circle"></i> Proszę uzupełnić wszystkie pola.</p>');
    }
  },
  checkProductName: function (name) {
    for (var i = 0; i < this.$el.data('autocomplete').length; i++) {
      if (this.$el.data('autocomplete')[i][0] === name) {
        return true
      }
    }
    return false
  },
  isExistProduct: function (name) {
    for (var i = 0; i < this.products.length; i++) {
      if (this.products[i] === name) {
        return true
      }
    }
    return false
  },
  setProduct: function (name) {
    this.products.push(name)
  },
  generateProducts: function () {
    var template = '',that = this;
    this.$productList.empty();
    $.each(this.products, function (i) {
      template += '<div class="dietAdditional-item" data-product data-name="' + this + '">' + this + '<input type="hidden" name="'+that.fieldProduct+'[]" value="' + this + '"><span class="dietAdditional-item__remove" data-productRemove><i class="fa fa-minus-circle"></i></span></div>';
    });

    this.$productList.append(template);
  },
  removeProduct: function (e) {
    var self = $(e.currentTarget).parent(),
      name = self.data('name'),
      index,
      isRemove = false;

    $.each(this.products, function (i, e) {
      if (e == name) {
        index = i;
        isRemove = true;
      }
    });
    isRemove ? this.products.splice(index, 1) : '';

    this.generateProducts();
  }
};

$(document).ready(function () {
  var diet = '.js-dietAdditional',
    dietObj;
  ($(diet).length > 0) ? (dietObj = new Object(DietAdditional), dietObj.init(diet)) : '';
});