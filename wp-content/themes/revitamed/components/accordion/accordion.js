var accordion = new (function () {

  var $accordion, $item, $hidden, $title;

  $(document).ready(function () {
    init();
  });

  function init () {
    $accordion = $('.accordion');

    if($accordion.length > 0) {
      $item = $accordion.find('.accordion__item');
      $hidden = $accordion.find('.accordion__hidden');
      $title = $accordion.find('.accordion__title')
      bindEvents();
      searchActive();
    }
  }

  function bindEvents() {
    $title.click(function () {
      showContent($(this));
    });
  }

  function showContent($this) {
    if($this.next('.accordion__hidden').hasClass('-active')){
      $this.next('.accordion__hidden').removeClass('-active').slideUp();
    }
    else{
      $this.closest('.faq__item').find('.accordion__hidden').removeClass('-active').slideUp();
      $this.next('.accordion__hidden').addClass('-active').slideDown();
    }
  }

  function searchActive() {
    if($hidden.has('.-active')){
      $accordion.find('.-active').parent().addClass('-active').parent().addClass('-active')
    }
  }

});