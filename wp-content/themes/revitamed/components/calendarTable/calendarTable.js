var TaskExecution = {
  init: function (target) {
    this.target = target;
    this.catchDOM();
    this.bindEvents();
    this.checkActive();

    return this
  },
  catchDOM: function () {
    this.$el = $(this.target);
    this.$task = this.$el.find('.fun-col');
    this.$confirmed = this.$task.find('.confirmed');
    this.$unconfirmed = this.$task.find('.unconfirmed');
    this.$existed = this.$task.find('.existed');

    return this
  },
  bindEvents: function () {
    this.$confirmed.on('click', this.confirmed.bind(this));
    this.$unconfirmed.on('click', this.unconfirmed.bind(this));
    this.$existed.on('click', this.existed.bind(this));
  },
  checkActive: function () {
    this.$confirmed.each(function () {
      if($(this).hasClass('-active')){
        $(this).off('click');
        $(this).siblings('.unconfirmed').addClass('-disabled').off('click');
        $(this).siblings('.existed').addClass('-disabled').off('click');
      }
      if($(this).hasClass('-disabled')){
        $(this).off('click');
      }
    });

    this.$unconfirmed.each(function () {
      if($(this).hasClass('-active')){
        $(this).off('click');
        $(this).siblings('.confirmed').addClass('-disabled').off('click');
        $(this).siblings('.existed').addClass('-disabled').off('click');
      }
      if($(this).hasClass('-disabled')){
        $(this).off('click');
      }
    });

    this.$existed.each(function () {
        if($(this).hasClass('-active')){
            $(this).off('click');
            $(this).siblings('.confirmed').addClass('-disabled').off('click');
            $(this).siblings('.unconfirmed').addClass('-disabled').off('click');
        }
        if($(this).hasClass('-disabled')){
            $(this).off('click');
        }
    });

  },
  confirmed: function (e) {
    var that = $(e.currentTarget);
    that.addClass('-active');
    that.siblings('.unconfirmed').off('click');
    that.siblings('.unconfirmed').addClass('-disabled');

    that.siblings('.existed').off('click');
    that.siblings('.existed').addClass('-disabled');
    this.sendToBackend(that)
  },
  existed: function (e) {
    var that = $(e.currentTarget);
    that.addClass('-active');
    that.siblings('.unconfirmed').off('click');
    that.siblings('.unconfirmed').addClass('-disabled');

    that.siblings('.confirmed').off('click');
    that.siblings('.confirmed').addClass('-disabled');
    this.sendToBackend(that)
  },
  unconfirmed: function (e) {
    var that = $(e.currentTarget);
    that.addClass('-active');
    that.siblings('.confirmed').off('click');
    that.siblings('.confirmed').addClass('-disabled');

    that.siblings('.existed').off('click');
    that.siblings('.existed').addClass('-disabled');
    this.sendToBackend(that)
  },

  sendToBackend: function (that) {

    $.ajax({
      url: ajaxurl + '?action=diet_meal_confirmation',
      method: 'POST',
      data: {date: that.closest('.day-row').data('day'), meal: that.parent().siblings('.meal-col').data('meal'), value: that.data('value')},
      success: function(data){
        if(typeof data.part !== 'undefined')
          $('#current_plan_real').html(data.part + '% ')
      }

    })
  }
};

$(document).ready(function () {
  if ($('.calendarTable').length > 0) {
    var individualDiagnostics = new Object(TaskExecution);
    individualDiagnostics.init('.calendarTable');
  }
});