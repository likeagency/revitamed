var modal = function () {
  var $modal = $(".modal");

  if ($modal.length > 0) {
    $modal.each(function () {
      if ($(this).hasClass('-active')) {
        $(this).modal( $(this).hasClass('modal-survey') ? {clickClose: false} : undefined);
      }
    })
  }
};


$(document).ready(function () {
  modal();
});