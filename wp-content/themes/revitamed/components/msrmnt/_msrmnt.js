var circuitAjax = function () {
  var $container = $('.msrmnt'),$submit = $container.find(".js-submit-circuit"),
      $alertContainer=$container.find('.js-alert-container');

  if ($submit.length > 0) {
    $submit.submit(function (e) {
      $.ajax({
        type: "POST",
        url: ajaxurl,
        data: $submit.serialize(),
        success: function (data) {
            var parsedJSON = JSON.parse(data);
            $alertContainer.append('<div class="msrmnt__success">'+parsedJSON.msg+'</div>');
            setTimeout(function () {
                $alertContainer.find('.msrmnt__success').remove();
            }, 4000);
        }
      });

      e.preventDefault();
    });
  }
};

var weightAjax = function () {
  var $container = $('.msrmnt'),$submit = $container.find(".js-submit-weight"),
      $alertContainer=$container.find('.js-alert-container');

  if ($submit.length > 0) {
    $submit.submit(function (e) {
      $.ajax({
        type: "POST",
        url: ajaxurl,
        data: $submit.serialize(),
        success: function (data) {
            var parsedJSON = JSON.parse(data);
          $alertContainer.append('<div class="msrmnt__success">'+parsedJSON.msg+'</div>');
            setTimeout(function () {
                $alertContainer.find('.msrmnt__success').remove();
            }, 4000);
        }
      });

      e.preventDefault();
    });
  }
};


$(document).ready(function () {
  circuitAjax();
  weightAjax();
});