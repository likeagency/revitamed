var printArticle = new (function () {

  var $print;

  $(document).ready(function () {
    init();
  });

  function init () {
    $print = $('.articleNav__button.-print');

    if($print.length > 0) {
      bindEvents();
    }
  }

  function bindEvents() {
    $print.click(function () {
      print()
    })
  }

  function print() {
    window.print()
  }

});