var FileUploader;
FileUploader = {
  init: function (target,callback) {
    this.target = target;
    this.isLocked = false;
    this.callback = callback;

    this.catchDOM() ? this.bindEvents() : '';
  },
  catchDOM: function () {
    this.$el = $(this.target);
    this.$item = this.$el.find('.fileUploader');
    this.$uploader = this.$el.find('.fileUploader__file');
    this.$button = this.$el.find('.fileUploader__upload');
    this.$box = this.$el.find('.fileUploader__filebox');
    this.$file = this.$el.find('.fileUploader__uploaded');
    this.$modal = this.$el.find('#file-research-modal');

    return !!this.$el.length
  },
  bindEvents: function () {
    this.$uploader.on('change', this.uploadFile.bind(this));
    this.$box.on('click', '.fileUploader__uploadedDelete', this.deleteFile.bind(this))
  },
  uploadFile: function (e) {
    var formData = new FormData(), self = $(e.currentTarget), that = this;
    if (!this.isLocked) {
      this.lockButton(self);
      formData.append("file", self[0].files[0]);
      formData.append("section", self.data('section'));
      $.ajax({
        url: ajaxurl + '?action=upload_file_user',
        data: formData,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function (data) {
          that.addFile(self, data);
          that.unlockButton(self);
          that.callback();
        },
        error: function () {
          that.unlockButton(self);
        }
      });
    }
  },
  deleteFile: function (e) {
    var self = $(e.currentTarget), that = this;

    $.ajax({
      url: ajaxurl + '?action=remove_file_user',
      data: {"id": self.data('delete')},
      type: 'POST',
      success: function (data) {
          that.removeFile(self, data);
          that.callback();
      },
      error: function () {
        that.unlockButton(self);
      }
    });
  },
  lockButton: function (input) {
    this.isLocked = true;
    input.closest(this.$item).find(this.$button).find('span').hide();
    input.closest(this.$item).find(this.$button).find('i').show();
  },
  unlockButton: function (input) {
    this.isLocked = false;
    input.closest(this.$item).find(this.$button).find('span').show();
    input.closest(this.$item).find(this.$button).find('i').hide();
  },
  addFile: function (self, data) {
    console.log(self,data);
    data = JSON.parse(data);
    if (!data.error) {
      $('.fileUploader__filebox[data-section="'+$(self).data('section')+'"]').append('' +
        '<div class="fileUploader__uploaded">' +
        '<div class="fileUploader__uploadedIcon">' +
        '<i class="fa fa-file-text-o"></i>' +
        '</div>' +
        '<div class="fileUploader__uploadedName">' + data.name + '</div>' +
        '<div class="fileUploader__uploadedDelete" data-delete="' + data.ID + '"><i class="fa fa-trash-o"></i></div>' +
        '</div>');
    } else {
      this.$modal.text(data.msg).modal();
    }
  },
  removeFile: function (self, data) {
    data = JSON.parse(data);
    if (!data.error) {
      $('[data-delete="'+$(self).data('delete')+'"]').closest('.fileUploader__uploaded').remove();
      // self.closest($('.fileUploader__uploaded')).remove()
    }
  }


};

$(document).ready(function () {
  var research;
  research = new Object(FileUploader);

  research.init('.js-file-research',callbackModifyUpload);
});