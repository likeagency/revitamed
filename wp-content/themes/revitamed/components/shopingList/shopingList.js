var PrintShopingList = {
  init: function (target) {
    this.target = target;

    this.catchDOM();
    this.bindEvents();
    this.initDatepickers();
    return this
  },
  catchDOM: function () {
    this.$el = $(this.target);
    this.$modal = $('.shopingList');
    this.$background = this.$modal.find('.shopingList__background');
    this.$exit = this.$modal.find('.shopingList__exit');
    this.$printPDF = this.$modal.find('.shopingList__button.-pdf');
    this.$sendMAIL = this.$modal.find('.shopingList__button.-mail');
    return true
  },
  bindEvents: function () {
    this.$el.on('click', this.showPopup.bind(this));
    this.$background.on('click', this.hidePopup.bind(this));
    this.$exit.on('click', this.hidePopup.bind(this));
    this.$printPDF.on('click', this.getDays.bind(this));
    this.$sendMAIL.on('click', this.generateListShoppingEmail.bind(this));
  },
  initDatepickers: function () {

    setDatepickers();
    var dateFormat = "dd.mm.yy", maxDate;
    function setDatepickers() {
      var from, to;
      from = $("#from");
        from.datepicker({
          dateFormat: "dd.mm.yy",
          minDate: $.datepicker.parseDate('dd.mm.yy',from.data('min')),
          autoclose: true,
          regional: 'pl'
        })
        .on("change", function () {
          var thisVal = $(this).val().split(".");
          thisVal.pop();
          thisVal.push(parseInt($(this).val().split(".").pop()) + 1);
          to.datepicker("option", "minDate", getDate(this));
        });
      to = $("#to");
      to.datepicker({
          dateFormat: "dd.mm.yy",
          autoclose: true,
          maxDate: $.datepicker.parseDate('dd.mm.yy',to.data('max')),
          regional: 'pl'
        })
        .on("change", function () {
          from.datepicker("option", "maxDate", getDate(this));
        });
    }

    function getDate(element) {
      var date;
      var string = element.value;
      var newDay = parseInt(string.slice(0, 2)) + 1;
      var newValue;
      if (element.id === 'from') {
        newValue = newDay.toString() + string.slice(2);
      }
      else {
        newValue = element.value;
      }
      try {
        date = $.datepicker.parseDate(dateFormat, newValue);
      } catch (error) {
        date = null;
      }
      return date;
    }
  },
  showPopup: function () {
    this.$modal.fadeIn('slow')
  },
  hidePopup: function () {
    this.$modal.fadeOut('slow')
  }, 
  printPDF: function () {
    $('.shopingList__list').removeClass('-notPrint');
    $('.calendar__title').addClass('-notPrint');
    $('.calendarTable').addClass('-notPrint');
    window.print();
  },
  getDays: function () {
    var from = $('#from'), to = $('#to');
    if(from.val() != "" && to.val() != ""){
      from.removeAttr('style');
      to.removeAttr('style');
      var start = from.datepicker('getDate');
      var end = to.datepicker('getDate');
      if (!start || !end) return;
      var days = (end - start) / 1000 / 60 / 60 / 24;
      this.getFromBackend(days, from, to);
    }
    else{
      from.css('border','1px solid red');
      to.css('border','1px solid red')
    }
  },
  getFromBackend: function (days, from, to) {
    $('.shopingList__dates').find('span.shopingList__fromDate').text(from.val());
    $('.shopingList__dates').find('span.shopingList__toDate').text(to.val());
    var  that = this;
    $.get(ajaxurl + '?action=generate_list_shopping&from=' + from.val() + '&to=' + to.val(), function(data) {
      $('.shopingList__itemsHolder').text("");
      $.each(data.data, function (key){
        var title = key;
        var itemsHTML = "";
        $.each(this, function (key){
          var product = key;
          itemsHTML = itemsHTML +
          '<div class="shopingList__item">' +
            '<div class="shopingList__productName">'+product+'</div>' +
            '<div class="shopingList__weight">'+this+'</div>'+
          '</div>';
        });
        $('.shopingList__itemsHolder').append('<div class="shopingList__heading">'+title+'</div>' +
          '<div class="shopingList__itemHolder">' + itemsHTML +
        '</div>');
      })
    }).success(function () {
        that.printPDF();
    });
  },
  generateListShoppingEmail: function () {
    var url = ajaxurl + '?action=generate_list_shopping_email',that=this;

    var from = $('#from'), to = $('#to');
    if(from.val() != "" && to.val() != ""){
      from.removeAttr('style');
      to.removeAttr('style');
      var start = from.datepicker('getDate');
      var end = to.datepicker('getDate');
      if (!start || !end) return;
    }
    else{
      from.css('border','1px solid red');
      to.css('border','1px solid red')
    }

    var data = {
      from: from.val(),
      to: to.val()
    };

    $.post(url, data, function(){

      that.$modal.find('.shopingList__success').show(200);
      setTimeout(function(){
          that.$modal.find('.shopingList__success').hide(200);
      },5000);
    });
  }
};

(function () {
  if ($('.button.-print').length > 0) {
    var printShopingList = new Object(PrintShopingList);
    printShopingList.init('.button.-printShopingList');
  }
})();