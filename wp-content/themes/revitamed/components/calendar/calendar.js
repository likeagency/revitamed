var gallery = (function () {
  var $el, $box;

  function init() {
    catchDOM();

    if (isElement($el)) {
      generateSlick()
    }
  }

  function catchDOM() {
    $el = $('.calendar');
    $box = $el.find('.calendar__slider');
  }


  function generateSlick() {
    $box.slick({
      mobileFirst: false,
      infinite: true,
      dots: false,
      arrows: true,
      prevArrow: '<div class="calendar__arrow -prev"><i class="fa fa-angle-left"></i></div>',
      nextArrow: '<div class="calendar__arrow -next"><i class="fa fa-angle-right"></i></div>',
      slidesToShow: 7,
      slidesToScroll: 7,
      autoplay: false,
      speed: 3000,
      responsive: [
        {
          breakpoint: 1280,
          settings: {
            slidesToShow: 6,
            slidesToScroll: 6
          }
        },
        {
          breakpoint: 969,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 5
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        }
      ]
    });

    if($('.calendar__link.-active').length > 0){
      var number = $('.calendar__link.-active').data('slick-index');
      $box.slick('slickGoTo',number)
    }
  }

  function isElement(item) {
    return item.length > 0
  }


  $(document).ready(function () {
    init();
  });
})();