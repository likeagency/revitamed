var questionnaireSave = new (function () {
  "use strict";
  var $saveQuestionnaire, $form, $navigation;

  $(document).ready(function () {
    init();
  });

  function init() {
    catchDOM();
    if ($saveQuestionnaire.length > 0 && $form.length > 0)
      bindEvents();
  }

  function catchDOM() {
    $navigation = $('[data-questionnaire-navigation]');
    $saveQuestionnaire = $navigation.find('[data-questionnaire-save]');
    $form = $('[data-questionnaire-form] :input[name!="action"]');
  }

  function bindEvents() {
    $saveQuestionnaire.on('click', function (e) {
      e.preventDefault();
      dietFormSubmit('saveCurrentQuestionnaire', function (response) {
        window.onbeforeunload = null;
        $navigation.prepend('<p class="form__navigationAlert -success"> <i class="fa fa-check-circle"></i> Ankieta została zapisana pomyślnie.</p>');
        setTimeout(function () {
          $navigation.find('.form__navigationAlert').remove();
        }, 2000)
      });
    });
  }

  function dietFormSubmit(action, callback) {
    $.ajax({
      url: ajaxurl + '?action=' + action,
      method: 'post',
      data:$form.serialize(),
      success: function (response) {
        callback(response);
      }
    });
  }

});