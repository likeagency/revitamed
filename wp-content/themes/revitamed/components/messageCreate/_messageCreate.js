(function () {
  var messageCreate = {
    init: function (target) {
      this.target = target;

      this.catchDOM();
      if (this.$el.length > 0) {
        this.bindEvent();
      }

      return this
    },
    catchDOM: function () {
      this.$el = $(this.target);

      return this
    },
    bindEvent: function () {
      this.$el.on('click', this.clickEvent.bind(this))
    },
    clickEvent: function (e) {
      var _self = $(e.currentTarget),recipientID = _self.data('message'),originalMsgID =_self.data('reply') ;
      if (_self.hasClass('-opened')) {
        _self.removeClass('-opened');
        _self.closest('.message').next().slideUp(400)
      } else {
        if (!_self.hasClass('-created')) {
          _self.addClass('-created');
          _self.closest('.message').after('<div class="messageCreate -answer" style="display: none"><form action="" method="post"><textarea name="msg_content" class="messageCreate__textarea"></textarea><button class="btn messageCreate__button">Wyślij wiadomość</button><input type="hidden" name="recipientID" value="'+ recipientID +'"><input type="hidden" name="replyToMsg" value="'+originalMsgID+'"><input type="hidden" name="action" value="replyToMsg"></form></div>')
        }
        _self.addClass('-opened');
        _self.closest('.message').next().slideDown(400)

      }
    }
  };

  messageCreate.init('.js-answer');
})();
