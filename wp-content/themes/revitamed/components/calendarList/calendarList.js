var PrintCalendarList = {
    init: function (target) {
        this.target = target;

        this.catchDOM();
        this.bindEvents();
        this.initDatepickers();
        return this
    },
    catchDOM: function () {
        this.$el = $(this.target);
        this.$modal = $('.calendarList');
        this.$background = this.$modal.find('.calendarList__background');
        this.$exit = this.$modal.find('.calendarList__exit');
        this.$printPDF = this.$modal.find('.calendarList__button.-pdf');
    },
    bindEvents: function () {
        this.$el.on('click', this.showPopup.bind(this));
        this.$background.on('click', this.hidePopup.bind(this));
        this.$exit.on('click', this.hidePopup.bind(this));
        this.$printPDF.on('click', this.getDays.bind(this));
    },
    initDatepickers: function () {

        var dateFormat = "dd.mm.yy";
        setDatepickers();
        function setDatepickers() {

            var from = $("#from2"), to= $("#to2");
            from.datepicker({
                    dateFormat: "dd.mm.yy",
                    minDate: $.datepicker.parseDate('dd.mm.yy',from.data('min')),
                    autoclose: true,
                    regional: 'pl'
                })
                .on("change", function () {
                    var thisVal = $(this).val().split(".");
                    thisVal.pop();
                    thisVal.push(parseInt($(this).val().split(".").pop()) + 1);
                    to.datepicker("option", "minDate", getDate(this));
                });
            to.datepicker({
                dateFormat: "dd.mm.yy",
                autoclose: true,
                maxDate: $.datepicker.parseDate('dd.mm.yy',to.data('max')),
                regional: 'pl'
            })
                .on("change", function () {
                    from.datepicker("option", "maxDate", getDate(this));
                });
        }

        function getDate(element) {
            var date;
            var string = element.value;
            var newDay = parseInt(string.slice(0, 2)) + 1;
            var newValue;
            if (element.id === 'from') {
                newValue = newDay.toString() + string.slice(2);
            }
            else {
                newValue = element.value;
            }
            try {
                date = $.datepicker.parseDate(dateFormat, newValue);
            } catch (error) {
                date = null;
            }
            return date;
        }
    },
    showPopup: function () {
        this.$modal.fadeIn('slow')
    },
    hidePopup: function () {
        this.$modal.fadeOut('slow')
    },
    printPDF: function () {
         $('.shopingList__list').addClass('-notPrint');
        // $('.calendar__title').addClass('-notPrint');
        // $('.calendarTable').addClass('-notPrint');
        window.print();
    },
    getDays: function () {
        var from = $(".calendarList__input.-from"), to = $(".calendarList__input.-to");
        if(from.val() != "" && to.val() != ""){
            from.removeAttr('style');
            to.removeAttr('style');
            var start = from.datepicker('getDate');
            var end = to.datepicker('getDate');
            if (!start || !end) return;
            this.getFromBackend(from.val(), to.val());
        }
        else{
            from.css('border','1px solid red');
            to.css('border','1px solid red')
        }
    },
    getFromBackend: function (from, to) {

        var that = this;
        var full = document.getElementById('full_recipe').checked;
        $.ajax({
            url: ajaxurl,
            data: {action: 'generate_calendar_user', from: from, to: to, full: full},
            success : function (data) {
                $('tr.-onlyForPrint').remove();
                $(".calendarTable__content > tbody").append(data.html);
                that.printPDF();
            }
        });
    }

};

(function () {
    // $(document).ready(function(){
    if ($('.button.-print').length > 0) {

            var printCalendarList = new Object(PrintCalendarList);
            printCalendarList.init('.button.-print');
    }
    // })
})();