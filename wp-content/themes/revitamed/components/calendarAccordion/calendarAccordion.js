var calendarAccordion = new (function () {

  var $calendarAccordion, $item, $hidden, $title;

  $(document).ready(function () {
    init();
  });

  function init () {
    $calendarAccordion = $('.calendarAccordion');

    if($calendarAccordion.length > 0) {
      $item = $calendarAccordion.find('.calendarAccordion__item');
      $hidden = $calendarAccordion.find('.calendarAccordion__hidden');
      $title = $calendarAccordion.find('.calendarAccordion__title');
      bindEvents();
      searchActive();
    }
  }

  function bindEvents() {
    $title.click(function () {
      showContent($(this));
    });
  }

  function showContent($this) {
    if($this.next('.calendarAccordion__hidden').hasClass('-active')){
      $this.next('.calendarAccordion__hidden').removeClass('-active').slideUp();
    }
    else{
      $this.closest('.calendarAccordion').find('.calendarAccordion__hidden').removeClass('-active').slideUp();
      $this.next('.calendarAccordion__hidden').addClass('-active').slideDown();
    }
  }

  function searchActive() {
    if($hidden.has('.-active')){
      $calendarAccordion.find('.-active').parent().addClass('-active').parent().addClass('-active')
    }
  }

});