<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */
global $wp_query;
$wp_query->query['posts_per_page'] = 16;
$wp_query = new WP_Query($wp_query->query);
remove_theme_support('title-tag');
add_action('wp_head', function () {
    echo "<title>Przepisy - Revitadiet</title>";
}, 0);
get_header(); ?>

  <div id="primary" class="content-area mb container content-fix">
    <div class="col-12">
    <div class="wrapper">
      <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
			            <span property="itemListElement" typeof="ListItem">
                <a property="item" typeof="WebPage" title="Go to Revitadiet." href="<?= home_url('/') ?>"
                   class="home"><span property="name">Revitadiet</span>
                </a><meta property="position" content="1">
            </span> &gt;
        <span property="itemListElement" typeof="ListItem">
                <span
                  property="name"><?= (is_search()) ? ('Wyniki wyszukiwania dla \'' . esc_html($_GET['s']) . '\'') : 'Przepisy' ?></span><meta
            property="position" content="2"></span>
      </div>
    </div><!-- ./wrapper -->
  </div>
    <main id="main" class="site-main col-9 przepisy-index" role="main">
      <header class="search-header searchHeader">
        <h2>Wyszukiwarka przepisów</h2>
        <form role="search" method="get" class="searchform group" action="<?= home_url('/') ?>">
          <label>
            <input type="search" class="search-field"
                   placeholder="<?php echo esc_attr_x('Wpisz szukaną frazę...', 'placeholder') ?>"
                   value="<?php echo esc_attr($_GET['s']) ?>" name="s"
                   title="<?php echo esc_attr_x('Wpisz szukaną frazę...', 'label') ?>"/>
          </label>
          <input type="image" alt="Szukaj" class="search-button"
                 src="<?php echo get_template_directory_uri(); ?>/images/search-icon.png">
        </form>
      </header><!-- .page-header -->
        <?php if (have_posts()) { ?>
          <section class="resport-wrapper recipesList">
              <?php while (have_posts()) : the_post(); ?>
                <article class="respost">
                    <?php if (has_post_thumbnail()) { ?>
                      <a class="thumb" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"
                         style="background-image:url('<?php echo ``; ?>')">
                          <?php the_post_thumbnail('recipe-list'); ?>
                      </a>
                    <?php } else { ?>
                      <a class="thumb" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                        <img src="<?= TEMP_URI ?>/components/vendor/src/placeholder-new.png">
                      </a>
                    <?php } ?>
                    <?php
                    $rows = get_the_terms(get_the_ID(), 'rodzaj');
                    $row = $rows[0];
                    $term_link = get_term_link($row);
                    if (is_wp_error($term_link))
                        $term_link = '#';
                    if (!is_object($row))
                        $row_name = '';
                    else
                        $row_name = $row->name;
                    ?>
                  <a class="lnk" href="<?php echo $term_link; ?>"><?php echo $row_name; ?></a>
                  <h5 class="r-title"><?php the_title(); ?></h5>
                </article>
              <?php endwhile; ?>
              <?php wp_pagenavi(); ?>
          </section>
        <?php } else { ?>


          Nie znaleziono wyników

        <?php } ?>
    </main><!-- #main -->

    <aside id="secondary" class="col-3">
			<?php echo do_shortcode('[open-lightbox]');?>
      <div class="filterit">
        <h2>Filtruj przepisy</h2>
          <?php
          $count_posts = wp_count_posts('przepisy')->publish;
          ?>
        <div class="av">Dostępne przepisy:<span class="no"><?php echo $count_posts; ?></span></div>

      </div>
      <section class="widget-cat ">
        <h3 class="widget-title">Kategoria posiłków</h3>
        <ul class="widget-cat__list">
            <?php foreach (get_terms('rodzaj') as $cat) { ?>
              <li class="recipeCategory__item <?= ($_GET['ts'] == $cat->slug) ? '-active' : '' ?>"><a
                  class="recipeCategory__link"
                  href="<?= home_url('/') . 'rodzaj/' . $cat->slug ?>"><i
                    class="fa fa-angle-right" aria-hidden="true"></i><?= $cat->name ?></a></li>
            <?php } ?>


        </ul>
      </section>
        <?php dynamic_sidebar('secondary-sidebar'); ?>
    </aside>
  </div><!-- #primary -->

<?php
get_footer();
