<?php
/**
 * The template for displaying all pages.
 * Template name: ankieta 4
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */
user_page();
saveDataFromFormToSession();
get_header(); ?>
<div id="primary" class="content-area  container">
  <div class="col-12">
  <div class="wrapper">
    <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
      <?php if (function_exists('bcn_display')) {
          bcn_display();
      } ?>
    </div>
  </div><!-- ./wrapper -->
</div>
  <main id="main" class="site-main bli" role="main">
    <div class="steps">
      <div class="steps__content">
        <a href="<?= get_permalink_template('ankieta_1.php'); ?>" class="steps__item ">
          <div class="steps__counter">Krok 1</div>
          <div class="steps__name">Wywiad ogólny</div>
        </a>
        <a href="<?= get_permalink_template('ankieta_2.php'); ?>" class="steps__item ">
          <div class="steps__counter">Krok 2</div>
          <div class="steps__name">Wywiad zdrowotny</div>
        </a>
        <a href="<?= get_permalink_template('ankieta_3.php'); ?>" class="steps__item">
          <div class="steps__counter">Krok 3</div>
          <div class="steps__name">Wywiad żywieniowy</div>
        </a>
        <a href="<?= get_permalink_template('ankieta_4.php'); ?>" class="steps__item -active">
          <div class="steps__counter">Krok 4</div>
          <div class="steps__name">Preferencje żywieniowe</div>
        </a>
        <div class="steps__item">
          <div class="steps__counter">Krok 5</div>
          <div class="steps__name">Podsumowanie</div>
        </div>
      </div>
    </div>
    <form method="post" class="form" action="<?= get_permalink_template('ankieta_5.php'); ?>" data-questionnaire-form>
      <div class="form__content">
        <div class="form__row -spaceBetween -marginBottom" data-group="nutrition_questionnaire">
          <div data-show>
            <div class="form__rowTitle">Ankieta szczegółowa</div>
            <div class="form__checkBoxHolder">
              <input data-required="sf_20_problem_0" <?= ($_SESSION['individualDiagnostic']['sf_20_problem_0']==="0") ? 'checked' : '' ?> name="user_20_problem_0" value="0" id="checkbox_2" type="checkbox" class="form__checkbox" data-item>
              <label for="checkbox_2" class="form__label">Jem wszystko ALBO po prostu chcę zrobić wszystko co możliwe dla mojego zdrowia niezależnie od moich preferencji żywieniowych</label>
            </div>
            <div class="form__checkBoxHolder">
              <input data-required="sf_20_problem_0" <?= ($_SESSION['individualDiagnostic']['sf_20_problem_0']==="1") ? 'checked' : '' ?>   name="user_20_problem_0" value="1"  id="checkbox_1" type="checkbox" class="form__checkbox" data-item data-toggle>
              <label for="checkbox_1" class="form__label">Chcę, żeby moje preferencje żywieniowe zostały uwzględnione (np. mięso tylko 3 x w tyg)</label>
            </div>
            <div class="-hideOnNo subform -width100">
                <label class="form__label  -hideOnNo">Preferencje</label>
                <textarea  name="user_20_problem_1" class="form__moreTextarea -hideOnNo -longInput"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_20_problem_1']) ?></textarea>
              </div>
        </div>
      </div>
        <div class="form__infoGray">
          <strong>Drodzy Państwo, nasz wspólny sukces opiera się na zbilansowaniu bardzo indywidualnej diety.</strong>
          <br>
           Punktem wyjścia dla ułożenia diety jest stan Państwa zdrowia tzn. możliwości przetwarzania metabolicznego produktów żywieniowych, co zależy od pracy wątroby, trzustki, tarczycy czy w końcu przewodu pokarmowego. Jednocześnie chcemy wyjść naprzeciw Państwa oczekiwaniom, czyli uwzględnić Państwa uwagi co do własnych preferencji żywieniowych. Dlatego prosimy osoby zainteresowane o wypełnienie poniższej ankiety.
        </div>
        <div class="form__row">
          <div class="form__col50">
            <div class="form__rowTitle">PRACA</div>
            <div class="form__hr"></div>


            <div class="form__checkBoxHolder">
              <input data-required="user_21_problem_3" <?= (in_array("1",$_SESSION['individualDiagnostic']['sf_21_problem_3'])) ? 'checked' : '' ?> name="user_21_problem_3[]" value="1" id="checkbox_5" type="checkbox" class="form__checkbox">
              <label for="checkbox_5" class="form__label">Siedząca</label>
            </div>
            <div class="form__checkBoxHolder">
              <input data-required="user_21_problem_3" <?= (in_array("2",$_SESSION['individualDiagnostic']['sf_21_problem_3'])) ? 'checked' : '' ?> name="user_21_problem_3[]" value="2" id="checkbox_6" type="checkbox" class="form__checkbox">
              <label for="checkbox_6" class="form__label">Praca ciężka fizyczna</label>
            </div>
            <div class="form__checkBoxHolder">
              <input data-required="user_21_problem_3" <?= (in_array("3",$_SESSION['individualDiagnostic']['sf_21_problem_3'])) ? 'checked' : '' ?> name="user_21_problem_3[]" value="3" id="checkbox_7" type="checkbox" class="form__checkbox">
              <label for="checkbox_7" class="form__label">Zmianowa</label>
            </div>
            <div class="form__checkBoxHolder">
              <input data-required="user_21_problem_3" <?= (in_array("4",$_SESSION['individualDiagnostic']['sf_21_problem_3'])) ? 'checked' : '' ?> name="user_21_problem_3[]" value="4" id="checkbox_8" type="checkbox" class="form__checkbox">
              <label for="checkbox_8" class="form__label">Częste podróże</label>
            </div>
            <div class="form__checkBoxHolder">
              <input data-required="user_21_problem_3" <?= (in_array("5",$_SESSION['individualDiagnostic']['sf_21_problem_3'])) ? 'checked' : '' ?> name="user_21_problem_3[]" value="5" id="checkbox_9" type="checkbox" class="form__checkbox">
              <label for="checkbox_9" class="form__label">Nie pracuję zawodowo</label>
            </div>
            <div class="form__checkBoxHolder">
              <input data-required="user_21_problem_3" <?= (in_array("6",$_SESSION['individualDiagnostic']['sf_21_problem_3'])) ? 'checked' : '' ?> name="user_21_problem_3[]" value="6" id="checkbox_10" type="checkbox" class="form__checkbox">
              <label for="checkbox_10" class="form__label">Uczeń/student</label>
            </div>
            <div class="form__hr"></div>
            <div class="form__rowTitle">JAKI JEST PANA/I STYL ŻYWIENIA </div>
            <div data-group>
              <div data-show>
                <div class="form__checkBoxHolder">
                  <input data-required="sf_23_problem_0" <?= ($_SESSION['individualDiagnostic']['sf_23_problem_0']==="0") ? 'checked' : '' ?> name="user_23_problem_0" value="0" id="checkbox_12" type="checkbox" class="form__checkbox" data-item data-toggle>
                  <label for="checkbox_12" class="form__label">Jestem weganinem</label>
                </div>
                <div class="-hideOnNo subform -width100">
                  <label class="form__label  -hideOnNo">Od kiedy?</label>
                  <textarea name="user_23_problem_0_w1"  class="form__moreTextarea -hideOnNo"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_23_problem_0_w1']) ?></textarea>
                </div>
              </div>
              <div data-show>
                <div class="form__checkBoxHolder">
                  <input data-required="sf_23_problem_0" <?= ($_SESSION['individualDiagnostic']['sf_23_problem_0']==="1") ? 'checked' : '' ?> name="user_23_problem_0" value="1" id="checkbox_13" type="checkbox" class="form__checkbox" data-item data-toggle>
                  <label for="checkbox_13" class="form__label">Jestem wegetarianinem</label>
                </div>
                <div class="-hideOnNo subform -width100">
                  <label class="form__label  -hideOnNo">Od kiedy?</label>
                  <textarea name="user_23_problem_0_w2"  class="form__moreTextarea -hideOnNo"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_23_problem_0_w2']) ?></textarea>
                </div>
              </div>
              <div>
                <div class="form__checkBoxHolder">
                  <input data-required="sf_23_problem_0" <?= ($_SESSION['individualDiagnostic']['sf_23_problem_0']==="2") ? 'checked' : '' ?> name="user_23_problem_0" value="2" id="checkbox_13b" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_13b" class="form__label">Jem wszystko</label>
                </div>
              </div>
              <div data-show>
                <div class="form__checkBoxHolder">
                  <input data-required="sf_23_problem_0" <?= ($_SESSION['individualDiagnostic']['sf_23_problem_0']==="3") ? 'checked' : '' ?> name="user_23_problem_0" value="3" id="checkbox_13c" type="checkbox" class="form__checkbox" data-item data-toggle>
                  <label for="checkbox_13c" class="form__label">Inne</label>
                </div>
                <div class="-hideOnNo subform -width100">
                  <textarea name="user_23_problem_0_w4"  class="form__moreTextarea -hideOnNo"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_23_problem_0_w4']) ?></textarea>
                </div>
              </div>

            </div>
            <div class="form__hr"></div>
            <div class="form__rowTitle">CZY SĄ PRODUKTY, PO KTÓRYCH PAN/I ŹLE SIĘ CZUJE? </div>
            <div data-show data-group>
              <div class="form__checkBoxHolder">
                <input data-required="sf_24_problem_0" <?= ($_SESSION['individualDiagnostic']['sf_24_problem_0']==="0") ? 'checked' : '' ?> name="user_24_problem_0" value="0" id="checkbox_13fa" type="checkbox" class="form__checkbox" data-item>
                <label for="checkbox_13fa" class="form__label">Nie</label>
              </div>
              <div class="form__checkBoxHolder">
                <input data-required="sf_24_problem_0" <?= ($_SESSION['individualDiagnostic']['sf_24_problem_0']==="1") ? 'checked' : '' ?> name="user_24_problem_0" value="1" id="checkbox_13ga" type="checkbox" class="form__checkbox" data-item data-toggle>
                <label for="checkbox_13ga" class="form__label">Tak</label>
              </div>
              <div class="-hideOnNo subform -width100">
                <label class="form__label -hideOnNo">Jakie?</label>
                <textarea name="user_24_problem_1"  class="form__moreTextarea -hideOnNo -longInput"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_24_problem_1']) ?></textarea>
              </div>
            </div>
          </div>
          <div class="form__col50">
            <div class="form__rowTitle">AKTYWNOŚĆ FIZYCZNA</div>
            <div class="form__hr"></div>
            <div data-show data-group>
              <div class="form__checkBoxHolder">
                <input data-required="sf_22_problem_0" <?= ($_SESSION['individualDiagnostic']['sf_22_problem_0']==="0") ? 'checked' : '' ?> name="user_22_problem_0" value="0" id="checkbox_13f" type="checkbox" class="form__checkbox" data-item>
                <label for="checkbox_13f" class="form__label">Nie</label>
              </div>
              <div class="form__checkBoxHolder">
                <input data-required="sf_22_problem_0" <?= ($_SESSION['individualDiagnostic']['sf_22_problem_0']==="1") ? 'checked' : '' ?> name="user_22_problem_0" value="1" id="checkbox_13g" type="checkbox" class="form__checkbox" data-item data-toggle>
                <label for="checkbox_13g" class="form__label">Tak</label>
              </div>
              <div class="-hideOnNo subform -width100">

              <div class="form__label -hideOnNo">Jak często w tygodniu Pan/i ćwiczy?</div>
              <textarea name="user_22_problem_0_w" class="form__moreTextarea -hideOnNo -longInput"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_22_problem_0_w']) ?></textarea>
              <div class="form__label -hideOnNo">W jakie dni tygodnia?</div>
              <textarea name="user_22_problem_2" class="form__moreTextarea -hideOnNo -longInput"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_22_problem_2']) ?></textarea>
              <div class="form__label -hideOnNo">Ile trwają ćwiczenia?</div>
              <textarea name="user_22_problem_1" class="form__moreTextarea -hideOnNo -longInput"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_22_problem_1']) ?></textarea>
              <div class="form__label -hideOnNo">Od kiedy Pan/i ćwiczy?</div>
              <textarea name="user_22_problem_3" class="form__moreTextarea -hideOnNo -longInput"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_22_problem_3']) ?></textarea>
              </div>
            </div>
            <div class="form__hr"></div>
            <div class="form__rowTitle">SUPLEMENTY</div>
            <div data-show data-group>
              <div class="form__checkBoxHolder">
                <input data-required="user_22_problem_4" <?= ($_SESSION['individualDiagnostic']['sf_22_problem_4']==="0") ? 'checked' : '' ?> name="user_22_problem_4" value="0" id="checkbox_13h" type="checkbox" class="form__checkbox" data-item>
                <label for="checkbox_13h" class="form__label">Nie</label>
              </div>
              <div class="form__checkBoxHolder">
                <input data-required="user_22_problem_4" <?= ($_SESSION['individualDiagnostic']['sf_22_problem_4']==="1") ? 'checked' : '' ?> name="user_22_problem_4" value="1" id="checkbox_13i" type="checkbox" class="form__checkbox" data-item data-toggle>
                <label for="checkbox_13i" class="form__label">Tak</label>
              </div>
              <div class="-hideOnNo subform -width100">

              <div class="form__label -hideOnNo">Suplementuje się następującymi produktami:</div>
              <textarea name="user_22_problem_5" class="form__moreTextarea -hideOnNo -longInput"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_22_problem_5']) ?></textarea>
            </div>
            </div>
          </div>

        </div>
        <div class="form__nav" data-questionnaire-navigation>
          <button class="btn form__saveDiet" data-questionnaire-save>Zapisz ankietę</button>
          <button class="form__button" onclick="window.onbeforeunload = null;">PRZEJDŹ DALEJ</button>
          <input type="hidden" value="ankieta_4" name="action">
          <input type="hidden" value="ankieta_4" name="step">
        </div>
      </div>
    </form>
  </main><!-- #main -->
</div><!-- #primary -->
  <script>
      var inputs = document.querySelectorAll('input, select, textarea');
      for(var i = 0; inputs && i < inputs.length; i++)
          inputs[i].addEventListener('change',function () {
              window.onbeforeunload = function () {
                  return true;
              };
          });
  </script>
<?php get_footer();
