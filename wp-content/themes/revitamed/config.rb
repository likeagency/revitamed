http_path = '/'
css_dir = 'css'
sass_dir = 'sass'
images_dir = 'images'
javascripts_dir = 'js'

# source map
sourcemap = true

# Relative paths active. Uncomment=disable
relative_assets = true

# To disable debugging comments that display the
# original location of your selectors. Uncomment:
line_comments = true

# Output_style = :expanded :nested :compact :compressed
output_style = :expanded

# Enviroment type - :development for work, :production for client output
environment == :development

# Enable Debugging (Line Comments, FireSass)
# Invoke from command line: compass watch -e development --force

# Workspace functions - in progress
#if environment == :development
#	line_comments = true
#	sass_options = { :debug_info => true }
#end
