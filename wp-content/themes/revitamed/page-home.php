<?php
/**
 * The template for displaying all pages.
 * Template name: Home page
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */

get_header();
?>
<?php showSlider(); ?>
<div class="slider-mobileBtn">  
	<a href="#!" class="button" data-id="slider-mobileBtn"></a>  
</div> 
<main id="main" class="site-main h-p" role="main">
	<aside class="hp-sidebar col-12">
		<div class="container">
			<div class="wdg-wrapper">
				<div class="title-container">
					<h2>
						<?php the_field('home_title_1'); ?>
					</h2>
					<h3>
						<?php the_field('home_subtitle_1'); ?>
					</h3>
				</div>
				<div id="demoTab1" class="tabs-wrapper">
					<div class="row">
						<ul class="tabs">
							<?php
							$cnt3 = 0;
							if ( have_rows( 'buttony_glowne_1' ) ):
								while ( have_rows( 'buttony_glowne_1' ) ): the_row();
							$cnt3++
							?>
							<li rel="tab<?php echo $cnt3; ?>" class="widget_custom_widget">
								<div class="tab-inner-box">
									<div class="tab-box">
										<!-- <h3 class="widget-title"><?php //the_sub_field('tytul'); ?></h3> -->
										<img src="<?php the_sub_field('w_ikonka'); ?>" alt="<?php echo strip_tags(get_sub_field('opis_widoczny')); ?>">
										<div class="tab-top-text">
											<?php the_sub_field('opis_widoczny'); ?>
										</div>
									</div>
									<div class="arrow">
										<img src="<?php echo get_template_directory_uri(); ?>/images/arrow-box.svg" alt="Box">
										<div class="more-box">więcej <i class="fa fa-angle-down" aria-hidden="true"></i>
										</div>
									</div>

								</div>

							</li>
							<?php
							endwhile;
							endif;
							?>
						</ul>
					</div>
					<div class="tab_container">
						<?php
						$cnt4 = 0;
						if ( have_rows( 'buttony_glowne_1' ) ):
							while ( have_rows( 'buttony_glowne_1' ) ): the_row();
						$cnt4++
						?>
						<h3 class="tab_drawer_heading" rel="tab<?php echo $cnt4; ?>"><img src="<?php the_sub_field('w_ikonka'); ?>" alt="<?php echo strip_tags(get_sub_field('opis_widoczny')); ?>"><?php the_sub_field('opis_widoczny'); ?></h3>
						<div id="tab<?php echo $cnt4; ?>" class="tab_content">
							<a href="#" class="close"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
							<?php the_sub_field('opis_rozszerzony'); ?>
						</div>
						<?php
						endwhile;
						endif;
						?>
					</div>
				</div>
				<!-- ./tabs -->
			</div>
			<!-- ./wdg -->
		</div>
		<!-- ./container -->
		<div class="container">
			<div class="wdg-wrapper wdg-wrapper-1">
				<div class="title-container">
					<?php
					the_field( 'tytul_szukasz_diety', 'option' );
					echo do_shortcode('[open-lightbox]');
					?>
					
				</div>
				<div id="demoTab" class="tabs-wrapper">
					<ul class="tabs">
						<?php
						$cnt = 0;
						if ( have_rows( 'buttony_glowne' ) ):
							while ( have_rows( 'buttony_glowne' ) ): the_row();
						$cnt++
						?>
						<li rel="tab<?php echo $cnt; ?>" class="widget_custom_widget">
							<!-- <h3 class="widget-title"><?php //the_sub_field('tytul'); ?></h3> -->
							<img src="<?php the_sub_field('w_ikonka'); ?>" alt="<?php echo strip_tags(get_sub_field('opis_widoczny')); ?>">
							<div class="tab-top-text">
								<?php the_sub_field('opis_widoczny'); ?>

							</div>
							<div class="more-box">więcej <i class="fa fa-angle-down" aria-hidden="true"></i>
							</div>
						</li>
						<?php
						endwhile;
						endif;
						?>
					</ul>
					<div class="tab_container">
						<?php
						$cnt2 = 0;
						if ( have_rows( 'buttony_glowne' ) ):
							while ( have_rows( 'buttony_glowne' ) ): the_row();
						$cnt2++
						?>
						<h3 class="tab_drawer_heading" rel="tab<?php echo $cnt2; ?>"><img src="<?php the_sub_field('w_ikonka'); ?>" alt="<?php the_sub_field('opis_widoczny'); ?>"><?php the_sub_field('opis_widoczny'); ?></h3>
						<div id="tab<?php echo $cnt2; ?>" class="tab_content">
							<a href="#" class="close"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
							<?php the_sub_field('opis_rozszerzony'); ?>
						</div>
						<?php
						endwhile;
						endif;
						?>
					</div>
				</div>
				<!-- ./tabs -->
			</div>
			<!-- ./wdg -->
		</div>
		<!-- ./container -->



	</aside>
	<section class="about">
		<div class="container">
			<div class="l-wrapper ain col-6">
				<?php
				the_content();
				?>
				<a class="rm button-inw" href="<?php the_field('wiecej_informacji_url');?>">Więcej informacji</a>
			</div>
			<div class="r-wrapper ain col-6">
				<div id="owl-carousel-lekarz" class="owl-carousel">
					<?php

					$medic_chooser = get_field( 'lekarz_na_stronie' );
					echo $medic_chooser;

					$the_query_map = new WP_Query( array(
						'post_type' => 'lekarz',
						'p' => $medic_chooser,
						'posts_per_page' => -1
					) );
					if ( $the_query_map->have_posts() ):
						while ( $the_query_map->have_posts() ):
							$the_query_map->the_post();
					?>
					<div class="item slide<?php the_ID(); ?>">
						<div class="thumb">
							<?php if ( has_post_thumbnail() ) : ?>
							<?php the_post_thumbnail('home-medic'); ?>
							<?php endif; ?>
						</div>
						<!-- ./thumb -->
						<div class="txt-wrapper">
							<h6>Nasi lekarze</h6>
							<h5>
								<?php the_title(); ?>
							</h5>

							<div class="desc">
								<?php the_field('short_desc'); ?>
							</div>
							<a href="/lekarz/#lekarz-<?php the_ID(); ?>" class="rm button-inw">Więcej</a>
						</div>
					</div>
					<?php
					endwhile;
					endif;
					wp_reset_postdata();
					?>
				</div>
			</div>
			<!-- ./r-wrapper -->
		</div>
	</section>
	<!-- ./about -->
	<section class="ttm">
		<div class="container">
			<div class="head">
				<?php
				the_field( 'tytul_rekomendacje', 'option' );
				?>
			</div>
			<div class="box-ttm">
				<?php showTestim(); ?>
			</div>
			<!-- ./testiinner -->
		</div>
		<!-- ./container -->
	</section>
</main> <!-- #main -->
<?php

if ( have_rows( 'banner_btm', 'option' ) ):
	while ( have_rows( 'banner_btm', 'option' ) ): the_row();
?>

<?php if( get_row_layout() == 'survey_btm_text_bg' ){  ?>
<?php
$image_object = get_sub_field( 'banner_btm_text_image' );
$image_size = 'misja_btm_bg2';
$image_url = $image_object[ 'sizes' ][ $image_size ];
$alt = $image_object[ 'alt' ];
$caption = $image_object[ 'caption' ];
?>
<?php
if ( get_sub_field( 'banner_btm_col' ) == 1 ) {
	?>
	<div class="container-bg-btm clear" style="background: url(<?php echo $image_url; ?>) no-repeat center center / cover">
		<div class="container">
			<?php
			echo "<div class='col-8'>";
			echo get_sub_field( "banner_btm_text_text" );

			echo "<div class='btns-box'>";
			foreach ( get_sub_field( "banner_btm_btns" ) as $przycisk ) {
				echo "<a class='button' href='" . $przycisk[ 'banner_btm_btn_link' ] . "'>";
				echo $przycisk[ 'banner_btm_btn_text' ];
				echo "</a>";
			}
			echo "</div>";

			echo "</div>";

			echo "<div class='col-4'>";
			echo "<img src='" . get_sub_field( "banner_btm_text_img_right" )[ 'sizes' ][ 'misja_btm_img' ] . "' alt='" . get_sub_field( "banner_btm_text_img_right" )[ 'alt' ] . "'>";
			echo "</div>";
			?>
		</div>
	</div>

	<?php } else {?>

	<div class="container-bg-btm clear" style="background: url(<?php echo $image_url; ?>) no-repeat center center / cover">
		<div class="container">
			<?php
			echo "<div class='banner-box'>";
			echo "<div class='col-12'>";
			echo "<h6>" . get_sub_field( "banner_btm_title" ) . "</h6>";
			echo "<h5>" . get_sub_field( "banner_btm_subtitle" ) . "</h5>";
			echo "</div>";
			foreach ( get_sub_field( "banner_btm_img_text" ) as $bannerBtm ) {
				if ( $bannerBtm[ 'banner_btm_pozycja_obrazka' ] == 1 ) {
					echo "<div class='col-6 col-left'>";
					echo "<div class='banner-btm-left'>";
					echo "<img src='" . $bannerBtm[ 'banner_btm_image' ][ 'sizes' ][ 'banner_btm_img' ] . " 'alt='" . $bannerBtm[ 'banner_btm_image' ][ 'alt' ] . "'>";
					echo "</div>";
					echo "<div class='banner-btm-right'>";
					echo "<h6>" . $bannerBtm[ 'banner_btm_tekst' ] . "</h6>";
					echo "<a class='button' href='" . $bannerBtm[ 'banner_btm_btn_link' ] . "' target='_blank'>";
					echo $bannerBtm[ 'banner_btm_btn_text' ];
					echo "</a>";
					echo "</div>";
					echo "</div>";
				} else {
					echo "<div class='col-6 col-right'>";
					echo "<div class='banner-btm-left'>";
					echo "<h6>" . $bannerBtm[ 'banner_btm_tekst' ] . "</h6>";
					echo "<a class='button' href='" . $bannerBtm[ 'banner_btm_btn_link' ] . "' target='_blank'>";
					echo $bannerBtm[ 'banner_btm_btn_text' ];
					echo "</a>";
					echo "</div>";
					echo "<div class='banner-btm-right'>";
					echo "<img src='" . $bannerBtm[ 'banner_btm_image' ][ 'sizes' ][ 'banner_btm_img' ] . " 'alt='" . $bannerBtm[ 'banner_btm_image' ][ 'alt' ] . "'>";
					echo "</div>";
					echo "</div>";
				}

			}
			echo "</div>";
			?>
		</div>
	</div>

	<?php }?>
	<?php }?>

	<?php
	endwhile;
	endif;
	?>

	<?php
	get_footer();