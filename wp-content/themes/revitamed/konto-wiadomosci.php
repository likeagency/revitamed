<?php
/**
 * The template for displaying all pages.
 * Template name: Konto PSD 10_10
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */
user_page(true);
$userRevita = new user();
$userID= $userRevita->getId();
$formSendMSG = ($_POST['action'] == 'send_msg_doc') ?  (send_private_msg($_POST['recipientID'],$_POST['message_content'],false)) : null;

$docID = $userRevita->getDoc();
get_header(); ?>
  <div id="primary" class="content-area konto-tpl">
    <div class="container">
      <div class="col-12">
      <div class="wrapper">
        <div class="title">Witaj, <span class="name"><?= getUserName($userID) ?>!</span> Zacznij z naszą pomocą dobry dzień</div>
        <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
            <?php if (function_exists('bcn_display')) {
                bcn_display();
            } ?>
        </div>
      </div><!-- ./wrapper -->
    </div>
    </div>
    <div class="container">
      <aside class="col-3 sidebar-konto">
        <div class="side-menu side-item">
		    <?php include 'template-parts/menu-user.php' ?>
        </div><!-- ./sidemenu -->
	      <?php include 'template-parts/progress-user.php' ?>
	      <?php include 'template-parts/subs-user.php' ?>
      </aside>
      <main id="main" class="site-main account-p col-9" role="main">
        <h3>Korespondencja z lekarzem</h3>

        <?php if($formSendMSG!==null){
          if($formSendMSG['success']==true){
          ?>
               <div class="form-message__alert -success">Wiadomość została wysłana</div>
              <?php }else{?>
              <div class="form-message__alert -warning"><?= $formSendMSG['msg'] ?></div>
         <?php }
           } ?>


        <?php foreach(getMsgUser($userRevita->getId()) as $msg){
          ?>
        <div class="message">
          <div class="message__header">
            <div class="message__userbox">
              <div class="message__avatar">
                <img src="<?php echo get_template_directory_uri().'/components/message/src/avatar.jpg'; ?>" alt="avatar">
              </div>
              <div class="message__author"><strong>Ty</strong>:</div>
            </div>
            <div class="message__date">Dnia: <?= date('d.m.Y',strtotime($msg->post_date)) ?>, godz. <?= date('H:i',strtotime($msg->post_date)) ?></div>
          </div>
          <div class="message__content">
            <?= apply_filters('the_content',get_post_meta($msg->ID,'sf_content',true)); ?>
          </div>
        </div>
            <?php
            foreach(getRepliesMsg($msg->ID,$userRevita->getId()) as $reply){
	            update_post_meta($reply->ID,'sf_read',1);
              $userName = getDocName(getDocOfDieter(get_post_meta($reply->ID,'sf_sender',true)));
	            $userAvatarOBJ= get_field('avatar_img',getDocOfDieter(get_post_meta($reply->ID,'sf_sender',true)));
	            if(is_array($userAvatarOBJ) && $userAvatarOBJ['sizes']['thumbnail']!=='')
		            $userAvatar = $userAvatarOBJ['sizes']['thumbnail'];
	            else
		            $userAvatar = get_template_directory_uri().'/components/message/src/avatar.jpg';
            ?>
          <div class="message -subpost">
            <div class="message__header">
              <div class="message__userbox">
                <div class="message__avatar">
                  <img src="<?= $userAvatar ?>" alt="avatar">
                </div>
                <div class="message__author"><strong><?= $userName ?></strong>:</div>
              </div>
              <div class="message__date">Dnia: <?= date('d.m.Y',strtotime($reply->post_date)) ?>, godz. <?= date('H:i',strtotime($reply->post_date)) ?></div>
            </div>
            <div class="message__content">
	            <?= apply_filters('the_content',get_post_meta($reply->ID,'sf_content',true)); ?>
            </div>
          </div>
    <?php }} ?>

        <div class="messageCreate">
          <form action="" method="post">
            <div class="messageCreate__title">NAPISZ NOWĄ WIADOMOŚĆ DO LEKARZA</div>
            <label class="messageCreate__label" for="messageCreate-input">Treść wiadomości</label>
            <textarea name="message_content" class="messageCreate__textarea" id="messageCreate-input" maxlength="800"></textarea>
            <input type="hidden" name="recipientID" value="<?= $docID ?>" >
            <input name="action" value="send_msg_doc" type="hidden">
            <button class="btn messageCreate__button">WYŚLIJ WIADOMOŚĆ</button>
          </form>
        </div>


      </main><!-- #main -->
    </div><!-- ./container -->
  </div><!-- #primary -->
<?php get_footer();
