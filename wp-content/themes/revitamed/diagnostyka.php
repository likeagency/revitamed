<?php
/**
 * The template for displaying all pages.
 * Template name: Diagnostyka
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */


get_header(); ?>
<div id="primary" class="content-area ">
	<div class="container">
		<div class="col-12">
	<div class="wrapper">
		<h2>Jak badamy?</h2>
		<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
			<?php if (function_exists('bcn_display')) {
					bcn_display();
			} ?>
		</div>
	</div><!-- ./wrapper -->
	</div>
</div>
	<main id="main" class="site-main" role="main">
		<?php
			while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/content', 'surveypage' );
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			endwhile;
			?>

	</main><!-- #main -->

</div>

<?php get_footer();
