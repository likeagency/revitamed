<div class="container-big" id="main-slider">
  <div id="owl-carousel-big" class="owl-carousel">
   <?php /** @var WP_Query $posts_array */
 while ( $posts_array->have_posts() ) { $posts_array->the_post(); $local_custom = get_post_custom(get_the_ID());


     ?>
     <div class="item slide<?php the_ID(); ?>">
       <div class="photo">
        <?php the_post_thumbnail('slider-thumb-big'); ?>
      </div>
      <div class="text">
        <div class="container">
         <div class="owl-overlay col-5">
           <div class="row">
            <?php the_content(); if($local_custom['sf_button_text'][0]!=''){ ?>
            <a href="<?php echo $local_custom['sf_url'][0]; ?>" class="button">
              <?php if ($local_custom['sf_button_text'][0] != '') echo '<span>'.$local_custom['sf_button_text'][0].'</span>'; ?>
            </a>
             <?php } ?>
          </div>
        </div><!-- ./cont -->
      </div>
    </div><!-- ./item -->
  </div>
  <?php } ?>
</div>
</div>
