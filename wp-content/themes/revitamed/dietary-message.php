<?php
/**
 * The template for displaying all pages.
 * Template name: Dietetyk wiadomości
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */
doc_page();
$userRevita = new userDoc();
$userID =  get_query_var('page');
$formResult = ($_POST['action']=='replyToMsg') ? send_private_msg($_POST['recipientID'],$_POST['msg_content'],true,$_POST['replyToMsg'])  : null;

$isDoc = get_post_meta($userRevita->getId(),'sf_doc',true) == 1;

if(!$isDoc)
{
  $arg = ( $userRevita->getId());
  $arg2 = [$userRevita->getId(), get_post_meta($userRevita->getId(),'sf_controlling_doc',true)];
}
else
{
  $arg = getAllDietaryForDoc($userRevita->getId());
	$arg[]=$userRevita->getId();
	$arg2 = $arg;
}

$clients = getMsgUserDieter($arg);
get_header(); ?>
	<div id="primary" class="content-area konto-tpl">
		<div class="container">
			<div class="col-12">
			<div class="wrapper">
				<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
					<?php if (function_exists('bcn_display')) {
						bcn_display();
					} ?>
				</div>
			</div>
			</div><!-- ./wrapper -->
		</div>
		<div class="container">
			<aside class="col-3 sidebar-konto">
				<div class="side-menu side-item">
		    	<?php include 'template-parts/menu-dieter.php'; ?>
				</div><!-- ./sidemenu -->
			</aside>
			<main id="main" class="site-main account-p col-9" role="main">
        <div class="message-navigation" data-url="<?= str_replace('http://'.(is_ssl() ? 's' : '').$_SERVER['SERVER_NAME'],'',get_the_permalink()) ?>"
             style="width: 29%; float:left; margin-top: 44px;"
        >
          <div class="message-navigation__list" ss-container>
            <?php foreach(getAllUsersMessage($clients) as $user) {
              if(empty($user)) continue;
              $test = respondedToMSG($user->ID,$arg2);
              ?>
            <div class="message-navigation__item <?= $userID == $user->ID ? '-active' : '' ?> <?=  $test ? '-highlighted' : '' ?>" data-id="<?= $user->ID ?>"><img src="<?= get_template_directory_uri().'/components/message/src/avatar.jpg'; ?>"> <span><?= getUserName($user->ID) ?></span></div>
            <?php } ?>
          </div>

        </div>

        <div
          style="width: 69%;float:right;"
        >
				<h3>Korespondencja z pacjentami</h3>
		  <?php if($formResult!==null){
			  if($formResult['success']==true){
				  ?>
                <div class="form-message__alert -success">Wiadomość została wysłana</div>
			  <?php }else{?>
                <div class="form-message__alert -warning"><?= $formResult['msg'] ?></div>
			  <?php }
		  } ?>

<?php



foreach($clients as $msg){
	$senderID = get_post_meta($msg->ID,'sf_sender',true);

	update_post_meta($msg->ID,'sf_read',1);
    ?>
  <div class="message" data-userid="<?= $senderID ?>">
    <div class="message__header">
      <div class="message__userbox">
        <div class="message__avatar">
          <img src="<?php echo get_template_directory_uri().'/components/message/src/avatar.jpg'; ?>" alt="avatar">
        </div>
        <div class="message__author"><a href="<?= get_the_permalink(get_id_after_template_filename('history-user.php'))."/{$senderID}/" ?>"><strong><?= esc_html(getUserName($senderID)); ?></strong></a>:</div>
      </div>
      <div class="message__date">Dnia: <?= date('d.m.Y',strtotime($msg->post_date)) ?>, godz. <?= date('H:i',strtotime($msg->post_date)) ?></div>
    </div>
    <div class="message__content">
		<?= apply_filters('the_content',get_post_meta($msg->ID,'sf_content',true)); ?>
    </div>
    <div class="message__footer">
      <span class="message__answer js-answer" data-reply="<?= $msg->ID; ?>" data-message="<?= get_post_meta($msg->ID,'sf_sender',true) ?>"><i class="fa fa-share"></i> Odpowiedz</span>
    </div>
  </div> <?php

	foreach(getRepliesMsg($msg->ID,$arg) as $reply){
		$userName = getDocName(getDocOfDieter(get_post_meta($reply->ID,'sf_sender',true)));
		$userAvatarOBJ= get_field('avatar_img',getDocOfDieter(get_post_meta($reply->ID,'sf_sender',true)));
		if(is_array($userAvatarOBJ) && $userAvatarOBJ['sizes']['thumbnail']!=='')
		  $userAvatar = $userAvatarOBJ['sizes']['thumbnail'];
		else
		  $userAvatar = get_template_directory_uri().'/components/message/src/avatar.jpg';
	  update_post_meta($msg->ID,'sf_read',1);


		?>
      <div class="message -subpost" data-userid="<?= $senderID ?>">
        <div class="message__header">
          <div class="message__userbox">
            <div class="message__avatar">
              <img src="<?= $userAvatar ?>" alt="avatar">
            </div>
            <div class="message__author"><strong><?= $userName ?></strong> pisze:</div>
          </div>
          <div class="message__date">Dnia: <?= date('d.m.Y',strtotime($reply->post_date)) ?>, godz. <?= date('H:i',strtotime($reply->post_date)) ?></div>
        </div>
        <div class="message__content">
			<?= apply_filters('the_content',get_post_meta($reply->ID,'sf_content',true)); ?>
        </div>
      </div>
	<?php }} ?>

        </div>
			</main><!-- #main -->
		</div><!-- ./container -->
	</div><!-- #primary -->
<?php get_footer();
