<?php
/**
 * The template for displaying all pages.
 * Template name: Układanie diety krok 3
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */
doc_page();
if($_POST['form']=='step_2_diet') stepTwoDietForm();
if($_POST['form']=='step_3_diet') {
  stepThreeDietForm(true);
  wp_redirect(get_the_permalink(get_id_after_template_filename('dietary-panel.php')));
}
get_header();
$userID = $_GET['userID'];
$currentDiet = getNewLastDiet($userID);
$dietID = $currentDiet->ID;
$forbidden = get_post_meta($dietID,'sf_forbidden',true);
$forbidden = (is_array($forbidden)) ? '' : $forbidden;
$supp = get_post_meta($dietID,'sf_supplements',true);
$comments_diet = get_post_meta($dietID,'sf_warnings',true);

?>
  <div id="primary" class="content-area konto-tpl">
    <div class="container">
      <div class="col-12">
      <div class="wrapper">
        <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
          <?php if (function_exists('bcn_display')) {
              bcn_display();
          } ?>
        </div>
      </div><!-- ./wrapper -->
    </div>
    </div>
    <div class="container">
      <aside class="col-3 sidebar-konto">
        <div class="side-menu side-item">
            <?php include 'template-parts/menu-dieter.php'; ?>
            <?php include 'template-parts/user-info-dieter.php'; ?>
        </div><!-- ./sidemenu -->
      </aside>
      <main id="main" class="site-main account-p col-9" role="main">
        <h3>Dieta - dodatkowe zalecenia</h3>
        <div class="dietAdditional js-dietAdditional" data-field_name="product" data-autocomplete="<?= esc_attr(json_encode([])); ?>">
          <form class="dietAdditional__form" method="post" action="#" data-diet-form>
            <!-- CONTENT -->
            <div class="dietAdditional__content" data-product_creator>
              <div class="dietAdditional__item">
                <label class="dietAdditional__label">NIE WOLNO</label>
                <div class="dietAdditional__group">
                  <textarea class="dietAdditional__textarea" name="forbidden"><?= esc_textarea($forbidden); ?></textarea>
                </div>
                <div class="dietAdditional__collection" data-product_list>
                </div>
              </div>
              <div class="dietAdditional__item">
                <label class="dietAdditional__label">CODZIENNA SUPLEMENTACJA</label>
                <textarea class="dietAdditional__textarea" name="daily-supplementation"><?= esc_textarea($supp); ?></textarea>
              </div>
              <div class="dietAdditional__item">
                <label class="dietAdditional__label">UWAGI</label>
                <textarea class="dietAdditional__textarea" name="comments"><?= esc_textarea($comments_diet); ?></textarea>
              </div>
            </div>

            <!-- NAWIGACJA -->
            <div class="dietAdditional__navigation" data-diet-navigation>
              <button class="btn dietAdditional__link" data-diet-save>Zapisz dietę</button>
              <button class="btn dietAdditional__button">Wyślij dietę do pacjenta <i class="fa fa-check"></i></button>
            </div>
            <input type="hidden" name="form" value="step_3_diet">
            <input type="hidden" value="<?= $dietID; ?>" name="dietID">
          </form>
        </div>

      </main><!-- #main -->
    </div><!-- ./container -->
  </div><!-- #primary -->
<?php get_footer();
