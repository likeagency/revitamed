<?php
function showSkin($data)
{
  $array = [];
  if($data['sf_2_problem_1']==="1")
    $array[]= 'Sucha';
  if($data['sf_2_problem_2']==="1")
    $array[]= 'Nadmierne pocenie';
  if($data['sf_2_problem_3']==="1")
    $array[]= 'Trądzik';
  if($data['sf_2_problem_4']==="1")
    $array[]= 'Świąd skóry';
  if($data['sf_2_problem_5']==="1")
    $array[]= 'Pokrzywki, Plamy, Wypryski';
  if($data['sf_2_problem_6']==="1")
    $array[]= 'Cellulit';
  if($data['sf_2_problem_0']==="1" || empty($array))
    $array[]= 'Prawidłowa';
  if($data['sf_2_problem_7']==="1")
  	$array[]= 'Inne' . (($data['sf_2_problem_7_w']!='') ? ( ': ( ' . $data['sf_2_problem_7_w'] . ' )') : (''));

  return implode(', ',$array);
}

function showAllergy($data)
{
  $array = [];
  if($data['sf_5_problem_1']==="1")
    $array[]= 'Pokarmowe';
  if($data['sf_5_problem_2']==="1")
    $array[]= 'Wziewne';
  if($data['sf_5_problem_3']==="1")
    $array[]= 'Skórne';
  if($data['sf_5_problem_4']==="1")
    $array[]= "Inne" . ($data['sf_5_problem_4_w']!='' ?  " ({$data['sf_5_problem_4_w']})" : '');



  return esc_html(implode(', ',$array));
}

function showInfection($data)
{
  $array = [];
  if($data['sf_6_problem_1']==="1")
    $array[]= 'Zapalenia pęcherza';
  if($data['sf_6_problem_2']==="")
    $array[]= 'Zapalenia migdałków';
  if($data['sf_6_problem_3']==="1")
    $array[]= 'Zapalenia uszu';
  if($data['sf_6_problem_4']==="1")
    $array[]= 'Zapalenia górnych dróg oddechowych';
  if($data['sf_6_problem_5']==="1")
    $array[]= 'Opryszczka';
  if($data['sf_6_problem_6']==="1")
    $array[]= 'Inne';
  if(empty($array))
    $array[]='Brak';

  return esc_html(implode(', ',$array));
}

function showWywiad($data)
{
  $array = [];
  if(in_array(1,$data['sf_10_problem_0']))
    $array[]='Choroby tarczycy';
  if(in_array(2,$data['sf_10_problem_0']))
    $array[]='Nadciśnienie';
  if(in_array(3,$data['sf_10_problem_0']))
    $array[]='Astma';
  if(in_array(4,$data['sf_10_problem_0']))
    $array[]='Cukrzyca';
  if(in_array(5,$data['sf_10_problem_0']))
    $array[]='Zawały';
  if(in_array(6,$data['sf_10_problem_0']))
    $array[]='Udary';
  if(in_array(7,$data['sf_10_problem_0']))
    $array[]='Nowotwory';
  if(in_array(8,$data['sf_10_problem_0']))
    $array[]= "Inne" . ($data['sf_10_problem_1']!='' ?  " ({$data['sf_10_problem_1']})" : '');

  if(empty($array))
    $array[]='Brak chorób';


  return esc_html(implode(', ',$array));
}


function favouriteProductsToString($list)
{
  $newList= [];

  foreach($list as $element)
  {
    switch($element)
    {
      case '1':
        $newList[1]='mięso';
        break;
      case '2':
        $newList[2]='ryby';
        break;
      case '3':
        $newList[3]='sery';
        break;
      case '4':
        $newList[4]='jogurty';
        break;
      case '5':
        $newList[5]='mleko';
        break;
      case '6':
        $newList[6]='pieczywo';
        break;
      case '7':
        $newList[7]='jajka';
        break;
      default:
        if($element!='')
        $newList[8]='ponadto: '.esc_html($element);
        break;
    }

  }
  return implode(', ',$newList);
}

function tolerateProducts($list)
{
  $newList= [];

  foreach($list as $element)
  {
    switch($element)
    {
      case '1':
        $newList[1]='oliwę z oliwek';
        break;
      case '2':
        $newList[2]='olej lniany nieoczyszczony';
        break;
      case '3':
        $newList[3]='masło klarowane';
        break;
      case '4':
        $newList[4]='olej kokosowy';
        break;
      default:
        break;
    }

  }
  return implode(', ',$newList);
}

function tolerateProductsDiet($list)
{
  $newList= [];

  foreach($list as $element)
  {
    switch($element)
    {
      case '1':
        $newList[]='wątróbka';
        break;
      case '2':
        $newList[]='żołądki';
        break;
      case '3':
        $newList[]='serduszka';
        break;
      case '4':
        $newList[]='ozorki';
        break;
      case '5':
        $newList[]='żelatyny, tzw. \'zimne nóżki\'';
        break;
      case '6':
        $newList[]='mięsa / ryby w galarecie';
        break;
      default:
        break;
    }

  }
  return implode(', ',$newList);
}

function saveSurvey()
{
  $result = array(
    'error_account' =>  null,
    'error_survey' => null,
    'msg' => '',
   );
  $userRevita = new user();


  if ((isset($_POST['post_nonce_field'])
    && wp_verify_nonce($_POST['post_nonce_field'], 'post_nonce'))
  )
  {

    $data = $_SESSION['individualDiagnostic'];
    $data['sf_12_problem_3'] = formatNumberToPHP($data['sf_12_problem_3']);
    if($userRevita->isActive())
    {
      //update
      $id = $userRevita->getId();
    }
    else
    {
      //create
      $result_register = handle_register(false);
      if($result_register['result']=='success')
      {
        $id = $result_register['post_id'];
      }
      else
      {
        $result['error_account'] = true;
        $result['msg'] = $result_register['text'];
        return $result;
      }
    }

    $data['sf_client_name'] = $id;
    $user_name = esc_html($_POST['user_name']);
    $user_name_invoice = esc_html($_POST['user_name_invoice']);
    $user_phone = esc_html($_POST['user_phone']);

    $user_surname = esc_html($_POST['user_surname']);
    $user_surname_invoice = esc_html($_POST['user_surname_invoice']);

    $user_street = esc_html($_POST['user_street']);
    $user_city = esc_html($_POST['user_city']);
    $user_zipcode = esc_html($_POST['user_zip']);
    $user_nipcode = esc_html($_POST['user_nip']);

    $invoice = ($_POST['vat']=='1') ? true : false;
    $age = esc_html($data['sf_age']);
    $height = esc_html($data['sf_height']);

    update_post_meta($id,'sf_name',$user_name);
    update_post_meta($id,'sf_surname',$user_surname);
    update_post_meta($id,'sf_phone',$user_phone);

    update_post_meta($id,'sf_name_invoice',$user_name_invoice);
    update_post_meta($id,'sf_surname_invoice',$user_surname_invoice);
    update_post_meta($id,'sf_nip',$user_nipcode);


    update_post_meta($id,'sf_street',$user_street);
    update_post_meta($id,'sf_place',$user_city);
    update_post_meta($id,'sf_zip_code',$user_zipcode);
    update_post_meta($id,'sf_have_invoice',$invoice);
    update_post_meta($id,'sf_age',$age);
    update_post_meta($id,'sf_height',$height);
    //trigger action hook
    wp_update_post( get_post($id));


    $survey = getUserSurvey($id);
    if(empty($survey))
    {
      //validate

      //create
      $args = array(
        'post_type' => 'ankieta',
        'post_status' => 'publish',
        'post_title' => getUserName($id),
        'meta_input' => $data
      );
      $id_survey = wp_insert_post($args);
    }
    else
    {
      //update
      $id_survey = $survey[0]->ID;
      deleteAllMetaPost($id_survey);
      $args = array(
        'ID' => $id_survey,
        'meta_input' => $data,
      );
      wp_update_post($args);

    }
    processArrayDBWordpress($id_survey,'sf_25_problem_6');
    processArrayDBWordpress($id_survey,'sf_25_problem_5');
    processArrayDBWordpress($id_survey,'sf_24_problem_2');
    processArrayDBWordpress($id_survey,'sf_24_problem_0');
    processArrayDBWordpress($id_survey,'sf_21_problem_3');

    //create/update measure
	$talia = $data['sf_circuit_1'];
	$brzuch = $data['sf_circuit_2'];
	$biodra = $data['sf_circuit_3'];
    updateMeasure($data['sf_weight'],$brzuch,$talia,$biodra,'not');


    unset($_SESSION['individualDiagnostic']);


    $docs = getSortedDocsByAccessibility();
    if(is_array($docs) && isset($docs[0]) && property_exists($docs[0],'ID')
    && get_post_meta($id,'sf_doc_diet',true)=='')
    update_post_meta($id,'sf_doc_diet',$docs[0]->ID);

    update_post_meta($userRevita->getId(),'sf_session_storage',[]);


    if($userRevita->hasActiveSub())
  //redirect to user panel
  wp_redirect(get_the_permalink(get_id_after_template_filename('konto-komunikaty.php')).'?survey=1');
    else
    //redirect to user panel
    wp_redirect(get_the_permalink(get_id_after_template_filename('konto-platnosci.php')).'?survey=1');


    exit();
  }
  return $result;
}

function stepOneDietForm()
{
	$userDoc = new userDoc();
	if($userDoc->isActive()) {
		$fullDiet        = get_term_by( 'slug', $_POST['period_full_diet'], 'dl_diety' );
		$currentStage    = get_term_by( 'slug', $_POST['current_stage'], 'etap_diety' );
		$numberDishes    = $_POST['number_dishes'];
		$template        = $_POST['sf_template_diet'];
		$dietID          = $_POST['dietID'];
		$weight_end      = $_POST['weight_end'];
		$weight_start    = $_POST['weight_start'];
		$purpose_of_diet = $_POST['purpose_of_diet'];
		$plan            = $_POST['user_plan'];
		if ( get_post_type( $dietID ) == 'diet' ) {
			update_post_meta( $dietID, 'sf_quantity_meal', $numberDishes );
			update_post_meta( $dietID, 'sf_weight_start', $weight_start );
			update_post_meta( $dietID, 'sf_weight_target', $weight_end );
			update_post_meta( $dietID, 'sf_purpose_diet', $purpose_of_diet );
			update_post_meta( $dietID, 'sf_meal_created', true );
//			update_post_meta( $dietID, 'sf_user', $_GET['userID'] );
			update_post_meta ( $dietID, 'sf_plan', $plan );
			wp_set_post_terms( $dietID, $fullDiet->term_id, 'dl_diety' );
			wp_set_post_terms( $dietID, $currentStage->term_id, 'etap_diety');

			if($template==='-1')
			{

			}
			elseif($template==='0')
			{
				//empty diet
				for($i_day = 1; $i_day<=7;$i_day++)
					for($i_meal = 1;$i_meal<=5; $i_meal++)
					{
						update_post_meta($dietID,"sf_recipe_{$i_day}_{$i_meal}",0);
						update_post_meta($dietID,"sf_ing_{$i_day}_{$i_meal}",[]);
					}
			}
			else
			{
				
				$dietTake = empty($template) ?  getLastEndedOrActiveDiet(get_post_meta($dietID,'sf_user',true)) : get_post($template);
				for($i_day = 1; $i_day<=7;$i_day++)
					for($i_meal = 1;$i_meal<=5; $i_meal++)
					{
						update_post_meta($dietID,"sf_recipe_{$i_day}_{$i_meal}",get_post_meta($dietTake->ID,"sf_recipe_{$i_day}_{$i_meal}",true));
						update_post_meta($dietID,"sf_ing_{$i_day}_{$i_meal}",get_post_meta($dietTake->ID,"sf_ing_{$i_day}_{$i_meal}",true));
					}

			}
		}
	}

}
function stepTwoDietForm()
{

	$userDoc = new userDoc();
	if($userDoc->isActive()) {
			$products = $_POST['meal'];
			$dietID   = $_POST['dietID'];
			updateProductsOfDiet( $products, $dietID );
	}

}


function stepThreeDietForm($doLastStep = false,$productName = 'product')
{
	$userDoc = new userDoc();
	$dietID = $_POST['dietID'];
	if($userDoc->isActive()) {
		$supply = $_POST['daily-supplementation'];
		$comments = $_POST['comments'];
		$forbidden = $_POST['forbidden'];
		
		update_post_meta($dietID,'sf_supplements',$supply);
		update_post_meta($dietID,'sf_warnings',$comments);
		update_post_meta($dietID,'sf_forbidden',$forbidden);
	}
	if($doLastStep)
	lastStepDiet($dietID);

}

function partialSurveyForm(){
	$surveyID = get_the_ID();

	for($i = 1; $i <=8 ; $i++)
		update_post_meta($surveyID,'sf_question_'.$i,$_POST['sf_question_'.$i]);
	for($i = 1; $i <=8 ; $i++)
		update_post_meta($surveyID,'sf_question_check_'.$i,$_POST['sf_question_check_'.$i]);

	for($i = 1; $i <=4 ; $i++)
		update_post_meta($surveyID,'sf_calc_'.($i+1),$_POST['user_calc_'.$i]);


		update_post_meta($surveyID,'sf_calc_1',$_POST['user_weight']);

	$time = current_time('mysql');

	wp_update_post(array(
		'ID' => $surveyID,
		'post_status' => 'private',
		'post_date'     => $time,
		'post_date_gmt' => get_gmt_from_date( $time )
	));

	foreach([4,5] as $day)
		update_post_meta($surveyID,'sf_reminded_days_'.$day,1);

	update_post_meta($surveyID,'sf_active',true);
	wp_redirect(get_the_permalink(get_id_after_template_filename('konto-komunikaty.php')).'/?ac=sukces');
	exit();
}