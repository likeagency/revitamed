<?php

add_action('wp_head','add_ajax_url');
function add_ajax_url()
{
  $admin_url = admin_url('admin-ajax.php');
  echo "<script>var ajaxurl = \"{$admin_url}\"; </script>";
}



if((isset($_GET['post']) && get_post_type($_GET['post'])=='przepisy') ||  (isset($_GET['post_type']) && $_GET['post_type']=='przepisy'))
add_action('admin_head', 'custom_options_for_recipe');

if(
	(isset($_GET['post']) && (get_post_type($_GET['post'])=='diet') || (isset($_GET['post_type']) && $_GET['post_type']=='diet') || get_post_type($_GET['post'])=='template_diet') || $_GET['post_type']=='template_diet')
add_action('admin_head', 'custom_options_for_diet');

function custom_options_for_diet()
{
  $custom_css = file_get_contents( get_template_directory().'/css/admin-diety.css');
  echo '<style>'.$custom_css.'</style>';
}


function custom_options_for_recipe()
{
    $custom_css = file_get_contents( get_template_directory().'/css/admin-przepisy.css');
    $custom_js  = ( get_template_directory_uri().'/js/checkbox_into_radio.js');
    echo '<style>'.$custom_css.'</style>';
    echo '<script src="'.$custom_js.'">';
}
if((isset($_GET['post']) && get_post_type($_GET['post'])=='protokol') || $_GET['post_type']=='protokol')
add_action('admin_head', 'custom_options_for_protocol');
function custom_options_for_protocol()
{
    $custom_css = file_get_contents( get_template_directory().'/css/admin-protokol.css');
    echo '<style>'.$custom_css.'</style>';
}


// on edit update
function update_hidden_field_all_ing ($post_id)
{
    global $blockFunction;
    if($blockFunction===TRUE) return;
    $blockFunction = true;
    if ( "przepisy" == get_post_type($post_id)) {

        $res ='';
        $ingArray = get_post_meta(get_the_ID(), 'rm_ing',true);
        if(!empty($ingArray))
        foreach($ingArray as $item)
            $res.= get_the_title($item['ing']);

        wp_update_post(
            array(
                'ID'           => $post_id,
                'post_content' => $res.'!@##$$*%#$#%'.get_post_meta($post_id,'rm_opis',true),
            )

        );
        update_post_meta($post_id,'rm_hidden_ing',$res);
    }
    $blockFunction = NULL;
}

add_action('save_post','update_hidden_field_all_ing');

function custom_menu_page_removing() {
  remove_menu_page('edit.php?post_type=realization_diet'  );
  remove_menu_page('edit.php?post_type=measurement'  );
}
add_action('admin_menu', 'custom_menu_page_removing');
add_filter('post_link', function( $permalink, $post, $leavename){

	return untrailingslashit($permalink).'/';
} ,1,999);