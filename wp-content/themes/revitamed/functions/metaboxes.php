<?php

if (!defined('ABSPATH')) return;

add_filter('rwmb_meta_boxes', 'rm_register_meta_boxes');
function rm_register_meta_boxes($meta_boxes)
{
  $prefix  = 'rm_';
  $prefix2 = 'sf_';
  $meals   = array();
  $n_meal  = 5;
  $post_type  = $_GET['post_type'];
  $post_type  =  empty($_GET['post_type']) ? get_post_type($_GET['post']) : $post_type;

  if($_GET['post']!='')
  	$n_meal = get_post_meta($_GET['post'],'sf_quantity_meal',true);


  for($i_day = 1; $i_day<=7;$i_day++){
	  $meals[] = array(
		  'name' => __( 'Dzień '.$i_day, 'revitadiet' ),
		  'type' => 'heading',
	  );
    for($i_meal = 1;$i_meal<=$n_meal; $i_meal++) {
	    $meals[] = array(
		    'name' => __( 'Posiłek '.$i_meal, 'revitadiet' ),
		    'type' => 'heading',
	    );
	    $meals[] = array(
		    'name' => 'Przepis',
		    'type' => 'post',
		    'post_type' => 'przepisy',
		    'id' => "{$prefix2}recipe_{$i_day}_{$i_meal}"
	    );
	    $meals[] = array(
		    'name'   => __( '', 'revitadiet' ),
		    'id'     => "{$prefix2}ing_{$i_day}_{$i_meal}",
		    'type'   => 'group',
		    'clone'  => true,
		    'before' => '
    <div class="beksa"><div class="col">Składnik</div>
    <div class="col">Ilość</div>
    <div class="col">Usuń</div></div>',
		    'fields' => array(
			    array(
				    'name'      => '',
				    'type'      => 'post',
				    'post_type' => 'skladnik',
				    'id'        => 'ing'
			    ),
			    array(
				    'name' => '',
				    'type' => 'number',
				    'step' => '1',
				    'id'   => 'amount',
			    ),
		    )
	    );
    }
  }


  /*** Składniki ***/

  $meta_boxes[] = array(
    'id' => 'standard',
    'title' => __('Dodatkowe parametry', 'revitadiet'),
    'post_types' => array('skladnik'),
    'context' => 'normal',
    'priority' => 'high',
    'autosave' => true,

    'fields' => array(
      array(
        'name' => __('Liczba mnoga', 'revitadiet'),
        'id' => "{$prefix}mnoga",
        'type' => 'text',
        'clone' => false,
      ),
	    array(
		    'name' => 'Jednostka',
		    'type' => 'post',
		    'post_type' => 'jednostki',
		    'field_type' => 'select_advanced',
		    'id' => "{$prefix}unit",
	    ),
      array(
        'id' => "{$prefix}kalorycznosc",
        'name' => __('Kaloryczność', 'revitadiet'),
        'type' => 'number',
        'step' => '0.01',
      ),
      array(
        'name' => __('Zawartość tłuszu', 'revitadiet'),
        'id' => "{$prefix}zaw_tluszczu",
        'type' => 'number',
        'step' => '0.01',
      ),
      array(
        'name' => __('Zawartość węglowodanów', 'revitadiet'),
        'id' => "{$prefix}zaw_weglo",
        'type' => 'number',
        'step' => '0.01',
      ),
      array(
        'name' => __('Zawartość białek', 'revitadiet'),
        'id' => "{$prefix}zaw_bialek",
        'type' => 'number',
        'step' => '0.01',
      ),
      array(
        'name' => __('Zawartość błonnika', 'revitadiet'),
        'id' => "{$prefix}zaw_blonnik",
        'type' => 'number',
        'step' => '0.01',
      ),
    ),

  );

  /*** Przepisy ***/

  $meta_boxes[] = array(
    'id' => 'standard-2',
    'title' => __('Informacje podstawowe', 'revitadiet'),
    'post_types' => array('przepisy'),
    'context' => 'normal',
    'priority' => 'high',
    'autosave' => true,
    'fields' =>
      array(
        array(
          'name' => __('Czy wyróżniony', 'revitadiet'),
          'id' => "{$prefix}featured",
          'type' => 'checkbox',
        ),
        array(
          'name' => __('Czas przygotowania', 'revitadiet'),
          'id' => "{$prefix}czas_przyg",
          'type' => 'text',
        ),
        array(
          'id' => "{$prefix}trudnosc",
          'name' => __('Poziom trudności', 'revitadiet'),
          'type' => 'select',
          'clone' => false,
          'options' => array(
            '1' => __('Łatwy', 'revitadiet'),
            '2' => __('Średni', 'revitadiet'),
            '3' => __('Trudny', 'revitadiet'),
          ),
          'multiple' => false,
          'std' => '2',
        ),

        array(
          'name' => __('Opis', 'revitadiet'),
          'id' => "{$prefix}opis",
          'type' => 'wysiwyg',
        ),
        array(
          'name' => __('Galeria', 'revitadiet'),
          'id' => "{$prefix}gallery",
          'type' => 'image_advanced',
        ),
      ),
  );

  /**
   * Składnik w przepisie
   */
  $meta_boxes[] = array(
    'id' => 'standard',
    'title' => __('Składniki', 'revitadiet'),
    'post_types' => array('przepisy'),
    'context' => 'normal',
    'priority' => 'high',
    'autosave' => true,
    'fields' => array(

      array(
        'name' => __('', 'revitadiet'),
        'id' => "{$prefix}ing",
        'type' => 'group',
        'clone' => true,
        'before' => '
    <div class="col">Składnik</div>
    <div class="col">Ilość [jednostka]</div>
    <div class="col">Jednostka</div>
    <div class="col">Usuń</div>',
        'fields' => array(
          array(
            'name' => '',
            'type' => 'post',
            'post_type' => 'skladnik',
            'id' => 'ing'
          ),
        array(
	        'name' => '',
	        'type' => 'number',
	        'step' => '0.1',
	        'id' => 'amount_show',
        ),

          array(
            'name' => '',
            'type' => 'text',
            'attributes' => array(
            'disabled' => true,
            'data-text' => '',
            ),
            'id' => ''
          ),
        )
      ),));
  /*** Jednostki ***/
	$meta_boxes[] = array(
		'id' => 'standard',
		'title' => __('Dodatkowe parametry', 'revitadiet'),
		'post_types' => array('jednostki'),
		'context' => 'normal',
		'priority' => 'high',
		'autosave' => true,

		'fields' => array(
			array(
				'name' => __('Liczba pojedyńcza', 'revitadiet'),
				'id' => "{$prefix}lp",
				'type' => 'text',
				'desc' => 'np. 1 łyżeczka',
			),
			array(
				'name' => __('Liczba mnoga (2-4)', 'revitadiet'),
				'id' => "{$prefix}lm_1",
				'type' => 'text',
				'desc' => 'np. 3 łyżeczki'
			),
			array(
				'name' => __('Liczba mnoga (0, 5-9)', 'revitadiet'),
				'id' => "{$prefix}lm_2",
				'type' => 'text',
				'desc' => 'np. 0 łyżeczek, 7 łyżeczek'
			),
		),
	);

  /*** Lekarze ***/

  $meta_boxes[] = array(
    'id' => 'standard',
    'title' => __('Dodatkowe parametry', 'revitadiet'),
    'post_types' => array('lekarz'),
    'context' => 'normal',
    'priority' => 'high',
    'autosave' => true,

    'fields' => array(
      array(
        'name' => __('Krótki opis', 'revitadiet'),
        'id' => "{$prefix}krotki_opis",
        'type' => 'wysiwyg',
        'clone' => false,
      ),
    ),
  );

  /**
   *  Protokół żywieniowy
   */
  $meta_boxes[] = array(
    'id' => 'standard',
    'title' => __('Dodatkowe parametry', 'revitadiet'),
    'post_types' => array('protokol'),
    'context' => 'normal',
    'priority' => 'high',
    'autosave' => true,

    'fields' => array(
      array(
        'name' => __('Produkty zakazane', 'revitadiet'),
        'id' => "{$prefix}zakazane",
        'type' => 'post',
        'post_type' => 'skladnik',
        'query_args' => [
          'orderby' => 'title',
          'order' => 'ASC',
        ],
        'clone' => true,
      ),
      array(
        'name' => __('Produkty ograniczone', 'revitadiet'),
        'id' => "{$prefix}ograniczone",
        'type' => 'post',
        'post_type' => 'skladnik',
        'query_args' => [
          'orderby' => 'title',
          'order' => 'ASC',
        ],
        'clone' => true,
      ),

      array(
        'name' => __('Produkty wskazane', 'revitadiet'),
        'id' => "{$prefix}wskazane",
        'type' => 'post',
        'post_type' => 'skladnik',
        'query_args' => [
          'orderby' => 'title',
          'order' => 'ASC',
        ],
        'clone' => true,
      ),
    ),
  );
  $meta_boxes[] = array(
    'id' => 'standard-2',
    'title' => __('Bannery dolne', 'revitadiet'),
    'post_types' => array('protokol'),
    'context' => 'normal',
    'priority' => 'high',
    'autosave' => true,
    'fields' => array(
      array(
        'name' => __('', 'revitadiet'),
        'id' => "{$prefix}group",
        'type' => 'group',
        'clone' => true,
        'sort_clone' => true,
        'fields' => [

          [
            'name' => 'Tekst',
            'id' => 'content__sf',
            'type' => 'WYSIWYG',
          ],
          [
            'name' => 'Fotka',
            'id' => 'image',
            'type' => 'image_advanced',
            'max_file_uploads' => 1,
          ],
          [
            'name' => 'Link',
            'id' => 'link',
            'type' => 'text',
          ],
          [
            'name' => 'Tytuł na przycisku',
            'id' => 'text',
            'type' => 'text',
          ],
          [
            'name' => 'Otwórz w nowym oknie',
            'id' => 'open',
            'type' => 'checkbox',
          ],

        ]
      )
    )
  );


  $meta_boxes[] = array(
    'id' => 'standard-2',
    'title' => __('Informacje podstawowe', 'revitadiet'),
    'post_types' => array('user_client'),
    'context' => 'normal',
    'priority' => 'high',
    'autosave' => true,
    'fields' => array(
      array(
        'name' => __('Dietetyk', 'revita'),
        'id' => "{$prefix2}doc_diet",
        'type' => 'post',
        'post_type' => 'doc'
      ),
      array(
        'name' => __('Nowe hasło', 'revita'),
        'id' => "{$prefix2}password_admin_create",
        'type' => 'text',
      ),
      array(
        'name' => __('Adres e-mail', 'revita'),
        'id' => "{$prefix2}email",
        'type' => 'text',
      ),

      array(
        'name' => __('Imię', 'revita'),
        'id' => "{$prefix2}name",
        'type' => 'text',
      ),
      array(
        'name' => __('Nazwisko', 'revita'),
        'id' => "{$prefix2}surname",
        'type' => 'text',
      ),
    array(
	    'name' => __('Telefon', 'revita'),
	    'id' => "{$prefix2}phone",
	    'type' => 'text',
    ),
    array(
	    'name' => __('Czy faktura', 'revita'),
	    'id' => "{$prefix2}have_invoice",
	    'type' => 'checkbox',
    ),
    array(
	    'name' => __('Imię (faktura VAT)', 'revita'),
	    'id' => "{$prefix2}name_invoice",
	    'type' => 'text',
    ),
    array(
	    'name' => __('Nazwisko (faktura VAT)', 'revita'),
	    'id' => "{$prefix2}surname_invoice",
	    'type' => 'text',
    ),
      array(
        'name' => __('Miejscowość (faktura VAT)', 'revita'),
        'id' => "{$prefix2}place",
        'type' => 'text',
      ),
      array(
        'name' => __('Ulica (faktura VAT)', 'revita'),
        'id' => "{$prefix2}street",
        'type' => 'text',
      ),
      array(
        'name' => __('Kod pocztowy (faktura VAT)', 'revita'),
        'id' => "{$prefix2}zip_code",
        'type' => 'text',
      ),
    array(
	    'name' => __('NIP (faktura VAT)', 'revita'),
	    'id' => "{$prefix2}nip",
	    'type' => 'text',
    ),
	    array(
		    'name' => __('Wiek', 'revita'),
		    'id' => "{$prefix2}age",
		    'type' => 'text',
	    ),
	    array(
		    'name' => __('Płeć', 'revita'),
		    'id' => "{$prefix2}sex",
		    'type' => 'select',
		    'options' => array(
			    '1' => 'Kobieta',
			    '2' => 'Mężczyzna'
		    ),
	    ),
	    array(
		    'name' => __('Wzrost', 'revita'),
		    'id' => "{$prefix2}height",
		    'type' => 'text',
	    ),

      array(
        'name' => __('Historia płatności', 'revita'),
        'id' => "{$prefix2}log",
        'type' => 'textarea',
        'attributes' => array(
          'disabled' => true,
        ),
      ), array(
        'name' => __('Subskrypcja zakupiona do', 'revita'),
        'id' => "{$prefix2}date_sub",
        'type' => 'date',
        'timestamp' => true,
      ),

//	    array(
//        'name' => __('Czy została przypomniana dieta ?', 'revita'),
//        'id' => "{$prefix2}user_has_diet",
//        'type' => 'checkbox',
//      ),

      array(
        'name' => __('Wykupione subskrypcje', 'revita'),
        'id' => "{$prefix2}subs_history",
        'type' => 'group',
        'fields' => array(
          array(
            'name' => __('Cennik', 'revita'),
            'id' => "cennik",
            'type' => 'post',
            'post_type' => 'cennik',
          ),
          array(
            'name' => __('Data od', 'revita'),
            'id' => "date_from",
            'type' => 'date',
            'timestamp' => true,
          ),
          array(
            'name' => __('Data do', 'revita'),
            'id' => "date_to",
            'type' => 'date',
            'timestamp' => true,
          ),


        ),
        'clone' => true,
      ),

	    array(
		    'name' => __('Subskrypcja rozmowy z lekarzem zakupiona do', 'revita'),
		    'id' => "{$prefix2}date_sub_doc",
		    'type' => 'date',
		    'timestamp' => true,
	    ),

	    array(
		    'name' => __('Wykupione subskrypcje rozmowy z lekarzem', 'revita'),
		    'id' => "{$prefix2}subs_doc_history",
		    'type' => 'group',
		    'fields' => array(
			    array(
				    'name' => __('Cennik', 'revita'),
				    'id' => "cennik",
				    'type' => 'post',
				    'post_type' => 'cennik_doc',
			    ),
			    array(
				    'name' => __('Data od', 'revita'),
				    'id' => "date_from",
				    'type' => 'date',
				    'timestamp' => true,
			    ),
			    array(
				    'name' => __('Data do', 'revita'),
				    'id' => "date_to",
				    'type' => 'date',
				    'timestamp' => true,
			    ),


		    ),
		    'clone' => true,
	    ),

    )
  );


  $meta_boxes[] = array(
    'id' => 'standard-2',
    'title' => __('Informacje podstawowe', 'revitadiet'),
    'post_types' => array('measurement'),
    'context' => 'normal',
    'priority' => 'high',
    'autosave' => true,
    'fields' => array(
      array(
        'name' => __('Użytkownik', 'revita'),
        'id' => "{$prefix2}user_relation",
        'type' => 'post',
        'post_type' => 'user_client'
      ),
      array(
        'name' => __('Waga', 'revita'),
        'id' => "{$prefix2}weight",
        'type' => 'text',
      ),
      array(
        'name' => __('Obwód talii', 'revita'),
        'id' => "{$prefix2}talia",
        'type' => 'text',
      ),
      array(
        'name' => __('Obwód brzucha', 'revita'),
        'id' => "{$prefix2}brzucha",
        'type' => 'text',
      ),
      array(
        'name' => __('Obwód bioder', 'revita'),
        'id' => "{$prefix2}biodra",
        'type' => 'text',
      ),
    )
  );
  $meta_boxes[] = array(
    'id' => 'standard-2',
    'title' => __('Informacje podstawowe', 'revitadiet'),
    'post_types' => array('doc'),
    'context' => 'normal',
    'priority' => 'high',
    'autosave' => true,
    'fields' => array(
      array(
        'name' => __('Nowe hasło', 'revita'),
        'id' => "{$prefix2}password_admin_create",
        'type' => 'text',
      ),
      array(
        'name' => __('Adres e-mail', 'revita'),
        'id' => "{$prefix2}email",
        'type' => 'text',
      ),
      array(
        'name' => __('Imię', 'revita'),
        'id' => "{$prefix2}name",
        'type' => 'text',
      ),
      array(
        'name' => __('Nazwisko', 'revita'),
        'id' => "{$prefix2}surname",
        'type' => 'text',
      ),
      array(
        'name' => __('Typ', 'revita'),
        'id' => "{$prefix2}doc",
        'type' => 'select',
        'options' => array(
          '1' => 'Lekarz',
          '0' => 'Dietetyk'
        ),
      ),
      array(
        'name' => __('Lekarz kontrolujący', 'revita'),
        'id' => "{$prefix2}controlling_doc",
        'type' => (get_post_meta($_GET['post'], 'sf_doc', true) != 1) ? 'post' : 'hidden',
        'post_type' => 'doc',
        'query_args' => array(
          'meta_query' => array(
            'relation' => 'AND',
            array(
              'key' => "{$prefix2}doc",
              'value' => 1,
              'compare' => '='
            ),
          )
        )

      ),

    )
  );
  $prefix = 'sf_';


  $meta_boxes[] = array(
	  'id' => 'not-so-standard',
	  'title' => __('Informacje podstawowe', 'revitadiet'),
	  'post_types' => array('part_ankieta'),
	  'context' => 'normal',
	  'priority' => 'high',
	  'autosave' => true,
	  'fields' =>
		  array(
			  array(
				  'name' => __('Klient', 'revitadiet'),
				  'id' => "{$prefix}client_name",
				  'type' => 'post',
				  'post_type' => 'user_client',
				  'field_type' => 'select_advanced',
				  'query_args' => array(
					  'post_status' => array('publish' , 'draft')
				  )
			  ),
			  array(
				  'name' => __('Zablokowana', 'revitadiet'),
				  'id' => "{$prefix}blocked",
				  'type' => 'checkbox',
			  ),
			  array(
				  'name' => __('Waga', 'revitadiet'),
				  'id' => "{$prefix}calc_1",
				  'type' => 'textarea',
			  ),

			  array(
				  'name' => __('Obwód brzucha: pomiar na wysokości pępka', 'revitadiet'),
				  'id' => "{$prefix}calc_2",
				  'type' => 'textarea',
			  ),
			  array(
				  'name' => __('Obwód talii: pomiar pod żebrami', 'revitadiet'),
				  'id' => "{$prefix}calc_3",
				  'type' => 'textarea',
			  ),			  array(
				  'name' => __('Obwód bioder: pomiar na wysokości kolców biodrowych', 'revitadiet'),
				  'id' => "{$prefix}calc_4",
				  'type' => 'textarea',
			  ),

			  array(
				  'name' => __('Czy dobrze się Pan/-i czuje po zakończeniu obecnego etapu
        diety? Tak/ Nie (objawy)', 'revitadiet'),
				  'id' => "{$prefix}question_1",
				  'type' => 'textarea',
			  ),
			  array(
				  'name' => __('Czy pojawiły się produkty, których nie chce Pan/-i
        kontynuować w kolejnym etapie diety (np. znudziły się )? NIE/TAK
        (Jakie?)'),
				  'id' => "{$prefix}question_2",
				  'type' => 'textarea',
			  ),
			  array(
				  'name' => __('Czy są produkty, których szczególnie Pan/-i brakuje w
        jadłospisie? NIE/TAK (Jakie?)'),
				  'id' => "{$prefix}question_3",
				  'type' => 'textarea',
			  ),
			  array(
				  'name' => __('Czy w tym okresie zostały spożyte produkty niedozwolone ?
        Nie/ Tak (Jakie? Ile razy?)', 'revitadiet'),
				  'id' => "{$prefix}question_4",
				  'type' => 'textarea',
			  ),
			  array(
				  'name' => __('Czy był Pan/-i głodny podczas diety? Nie/ Tak (W jakich
        porach dnia i po jakich posiłkach?)', 'revitadiet'),
				  'id' => "{$prefix}question_5",
				  'type' => 'textarea',
			  ),
			  array(
				  'name' => __('Czy rozpoczął Pan/-i dodatkową aktywność fizyczną? NIE/TAK
        (jak często?)', 'revitadiet'),
				  'id' => "{$prefix}question_6",
				  'type' => 'textarea',
			  ),
			  array(
				  'name' => __('Czy ma Pan/-i dodatkowe uwagi dotyczące diety? Nie/Tak
        (jakie?) ', 'revitadiet'),
				  'id' => "{$prefix}question_7",
				  'type' => 'textarea',
			  ),
		  ),
  );



  /**
   * Ankieta
   */
  $meta_boxes[] = array(
    'id' => 'not-so-standard',
    'title' => __('Informacje podstawowe', 'revitadiet'),
    'post_types' => array('ankieta'),
    'context' => 'normal',
    'priority' => 'high',
    'autosave' => true,
    'fields' =>
      array(
        array(
          'name' => __('Klient', 'revitadiet'),
          'id' => "{$prefix}client_name",
          'type' => 'post',
          'post_type' => 'user_client',
          'field_type' => 'select',
          'query_args' => array(
            'post_status' => array('publish' , 'draft')
          )
        ),


        array(
          'name' => __('Waga', 'revitadiet'),
          'id' => "{$prefix}weight",
          'type' => 'text',

        ),
	      array(
          'name' => __('Wzrost', 'revitadiet'),
          'id' => "{$prefix}height",
          'type' => 'text',

        ),
	      array(
          'name' => __('Waga przy której Pan/Pani czuje się najlepiej', 'revitadiet'),
          'id' => "{$prefix}nice_weight",
          'type' => 'text',

        ),
	      array(
          'name' => __('Najwyższa waga w dorosłym życiu (u kobiet bez ciąży)', 'revitadiet'),
          'id' => "{$prefix}highest_weight",
          'type' => 'text',

        ),
	      array(
          'name' => __('Obwód talii', 'revitadiet'),
          'id' => "{$prefix}circuit_1",
          'type' => 'text',

        ),
	      array(
          'name' => __('Obwód brzucha', 'revitadiet'),
          'id' => "{$prefix}circuit_2",
          'type' => 'text',

        ),
	      array(
          'name' => __('Obwód bioder', 'revitadiet'),
          'id' => "{$prefix}circuit_3",
          'type' => 'text',

        ),array(
          'name' => __('Obwód nadgarstka', 'revitadiet'),
          'id' => "{$prefix}circuit_4",
          'type' => 'text',

        ),


        // CEL WIZYTY
        array(
          'name' => __('Cel wizyty', 'revitadiet'),
          'id' => "{$prefix}purpose",
          'type' => 'checkbox_list',
          'options' => array(
            '1' => 'Odchudający',
            '2' => 'Zdrowotny',
            '3' => 'Przyrost masy ciała',
            '4' => 'Regenerujący budujący masę mięśniową',
            '5' => 'Odmładzający, rewitalizujący',
            '7' => 'Podnoszący poziom energii',
            '6' => 'Inny',
          ),

        ),
        array(
          'name' => __('Inny cel wizyty', 'revitadiet'),
          'id' => "{$prefix}user_other_purposes",
          'type' => 'text',

        ),
        // Operacje
        array(
          'name' => __('Czy osoba przechodziła operacje od okresu dziecięcego', 'revitadiet'),
          'id' => "{$prefix}operation_yes_no",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Jakie operacje ? Kiedy?', 'revitadiet'),
          'id' => "{$prefix}operation_ww",
          'type' => (get_post_meta($_GET['post'], 'sf_operation_yes_no', true)) ? 'text' : 'hidden',

        ),

        //Leki
        array(
          'name' => __('Czy osoba zażywa leki?', 'revitadiet'),
          'id' => "{$prefix}medicines_yes_no",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Jakie lekarstwa?', 'revitadiet'),
          'id' => "{$prefix}medicines_w",
          'type' => (get_post_meta($_GET['post'], 'sf_medicines_yes_no', true)) ? 'text' : 'hidden',

        ),
        array(
          'name' => __('Przewlekle brane', 'revitadiet'),
          'id' => "{$prefix}medicines_season",
          'type' => (get_post_meta($_GET['post'], 'sf_medicines_yes_no', true)) ? 'checkbox' : 'hidden',
        ),
	      array(
          'name' => __('Przewlekle brane - jakie ?', 'revitadiet'),
          'id' => "{$prefix}medicines_season_w",
          'type' => (get_post_meta($_GET['post'], 'sf_medicines_season', true)) ? 'checkbox' : 'hidden',
        ),
        array(
          'name' => __('Okresowo/doraźnie np. antyalergicznie', 'revitadiet'),
          'id' => "{$prefix}medicines_chronic",
          'type' => (get_post_meta($_GET['post'], 'sf_medicines_yes_no', true)) ? 'checkbox' : 'hidden',
        ),
	      array(
		      'name' => __('Okresowo/doraźnie - jakie ?', 'revitadiet'),
		      'id' => "{$prefix}medicines_chronic_w",
		      'type' => (get_post_meta($_GET['post'], 'sf_medicines_chronic', true)) ? 'checkbox' : 'hidden',
	      ),

        // Rozpoznane choroby
        array(
          'name' => __('Zdiagnozowane choroby obecne i istotne przebyte w przeszłości', 'revitadiet'),
          'id' => "{$prefix}disease_yes_no",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Jakie choroby?', 'revitadiet'),
          'id' => "{$prefix}disease_w",
          'type' => (get_post_meta($_GET['post'], 'sf_disease_yes_no', true) == true) ? 'text' : 'hidden',

        ),
	      array(
          'name' => __('Ciśnienie tętnicze', 'revitadiet'),
          'id' => "{$prefix}pre",
          'type' => 'select',
          'options' => array(
	          '4' => 'Nie wiem',
	          '1' => 'Prawidłowe',
	          '2' => 'Niskie',
	          '3' => 'Wysokie',
          ),
        ),

        // Przewód pokarmowy

        array(
          'name' => __('1. Przewód pokarmowy', 'revitadiet'),
          'type' => 'heading',
        ),

        array(
          'name' => __('Problemy z przewodem pokarmowym', 'revitadiet'),
          'id' => "{$prefix}1_problem_0",
          'type' => 'select',
          'options' => array(
            '1' => 'Bez dolegliwości',
            '2' => 'Zaparcia / Biegunki / Naprzemiennie ale z przewagą zaparć / biegunek',
          ),


        ),
        array(
          'name' => __('Wzdęcia', 'revitadiet'),
          'id' => "{$prefix}1_problem_1",
          'type' => 'checkbox',

        ),

        array(
          'name' => __('Zgaga', 'revitadiet'),
          'id' => "{$prefix}1_problem_2",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Refluks', 'revitadiet'),
          'id' => "{$prefix}1_problem_3",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Gazy o intensywnym zapachu', 'revitadiet'),
          'id' => "{$prefix}1_problem_4",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Pieczenie odbytu lub świąd odbytu', 'revitadiet'),
          'id' => "{$prefix}1_problem_5",
          'type' => 'checkbox',

        ),

        //Skóra
        array(
          'name' => __('2. Skóra ', 'revitadiet'),
          'type' => 'heading',
        ),


        array(
          'name' => __('Prawidłowa', 'revitadiet'),
          'id' => "{$prefix}2_problem_0",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Sucha', 'revitadiet'),
          'id' => "{$prefix}2_problem_1",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Nadmierne pocenie', 'revitadiet'),
          'id' => "{$prefix}2_problem_2",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Trądzik', 'revitadiet'),
          'id' => "{$prefix}2_problem_3",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Świąd skóry', 'revitadiet'),
          'id' => "{$prefix}2_problem_4",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Pokrzywki, plamy, wypryski', 'revitadiet'),
          'id' => "{$prefix}2_problem_5",
          'type' => 'checkbox',
        ),
        array(
          'name' => __('Cellulit', 'revitadiet'),
          'id' => "{$prefix}2_problem_6",
          'type' => 'checkbox',

        ),


        array(
          'name' => __('3. Energia ', 'revitadiet'),
          'type' => 'heading',
        ),
	      array(
		      'name' => __('Nieprawidłowa', 'revitadiet'),
		      'id' => "{$prefix}3_problem_0",
		      'type' => 'checkbox',

	      ),
	      array(
		      'name' => __('Zmęczenie poranne (po nocy)', 'revitadiet'),
		      'id' => "{$prefix}3_problem_1",
		      'type' => 'checkbox',

	      ),
	      array(
		      'name' => __('Zmęczenie całodniowe', 'revitadiet'),
		      'id' => "{$prefix}3_problem_2",
		      'type' => 'checkbox',

	      ),
	      array(
		      'name' => __('Zmęczenie popołudniowe', 'revitadiet'),
		      'id' => "{$prefix}3_problem_3",
		      'type' => 'checkbox',
	      ),


        array(
          'name' => __('4. Sen', 'revitadiet'),
          'type' => 'heading',
        ),
        array(
          'name' => __('Prawidłowy', 'revitadiet'),
          'id' => "{$prefix}4_problem_0",
          'type' => 'checkbox',

        ),


        array(
          'name' => __('5. Alergie', 'revitadiet'),
          'type' => 'heading',
        ),
        array(
          'name' => __('TAK / NIE', 'revitadiet'),
          'id' => "{$prefix}5_problem_0",
          'type' => 'checkbox',

        ),

        array(
          'name' => __('Pokarmowe', 'revitadiet'),
          'id' => "{$prefix}5_problem_1",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Wziewne', 'revitadiet'),
          'id' => "{$prefix}5_problem_2",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Skórne', 'revitadiet'),
          'id' => "{$prefix}5_problem_3",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Inne', 'revitadiet'),
          'id' => "{$prefix}5_problem_4",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Jakie?', 'revitadiet'),
          'id' => "{$prefix}5_problem_5",
          'type' => (get_post_meta($_GET['post'], 'sf_5_problem_4', true) == true) ? 'text' : 'hidden',

        ),


        array(
          'name' => __('6. Odporność', 'revitadiet'),
          'type' => 'heading',
        ),

        array(
          'name' => __('Odporność', 'revitadiet'),
          'id' => "{$prefix}6_problem_0",
          'type' => 'select',
          'options' => array(
            '1' => 'Prawidłowa',
            '2' => 'Nieprawidłowa',
            '3' => 'Częste infekcje',
          ),


        ),
        array(
          'name' => __('Zapalenia pęcherza', 'revitadiet'),
          'id' => "{$prefix}6_problem_1",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Zapalenia migdałków', 'revitadiet'),
          'id' => "{$prefix}6_problem_2",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Zapalenia uszu', 'revitadiet'),
          'id' => "{$prefix}6_problem_3",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Zapalenia górnych dróg oddechowych', 'revitadiet'),
          'id' => "{$prefix}6_problem_4",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Opryszczka', 'revitadiet'),
          'id' => "{$prefix}6_problem_5",
          'type' => 'checkbox',

        ), array(
        'name' => __('Inne', 'revitadiet'),
        'id' => "{$prefix}6_problem_6",
        'type' => 'checkbox',

      ),

        array(
          'name' => __('CZĘSTE ANTYBIOTYKOTERAPIE', 'revitadiet'),
          'id' => "{$prefix}6_problem_7",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Kiedy? Obecnie / W dzieciństwie?', 'revitadiet'),
          'id' => "{$prefix}6_problem_8",
          'type' => (get_post_meta($_GET['post'], 'sf_6_problem_7', true)!='') ? 'text' : 'hidden',

        ),


        array(
          'name' => __('7. Stawy', 'revitadiet'),
          'type' => 'heading',
        ),

        array(
          'name' => __('Bóle', 'revitadiet'),
          'id' => "{$prefix}7_problem_0",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Obrzęki', 'revitadiet'),
          'id' => "{$prefix}7_problem_1",
          'type' => 'checkbox',
        ),
	      array(
		      'name' => __('Obrzęki - jakie ?', 'revitadiet'),
		      'id' => "{$prefix}7_problem_1",
		      'type' => 'select',
		      'options' => array(
			      '0' => 'Brak',
			      '1' => 'głównie nóg',
			      '2' => 'głównie twarzy',
			      '3' => 'całe ciało',
			      '4' => 'nie umiem określić',
		      )
	      ),
	      array(
		      'name' => __('Obrzęki - kiedy ?', 'revitadiet'),
		      'id' => "{$prefix}7_problem_1",
		      'type' => 'select',
		      'options' => array(
		      	'0' => 'Brak',
		      	'1' => 'poranne',
		      	'2' => 'wieczorne',
		      	'3' => 'cały dzień',
		      )
	      ),
        array(
          'name' => __('Ostrogi', 'revitadiet'),
          'id' => "{$prefix}7_problem_2",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Choroba Zwyrodnieniowa', 'revitadiet'),
          'id' => "{$prefix}7_problem_3",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Osteoporoza', 'revitadiet'),
          'id' => "{$prefix}7_problem_4",
          'type' => 'checkbox',

        ),


        array(
          'name' => __('8. Wywiad ginekologiczny', 'revitadiet'),
          'type' => 'heading',
        ),

        array(
          'name' => __('Miesiączkuje', 'revitadiet'),
          'id' => "{$prefix}8_problem_0",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Bolesne miesiączki', 'revitadiet'),
          'id' => "{$prefix}8_problem_1",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Rodziła', 'revitadiet'),
          'id' => "{$prefix}8_problem_2",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Poronienia', 'revitadiet'),
          'id' => "{$prefix}8_problem_3",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Zespół Napięcia Przedmiesiączkowego', 'revitadiet'),
          'id' => "{$prefix}8_problem_4",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Regularnie', 'revitadiet'),
          'id' => "{$prefix}8_problem_5",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Antykoncepcja', 'revitadiet'),
          'id' => "{$prefix}8_problem_6",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Hormonalne leczenie', 'revitadiet'),
          'id' => "{$prefix}8_problem_7",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('MIĘŚNIAKI / TORBIELE', 'revitadiet'),
          'id' => "{$prefix}8_problem_8",
          'type' => 'checkbox',

        ),

        array(
          'name' => __('9. Obrzęki i skurcze', 'revitadiet'),
          'type' => 'heading',
        ),

        array(
          'name' => __('Uczucie zatrzymania wody', 'revitadiet'),
          'id' => "{$prefix}9_problem_0",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Skurcze łydek', 'revitadiet'),
          'id' => "{$prefix}9_problem_1",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Skurcze powiek', 'revitamed'),
          'id' => "{$prefix}9_problem_2",
          'type' => 'checkbox',

        ),


        array(
          'name' => __('10. Wywiad rodzinny', 'revitamed'),
          'type' => 'heading',
        ),

        array(
          'name' => __('Choroby tarczycy', 'revitamed'),
          'id' => "{$prefix}10_problem_0",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Nadciśnienie', 'revitamed'),
          'id' => "{$prefix}10_problem_1",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Astma', 'revitamed'),
          'id' => "{$prefix}10_problem_2",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Cukrzyca', 'revitamed'),
          'id' => "{$prefix}10_problem_3",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Zawały', 'revitamed'),
          'id' => "{$prefix}10_problem_4",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Udary', 'revitamed'),
          'id' => "{$prefix}10_problem_5",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Nowotwory', 'revitamed'),
          'id' => "{$prefix}11_problem_6",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Jakie?', 'revitamed'),
          'id' => "{$prefix}11_problem_7",
          'type' => 'text',

        ),
        array(
          'name' => __('11. Historia wagi ', 'revitamed'),
          'type' => 'heading',
        ),
        array(
          'name' => __('Maksymalna waga w życiu dorosłym ( bez ciąży u kobiet) ', 'revitamed'),
          'id' => "{$prefix}11_problem_0",
          'type' => 'text',

        ),
        array(
          'name' => __('Waga optymalna przy której czuje się Pan Pani dobrze', 'revitamed'),
          'id' => "{$prefix}11_problem_1",
          'type' => 'text',

        ),

        array(
          'name' => __('12. Stosowane diety', 'revitamed'),
          'type' => 'heading',
        ),
        array(
          'name' => __('Czy w przeszłości stosowano diety ?', 'revitamed'),
          'id' => "{$prefix}12_problem_0",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Kiedy? Jaki efekt? Jakie samopoczucie?', 'revitamed'),
          'id' => "{$prefix}12_problem_1",
          'type' => 'text',
        ),
	      array(
          'name' => __('Czy jesteś lub byłeś pacjentem Revitamed? (Dr Arendarczyk lub Dr Sobkowiak) ?', 'revitamed'),
          'id' => "{$prefix}12_problem_3",
          'type' => 'checkbox',
        ),
        array(
          'name' => __('13. Wywiad żywieniowy - Posiłki', 'revitamed'),
          'type' => 'heading',
        ),

        array(
          'name' => __('I Śniadanie', 'revitamed'),
          'id' => "{$prefix}13_problem_0",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Posiłki Śniadanie', 'revitamed'),
          'id' => "{$prefix}13_problem_0_t",
          'type' => (get_post_meta($_GET['post'], 'sf_13_problem_0', true)) ? 'text' : 'hidden',
        ),
	      array(
          'name' => __('Godziny Śniadanie', 'revitamed'),
          'id' => "{$prefix}13_problem_0_w",
          'type' => (get_post_meta($_GET['post'], 'sf_13_problem_0', true)) ? 'text' : 'hidden',
        ),

        array(
          'name' => __('II Śniadanie', 'revitamed'),
          'id' => "{$prefix}13_problem_1",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Posiłki II Śniadanie', 'revitamed'),
          'id' => "{$prefix}13_problem_1_t",
          'type' => (get_post_meta($_GET['post'], 'sf_13_problem_1', true)) ? 'text' : 'hidden',

        ),

        array(
          'name' => __('Obiad', 'revitamed'),
          'id' => "{$prefix}13_problem_2",
          'type' => 'checkbox',

        ),

        array(
          'name' => __('Posiłki obiadu', 'revitamed'),
          'id' => "{$prefix}13_problem_2_t",
          'type' => (get_post_meta($_GET['post'], 'sf_13_problem_2', true)) ? 'text' : 'hidden',

        ),
        array(
          'name' => __('Podwieczorek', 'revitamed'),
          'id' => "{$prefix}13_problem_3",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Posiłki podwieczorku', 'revitamed'),
          'id' => "{$prefix}13_problem_3_t",
          'type' => (get_post_meta($_GET['post'], 'sf_13_problem_3', true)) ? 'text' : 'hidden',

        ),

        array(
          'name' => __('Kolacja', 'revitamed'),
          'id' => "{$prefix}13_problem_4",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Posiłki kolacji', 'revitamed'),
          'id' => "{$prefix}13_problem_4_t",
          'type' => (get_post_meta($_GET['post'], 'sf_13_problem_4', true) == true) ? 'text' : 'hidden',

        ),
        array(
          'name' => __('14. Wywiad żywieniowy - Przekąski', 'revitamed'),
          'type' => 'heading',
        ),

        array(
          'name' => __('Owoce', 'revitamed'),
          'id' => "{$prefix}14_problem_0",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Owoce  ( ile, jakie ,  w jakiej porze dnia )', 'revitamed'),
          'id' => "{$prefix}14_problem_0_t",
          'type' => 'text',

        ),
        array(
          'name' => __('Orzeszki', 'revitamed'),
          'id' => "{$prefix}14_problem_1",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Pestki słonecznika, dyni', 'revitamed'),
          'id' => "{$prefix}14_problem_2",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Owoce suszone', 'revitamed'),
          'id' => "{$prefix}14_problem_3",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Słodycze, ciasta, batony', 'revitamed'),
          'id' => "{$prefix}14_problem_4",
          'type' => 'checkbox',

        ),
        array(
          'name' => __('Nabiał ( jak często w tygodniu, ile , jaki ?)', 'revitamed'),
          'id' => "{$prefix}14_problem_5",
          'type' => 'text',

        ),
        array(
          'name' => __('15. Wywiad żywieniowy ', 'revitamed'),
          'type' => 'heading',
        ),


        array(
          'name' => __('CZY WYSTĘPUJĄ  PRODUKTY ŻYWIENIOWE, KTÓRYCH PAN/I ZDECYDOWNIE  NIE ŻYCZY SOBIE W DIECIE ', 'revitamed'),
          'id' => "{$prefix}15_problem_0",
          'type' => 'checkbox',

        ),
        array(
        'name' => __('Jakie ?', 'revitamed'),
        'id' => "{$prefix}15_problem_1",
        'type' => 'text',

      ),

        array(
        'name' => __('Jakie  rodzaje olei /tłuszczów  występują w Pana/i  diecie?', 'revitamed'),
        'id' => "{$prefix}15_problem_3",
        'type' => 'text',

      ),
	      array(
        'name' => __('JAKIEGO RODZAJU POSIŁKI SPOŻYWA PAN/I?', 'revitamed'),
        'id' => "{$prefix}15_problem_2",
        'type' => 'checkbox_list',
        'options' => array(
		      '1' => 'Jedzenie domowe',
		      '2' => 'Jedzenie przygotowane na stołówce',
		      '3' => 'Jedzenie w barze, restauracji',
		      '4' => 'Fast Food',
	      )
      ),
        array(
          'name' => __('16. Wywiad żywieniowy - MĄCZNE PRODUKTY', 'revitamed'),
          'type' => 'heading',
        ),

        array(
          'name' => __('Makarony', 'revitamed'),
          'id' => "{$prefix}16_problem_0",
          'type' => 'checkbox',
        ),
        array(
        'name' => __('Pierogi', 'revitamed'),
        'id' => "{$prefix}16_problem_1",
        'type' => 'checkbox',
      ),
        array(
        'name' => __('Naleśniki', 'revitamed'),
        'id' => "{$prefix}16_problem_2",
        'type' => 'checkbox',
      ),
        array(
        'name' => __('Pizza', 'revitamed'),
        'id' => "{$prefix}16_problem_3",
        'type' => 'checkbox',
      ),
        array(
        'name' => __('Zapiekanki', 'revitamed'),
        'id' => "{$prefix}16_problem_4",
        'type' => 'checkbox',
      ),
        array(
        'name' => __('Jak często w tygodniu?', 'revitamed'),
        'id' => "{$prefix}16_problem_5",
        'type' => 'text',
      ),
        array(
          'name' => __('17. Wywiad żywieniowy - FORMA PRZYGOTOWYWANIA POTRAW', 'revitamed'),
          'type' => 'heading',
        ),
        array(
          'name' => __('Smażone / gotowane / pieczone - w kolejności od najczęstrzej do najrzadszej', 'revitamed'),
          'id' => "{$prefix}17_problem_0",
          'type' => 'text',

        ),

        array(
          'name' => __('18. Wywiad żywieniowy - ZŁE NAWYKI', 'revitamed'),
          'type' => 'heading',
        ),
        array(
          'name' => __('Alkohol ( jaki jak często?)', 'revitamed'),
          'id' => "{$prefix}18_problem_0",
          'type' => 'text',

        ),
        array(
          'name' => __('Papierosy i inne używki - jakie? Jak często?', 'revitamed'),
          'id' => "{$prefix}18_problem_1",
          'type' => 'text',

        ),
        array(
          'name' => __('Fast Foody - Jak często? ', 'revitamed'),
          'id' => "{$prefix}18_problem_2",
          'type' => 'text',

        ),
        array(
          'name' => __('19. Wywiad żywieniowy - NAPOJE', 'revitamed'),
          'type' => 'heading',
        ),
        array(
          'name' => __('Ile i co pije Pan / Pani przez cały dzień? (jaka woda)', 'revitamed'),
          'id' => "{$prefix}19_problem_0",
          'type' => 'text',
        ),
        array(
          'name' => __('Herbaty, kawy (jakie, ile?)', 'revitamed'),
          'id' => "{$prefix}19_problem_1",
          'type' => 'text',
        ),
        array(
          'name' => __('Soki, energetyki (jakie, ile?)', 'revitamed'),
          'id' => "{$prefix}19_problem_2",
          'type' => 'text',
        ),
        array(
          'name' => __('20. Ankieta szczegółowa', 'revitamed'),
          'type' => 'heading',
        ),
        array(
          'name' => __('Tak - Chcę, żeby moje preferencje żywieniowe zostały uwzględnione', 'revitamed'),
          'id' => "{$prefix}20_problem_0",
          'type' => 'checkbox',
        ),
        array(
          'name' => __('Nie - nie jest to istotne dla mnie = jem wszystko ALBO po prostu chcę zrobić wszystko co możliwe dla mojego zdrowia niezależnie od moich preferencji żywieniowych', 'revitamed'),
          'id' => "{$prefix}20_problem_1",
          'type' => 'checkbox',
        ),
        array(
          'name' => __('21. Ankieta szczegółowa - Praca', 'revitamed'),
          'type' => 'heading',
        ),
        array(
          'name' => __('Godziny pracy (przerwy w pracy na posiłek):', 'revitamed'),
          'id' => "{$prefix}21_problem_0",
          'type' => 'text',
        ),
        array(
          'name' => __('Przerwy', 'revitamed'),
          'id' => "{$prefix}21_problem_1",
          'type' => 'select',
          'options' => array(
            '1' => 'Przerwy są wyznaczone odgórnie',
            '2' => 'Przerwy są regulowane indywidualnie',
          ),


        ),
        array(
          'name' => __('CHARAKTER PRACY', 'revitamed'),
          'id' => "{$prefix}21_problem_3",
          'type' => 'checkbox_list',
          'options' => array(
            '1' => 'w aucie, cały dzień w podróży',
            '2' => 'bez zaplecza typu lodówka, kuchenka do odgrzania',
            '3' => 'korzystam z restauracji, barów',
            '4' => 'mam pracę zmianową',
            '5' => 'mam pomieszczeni socjalne z lodówką, z kuchenką',
            '6' => 'ruszam się dużo w pracy',
            '7' => 'dźwigam dużo ciężkich rzeczy w pracy',
          )
        ),
        array(
          'name' => __('22. Ankieta szczegółowa - Ćwiczę', 'revitamed'),
          'type' => 'heading',
        ),
        array(
          'name' => __('Jak często w tygodniu ćwiczysz?', 'revitamed'),
          'id' => "{$prefix}22_problem_0",
          'type' => 'text',
        ),
        array(
          'name' => __('Ile trwają ćwiczenia?', 'revitamed'),
          'id' => "{$prefix}22_problem_1",
          'type' => 'text',
        ),
        array(
          'name' => __('Jaki to rodzaj wysiłku?', 'revitamed'),
          'id' => "{$prefix}22_problem_2",
          'type' => 'text',
        ),
        array(
          'name' => __('Od kiedy ćwiczysz?', 'revitamed'),
          'id' => "{$prefix}22_problem_3",
          'type' => 'text',
        ),
        array(
          'name' => __('Suplementuje się w związku z tym następującymi produktami:', 'revitamed'),
          'id' => "{$prefix}22_problem_4",
          'type' => 'text',
        ),
        array(
          'name' => __('23. Ankieta szczegółowa - Brak tolerancji dla produktów', 'revitamed'),
          'type' => 'heading',
        ),
        array(
          'name' => __('Jestem weganinem', 'revitamed'),
          'id' => "{$prefix}23_problem_0",
          'type' => 'checkbox',
        ),
        array(
          'name' => __('Jestem wegetarianinem', 'revitamed'),
          'id' => "{$prefix}23_problem_1",
          'type' => 'checkbox',
        ),
        array(
          'name' => __('Od kiedy?', 'revitamed'),
          'id' => "{$prefix}23_problem_2",
          'type' => 'text',
        ),
        array(
          'name' => __('NIE TOLERUJE W MOJEJ DIECIE', 'revitamed'),
          'id' => "{$prefix}23_problem_3",
          'type' => 'text',
        ),
        array(
          'name' => __('CZUJĘ SIĘ ŹLE PO TAKICH PRODUKTACH JAK', 'revitamed'),
          'id' => "{$prefix}23_problem_4",
          'type' => 'text',
        ),
        array(
          'name' => __('MAM OBJAWY ALERGII PO:', 'revitamed'),
          'id' => "{$prefix}23_problem_5",
          'type' => 'text',
        ),
        array(
          'name' => __('24. Ankieta szczegółowa - CZY MOGĄ POJAWIĆ SIĘ W PANA/PANI DIECIE:', 'revitamed'),
          'type' => 'heading',
        ),
        array(
          'name' => __('Produkty', 'revitamed'),
          'id' => "{$prefix}24_problem_0",
          'type' => 'checkbox_list',
          'options' => array(
            '1' => 'ryby wędzone / w puszkach / śledzie',
            '2' => 'jajka',
            '3' => 'sery owcze / kozie',
            '4' => 'soczewica / ciecierzyca / fasole',
            '5' => 'chleb na zakwasie żytni',
          ),
        ),
        array(
          'name' => __('CZY SĄ W PANA/PANI DIECIE PRODUKTY, KTÓRYCH NIE ZJE PAN/PANI ZA CZĘSTO CHOĆ CZASAMI MOGĄ BYĆ', 'revitamed'),
          'id' => "{$prefix}24_problem_1",
          'type' => 'text',
        ),
        array(
          'name' => __('Moimi ulubionymi produktami są:', 'revitamed'),
          'id' => "{$prefix}24_problem_2",
          'type' => 'checkbox_list',
          'options' => array(
            '1' => 'mięso',
            '2' => 'ryby',
            '3' => 'sery',
            '4' => 'jogurty',
            '5' => 'mleko',
            '6' => 'pieczywo',
            '7' => 'jajka',
          ),
        ),
        array(
          'name' => __('Inne, jakie ?', 'revitamed'),
          'id' => "{$prefix}24_problem_3",
          'type' => 'text',
        ),
        array(
          'name' => __('25. Ankieta szczegółowa - Preferencje', 'revitamed'),
          'type' => 'heading',
        ),
        array(
          'name' => __('JAK CZĘSTO JE PAN/PANI OWOCE, PIJE SOKI OWOCOWE?', 'revitamed'),
          'id' => "{$prefix}25_problem_0",
          'type' => 'text',
        ),
        array(
          'name' => __('Jakie są to owoce/soki?', 'revitamed'),
          'id' => "{$prefix}25_problem_1",
          'type' => 'text',
        ),
        array(
          'name' => __('ILE W PANA/PANI DIECIE JEST CHLEBA (Kromek na dzień / na tydzień)?', 'revitamed'),
          'id' => "{$prefix}25_problem_2",
          'type' => 'text',
        ),
        array(
          'name' => __('JAKI RODZAJ MIĘSA DOMINUJE U PANA/PANI W DIECIE?', 'revitamed'),
          'id' => "{$prefix}25_problem_3",
          'type' => 'text',
        ),
        array(
          'name' => __('Ile razy w tygodniu je Pan/Pani mięso lub ryby?
', 'revitamed'),
          'id' => "{$prefix}25_problem_4",
          'type' => 'text',
        ),
        array(
          'name' => __('Dobrze toleruje pan/pani', 'revitamed'),
          'id' => "{$prefix}25_problem_5",
          'type' => 'checkbox_list',
          'options' => array(
            '1' => 'oliwę z oliwek',
            '2' => 'olej lniany nieoczyszczony',
            '3' => 'masło klarowane',
            '4' => 'olej kokosowy',
          ),
        ),
        array(
          'name' => __('Mogą znaleźć się u pana/pani w diecie: ', 'revitamed'),
          'id' => "{$prefix}25_problem_6",
          'type' => 'checkbox_list',
          'options' => array(
            '1' => 'wątróbka',
            '2' => 'żołądki',
            '3' => 'serduszka',
            '4' => 'ozorki',
            '5' => 'żelatyny, tzw. \'zimne nóżki\'',
            '6' => 'mięsa / ryby w galarecie',
          ),
        ),

      )
  );

  /**
   * Dieta
   */
  $meta_boxes[] = array(
    'id' => 'standard_0',
    'title' => __('Dodatkowe parametry', 'revitamed'),
    'post_types' => array('diet'),
    'context' => 'normal',
    'priority' => 'high',
    'autosave' => true,
    'fields' => array(
      array(
        'name' => __('Klient', 'revitamed'),
        'id' => "{$prefix}user",
        'type' => 'post',
        'post_type' => 'user_client'
      ),
      array(
        'name' => __('Dietetyk / lekarz', 'revitamed'),
        'id' => "{$prefix}doc",
        'type' => 'post',
        'post_type' => 'doc'
      ),
      array(
        'name' => __('Ilość dań', 'revitamed'),
        'id' => "{$prefix}quantity_meal",
        'type' => 'select',
        'options' => [
          '2' => '2 dania',
          '3' => '3 dania',
          '4' => '4 dania',
          '5' => '5 dań',
        ],
      ),
	    array(
		    'name' => __('Data zgłoszenia', 'revitamed'),
		    'id' => "{$prefix}date_order",
		    'type' => 'date',
		    'timestamp' => true,
	    ),
      array(
        'name' => __('Waga początkowa', 'revitamed'),
        'id' => "{$prefix}weight_start",
        'type' => 'text',
      ),
      array(
        'name' => __('Waga docelowa', 'revitamed'),
        'id' => "{$prefix}weight_target",
        'type' => 'text',
      ),
      array(
        'name' => __('Cel diety', 'revitamed'),
        'id' => "{$prefix}purpose_diet",
        'type' => 'text',
      ),
      array(
        'name' => __('Data od', 'revitamed'),
        'id' => "{$prefix}date_from",
        'type' => 'date',
        'timestamp' => true,
      ),
      array(
        'name' => __('Data do', 'revitamed'),
        'id' => "{$prefix}date_to",
        'type' => 'date',
        'timestamp' => true,
      ),
      array(
        'name' => __('Wskazany przez lekarza czas trwania diety', 'revitamed'),
        'id' => "{$prefix}duration",
        'type' => 'text',
      ),
      array(
        'name' => __('Codzienna suplementacja', 'revitamed'),
        'id' => "{$prefix}supplements",
        'type' => 'textarea',
      ),
      array(
        'name' => __('Uwagi', 'revitamed'),
        'id' => "{$prefix}warnings",
        'type' => 'textarea',
      ),
      array(
        'name' => __('Produkty zakazane', 'revitamed'),
        'id' => "{$prefix}forbidden",
        'type' => 'textarea',
      ),
      array(
        'name' => __('Aktualna', 'revitamed'),
        'id' => "{$prefix}current",
        'type' => 'checkbox',
      ),
    array(
	    'name' => __('Stworzona', 'revitamed'),
	    'id' => "{$prefix}active",
	    'type' => 'checkbox',
    ),
      array(
        'name' => __('Status', 'revitamed'),
        'id' => "{$prefix}status",
        'type' => 'select',
        'options' => [
	        0 => 'Nieopłacona',
	        1 => 'W trakcie',
	        2 => 'Nowa do ułożenia',
	        3 => 'Zakończona',
	        4 => 'Kontynuacja',
	        5 => 'Oczekuje na termin rozpoczęcia',
        ]
      ),


    ),
  );
  $meta_boxes[] = array(
    'id' => 'standard',
    'title' => __('Posiłki', 'revitamed'),
    'post_types' => array('diet','template_diet'),
    'context' => 'normal',
    'priority' => 'high',
    'autosave' => true,
    'fields' => $meals
  );
	$meta_boxes[] = array(
		'id' => 'standard_0',
		'title' => __('Dodatkowe parametry', 'revitamed'),
		'post_types' => array('template_diet'),
		'context' => 'normal',
		'priority' => 'high',
		'autosave' => true,
		'fields' => array(
			array(
				'name' => __('Ilość dań', 'revitamed'),
				'id' => "{$prefix}quantity_meal",
				'type' => 'select',
				'options' => [
					'2' => '2 dania',
					'3' => '3 dania',
					'4' => '4 dania',
					'5' => '5 dań',
				],
			),
			array(
				'name' => __('Codzienna suplementacja', 'revitamed'),
				'id' => "{$prefix}supplements",
				'type' => 'textarea',
			),
			array(
				'name' => __('Uwagi', 'revitamed'),
				'id' => "{$prefix}warnings",
				'type' => 'textarea',
			),
			array(
				'name' => __('Produkty zakazane', 'revitamed'),
				'id' => "{$prefix}forbidden",
				'type' => 'textarea',
			),
		),
	);
  /**
   * Wiadomości
   */
  $meta_boxes[] = array(
    'id' => 'standard',
    'title' => __('Dodatkowe parametry', 'revitamed'),
    'post_types' => array('msg'),
    'context' => 'normal',
    'priority' => 'high',
    'autosave' => true,
    'fields' => array(
      array(
        'name' => __('Od', 'revitamed'),
        'id' => "{$prefix}sender",
        'type' => 'post',
        'post_type' => ['user_client','doc']
      ),
      array(
        'name' => __('Do', 'revitamed'),
        'id' => "{$prefix}receiver",
        'type' => 'post',
        'post_type' => ['user_client','doc']
      ),
      array(
        'name' => __('Treść', 'revitamed'),
        'id' => "{$prefix}content",
        'type' => 'textarea',
      ),
    )
  );
  /**
   * Cennik
   */
  $meta_boxes[] = array(
    'id' => 'standard',
    'title' => __('Dodatkowe parametry', 'revitamed'),
    'post_types' => array('cennik'),
    'context' => 'normal',
    'priority' => 'high',
    'autosave' => true,
    'fields' => array(
      array(
        'name' => __('Cena aktualna', 'revitamed'),
        'id' => "{$prefix}price_current",
        'type' => 'text',
      ),
      array(
        'name' => __('Cena poprzedna', 'revitamed'),
        'id' => "{$prefix}price_previous",
        'type' => 'text',
      ),
      array(
        'name' => __('Dostęp na [dni]', 'revitamed'),
        'id' => "{$prefix}days",
        'type' => 'number',
      ),
      array(
        'name' => __('Zakres abonamentu', 'revitamed'),
        'id' => "{$prefix}text",
        'size' => 100,
        'type' => 'text',
        'clone' => true
      ),
    )
  );

	/**
	 *
	 */
	$meta_boxes[] = array(
		'id' => 'standard',
		'title' => __('Dodatkowe parametry', 'revitamed'),
		'post_types' => array('cennik_doc'),
		'context' => 'normal',
		'priority' => 'high',
		'autosave' => true,
		'fields' => array(
			array(
				'name' => __('Cena aktualna', 'revitamed'),
				'id' => "{$prefix}price_current",
				'type' => 'text',
			),
			array(
				'name' => __('Cena poprzedna', 'revitamed'),
				'id' => "{$prefix}price_previous",
				'type' => 'text',
			),
			array(
				'name' => __('Dostęp na [dni]', 'revitamed'),
				'id' => "{$prefix}days",
				'type' => 'number',
			),
		)
	);

  $meta_boxes[] = array(
    'id' => 'standard',
    'title' => __('Dodatkowe parametry', 'revitamed'),
    'post_types' => array('realization_diet'),
    'context' => 'normal',
    'priority' => 'high',
    'autosave' => true,
    'fields' => array(
      array(
        'name' => __('Data', 'revitamed'),
        'id' => "{$prefix}date",
        'type' => 'date',
        'timestamp' => true,
      ),
      array(
        'name' => __('Posiłek', 'revitamed'),
        'id' => "{$prefix}meal",
        'type' => 'select',
        'options' => [
          '1'=> 'Śniadanie',
          '2'=> 'II śniadanie',
          '3'=> 'Obiad',
          '4'=> 'Przekąski',
          '5'=> 'Kolacja',
        ]
      ),
      array(
        'name' => __('Zjadłem', 'revitamed'),
        'id' => "{$prefix}eaten",
        'type' => 'checkbox'
      ),
      array(
        'name' => __('Dieta', 'revitamed'),
        'id' => "{$prefix}diet",
        'type' => 'post',
        'post_type' => ['diet']
      ),
    )
  );
	$meta_boxes[] = array(
		'id' => 'standard',
		'title' => __('Dodatkowe parametry', 'revitamed'),
		'post_types' => array('custom_file'),
		'context' => 'normal',
		'priority' => 'high',
		'autosave' => true,
		'fields' => array(
			array(
				'name' => __('Nazwa pliku', 'revitamed'),
				'id' => "{$prefix}name",
				'type' => 'text',
			),
			array(
				'name' => __('Lokalizacja pliku', 'revitamed'),
				'id' => "{$prefix}path_var",
				'type' => 'text',
			),
			array(
				'name' => __('Adres pliku względny', 'revitamed'),
				'id' => "{$prefix}path_url",
				'type' => 'text'
			),
			array(
				'name' => __('Sekcja', 'revitamed'),
				'id' => "{$prefix}section",
				'type' => 'text'
			),
			array(
				'name' => __('Klient', 'revitamed'),
				'id' => "{$prefix}user",
				'type' => 'post',
				'post_type' => 'user_client',
				'field_type' => 'select',
				'query_args' => array(
					'post_status' => array('publish' , 'draft')
				)
			),
		)
	);


  return $meta_boxes;
}