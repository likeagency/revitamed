<?php
function updateDiets(){
	set_time_limit(0);
	foreach(getAllDiet() as $diet)
		updateStateDiet($diet);

	foreach(getAllUsers() as $user)
	{
		cronJobsUser($user);
		getNewLastDiet($user->ID);
	}



}

/**
 * @param $user WP_post
 */
function cronJobsUser($user){


	$userEmail = get_post_meta($user->ID,'sf_email',true);

	//reminder about new diet tomorrow
	sentEmailsTomorrowDiet($user,$userEmail);

	// reminder about payment
	sentReminderAboutPayment($user,$userEmail);


	sentReminderAboutIncompleteAccount($user,$userEmail);

	// reminder about partial survey
	sentReminderAboutPartialSurvey($user,$userEmail);

}

function updateOldestDiet($userID){
	$oldestDiet = getFirstDiet($userID);
	if($oldestDiet!==null)
	update_post_meta( $oldestDiet->ID, 'sf_new_diet', 1);
}

function sentEmailsTomorrowDiet($user,$userEmail){
	$titan = TitanFramework::getInstance('revita');
	$userCurrentDiet = getCurrentActiveDiet($user->ID,time());
	$userTomorrowDiet = getCurrentActiveDiet($user->ID,strtotime('+1 day'));
	$userHaveDietReminded = (int)get_post_meta($user->ID,'sf_diet_reminder',true); // czy uzytkownikowi została zapowiedziana nowa dieta


	if( $userTomorrowDiet !== null && $userCurrentDiet->ID !== $userTomorrowDiet->ID)
	{
		if($userTomorrowDiet->ID  != $userHaveDietReminded)
		{
			$title = $titan->getOption('email_new_diet_title');
			$content = $titan->getOption('email_new_diet_text');
			revitamed_send_mail($userEmail,$content,$title);

			update_post_meta($user->ID,'sf_diet_reminder',$userTomorrowDiet->ID);
		}

	}
}
function sentReminderAboutPayment($user,$userEmail){

	$titan = TitanFramework::getInstance('revita');
	$days = array(10,6);
	foreach($days as $day) {

		$reminderAboutPayment = (bool) get_post_meta( $user->ID, 'sf_user_needs_to_pay_day_'.$day, true );
		if ( ! $reminderAboutPayment && ! empty( getUserSurvey( $user->ID ) ) ) {
			$plan = (int) getCurrentUserPlan( $user->ID, strtotime( '+'. $day .' days' ) );
			if ( $plan === 0 ) {
				update_post_meta( $user->ID, 'sf_user_needs_to_pay_day_'.$day, true );
				$title   = $titan->getOption( 'email_end_subs_title_day_'. $day );
				$content = $titan->getOption( 'email_end_subs_text_day_'. $day );
				revitamed_send_mail( $userEmail, $content, $title );
			}
		}
	}
}

/**
 * @param $user WP_Post
 * @param $userEmail string
 */
function sentReminderAboutIncompleteAccount($user,$userEmail){
	$days = array(1,3,7,30);
	$titan = TitanFramework::getInstance('revita');


	$sub = get_post_meta($user->ID,'sf_subs_history',true);
	$subActive = !empty($sub) && is_array($sub) && $sub[0]['cennik']!='';
	$surveyCompleted = !empty(getUserSurvey($user->ID));


	if(!$subActive && !$surveyCompleted)
	{
		$timeOfRegister = strtotime($user->post_date);
		foreach($days as $day){
			$checkTime = strtotime("+$day days",$timeOfRegister);
			if(time() > $checkTime && get_post_meta($user->ID,'sf_reminder_all_'.$day.'_day',true)=='')
			{
				update_post_meta($user->ID,'sf_reminder_all_'.$day.'_day',1);
				$topic =  $titan->getOption('email_no_everything_days_'.$day);
				$text = $titan->getOption('email_no_everything_days_content_'.$day);
				revitamed_send_mail($userEmail,$text,$topic);
			}

		}

	}

	if(!$subActive && $surveyCompleted)
	{
		$survey = getUserSurvey($user->ID)[0];
		$timeOfSurveyCreation = strtotime($survey->post_date);
		foreach($days as $day){
			$checkTime = strtotime("+$day days",$timeOfSurveyCreation);
			if(time() > $checkTime && get_post_meta($user->ID,'sf_reminder_no_payment_'.$day.'_day',true)=='')
			{
				update_post_meta($user->ID,'sf_reminder_no_payment_'.$day.'_day',1);
				$topic =  $titan->getOption('email_no_payment_days_'.$day);
				$text = $titan->getOption('email_no_payment_days_content_'.$day);
				revitamed_send_mail($userEmail,$text,$topic);
			}

		}

	}

	if($subActive && !$surveyCompleted)
	{
		$timeOfFirstPaying = get_post_meta($user->ID,'sf_first_payment',true);
		foreach($days as $day){
			$checkTime = strtotime("+$day days",$timeOfFirstPaying);
			if(time() > $checkTime && get_post_meta($user->ID,'sf_reminder_no_survey_'.$day.'_day',true)=='')
			{
				update_post_meta($user->ID,'sf_reminder_no_survey_'.$day.'_day',1);
				$topic =  $titan->getOption('email_no_survey_days_'.$day);
				$text = $titan->getOption('email_no_survey_days_content_'.$day);
				revitamed_send_mail($userEmail,$text,$topic);
			}

		}

	}

}

function sentReminderAboutPartialSurvey($user,$userEmail){

	$titan = TitanFramework::getInstance('revita');
	$partialSurvey = getLastPartialSurvey($user->ID,'publish');
	$days = array(4,5);

	if($partialSurvey!== null)
	{
		$currentDiet = getCurrentActiveDiet($user->ID);
		if(empty($currentDiet)) return;
		$day = 3;
		$time = (get_post_meta($currentDiet->ID,'sf_date_to',true)-$day*(60*60*24));
		if(time() > $time)
		{
			update_post_meta($partialSurvey->ID,'sf_blocked',1);
		}

		if(get_post_meta($partialSurvey->ID,'sf_blocked',true)) return;

		foreach($days as $day)
		{
			$reminded = (bool)get_post_meta($partialSurvey->ID,'sf_reminded_days_'.$day,true);
			$time = get_post_meta($currentDiet->ID,'sf_date_to',true)-$day*(60*60*24);

			if(time() > $time && !$reminded)
			{
				update_post_meta($partialSurvey->ID,'sf_reminded_days_'.$day,true);
				$title = $titan->getOption('email_no_partial_survey_title_day_'.$day);
				$content = $titan->getOption('email_no_partial_survey_text_day_'.$day);
				$content.= '<a href="' . get_the_permalink($partialSurvey->ID) .'">Kliknij tutaj</a>';
				revitamed_send_mail($userEmail,$content,$title);
			}
		}


	}
}

function updateStateDiet( $diet ) {
	$dietID = $diet->ID;
	$userID = get_post_meta( $dietID, 'sf_user', true );
	$diets  = getAllDietUser( $userID );
	$active = get_post_meta( $dietID, 'sf_active', true );
	$status = get_post_meta( $dietID, 'sf_status', true );
	$dateTo = get_post_meta( $dietID, 'sf_date_to', true );
	$dateFrom = get_post_meta( $dietID, 'sf_date_from', true );
	$newDiet = 	get_post_meta( $dietID, 'sf_new_diet', true);



	$titan = TitanFramework::getInstance( 'revita' );

	if ( getCurrentUserPlan( $userID ) == 0 || ($active == false && getCurrentUserPlan( $userID,strtotime('
	+21 days') ) == 0) ) {


		update_post_meta( $dietID, 'sf_status', 0 ); //nieopłacona
	}
	elseif ( $active == false ) //jezeli dieta nie jest wypełniona
	{
		if ( $status === '0' || $status === 0 ) {
			update_post_meta( $dietID, "sf_date_order", time() );
		}

			if ( isThatDietContinuation( $diets, $dietID,$userID ) ) {
				update_post_meta( $dietID, 'sf_status', 4 );
			} //kontynuacja
			else {
				update_post_meta( $dietID, 'sf_status', 2 );
				update_post_meta( $dietID, 'sf_new_diet', 1);
			} //nowa do ułożenia

	} elseif ( strtotime( 'now' ) < $dateFrom ) {

		if ( !get_post_meta( $dietID, 'sf_send_email', true ) ) {
			if($newDiet)
			{
				$title   = $titan->getOption( 'email_tomorrow_diet_title' );
				$content = $titan->getOption( 'email_tomorrow_diet_text' );
			}
			else
			{
				$title   = $titan->getOption('email_tomorrow_diet_title_continuation' );
				$content = $titan->getOption('email_tomorrow_diet_text_continuation'  );
			}

			revitamed_send_mail( get_post_meta( $userID, 'sf_email', true ), $content, $title );
			update_post_meta( $dietID, 'sf_send_email', true );
		}


		update_post_meta( $dietID, 'sf_status', 5 ); //Oczekująca
	} elseif ( strtotime( 'now' ) < $dateTo ) {
		update_post_meta( $dietID, 'sf_status', 1 ); //w trakcie
		if ( strtotime( '+5 days' ) > $dateTo )// 5 dni przed końcem diety
		{
			if ( !getCurrentUserPlan( $userID, strtotime( '+21 days', $dateTo ) )
			     && get_post_meta( $dietID, 'sf_email_warning', true ) == ''
			) {
				update_post_meta( $dietID, 'sf_email_warning', 1 );
				$title   = $titan->getOption( 'email_not_payed_title' );
				$content = $titan->getOption( 'email_not_payed_text' );
				revitamed_send_mail( get_post_meta( $userID, 'sf_email', true ), $content, $title );
			}

		}
		$addedUselessDays = get_post_meta($dietID,'sf_added_days',true);

		//dodanie nieużytych dni
		if ( !$addedUselessDays )
		{
			$previousDiet = getPreviousDiet($dietID);

			$timestampFrom = ($previousDiet==null) ? getStartOfSubscription($userID) : get_post_meta($previousDiet->ID,'sf_date_to',true);
			if($timestampFrom!==null)
			{
				$addedTime = time() - $timestampFrom;
				addTimeToCurrentPlan($userID,$addedTime);
			}

			update_post_meta($dietID,'sf_added_days',1);
		}

	} else {
		update_post_meta( $dietID, 'sf_status', 3 ); //zakonczona
	}


}

/**
 * @param int $userID
 * @return WP_post|null
 */
function getNewLastDiet($userID)
{

	if($userID=='') return null;

	$diet = getLastDiet($userID);
	$dietCurrent = getCurrentActiveDiet($userID);
	if($diet!==NULL) updateStateDiet($diet);

	if($diet===NULL || ($diet == $dietCurrent && endOfDietIsClose($dietCurrent->ID))  || get_post_meta($diet->ID,'sf_status',true)==3 )
		return createEmptyDiet($userID);
	else
		return $diet;
}



//TODO test this on different data
/**
 * @param $userID
 *
 * @return null|WP_Post
 */
function createEmptyDiet($userID)
{
	if($userID=='') return null;

	$prefix = 'sf_';
	$doc = get_post_meta($userID,'sf_doc_diet',true);
	$status = (getCurrentUserPlan($userID)) ? 2 : 0;

	$last_date_order= strtotime('now');


	$args = array(
		'post_type' => 'diet',
		'post_status' => 'publish',
		'post_title' => get_the_title($userID),
		'meta_input' => array(
			"{$prefix}user" => $userID,
			"{$prefix}doc" => $doc,
			"{$prefix}status" => $status,
			"{$prefix}date_order" => $last_date_order,
		)

	);
	$id = wp_insert_post($args);


	$link =  get_the_permalink(get_id_after_template_filename('diet-laying-step-1.php')) .'?userID=' . $userID;
	$to = getUserDieterEmail($userID);
	$to2 = getUserDocEmail($userID);
	$title = 'Nowa dieta do ułożenia';
	$content = 'Nowa dieta do ułożenia. Sprawdź ją <a href="'.$link.'">tutaj</a>.';

	if($status!==0){
		revitamed_send_mail($to,$content,$title);
		revitamed_send_mail($to2,$content,$title);
	}



	return (is_wp_error($id)) ? null : get_post($id);
}

function lastStepDiet( $dietID ) {

	$userID = get_post_meta($dietID,'sf_user',true);
	update_post_meta( $dietID, 'sf_active', 1 ); // ready to use
	update_post_meta( $dietID, 'sf_current', true );
	$start = get_post_meta( $dietID, 'sf_new_diet', 1) ? 2 : 1 ;

	$dateStart =  strtotime(date('d.m.Y',strtotime( '+'.$start.' day' ) ));
	if(getCurrentActiveDiet($userID,$dateStart)!==null)
	{
		$thatDiet = getCurrentActiveDiet($userID,$dateStart);
		$dateStart = max(get_post_meta($thatDiet->ID,'sf_date_to',true),$dateStart);
	}

	update_post_meta( $dietID, 'sf_date_from',$dateStart);

	$days = 28;

	$dateStop = strtotime('+'.$days.' days',$dateStart);
	update_post_meta( $dietID, 'sf_date_to',$dateStop);

	$userID    = get_post_meta( $dietID, 'sf_user', true );
	$termsEtap = wp_get_post_terms( $dietID, 'etap_diety' );
	/** @var WP_Term $termEtap */
	$termEtap = $termsEtap[0];
	wp_update_post( [
		'ID'         => $dietID,
		'post_title' => getUserName( $userID ) . ' ' . $termEtap->name,
	] );

}

function endOfDietIsClose($dietID){

	$amountOfSecondsEqualToClose = 60*60*24*7;
	if(get_post_meta($dietID,'sf_date_to',true) - strtotime('now') < $amountOfSecondsEqualToClose)
		return true;
	else
		return false;
}

function isThatDietContinuation($diets,$dietID,$userID)
{

	if(count($diets)<2) return false;


	$lastStartingDiet = getLastStartingDiet($userID);
	$current_stages = wp_get_post_terms( $lastStartingDiet->ID, 'dl_diety' );
	$slug  = ( is_object( $current_stages[0] ) ) ? $current_stages[0]->slug : '';
	if($slug!=='')
	{
			$months = explode('-',$slug)[0];

			$days = $months*28;
	}
	else
		$days = 28;

	$lastActiveDiet = getLastEndedOrActiveDiet($userID);
	$diff = 0;

	foreach(getPreviousDiets($lastActiveDiet->ID) as $diet)
	{
		$start = get_post_meta($diet->ID,'sf_date_from',true);
		$stop = get_post_meta($diet->ID,'sf_date_to',true);
		$diff+= $stop-$start;
	}

	$diff = $diff/(60*60*24);
//	echo "$start $stop $diff $days \n\r";
	return $diff < $days;
}

