<?php
if (!defined('ABSPATH')) return;

define('USER_CLASS','user_client');
class user
{
    private $_login;
    private $_id;
    private $_user_id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->_login;
    }

    public function isActive()
    {
        if ($this->getId() == $_SESSION['user'] && $_SESSION['user'] !== NULL && $_SESSION['agent'] == $_SERVER['HTTP_USER_AGENT'])
            return true;
        else
            return false;
    }


    function __construct()
    {
        $id = $_SESSION['user'];
        if (get_post_type($id) == USER_CLASS) {
            $this->_id = ($id);
            $this->regenerate();
        }

    }
    public function  getIsFemale()
    {
      return get_post_meta($this->getId(),'sf_sex',true)==1;
    }
    private function regenerate()
    {
        $this->_login = (get_post_meta($this->_id, 'sf_email', true));
        $this->_user_id = (get_post_meta($this->_id, 'sf_user', true));
    }
    private function regenerateSession()
    {
        $this->regenerate();
        $_SESSION['user'] = $this->_id;
        $_SESSION['agent'] = $_SERVER['HTTP_USER_AGENT'];
        wp_logout();
        wp_set_auth_cookie($this->_user_id);
    }

    public function logon($login, $password)
    {
        if ($login == '' || $password == '') return false;

        $the_query = new WP_Query(
            array(
                'post_status' => 'publish',
                'post_type' => USER_CLASS,
                'meta_query' => array(
                    'relation' => 'AND',
                    array(
                        'key' => 'sf_email',
                        'value' => $login,
                        'compare' => '=',
                    ),
                ),
            )
        );
        if ($the_query->post_count == 1 && get_post_meta($the_query->posts[0]->ID, 'sf_password_hash', true) == create_hash_password($password, get_post_meta($the_query->posts[0]->ID, 'sf_salt', true))) {
            $this->_id = $the_query->posts[0]->ID;
            $this->regenerateSession();
            return true;
        }
        return false;


    }

    public static function checkNotActivatedAccount($login, $password){

	    if ($login == '' || $password == '') return false;

	    $the_query = new WP_Query(
		    array(
			    'post_status' => 'draft',
			    'post_type' => USER_CLASS,
			    'meta_query' => array(
				    'relation' => 'AND',
				    array(
					    'key' => 'sf_email',
					    'value' => $login,
					    'compare' => '=',
				    ),
			    ),
		    )
	    );
	    if ($the_query->post_count == 1 && get_post_meta($the_query->posts[0]->ID, 'sf_password_hash', true) == create_hash_password($password, get_post_meta($the_query->posts[0]->ID, 'sf_salt', true))) {
		    return true;
	    }
	    return false;
    }

    public function logon_no_password($login)
    {
        $the_query = new WP_Query(
            array(
                'post_status' => 'publish',
                'post_type' => USER_CLASS,
                'meta_query' => array(
                    'relation' => 'AND',
                    array(
                        'key' => 'sf_email',
                        'value' => $login,
                        'compare' => '=',
                    ),
                ),
            )
        );
        if ($the_query->post_count == 1) {
            $this->_id = $the_query->posts[0]->ID;
            $this->regenerateSession();
            return true;
        }
        return false;
    }


    public function logout()
    {
        unset ($_SESSION['user']);
        unset ($_SESSION['agent']);
        wp_logout();
    }

    public function getName()
    {
      return get_post_meta($this->getId(),'sf_name',true) . ' ' .get_post_meta($this->getId(),'sf_surname',true) ;
    }

  public function hasSurvey()
  {
    return !empty(getUserSurvey($this->getId()));
  }

  public function hasActiveDiet()
  {
    return !empty(getCurrentActiveDiet($this->getId()));
  }
  public function hasFutureDiet()
  {
    return !empty(getFutureDiet($this->getId()));
  }

  public function hasActiveSub()
  {
    return getCurrentUserPlan($this->getId())!==0;
  }
  public function hasActiveSubPremium()
  {
    return canSendDocMsg($this->getId());
  }

  public function getUserName()
  {
    return get_post_meta($this->getId(),'sf_name',true);
  }
  public function getUserSurname()
  {
    return get_post_meta($this->getId(),'sf_surname',true);
  }
  public function getUserStreet()
  {
    return get_post_meta($this->getId(),'sf_street',true);
  }
  public function getUserCity()
  {
    return get_post_meta($this->getId(),'sf_place',true);
  }
  public function getUserZipCode()
  {
    return get_post_meta($this->getId(),'sf_zip_code',true);
  }
  public function getUserInvoice()
  {
    return get_post_meta($this->getId(),'sf_have_invoice',true);
  }
  public function getDoc()
  {
    return get_post_meta($this->getId(),'sf_doc_diet',true);
  }
  public function hasEndedDiets()
  {
    $diets = getEndedDiets(get_post($this->getId()));
    return !empty($diets);
  }

  public function getUserNameVAT(){
	  return get_post_meta($this->getId(),'sf_name_invoice',true);
  }
  public function getUserSurnameVAT(){
	  return get_post_meta($this->getId(),'sf_surname_invoice',true);
  }
	public function getUserPhone(){
		return get_post_meta($this->getId(),'sf_phone',true);
	}
	public function getUserNipCode(){
		return get_post_meta($this->getId(),'sf_nip',true);
	}

	public function getUnreadMsgCount(){
    return count(getUnreadMsg($this->getId()));
  }
  public function getUserLastActiveDay()
  {
    return getUserLastActiveDay($this->getId());
  }
}
function user_page($premiumOnly = false, $payedOnly = false)
{ 
	$user_fiberFox = new user;
	if (!$user_fiberFox->isActive()) {
	  wp_redirect(get_the_permalink(get_id_after_template_filename('konto-logowanie.php')).'?redirect='.get_the_permalink());
	  die();
  }
  elseif($premiumOnly && !$user_fiberFox->hasActiveSubPremium())
  {
    //dunno tbh, maybe create special site with error ?
	  wp_redirect(home_url('/'));
	  die();
  }
  elseif($payedOnly && !$user_fiberFox->hasActiveSub())
  {
	  wp_redirect(get_the_permalink(get_id_after_template_filename('cennik.php')));
	  die();
  }
}

function login_user()
{
        $user_fiberFox = new user;
        $user_fiberFox->logon($_POST['user_email'], $_POST['user_pass']);
        if ($user_fiberFox->isActive()) {
            if($_GET['redirect']=='')
            {
                wp_redirect(get_the_permalink(get_id_after_template_filename('konto-komunikaty.php')), 302);
            }
            else
                wp_redirect($_GET['redirect'], 302);
            die();
        } else
            return user::checkNotActivatedAccount($_POST['user_email'], $_POST['user_pass']);

}
$activated_account = NULL;
function active_account()
{
    if (isset($_GET['token']) && $_GET['token'] != '') {
        $token = $_GET['token'];
        global $wpdb,$activated_account;
        $id = $wpdb->get_var($wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_key='sf_token' AND meta_value=%s", array($token)));
        if ($id != '' && get_post_type($id) == USER_CLASS) {
          $activated_account = true;
            delete_post_meta($id, 'sf_token'); // remove token
            wp_update_post(
                array(
                    'ID' => $id,
                    'post_status' => 'publish',
                )
            ); // activate token

           $user = new user();
           $user->logon_no_password(get_post_meta($id,'sf_email',true));
           wp_redirect(home_url('/moje-konto/'));
           exit();
        }
        else
        {
          $activated_account = false;
        }
    }

}

function logout_account()
{
    if (isset($_GET['logout']) && $_GET['logout'] == 'true') {
        $user = new user();
        $user->logout();
        wp_redirect(home_url('/'), 301);
        exit;
    }

}
add_action('wp_loaded', 'active_account');
add_action('wp_loaded', 'logout_account');

function create_hash_password($var, $salt)
{
    return hash('sha512', $salt . $var);
}

function handle_register($cleanPOST = true)
{
    $titan = TitanFramework::getInstance( 'revita' );
    $result = array('result' => null, 'text' => null);

    if ((isset($_POST['post_nonce_field'])
        && wp_verify_nonce($_POST['post_nonce_field'], 'post_nonce'))
    ) {
        if (!is_email($_POST['user_email'])) { //niepoprawny email
            $result['result'] = 'error';
            $result['text'] = $titan->getOption('register-wrong-email');
        } elseif (checkIfThereIsUserWithThisEmail($_POST['user_email'])) //taki email istnieje
        {
            $result['result'] = 'error';
            $result['text'] = $titan->getOption('register-taken-email');
        } elseif ($_POST['user_password_1'] !== $_POST['user_password_2'])//hasla sa rozne
        {
            $result['result'] = 'error';
            $result['text'] = $titan->getOption('register-diff-passwords');
        } elseif ($_POST['user_password_1'] == '' || strlen($_POST['user_password_1']) < 5) {
            $result['result'] = 'error';
            $result['text'] = $titan->getOption('register-short-password');
        } elseif (esc_html($_POST['user_name']) == '') {
            $result['result'] = 'error';
            $result['text'] = $titan->getOption('register-empty-name');
        } elseif (esc_html($_POST['user_surname']) == '') {
            $result['result'] = 'error';
            $result['text'] = $titan->getOption('register-empty-surname');
        }
        else //stworz konto
        {
            $email = $_POST['user_email'];
            $name = esc_html($_POST['user_name']);
            $surname = esc_html($_POST['user_surname']);
            $token = generateRandomString();
            $user_id = wp_create_user($email,'',$email);
            if(is_wp_error($user_id))
              $user_id = 0;
            wp_update_user(array(
                    'ID' => $user_id,
                    'display_name' => ($name. ' ' . $surname),
            ));

            $salt = uniqid(mt_rand(), true);
            update_user_meta($user_id,'first_name',$name);
            update_user_meta($user_id,'last_name',$surname);
            update_user_meta($user_id,'nickname',$name. ' ' . $surname);
            $password_hash = create_hash_password($_POST['user_password_1'], $salt);
            $array_create_post = array(
                'post_title' =>  ' ' . $email,
                'post_type' => USER_CLASS,
                'meta_input' => array(
                    'sf_name' => $name,
                    'sf_surname' => $surname,
                    'sf_email' => $email,
                    'sf_password_hash' => $password_hash,
                    'sf_token' => $token,
                    'sf_salt' => $salt,
                    'sf_user' => $user_id
                )
            );
            if($cleanPOST)
            $_POST = array();
            $post_id=wp_insert_post($array_create_post);
	          fixUserToUserRelation($post_id);
            $link = home_url('/') . '?token=' . $token;
            $body = $titan->getOption('register-activation-email');
            $title = $titan->getOption('register-activation-email-title');

            register_shutdown_function(function() use($email,$body,$link,$title){
              @fastcgi_finish_request();
	            ignore_user_abort(true);
	            revitamed_send_mail($email, $body.'<br><a href="' . $link . '">' . $link . '</a>', $title);
            });
//            revitamed_send_mail($email, $body.'<br><a href="' . $link . '">' . $link . '</a>', $title);
            $result['result'] = 'success';
            $result['id'] = $user_id;
            $result['post_id'] = $post_id;
        }

    }
    return $result;

}

function viewEmailActivation()
{
    $titan = TitanFramework::getInstance( 'revita' );
    $link = home_url('/') . '?token=' . 'TOKEN_AKTYWACYJNY';
    return "{$titan->getOption('register-activation-email')}<br><a href=\"$link\">$link</a>";
}
function handle_update($userID)
{

}



function reset_password_for_user()
{
    $titan = TitanFramework::getInstance( 'revita' );
    $result = array('result' => null, 'text' => null);
    $email = $_POST['user_email'];



        if (!is_email($email)) {
            $result['result'] = 'error';
            $result['text'] = $titan->getOption('register-wrong-email');
        } elseif (checkIfThereIsUserWithThisEmail($email, false) != 1) {
            $result['result'] = 'error';
            $result['text'] =  $titan->getOption('forgot-password-no-email');
        } elseif(get_post_status(getUserIDafterEmail($email))!='publish')
        {
            $result['result'] = 'error';
            $result['text'] =  $titan->getOption('forgot-password-not-active');
            $token = get_post_meta(getUserIDafterEmail($email),'sf_token',true);
            $link = home_url('/') . '?token=' . $token;
            $body = $titan->getOption('register-activation-email');
            $title = $titan->getOption('register-activation-email-title');
            if($token!='')
            revitamed_send_mail($email, $body.'<br><a href="' . $link . '">' . $link . '</a>', $title);
        }
        else{
            $result['result'] = 'success';
            $result['text'] = $titan->getOption('forgot-password-success');
            $id_user = getUserIDafterEmail($email);

            $new_password = generateRandomString(10);
            update_post_meta($id_user, 'sf_password_hash', create_hash_password($new_password, get_post_meta($id_user, 'sf_salt', true)));

            $body = ($titan->getOption('forgot-password-email-content') ).' "' . $new_password . '"';
            $title = $titan->getOption('forgot-password-email-title');
            revitamed_send_mail($email, $body, $title);
        }


    return $result;

}
function viewForgottenPassword()
{
    $titan = TitanFramework::getInstance( 'revita' );
    return $body = ($titan->getOption('forgot-password-email-content')) . ' "NOWE HASŁO"' . ($titan->getOption('forgot-password-email-content-2'));
}

function getUserName($userID)
{
	return (get_post_type($userID)=='user_client') ?  get_post_meta($userID,'sf_name',true) . ' ' . get_post_meta($userID,'sf_surname',true) :  '';
}

/**
 * @param $userID
 *
 * @return null|WP_post
 */
function getLastUserMeasure($userID)
{
  $measures = getUserMeasures($userID);
  return (!empty($measures)) ? $measures[0] : null;
}
function getUserWeigh($userID)
{
  $lastMeasure = getLastUserMeasure($userID);
  return ($lastMeasure===null) ? null : get_post_meta($lastMeasure->ID,'sf_weight',true);
}
function getUserHeight($userID)
{
 return get_post_meta($userID,'sf_height',true);
}

/**
 * @param $email
 * @param bool $returnBoolean
 * @return bool|int
 */
function checkIfThereIsUserWithThisEmail($email,$returnBoolean=true)
{

    $args        = array(
        'post_type'   => USER_CLASS,
        'post_status' => array('publish','draft'),
        'nopaging'    => true,
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key'     => 'sf_email',
                'value'   => $email,
                'compare' => '='
            )
        )
    );
    $user_list_count = count(get_posts( $args ));
    if($returnBoolean)
    {
        if($user_list_count==0) return false;
        else return true;
    }
    else
        return $user_list_count;

}

/**
 * @param $email string
 * @return int
 */
function getUserIDafterEmail($email)
{
    if(!is_email($email)) return 0;

    $args        = array(
        'post_type'   => USER_CLASS,
        'post_status' => array('publish','draft'),
        'nopaging'    => true,
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key'     => 'sf_email',
                'value'   => $email,
                'compare' => '='
            )
        )
    );
    $posts = get_posts($args);
    if(count($posts)==0) return 0;
    else return $posts[0]->ID;
}


class ActionHookEditPassword {
    public function __construct(){
        add_action( 'save_post', array( $this, 'save_post' ) );
        add_action( 'admin_notices', array( $this, 'admin_notices' ) );
    }

    public function save_post( $post_id ) {

        if ( USER_CLASS == get_post_type($post_id)) {

            if ( get_post_meta( $post_id, 'sf_password_admin_create', true ) != '' ) {
                $password = get_post_meta( $post_id, 'sf_password_admin_create', true );
                update_post_meta( $post_id, 'sf_password_admin_create', '' );
                update_post_meta( $post_id, 'sf_password_hash', create_hash_password( $password,get_post_meta($post_id,'sf_salt',true) ) );
                add_filter( 'redirect_post_location', array( $this, 'add_notice_query_var' ), 99 );
            }
        }

    }

    public function add_notice_query_var( $location ) {
        remove_filter( 'redirect_post_location', array( $this, 'add_notice_query_var' ), 99 );
        return add_query_arg( array( 'password_create' => 'true' ), $location );
    }

    public function admin_notices() {
        if ( ! isset( $_GET['password_create']) || $_GET['password_create']!='true' ) {
            return;
        }
        ?>
        <div class="updated">
            <p><?php _e( 'Hasło zostało zmienione' , 'revita' ); ?></p>
        </div>
        <?php
    }
}
$abc = new ActionHookEditPassword();

function register_my_session() {
    if ( ! session_id() ) {
        session_start();
    }
}
add_action( 'init', 'register_my_session' );

class ActionHookSaveAccount
{
  public function __construct()
  {
    add_action('save_post', array($this, 'save_post'));
  }

  public function save_post($post_id)
  {

    global $doYouLikeRecursion;

    if ($doYouLikeRecursion === true)
      return;
    $doYouLikeRecursion = true;

    $post_type = get_post_type($post_id);
    if (USER_CLASS == $post_type) {
      $this->setName($post_id);
    }

    $doYouLikeRecursion = NULL;
  }

  public function setName($postID)
  {
    // name surname email
    $name = get_post_meta($postID,'sf_name',true);
    $surname =  get_post_meta($postID,'sf_surname',true);
    $email =  get_post_meta($postID,'sf_email',true);
    $title = "$name $surname $email";
    wp_update_post(
      array(
        'ID' => $postID,
        'post_title' => $title,
        'post_name' => sanitize_title($title)
      )
    );
  }
}
new ActionHookSaveAccount();