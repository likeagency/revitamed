<?php

function add_image_from_user( $file, $userID, $section ) {

	require_once( ABSPATH . "wp-admin" . '/includes/image.php' );
	require_once( ABSPATH . "wp-admin" . '/includes/file.php' );

	$prefix = 'sf_';
	if ( ( $file['error'] != 4 && $file['error'] != 0 ) || $file['size'] > 2 * 1024 * 1024 ) {

		if ( $file['error'] == 1 || $file['error'] == 2 || $file['size'] > 2 * 1024 * 1024 ) {
			$result = array(
				'error' => true,
				'msg'   => 'Plik był zbyt duży, spróbuj mniejszy rozmiar',
			);
		} else {
			$result = array(
				'error' => true,
				'msg'   => 'Błąd serwera nr.' . $file['error'] . ', spróbuj skontaktować się administracją strony.',
				'code'  => $file['error'],
			);
		}
	} elseif ( $file['error'] != 4 ) {

		$arr_file_type      = wp_check_filetype( basename( $file['name'] ) );
		$uploaded_file_type = $arr_file_type['type'];
		$allowed_file_types = array(
			'image/jpg',
			'image/jpeg',
			'image/gif',
			'image/png',
			'application/pdf',
			'text/plain',
			'application/msword',
			'application/vnd.oasis.opendocument.text'
		);
		if ( in_array( $uploaded_file_type, $allowed_file_types ) && is_uploaded_file($file['tmp_name']) ) {

			$extension= '.file';
			if($uploaded_file_type=='image/jpg' || $uploaded_file_type=='image/jpeg')
				$extension = '.jpg';
			elseif($uploaded_file_type=='image/gif')
				$extension = '.gif';
			elseif($uploaded_file_type=='image/png')
				$extension = '.png';
			elseif($uploaded_file_type=='application/pdf')
				$extension = '.pdf';
			elseif($uploaded_file_type=='text/plain')
				$extension = '.txt';
			elseif($uploaded_file_type=='application/msword')
				$extension = '.docx';
			elseif($uploaded_file_type=='application/vnd.oasis.opendocument.text')
				$extension = '.odt';

			$uri           = 'wp-content/uploads/badania/' . md5( generateRandomString( 10 ) ). generateRandomString( 10 ) . $extension;
			$newFile       = ABSPATH . $uri;
			$move_new_file = @move_uploaded_file( $file['tmp_name'], $newFile );
			if ( $move_new_file ) {

				$nameFile = esc_html( $file['name'] );


				$post_args = array(
					'post_title'   => $nameFile,
					'post_content' => '$'.$userID .'$',
					'post_status'  => 'publish',
					'post_type'    => 'custom_file',
					'meta_input'   => array(
						"{$prefix}user"     => $userID,
						"{$prefix}name"     => $nameFile,
						"{$prefix}path_var" => $newFile,
						"{$prefix}path_url" => $uri,
						"{$prefix}section"  => $section,
						"{$prefix}size"  => $file['size'],
					)
				);
				$attach_id = wp_insert_post( $post_args );
				$result    = array(
					'error' => false,
					'ID'    => $attach_id,
					'name' => $nameFile,
				);
			} else {
				$result = array(
					'error' => true,
					'msg'   => 'Błąd serwera nr.131, spróbuj skontaktować się administracją strony.'
				);
			}
		} else {
			$result = array(
				'error' => true,
				'msg'   => 'Wysyłany plik posiada niedozwolone rozszerzenie'
			);
		}


	} else {
		$result = array(
			'error' => null,
			'msg'   => 'Brak zdjęcia'
		);
	}


	return $result;
}

function deleteFilesWithPost($filePostID)
{
	if(get_post_type($filePostID)=='custom_file')
	{
		$filePath = get_post_meta($filePostID,'sf_path_var',true);
		unlink($filePath);
	}
}
add_action('before_delete_post','deleteFilesWithPost');


/**
 * @param $file string of file path
 *
 * @return bool
 */
function isFileImage($file)
{
	$arr_file_type      = wp_check_filetype( basename( $file ) );
	$uploaded_file_type = $arr_file_type['type'];
	$allowed_file_types = array(
		'image/jpg',
		'image/jpeg',
		'image/gif',
		'image/png',
	);

	return in_array( $uploaded_file_type, $allowed_file_types );
}