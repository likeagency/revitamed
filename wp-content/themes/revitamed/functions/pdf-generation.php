<?php

function makePDFForRequestedRecipe()
{

    require_once 'libs/mdpf2/mpdf.php';
    $id = $_GET['id'];
    $width_text = '750px';
    $width_separator = '50px';
    $width_ing = '200px';
    $difficultness_translate = array(
        '1' => __( 'Łatwy', 'revitamed' ),
        '2' => __( 'Średni', 'revitamed' ),
        '3' => __( 'Trudny', 'revitamed' ),
    );
    $difficultness = $difficultness_translate[get_post_meta($id,'rm_trudnosc',true)];
    $time =get_post_meta($id,'rm_czas_przyg',true);
    $rows = get_the_terms( $id, 'rodzaj' );
    $row = $rows[0];
    $category = $row->name;
    $content_post = get_post_meta($id,'rm_opis',true);
    if(get_post_type($id)!= 'przepisy')
    {
        status_header( 404 );
        nocache_headers();
        include( get_query_template( '404' ) );
        die();
    }



    $content = '<style>'.file_get_contents(TEMP_VAR.'/css/pdf.css').'</style>';

    $content .= sprintf('<h1>%s</h1>',get_the_title($id));

    $content .= '<div style="">';
    if(!empty($category))
    $content .= sprintf('<img src="%s/images/ico-cat.png"><b>Kategoria:</b> %s<br>',TEMP_VAR,$category);
    if(!empty($time))
    $content .= sprintf('<img src="%s/images/ico-clock.png"><b>Czas przygotowania:</b> %s<br>',TEMP_VAR,$time);
    if(!empty($difficultness))
    $content .= sprintf('<img src="%s/images/ico-level.png"><b>Poziom trudności:</b> %s',TEMP_VAR,$difficultness);
    $content .= '</div>';

    if(has_post_thumbnail($id))
    $content .= sprintf('<table  cellpadding="0px" cellspacing="0px" width="1000px">
<tbody><tr><td align="center"><img src="%s"></td></tr></tbody></table><br>',get_the_post_thumbnail_url($id,''));

    $content .= '<table  cellpadding="0px" cellspacing="0px" width="1000px"><tbody><tr>';

    $ings = get_post_meta($id, 'rm_ing', true);
    if (is_array($ings))
        usort($ings, function ($a, $b) {
            return ($a['main'] == $b['main']) ? get_post($a['ing'])->menu_order  > get_post($b['ing'])->menu_order : $a['main'] < $b['main'];
        });


    //składniki
    $content.= sprintf('<td width="200px" style="/*background-color: #F3F3F3;*/"><h2>%s</h2><ul>',(empty($ings) ? '' : 'Składniki' ));
    foreach($ings as $ing)
    {

        $ing_text = get_the_title($ing['ing']) . ' ';
	    $ing_text .=  ($ing['amount_show']!='' && getUnitByIng($ing['ing'])!='') ? (formatNumberToPolish($ing['amount_show'],null) . ' ' . mb_strtolower(get_unit_name(getUnitByIng($ing['ing']),$ing['amount_show']))) : (formatNumberToPolish($ing['amount'],null) . ' g' );
        $content.= sprintf('<li>%s</li>',$ing_text);
    }

    $content.= '</ul></td>';

    //separator
    $content.= sprintf('<td width="50px"></td>',$width_separator);

    //tekst
    $content.= sprintf('<td width="750px">'.$content_post,$width_text);
    $content.= '</td>';

    $content .= '</tr></tbody></table>';


    $mpdf = new mPDF();
    $mpdf->WriteHTML(trim($content));
    $mpdf->Output('Przepis - '. get_the_title($id).'.pdf','D');
    exit;
}
add_action( 'wp_ajax_nopriv_recipe_pdf', 'makePDFForRequestedRecipe' );
add_action( 'wp_ajax_recipe_pdf', 'makePDFForRequestedRecipe' );