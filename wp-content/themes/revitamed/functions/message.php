<?php
function send_private_msg($receiver = null , $content ,$dieter = false,$replyTo = 0)
{
	if($dieter)
		$user = new userDoc();
	else
		$user = new user;

	$idSender = $user->getId();



	if( !$user->isActive() )
	{
		return array(
			'success' => false,
			'msg' => 'Nie jesteś aktywnym użytkownikiem'
		);
	}


	if( get_class($user) == 'user' &&  !$user->hasActiveSubPremium())
	{
		return array(
			'success' => false,
			'msg' => 'Nie możesz wysyłać wiadomości, nie masz wykupionego odpowiedniego pakietu'
		);
	}


	$title = '';
	$content = nl2br(esc_html($content));

	$subject = 'Nowa wiadomość na revitadiet';
	$content_msg = 'Otrzymałeś wiadomość na revitadiet';


	//receiver
		$data = array(
			'post_type' => 'msg',
			'post_content' => $content,
			'post_status' => 'publish',
			'post_title' => $title,
			'meta_input' => array(
				'sf_content' => $content,
				'sf_receiver' => $receiver,
				'sf_sender' => $idSender,
				'sf_owner' => $receiver,
				'sf_seen' => false,
				'sf_reply' => getSisterMsgID($replyTo)
			),
		);

		(wp_insert_post($data));

		//sendera
		$data = array(
			'post_type' => 'msg',
			'post_content' => $content,
			'post_status' => 'publish',
			'post_title' => $title,
			'meta_input' => array(
				'sf_content' => $content,
				'sf_receiver' => $receiver,
				'sf_sender' => $idSender,
				'sf_owner' => $idSender,
				'sf_seen' => false,
				'sf_reply' => $replyTo
			),
		);

		 wp_insert_post($data);

		 $recEmail = get_post_meta($receiver,'sf_email',true);
			revitamed_send_mail($recEmail,$content_msg,$subject);

		 if(get_post_type($receiver) === 'doc')
		 {
	        $docEmail = getUserDocEmail($receiver);
	        revitamed_send_mail($docEmail,$content_msg,$subject);
		 }

	return array(
		'success' => true,
		'msg' => '',
	);
}


function getMsgUser($idSender)
{
	$array = [
		'post_type' => 'msg',
		'post_status' => 'publish',
		'nopaging' => true,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'sf_sender',
				'value' => $idSender,
				'compare' => '='
			),
			array(
				'key' => 'sf_reply',
				'value' => 0,
				'compare' => '='
			),
			array(
				'key' => 'sf_owner',
				'value' => $idSender,
				'compare' => '='
			),
		)
	];
	return (get_posts($array));
}


function getMsgUserDieter($idSender)
{
	$array = [
		'post_type' => 'msg',
		'post_status' => 'publish',
		'nopaging' => true,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'sf_receiver',
				'value' => $idSender,
				'compare' => (is_array($idSender)) ? 'IN' : '='
			),
			array(
				'key' => 'sf_reply',
				'value' => 0,
				'compare' => '='
			),
			array(
				'key' => 'sf_owner',
				'value' => $idSender,
				'compare' => (is_array($idSender)) ? 'IN' : '='
			),
		)
	];


	return (get_posts($array));
}

/**
 * @param $msgs array
 *
 * @return array
 */
function getAllUsersMessage($msgs){
	$posts = [];

	foreach($msgs as $msg)
	{
		$sender= get_post_meta($msg->ID,'sf_sender',true);
		if(!empty($sender))
		$posts[$sender]=  get_post($sender);

	}
	return $posts;
}
function getAllMsg()
{
	$args = array(
		'post_type' => 'msg',
		'nopaging'    => true,
		'post_status' => 'publish',

	);
	return get_posts($args);
}


/**
 * @param $idUser int
 * @param $idDocs int|array
 *
 * @return bool
 */
function respondedToMSG($idUser,$idDocs){

	if(is_array($idDocs) && count($idDocs) == 1)
		$idDocs = $idDocs[0];

	$array = [
		'post_type' => 'msg',
		'post_status' => 'publish',
		'posts_per_page' => 1,
		'orderby' => 'date',
		'order' => 'DESC',
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'relation' => 'OR',
				array(
					'key' => 'sf_receiver',
					'value' => $idDocs,
					'compare' => (is_array($idDocs)) ? 'IN' : '='
				),
				array(
					'key' => 'sf_receiver',
					'value' => $idUser,
					'compare' =>  '='
				),
			),
			array(
				'relation' => 'OR',
				array(
					'key' => 'sf_sender',
					'value' => $idDocs,
					'compare' => (is_array($idDocs)) ? 'IN' : '='
				),
				array(
					'key' => 'sf_sender',
					'value' => $idUser,
					'compare' =>  '='
				),
			),

			array(
				'key' => 'sf_owner',
				'value' => $idDocs,
				'compare' => (is_array($idDocs)) ? 'IN' : '='
			),
		)
	];

	$posts =  (get_posts($array));

	return (!empty($posts) && get_post_meta($posts[0]->ID,'sf_sender',true)==$idUser);
}

function getRepliesMsg($idMsg,$idSender)
{

	$array = [
		'post_type' => 'msg',
		'post_status' => 'publish',
		'nopaging' => true,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'sf_reply',
				'value' => $idMsg,
				'compare' => '='
			),
			array(
				'key' => 'sf_owner',
				'value' => $idSender,
				'compare' => (is_array($idSender)) ? 'IN' : '='
			),
		)
	];
	//print_r_e((get_posts($array)););
	return (get_posts($array));
}

function getUnreadMsg($idSender){

	$array = [
		'post_type' => 'msg',
		'post_status' => 'publish',
		'nopaging' => true,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'sf_sender',
				'value' => $idSender,
				'compare' => '!='
			),
			array(
				'key' => 'sf_owner',
				'value' => $idSender,
				'compare' => '='
			),
			array(
				'key' => 'sf_read',
				'compare' => 'NOT EXISTS'
			),
		)
	];


	return get_posts($array);
}

/**
 * @param $idSender array|int
 *
 * @return array
 */
function getMsgUserDieterUnread($idSender)
{
	$array = [
		'post_type' => 'msg',
		'post_status' => 'publish',
		'nopaging' => true,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'sf_receiver',
				'value' => $idSender,
				'compare' => (is_array($idSender)) ? 'IN' : '='
			),
			array(
				'key' => 'sf_read',
				'compare' => 'NOT EXISTS'
			),
			array(
				'key' => 'sf_owner',
				'value' => $idSender,
				'compare' => (is_array($idSender)) ? 'IN' : '='
			),
		)
	];


	return (get_posts($array));
}

function getSisterMsgID($id)
{
	$idowner = get_post_meta($id,'sf_owner',true);
	$idreceiver = get_post_meta($id,'sf_receiver',true);
	$idSender = get_post_meta($id,'sf_sender',true);

	$array = [
		'post_type' => 'msg',
		'post_status' => 'publish',
		'nopaging' => true,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key' => 'sf_owner',
				'value' => $idowner,
				'compare' => '!='
			),
			array(
				'key' => 'sf_receiver',
				'value' => $idreceiver,
				'compare' => '='
			),
			array(
				'key' => 'sf_sender',
				'value' => $idSender,
				'compare' => '='
			),
		)
	];
	$posts = (get_posts($array));;
	return (is_object($posts[0])) ? $posts[0]->ID : 0;
}
