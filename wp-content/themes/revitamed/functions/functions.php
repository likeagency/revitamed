<?php
function get_id_by_slug( $page_slug, $slug_page_type = 'page' ) {
    $find_page = get_page_by_path( $page_slug, OBJECT, $slug_page_type );
    if ( $find_page ) {
        return $find_page->ID;
    } else {
        return null;
    }
}

function set_html_content_type()
{
    return 'text/html';
}

function generateRandomString($length = 30)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGIJKLMNOPRSTUWYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[random_int(0, $charactersLength - 1)];
    }

    return $randomString;
}
function getTemplateEmail($content)
{

  $text = file_get_contents(get_template_directory().'/functions/template.html');
  return str_replace(array(
    '%s%',
    '%content%'
  ),array(
    home_url(),
    $content
  ),$text);
}
function revitamed_send_mail( $to, $text, $title, $useTemplate = true ) {
    add_filter( 'wp_mail_content_type', 'set_html_content_type' );


    $content = ($useTemplate) ?  getTemplateEmail($text) : $text;

    $sent           = wp_mail( $to, $title, $content );
    remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

    return $sent;
}

function get_id_after_template_filename($filename)
{
    global $wpdb;
    return $wpdb->get_var("SELECT wp_pm.post_id FROM {$wpdb->posts} wp_p JOIN {$wpdb->postmeta} wp_pm ON wp_p.ID=wp_pm.post_id WHERE wp_p.post_type='page' AND wp_p.post_status='publish' AND meta_key='_wp_page_template' AND meta_value='$filename' ");
}

function get_permalink_template($filename){
  return get_the_permalink(get_id_after_template_filename($filename));
}
function print_r_e($var)
{
    echo '<pre>';
    echo htmlspecialchars(print_r($var,true));
    echo '</pre>';
}

function format_item_protokol($id)
{
  return get_post_type($id)=='skladnik' ? '<a>'.get_the_title($id).'</a>' : '';
}

function saveDataFromFormToSession()
{
   $userRevita = new user;

    if(empty($_SESSION['individualDiagnostic'])) {

      if($userRevita->isActive())
      {
         $survey = get_post_meta($userRevita->getId(),'sf_session_storage',true);
         $_SESSION['individualDiagnostic'] = is_array($survey) ? $survey : [];

         if(get_post_meta($userRevita->getId(),'sf_age',true)!='')
        $_SESSION['individualDiagnostic']['sf_age'] = get_post_meta($userRevita->getId(),'sf_age',true);
          if(get_post_meta($userRevita->getId(),'sf_height',true)!='')
        $_SESSION['individualDiagnostic']['sf_height'] =  get_post_meta($userRevita->getId(),'sf_height',true);

      }
    }

    if($_POST['action']=='ankieta_1')
    {

        $_SESSION['individualDiagnostic']['sf_age']=$_POST['user_age'];
        $_SESSION['individualDiagnostic']['sf_sex']=$_POST['user_sex'];
        $_SESSION['individualDiagnostic']['sf_weight']=$_POST['user_weight'];
        $_SESSION['individualDiagnostic']['sf_height']=$_POST['user_height'];
        $_SESSION['individualDiagnostic']['sf_circuit_1']=$_POST['user_circuit_1'];
        $_SESSION['individualDiagnostic']['sf_circuit_2']=$_POST['user_circuit_2'];
        $_SESSION['individualDiagnostic']['sf_circuit_3']=$_POST['user_circuit_3'];
        $_SESSION['individualDiagnostic']['sf_circuit_4']=$_POST['user_circuit_4'];


        $_SESSION['individualDiagnostic']['sf_nice_weight']=$_POST['user_nice_weight'];
        $_SESSION['individualDiagnostic']['sf_highest_weight']=$_POST['user_highest_weight'];
        $_SESSION['individualDiagnostic']['sf_pressure']=$_POST['user_pressure'];




        $_SESSION['individualDiagnostic']['sf_purpose']=$_POST['user_purpose'];
        $_SESSION['individualDiagnostic']['sf_user_other_purposes']=$_POST['user_other_purposes'];
        $_SESSION['individualDiagnostic']['sf_operation_yes_no']=$_POST['user_operation_yes_no'];
        $_SESSION['individualDiagnostic']['sf_operation_ww']=$_POST['user_operation_ww'];
        $_SESSION['individualDiagnostic']['sf_disease_yes_no']=$_POST['user_disease_yes_no'];
        $_SESSION['individualDiagnostic']['sf_disease_w']=$_POST['user_disease_w'];

        $_SESSION['individualDiagnostic']['sf_medicines_yes_no']=$_POST['user_medicines_yes_no'];
        $_SESSION['individualDiagnostic']['sf_medicines_w']=$_POST['user_medicines_w'];
        $_SESSION['individualDiagnostic']['sf_medicines_season']=$_POST['user_medicines_season'];
        $_SESSION['individualDiagnostic']['sf_medicines_season_w']=$_POST['user_medicines_season_w'];
        $_SESSION['individualDiagnostic']['sf_medicines_chronic']=$_POST['user_medicines_chronic'];
        $_SESSION['individualDiagnostic']['sf_medicines_chronic_w']=$_POST['user_medicines_chronic_w'];

        $_SESSION['individualDiagnostic']['part_1'] = true;
    }

    if($_POST['action']=='ankieta_2')
    {
      $_SESSION['individualDiagnostic']['sf_1_problem_0'] = $_POST['user_1_problem_0'];



      $_SESSION['individualDiagnostic']['sf_2_problem_0'] = $_POST['user_2_problem_0'];
      $_SESSION['individualDiagnostic']['sf_2_problem_1'] = $_POST['user_2_problem_1'];
      $_SESSION['individualDiagnostic']['sf_2_problem_2'] = $_POST['user_2_problem_2'];
      $_SESSION['individualDiagnostic']['sf_2_problem_3'] = $_POST['user_2_problem_3'];
      $_SESSION['individualDiagnostic']['sf_2_problem_4'] = $_POST['user_2_problem_4'];
      $_SESSION['individualDiagnostic']['sf_2_problem_5'] = $_POST['user_2_problem_5'];
      $_SESSION['individualDiagnostic']['sf_2_problem_6'] = $_POST['user_2_problem_6'];
      $_SESSION['individualDiagnostic']['sf_2_problem_7'] = $_POST['user_2_problem_7'];
      $_SESSION['individualDiagnostic']['sf_2_problem_7_w'] = $_POST['user_2_problem_7_w'];


      $_SESSION['individualDiagnostic']['sf_3_problem_0'] = $_POST['user_3_problem_0'];
      if($_SESSION['individualDiagnostic']['sf_3_problem_0']==='1')
      {
         $_SESSION['individualDiagnostic']['sf_3_problem_1'] = $_POST['user_3_problem_1'];
         $_SESSION['individualDiagnostic']['sf_3_problem_2'] = $_POST['user_3_problem_2'];
         $_SESSION['individualDiagnostic']['sf_3_problem_3'] = $_POST['user_3_problem_3'];
      }
      else
      {
         $_SESSION['individualDiagnostic']['sf_3_problem_1'] = null;
         $_SESSION['individualDiagnostic']['sf_3_problem_2'] = null;
         $_SESSION['individualDiagnostic']['sf_3_problem_3'] = null;
      }


      $_SESSION['individualDiagnostic']['sf_4_problem_0'] = $_POST['user_4_problem_0'];
      $_SESSION['individualDiagnostic']['sf_4_problem_2'] = $_POST['user_4_problem_2'];

      $_SESSION['individualDiagnostic']['sf_4_problem_2_w'] = $_POST['user_4_problem_2_w'];


      $_SESSION['individualDiagnostic']['sf_5_problem_0'] = $_POST['user_5_problem_0'];
      $_SESSION['individualDiagnostic']['sf_5_problem_1'] = $_POST['user_5_problem_1'];
      $_SESSION['individualDiagnostic']['sf_5_problem_2'] = $_POST['user_5_problem_2'];
      $_SESSION['individualDiagnostic']['sf_5_problem_3'] = $_POST['user_5_problem_3'];
      $_SESSION['individualDiagnostic']['sf_5_problem_4'] = $_POST['user_5_problem_4'];
      $_SESSION['individualDiagnostic']['sf_5_problem_4_w'] = $_POST['user_5_problem_4_w'];

      $_SESSION['individualDiagnostic']['sf_6_problem_0'] = $_POST['user_6_problem_0'];
      $_SESSION['individualDiagnostic']['sf_6_problem_0a'] = $_POST['user_6_problem_0a'];
      $_SESSION['individualDiagnostic']['sf_6_problem_1'] = $_POST['user_6_problem_1'];
      $_SESSION['individualDiagnostic']['sf_6_problem_2'] = $_POST['user_6_problem_2'];
      $_SESSION['individualDiagnostic']['sf_6_problem_3'] = $_POST['user_6_problem_3'];
      $_SESSION['individualDiagnostic']['sf_6_problem_4'] = $_POST['user_6_problem_4'];
      $_SESSION['individualDiagnostic']['sf_6_problem_5'] = $_POST['user_6_problem_5'];
      $_SESSION['individualDiagnostic']['sf_6_problem_6'] = $_POST['user_6_problem_6'];
      $_SESSION['individualDiagnostic']['sf_6_problem_6_w'] = $_POST['user_6_problem_6_w'];
      $_SESSION['individualDiagnostic']['sf_6_problem_7'] = $_POST['user_6_problem_7'];
      $_SESSION['individualDiagnostic']['sf_6_problem_8'] = $_POST['user_6_problem_8'];

      $_SESSION['individualDiagnostic']['sf_7_problem_0'] = $_POST['user_7_problem_0'];

      $_SESSION['individualDiagnostic']['sf_7_problem_1']   = $_POST['user_7_problem_1'];
      if(!$_SESSION['individualDiagnostic']['sf_7_problem_1'])
      {
        $_POST['user_7_problem_1_s'] = null;
        $_POST['user_7_problem_1_w'] = null;
      }
      $_SESSION['individualDiagnostic']['sf_7_problem_1_s'] = $_POST['user_7_problem_1_s'];
      $_SESSION['individualDiagnostic']['sf_7_problem_1_w'] = $_POST['user_7_problem_1_w'];



      $_SESSION['individualDiagnostic']['sf_7_problem_2'] = $_POST['user_7_problem_2'];
      $_SESSION['individualDiagnostic']['sf_7_problem_3'] = $_POST['user_7_problem_3'];
      $_SESSION['individualDiagnostic']['sf_7_problem_4'] = $_POST['user_7_problem_4'];
      $_SESSION['individualDiagnostic']['sf_8_problem_0'] = $_POST['user_8_problem_0'];
      $_SESSION['individualDiagnostic']['sf_8_problem_1'] = $_POST['user_8_problem_1'];
      $_SESSION['individualDiagnostic']['sf_8_problem_2'] = $_POST['user_8_problem_2'];
      $_SESSION['individualDiagnostic']['sf_8_problem_3'] = $_POST['user_8_problem_3'];
      $_SESSION['individualDiagnostic']['sf_8_problem_4'] = $_POST['user_8_problem_4'];
      $_SESSION['individualDiagnostic']['sf_8_problem_5'] = $_POST['user_8_problem_5'];
      $_SESSION['individualDiagnostic']['sf_8_problem_6'] = $_POST['user_8_problem_6'];
      $_SESSION['individualDiagnostic']['sf_8_problem_7'] = $_POST['user_8_problem_7'];
      $_SESSION['individualDiagnostic']['sf_8_problem_8'] = $_POST['user_8_problem_8'];

      $_SESSION['individualDiagnostic']['sf_12_problem_0'] = $_POST['user_12_problem_0'];
      $_SESSION['individualDiagnostic']['sf_12_problem_3'] = $_POST['user_12_problem_3'];
      $_SESSION['individualDiagnostic']['sf_12_problem_3_w'] = $_POST['user_12_problem_3_w'];
      $_SESSION['individualDiagnostic']['sf_12_problem_1'] = $_POST['user_12_problem_1'];

      $_SESSION['individualDiagnostic']['sf_9_problem_1'] = $_POST['user_9_problem_1'];
      $_SESSION['individualDiagnostic']['sf_9_problem_2'] = $_POST['user_9_problem_2'];
      $_SESSION['individualDiagnostic']['sf_9_problem_3'] = $_POST['user_9_problem_3'];

      $_SESSION['individualDiagnostic']['sf_10_problem_0'] = $_POST['user_10_problem_0'];
      $_SESSION['individualDiagnostic']['sf_10_problem_1'] = $_POST['user_10_problem_1'];

      $_SESSION['individualDiagnostic']['part_2'] = true;
      redirectToCorrectStep(2);
    }

    if($_POST['action']=='ankieta_3')
    {
      $_SESSION['individualDiagnostic']['sf_13_problem_0']=$_POST['user_13_problem_0'];
      $_SESSION['individualDiagnostic']['sf_13_problem_0_t']=$_POST['user_13_problem_0_t'];
      $_SESSION['individualDiagnostic']['sf_13_problem_0_w']=$_POST['user_13_problem_0_w'];
      $_SESSION['individualDiagnostic']['sf_13_problem_1']=$_POST['user_13_problem_1'];
      $_SESSION['individualDiagnostic']['sf_13_problem_1_t']=$_POST['user_13_problem_1_t'];
      $_SESSION['individualDiagnostic']['sf_13_problem_1_w']=$_POST['user_13_problem_1_w'];
      $_SESSION['individualDiagnostic']['sf_13_problem_2']=$_POST['user_13_problem_2'];
      $_SESSION['individualDiagnostic']['sf_13_problem_2_t']=$_POST['user_13_problem_2_t'];
      $_SESSION['individualDiagnostic']['sf_13_problem_2_w']=$_POST['user_13_problem_2_w'];
      $_SESSION['individualDiagnostic']['sf_13_problem_4']=$_POST['user_13_problem_4'];
      $_SESSION['individualDiagnostic']['sf_13_problem_4_t']=$_POST['user_13_problem_4_t'];
      $_SESSION['individualDiagnostic']['sf_13_problem_4_w']=$_POST['user_13_problem_4_w'];


      $_SESSION['individualDiagnostic']['sf_14_problem_0']=$_POST['user_14_problem_0'];
      $_SESSION['individualDiagnostic']['sf_14_problem_0_t']=$_POST['user_14_problem_0_t'];
      $_SESSION['individualDiagnostic']['sf_14_problem_1']=$_POST['user_14_problem_1'];
      $_SESSION['individualDiagnostic']['sf_14_problem_1_t']=$_POST['user_14_problem_1_t'];
      $_SESSION['individualDiagnostic']['sf_14_problem_2']=$_POST['user_14_problem_2'];
      $_SESSION['individualDiagnostic']['sf_14_problem_2_t']=$_POST['user_14_problem_2_t'];
      $_SESSION['individualDiagnostic']['sf_14_problem_3']=$_POST['user_14_problem_3'];
      $_SESSION['individualDiagnostic']['sf_14_problem_3_t']=$_POST['user_14_problem_3_t'];
      $_SESSION['individualDiagnostic']['sf_14_problem_4']=$_POST['user_14_problem_4'];
      $_SESSION['individualDiagnostic']['sf_14_problem_4_t']=$_POST['user_14_problem_4_t'];

      $_SESSION['individualDiagnostic']['sf_14_problem_5']=$_POST['user_14_problem_5'];
      $_SESSION['individualDiagnostic']['sf_14_problem_5_t']=$_POST['user_14_problem_5_t'];

      $_SESSION['individualDiagnostic']['sf_15_problem_0']=$_POST['user_15_problem_0'];
      $_SESSION['individualDiagnostic']['sf_15_problem_1']=$_POST['user_15_problem_1'];
      $_SESSION['individualDiagnostic']['sf_15_problem_2']=$_POST['user_15_problem_2'];
      $_SESSION['individualDiagnostic']['sf_15_problem_3']=$_POST['user_15_problem_3'];

      $_SESSION['individualDiagnostic']['sf_15_problem_2_0']=$_POST['user_15_problem_2_0'];
      $_SESSION['individualDiagnostic']['sf_15_problem_2_1']=$_POST['user_15_problem_2_1'];
      $_SESSION['individualDiagnostic']['sf_15_problem_2_2']=$_POST['user_15_problem_2_2'];
      $_SESSION['individualDiagnostic']['sf_15_problem_2_3']=$_POST['user_15_problem_2_3'];

      $_SESSION['individualDiagnostic']['sf_16_problem_0s']=$_POST['user_16_problem_0s'];
      $_SESSION['individualDiagnostic']['sf_16_problem_0']=$_POST['user_16_problem_0'];
      $_SESSION['individualDiagnostic']['sf_16_problem_0_w']=$_POST['user_16_problem_0_w'];
      $_SESSION['individualDiagnostic']['sf_16_problem_1']=$_POST['user_16_problem_1'];
      $_SESSION['individualDiagnostic']['sf_16_problem_1_w']=$_POST['user_16_problem_1_w'];
      $_SESSION['individualDiagnostic']['sf_16_problem_2']=$_POST['user_16_problem_2'];
      $_SESSION['individualDiagnostic']['sf_16_problem_2_w']=$_POST['user_16_problem_2_w'];
      $_SESSION['individualDiagnostic']['sf_16_problem_3']=$_POST['user_16_problem_3'];
      $_SESSION['individualDiagnostic']['sf_16_problem_3_w']=$_POST['user_16_problem_3_w'];
      $_SESSION['individualDiagnostic']['sf_16_problem_4']=$_POST['user_16_problem_4'];
      $_SESSION['individualDiagnostic']['sf_16_problem_4_w']=$_POST['user_16_problem_4_w'];
      $_SESSION['individualDiagnostic']['sf_16_problem_5']=$_POST['user_16_problem_5'];
      $_SESSION['individualDiagnostic']['sf_16_problem_5_w']=$_POST['user_16_problem_5_w'];
      $_SESSION['individualDiagnostic']['sf_16_problem_5_d']=$_POST['user_16_problem_5_d'];


      $_SESSION['individualDiagnostic']['sf_17_problem_0']=$_POST['user_17_problem_0'];
      $_SESSION['individualDiagnostic']['sf_17_problem_0_w']=$_POST['user_17_problem_0_w'];
      $_SESSION['individualDiagnostic']['sf_17_problem_0_t']=$_POST['user_17_problem_0_t'];
      $_SESSION['individualDiagnostic']['sf_17_problem_1']=$_POST['user_17_problem_1'];
      $_SESSION['individualDiagnostic']['sf_17_problem_1_w']=$_POST['user_17_problem_1_w'];
      $_SESSION['individualDiagnostic']['sf_17_problem_1_t']=$_POST['user_17_problem_1_t'];
      $_SESSION['individualDiagnostic']['sf_17_problem_2']=$_POST['user_17_problem_2'];
      $_SESSION['individualDiagnostic']['sf_17_problem_2_w']=$_POST['user_17_problem_2_w'];
      $_SESSION['individualDiagnostic']['sf_17_problem_2_t']=$_POST['user_17_problem_2_t'];



      $_SESSION['individualDiagnostic']['sf_18_problem_0']=$_POST['user_18_problem_0'];
      $_SESSION['individualDiagnostic']['sf_18_problem_1']=$_POST['user_18_problem_1'];
      $_SESSION['individualDiagnostic']['sf_18_problem_2']=$_POST['user_18_problem_2'];

      $_SESSION['individualDiagnostic']['sf_19_problem_0']=$_POST['user_19_problem_0'];
      $_SESSION['individualDiagnostic']['sf_19_problem_1']=$_POST['user_19_problem_1'];
      $_SESSION['individualDiagnostic']['sf_19_problem_2']=$_POST['user_19_problem_2'];
      $_SESSION['individualDiagnostic']['sf_19_problem_3']=$_POST['user_19_problem_3'];
      $_SESSION['individualDiagnostic']['sf_19_problem_4']=$_POST['user_19_problem_4'];
      $_SESSION['individualDiagnostic']['sf_19_problem_5']=$_POST['user_19_problem_5'];
      $_SESSION['individualDiagnostic']['part_3'] = true;
      redirectToCorrectStep(3);
    }
    if($_POST['action']=='ankieta_4')
    {
      $_SESSION['individualDiagnostic']['sf_20_problem_0']=$_POST['user_20_problem_0'];
      $_SESSION['individualDiagnostic']['sf_20_problem_1']=$_POST['user_20_problem_1'];
      $_SESSION['individualDiagnostic']['sf_21_problem_0']=$_POST['user_21_problem_0'];
      $_SESSION['individualDiagnostic']['sf_21_problem_1']=$_POST['user_21_problem_1'];
      $_SESSION['individualDiagnostic']['sf_21_problem_2']=$_POST['user_21_problem_2'];
      $_SESSION['individualDiagnostic']['sf_21_problem_3']=$_POST['user_21_problem_3'];

      $_SESSION['individualDiagnostic']['sf_22_problem_0']=$_POST['user_22_problem_0'];
      $_SESSION['individualDiagnostic']['sf_22_problem_0_w']=$_POST['user_22_problem_0_w'];
      $_SESSION['individualDiagnostic']['sf_22_problem_1']=$_POST['user_22_problem_1'];
      $_SESSION['individualDiagnostic']['sf_22_problem_2']=$_POST['user_22_problem_2'];
      $_SESSION['individualDiagnostic']['sf_22_problem_3']=$_POST['user_22_problem_3'];
      $_SESSION['individualDiagnostic']['sf_22_problem_4']=$_POST['user_22_problem_4'];
      $_SESSION['individualDiagnostic']['sf_22_problem_5']=$_POST['user_22_problem_5'];

      $_SESSION['individualDiagnostic']['sf_23_problem_0']=$_POST['user_23_problem_0'];
      $_SESSION['individualDiagnostic']['sf_23_problem_0_w1']=$_POST['user_23_problem_0_w1'];
      $_SESSION['individualDiagnostic']['sf_23_problem_0_w2']=$_POST['user_23_problem_0_w2'];
      $_SESSION['individualDiagnostic']['sf_23_problem_0_w3']=$_POST['user_23_problem_0_w3'];
      $_SESSION['individualDiagnostic']['sf_23_problem_0_w4']=$_POST['user_23_problem_0_w4'];

      
      $_SESSION['individualDiagnostic']['sf_23_problem_1']=$_POST['user_23_problem_1'];
      $_SESSION['individualDiagnostic']['sf_23_problem_2']=$_POST['user_23_problem_2'];
      $_SESSION['individualDiagnostic']['sf_23_problem_3']=$_POST['user_23_problem_3'];
      $_SESSION['individualDiagnostic']['sf_23_problem_4']=$_POST['user_23_problem_4'];
      $_SESSION['individualDiagnostic']['sf_23_problem_5']=$_POST['user_23_problem_5'];

      $_SESSION['individualDiagnostic']['sf_24_problem_0']=$_POST['user_24_problem_0'];
      $_SESSION['individualDiagnostic']['sf_24_problem_1']=$_POST['user_24_problem_1'];
      $_SESSION['individualDiagnostic']['sf_24_problem_2']=$_POST['user_24_problem_2'];
      $_SESSION['individualDiagnostic']['sf_24_problem_3']=$_POST['user_24_problem_3'];

      $_SESSION['individualDiagnostic']['sf_25_problem_0']=$_POST['user_25_problem_0'];
      $_SESSION['individualDiagnostic']['sf_25_problem_1']=$_POST['user_25_problem_1'];
      $_SESSION['individualDiagnostic']['sf_25_problem_2']=$_POST['user_25_problem_2'];
      $_SESSION['individualDiagnostic']['sf_25_problem_3']=$_POST['user_25_problem_3'];
      $_SESSION['individualDiagnostic']['sf_25_problem_4']=$_POST['user_25_problem_4'];
      $_SESSION['individualDiagnostic']['sf_25_problem_5']=$_POST['user_25_problem_5'];
      $_SESSION['individualDiagnostic']['sf_25_problem_6']=$_POST['user_25_problem_6'];
      $_SESSION['individualDiagnostic']['part_4'] = true;
      redirectToCorrectStep(4);
    }
    if($_POST['action']=='saveSurvey')
      {
        $id = $userRevita->getId();
        $user_name = esc_html($_POST['user_name']);
        $user_name_invoice = esc_html($_POST['user_name_invoice']);
        $user_phone = esc_html($_POST['user_phone']);

        $user_surname = esc_html($_POST['user_surname']);
        $user_surname_invoice = esc_html($_POST['user_surname_invoice']);

        $user_street = esc_html($_POST['user_street']);
        $user_city = esc_html($_POST['user_city']);
        $user_zipcode = esc_html($_POST['user_zip']);
        $user_nipcode = esc_html($_POST['user_nip']);

        $invoice = ($_POST['vat']=='1') ? true : false;

        update_post_meta($id,'sf_name',$user_name);
        update_post_meta($id,'sf_surname',$user_surname);
        update_post_meta($id,'sf_phone',$user_phone);

        update_post_meta($id,'sf_name_invoice',$user_name_invoice);
        update_post_meta($id,'sf_surname_invoice',$user_surname_invoice);
        update_post_meta($id,'sf_nip',$user_nipcode);


        update_post_meta($id,'sf_street',$user_street);
        update_post_meta($id,'sf_place',$user_city);
        update_post_meta($id,'sf_zip_code',$user_zipcode);
        update_post_meta($id,'sf_have_invoice',$invoice);

      }
    if($_POST['action']=='individualDiagnostic')
    {
        $_SESSION['individualDiagnostic']['sf_purpose'] = $_POST['user_purpose_of_visit'];
        $_SESSION['individualDiagnostic']['sf_user_other_purposes'] = $_POST['purpose-of-visit-other'];
        $_SESSION['individualDiagnostic']['sf_disease_yes_no'] = $_POST['user_disease'];
        $_SESSION['individualDiagnostic']['sf_disease_w'] = $_POST['recognized-disease-other'];
        $_SESSION['individualDiagnostic']['sf_problem'] = $_POST['what-is-your-problem'];
    }

      if($_SESSION['individualDiagnostic']['sf_sex']=='')
         $_SESSION['individualDiagnostic']['sf_sex'] = 2;

      normalizeArray($_SESSION['individualDiagnostic']['sf_1_problem_0']);
      normalizeArray($_SESSION['individualDiagnostic']['sf_4_problem_2']);
      normalizeArray($_SESSION['individualDiagnostic']['sf_10_problem_0']);
      normalizeArray($_SESSION['individualDiagnostic']['sf_15_problem_2']);
      normalizeArray($_SESSION['individualDiagnostic']['sf_21_problem_3']);
      normalizeArray($_SESSION['individualDiagnostic']['sf_25_problem_5']);
      normalizeArray($_SESSION['individualDiagnostic']['sf_25_problem_6']);
      normalizeArray($_SESSION['individualDiagnostic']['sf_purpose']);

      if($_POST['action']!='')
      update_post_meta($userRevita->getId(),'sf_session_storage',$_SESSION['individualDiagnostic']);

}
function redirectToCorrectStep($number){
  return $number; //TODO FIX THIS SHIT
  for($i = 1 ; $i < $number ; $i++)
  {
    if(!$_SESSION['individualDiagnostic']['part_'.$i])
    {
      wp_redirect(get_permalink_template('ankieta_'.$i.'.php'));
      die();
    }
  }
}
/**
 * @param $var double,string
 * @param $dec int
 *
 * @return string
 */
function formatNumberToPolish($var,$dec)
{

  if($var!='')
  {
    if($dec!==null)
	    {$var = number_format($var,$dec);}

    return str_replace('.',',',str_replace(',',' ',$var ));
  }
  else
  {
	  if($dec!==null)
	    {return number_format(0,intval($dec));}
	  else
	    {return '0';}
  }

}

/**
 * @param $var string
 *
 * @return double
 */
function formatNumberToPHP($var)
{
  if($var!='')
    {return trim(str_replace(' ','',str_replace(',','.',$var )));}
  else
    {return 0.0;}
}

function normalizeArray(&$array)
{
  $array = (is_array($array)) ? $array : [] ;
}

function processArrayDBWordpress($id,$key)
{
 $data = get_post_meta($id,$key,true);
 if(is_array($data))
 {
   delete_post_meta($id,$key);
   foreach($data as $single)
     {add_post_meta($id,$key,$single);}
 }

}

/**
* @param $weight float
* @param $brzuch float
* @param $talia float
* @param $biodra float
* @param $action null|string
 *
 * @return array
 */
function updateMeasure($weight,$brzuch,$talia,$biodra,$action=null)
{
  $userRevitamed = new user;
  $todayMeasure = getTodayMeasurePost($userRevitamed->getId());
  $result = [];
  if(empty($todayMeasure))
  {
    $result['found'] = false;
    $prefix='sf_';
    $result['id']= wp_insert_post(array(
      'post_type' => 'measurement',
      'post_status' => 'publish',
      'post_title' => $userRevitamed->getName() . ' ' . date('Y-m-d'),
      'meta_input' => array(
        "{$prefix}user_relation" => $userRevitamed->getId(),
        "{$prefix}weight" => $weight,
        "{$prefix}brzucha" => $brzuch,
        "{$prefix}talia" => $talia,
        "{$prefix}biodra" => $biodra,
      )
    ));

  }
  else
  {
    $result['found'] = true;
    $result['id'] = $id = $todayMeasure[0]->ID;
    if($_REQUEST['action']=='weight_update')
      {update_post_meta($id,'sf_weight',$weight);}
    elseif($_REQUEST['action']=='circuit_update')
    {
      update_post_meta($id,'sf_talia',$talia);
      update_post_meta($id,'sf_brzucha',$brzuch);
      update_post_meta($id,'sf_biodra',$biodra);
    }
    elseif($action=='not')
    {
      update_post_meta($id,'sf_weight',$weight);
      update_post_meta($id,'sf_talia',$talia);
      update_post_meta($id,'sf_brzucha',$brzuch);
      update_post_meta($id,'sf_biodra',$biodra);
    }
    $result['fields']['ob-talia'] = get_post_meta($id,'sf_talia',true);
    $result['fields']['ob-brzuch'] = get_post_meta($id,'sf_brzucha',true);
    $result['fields']['ob-biodra'] = get_post_meta($id,'sf_biodra',true);
    $result['fields']['user_weight'] = get_post_meta($id,'sf_weight',true);
  }

  return $result;

}
function calculateFatDegree($surveyID)
{

  $userID = get_post_meta($surveyID,'sf_client_name',true);
  $gender = get_post_meta($userID,'sf_sex',true);
  $talia = get_post_meta($surveyID,'sf_circuit_1',true);
  $tummy = get_post_meta($surveyID,'sf_circuit_2',true);
  $waist = get_post_meta($surveyID,'sf_circuit_3',true);
  $height = get_post_meta($surveyID,'sf_height',true);
	$hand = get_post_meta($surveyID,'sf_circuit_4',true);
	$weight = get_post_meta($surveyID,'sf_weight',true);

  if($gender==1)
  {
    $a = -8.5603245934 + 0.5499353997 * $waist;
    $b = -0.0641998533 + 0.2798258976 * $tummy;
    $c = -0.0456139637 + 0.2398631808 * $height;
	  return $a+$b-$c;

  }
  elseif($gender==2)
  {
	  $a = (int)($talia - $hand);
	  $c = (int)$weight;
    $result = 81.6592026952*(($c**-0.9979565577))*$a + (-3159.7570187823*($c**-0.9204102624));

    return min(55,max(3,$result));
  }
  else
  {
    // use woman
	  $a = -8.5603245934 + 0.5499353997 *$waist;
	  $b = -0.0641998533 + 0.2798258976 *$tummy;
	  $c = -0.0456139637 + 0.2398631808*$height;
	  return $a+$b-$c;
    //dunno really
  }


}

function viewSurvey($data,$showUserData=false,$idUser=0)
{

  $data['sf_1_problem_0'] = maybe_unserialize( $data['sf_1_problem_0']);
  $data['sf_4_problem_2'] = maybe_unserialize( $data['sf_4_problem_2']);
  $data['sf_10_problem_0'] = maybe_unserialize( $data['sf_10_problem_0']);
  $data['sf_15_problem_2'] = maybe_unserialize( $data['sf_15_problem_2']);
  $data['sf_purpose'] = maybe_unserialize( $data['sf_purpose']);

  if(!is_array($data['sf_purpose']))
    {$data['sf_purpose'] = array($data['sf_purpose']);}

  if(!is_array($data['sf_1_problem_0']))
    {$data['sf_1_problem_0'] = array($data['sf_1_problem_0']);}

  if(!is_array($data['sf_10_problem_0']))
    {$data['sf_10_problem_0'] = array($data['sf_10_problem_0']);}

  if(!is_array($data['sf_15_problem_2']))
    {$data['sf_15_problem_2'] = array($data['sf_15_problem_2']);}

  $survey  = getUserSurvey($idUser);
	$purposes =  array(
		'1' => 'Odchudający',
		'2' => 'Zdrowotny',
		'3' => 'Przyrost masy ciała',
		'4' => 'Regenerujący budujący masę mięśniową',
		'5' => 'Odmładzający, rewitalizujący',
		'6' => 'Inny' ,
		'7' => 'Podnoszący poziom energii',
	);
	if(!empty(esc_html($data['sf_user_other_purposes'])))
      $purposes['6'].= ' : '.esc_html($data['sf_user_other_purposes']);

	$resistances = array(
		'1' => 'Brak częstych infekcji',
		'2' => 'U dorosłych więcej niż 2 infekcje w roku ',
		'3' => 'U dzieci więcej niż 4 infekcje w roku ',
	);
	$pressure = array(
	  '1' => 'Prawidłowe',
	  '2' => 'Niskie',
	  '3' => 'Wysokie',
	  '4' => 'Nie wiem',
	);
	$problemsTummy = array(
	  '1' => 'Bez dolegliwości',
	  '2' => 'Zaparcia',
	  '3' => 'Biegunki',
	  '4' => 'Naprzemiennie ale z przewagą zaparć',
	  '5' => 'Wzdęcia',
	  '6' => 'Zgaga',
	  '7' => 'Refluks',
	  '8' => 'Nadmierne gazy',
	  '9' => 'Pieczenie odbytu lub świąd odbytu',
	  '10' => 'Naprzemiennie ale z przewagą biegunek',
	);
	$sleepProblems = array(
	  '1' => 'trudności z zasypianiem',
	  '2' => 'częste wybudzanie się w nocy',
	  '3' => 'inne uwagi do snu np. poty nocne',
	);
  $problemTummyAr =[];
	$purposeAr = [];


	if(!$data['sf_7_problem_1'])
  {
    $swelling = 'Nie';
  }
  else
  {
    $swelling = 'Tak';
    $swArray  = array(
      '1' => 'głównie nóg',
      '2' => 'głównie twarzy',
      '3' => 'całe ciało',
      '4' => 'nie umiem określić',
    );
    $swArray2 = array(
      '1' => 'poranne',
      '2' => 'wieczorne',
      '3' => 'cały dzień',
    );
    $swelling1 = $swArray[$data['sf_7_problem_1_s']];
    $swelling2 = $swArray2[$data['sf_7_problem_1_w']];
    if(!empty($swelling1) && !empty($swelling2))
    {
      $swelling .= " ( $swelling1, $swelling2 )";
    }
    elseif(!empty($swelling2))
    {
      $swelling .= " ( $swelling2 )";
    }
    elseif(!empty($swelling1))
    {
      $swelling .= " ( $swelling1 )";
    }

  }


	foreach($data['sf_purpose'] as $single)
	  $purposeAr[] = $purposes[$single];

	foreach($data['sf_1_problem_0'] as $single)
	  $problemTummyAr[] = $problemsTummy[$single];

	if(empty($problemTummyAr))
	  $problemTummyAr[] = 'Bez dolegliwości';

	$sleep = ($data['sf_4_problem_0']==="1") ? 'Prawidłowy' : 'Nieprawidłowy';
  $meals = array_map(function ($a) use($data){

    $text = ($data['sf_15_problem_2_'.($a-1)]!='') ? ' ( ' . $data['sf_15_problem_2_'.($a-1)]. ' )' : '';

    switch($a){
     case 1:
       return 'Jedzenie domowe' . $text;
     case 2:
      return 'Jedzenie przygotowane na stołówce' . $text;
     case 3:
      return 'Jedzenie w barze/restauracji' . $text;
     case 4:
      return 'Fast Food' . $text;
    }
    return '';
  },$data['sf_15_problem_2']);

	if($data['sf_4_problem_0']==="1")
  {
    if(!empty($data['sf_4_problem_2']) && !is_array($data['sf_4_problem_2']))
    foreach($data['sf_4_problem_2'] as $single)
    $sleep.= $sleepProblems[$single];
  }

	$resistance = $resistances[$data['sf_6_problem_0']];

	$energy =  $data['sf_3_problem_0']==="1" ? "Tak" : "Nie";
	if($data['sf_3_problem_0']==="1")
  {
    if($data['sf_3_problem_1']==="1")
      $energy.= ' (Zmęczenie poranne (po nocy))';
    if($data['sf_3_problem_2']==="1")
      $energy.= ' (Zmęczenie całodniowe)';
    if($data['sf_3_problem_3']==="1")
      $energy.= ' (Zmęczenie popołudniowe)';
  }
  $work = array(
    '1' => 'Siedząca',
    '2' => 'Praca ciężka fizyczna',
    '3' => 'Zmianowa',
    '4' => 'Częste podróże',
    '5' => 'Nie pracuję zawodowo',
    '6' => 'Uczeń/student',
  );
	$workA = array_map(function($a) use($work){
	  return $work[$a];
	},$data['sf_21_problem_3']);

	$firstInfo = array(
	  array(
	    'value' => (int)$data['sf_age'],
	    'text' => 'Wiek: %s lat / ',
	    'showForUser' => true
	  ),
	   array(
	    'value' => (int)$data['sf_weight'],
	    'text' => 'Waga: %s kg / ',
	    'showForUser' => true
	  ),
	   array(
	    'value' => (int)$data['sf_nice_weight'],
	    'text' => 'Waga przy której Pan/Pani czuje się najlepiej: %s kg / ',
	    'showForUser' => true
	  ),
	  array(
	    'value' => (int)$data['sf_highest_weight'],
	    'text' => 'Najwyższa waga w dorosłym życiu (u kobiet bez ciąży): %s kg / ',
	    'showForUser' => true
	  ),
	   array(
	    'value' => (int)$data['sf_circuit_1'],
	    'text' => 'Obwód talii: %s cm / ',
	    'showForUser' => true
	  ),
     array(
	    'value' => (int)$data['sf_circuit_2'],
	    'text' => 'Obwód brzucha: %s cm / ',
	    'showForUser' => true
	  ),
     array(
	    'value' => (int)$data['sf_circuit_3'],
	    'text' => 'Obwód bioder: %s cm  ',
	    'showForUser' => true
	  ),
	  array(
	    'value' => (int)$data['sf_circuit_4'],
	    'text' => 'Obwód nadgarstka: %s cm  ',
	    'showForUser' => true
	  ),
	   array(
	    'value' => abs(formatNumberToPolish(calculateFatDegree($survey[0]->ID),0)),
	    'text' => '/ Procent tkanki tłuszczowej: %s %%',
	    'showForUser' => false
	  ),
	);

	$flowerProducts = array(
	  array(
      'show'=> (bool)$data['sf_16_problem_0'],
      'value'=> $data['sf_16_problem_0_w'],
      'text' => 'Makarony',
	  ),
    array(
      'show'=> (bool)$data['sf_16_problem_1'],
      'value'=> $data['sf_16_problem_1_w'],
       'text' => 'Pierogi',
	  ),array(
      'show'=> (bool)$data['sf_16_problem_2'],
      'value'=> $data['sf_16_problem_2_w'],
       'text' => 'Naleśniki',
	  ),array(
      'show'=> (bool)$data['sf_16_problem_3'],
      'value'=> $data['sf_16_problem_3_w'],
       'text' => 'Pizza',
	  ),array(
      'show'=> (bool)$data['sf_16_problem_4'],
      'value'=> $data['sf_16_problem_4_w'],
       'text' => 'Zapiekanki',
	  ),
	);

	$drinks = array(
	  array(
	    'text' => 'Ile wody Pan/Pani  pije przez cały dzień, proszę podać nazwę wody  ? ',
	    'value' => $data['sf_19_problem_0'],
	  ),
	  	  array(
	    'text' => 'Herbaty (jakie, ile?)',
	    'value' => $data['sf_19_problem_1'],
	  ),
	  	  array(
	    'text' => 'Kawy (jakie, ile?)',
	    'value' => $data['sf_19_problem_2'],
	  ),
	  	  array(
	    'text' => 'Soki (jakie, ile?)',
	    'value' => $data['sf_19_problem_3'],
	  ),
	  	  array(
	    'text' => 'Napoje gazowane (jakie, ile?)',
	    'value' => $data['sf_19_problem_4'],
	  ),
	  	  array(
	    'text' => 'Energetyki (jakie, ile?)',
	    'value' => $data['sf_19_problem_5'],
	  ),
	);

	$drinks = array_filter($drinks,function ($a){
	  return $a['value'] != '';
	});


	$drugs = array(
	  array(
	    'text' => 'Alkohol - jaki, jak często ? ',
	    'value' => $data['sf_18_problem_0'],
	  ),
	  	  array(
	    'text' => 'Papierosy i inne używki - jakie? Jak często?',
	    'value' => $data['sf_18_problem_1'],
	  ),
	  	  array(
	    'text' => 'Fast Foody - jak często?',
	    'value' => $data['sf_18_problem_2'],
	  ),
	);

	$drugs = array_filter($drugs,function ($a){
	  return $a['value'] != '';
	});

	$infections = [];
  if($data['sf_6_problem_1']==='1')
    $infections[] = 'Zapalenia pęcherza';
  if($data['sf_6_problem_1']==='1')
    $infections[] = 'Zapalenia migdałków';
  if($data['sf_6_problem_1']==='1')
    $infections[] = 'Zapalenia uszu';
  if($data['sf_6_problem_1']==='1')
    $infections[] = 'Zapalenia górnych dróg oddechowych';
  if($data['sf_6_problem_1']==='1')
    $infections[] = 'Opryszczka';
  if($data['sf_6_problem_1']==='1')
    $infections[] = 'Inne' . ((!empty($data['sf_6_problem_6_w'])) ? (': ('. $data['sf_6_problem_6_w']. ')' ) : '');



  $wayOfEating = null;
  if($data['sf_23_problem_0']==="0")
    $wayOfEating = 'Jestem weganinem';
  elseif($data['sf_23_problem_0']==="1")
    $wayOfEating = 'Jestem wegetarianinem';
  elseif($data['sf_23_problem_0']==="2")
    $wayOfEating = 'Jem wszystko';
  elseif($data['sf_23_problem_0']==="3")
    $wayOfEating = 'Inne' . ((!empty($data['sf_23_problem_0_w4'])) ? (': ('. $data['sf_23_problem_0_w4']. ')' ) : '');

  ?>
  <div class="form__title">PODSUMOWANIE ANKIETY</div>
  <div class="form__hr"></div>
  <div class="form__rowTitle -red">Wywiad ogólny</div>
    <?php if($showUserData){?>
  <div class="form__info"><?= getUserName($idUser) ?></div>
    <?php } ?>
  <div class="form__info">
  <?php foreach($firstInfo as $single){
    if($single['value'] !='' && ($showUserData || $single['showForUser']) )
      printf($single['text'],$single['value']);
  } ?>

  </div>

  <div class="form__info">
    CEL WIZYTY: <?= implode($purposeAr,', ') ?> /
    OPERACJE OD OKRESU DZIECIĘGO: <?= $data['sf_operation_yes_no']==="1" ? 'Tak' : 'Nie' ?> <?= ($data['sf_operation_ww']!='' && $data['sf_operation_yes_no']==="1") ? ("( {$data['sf_operation_ww']} )") : ('') ?> /
    LEKI: <?= $data['sf_medicines_yes_no']==="1" ? 'Tak' : 'Nie' ?> /
    <?php if($data['sf_medicines_yes_no']==="1"){ ?>
    LEKI PRZEWLEKLE BRANE:  <?= $data['sf_medicines_chronic']==="1" ? 'Tak' : 'Nie' ?> <?= $data['sf_medicines_chronic_w']!='' && $data['sf_medicines_chronic']==="1" ? "( {$data['sf_medicines_chronic_w']} )" : '' ?> /
    LEKI DORAŹNE: <?= $data['sf_medicines_season']==="1" ? 'Tak' : 'Nie' ?> <?= $data['sf_medicines_season_w']!='' && $data['sf_medicines_season']==="1" ? "( {$data['sf_medicines_season_w']} )" : '' ?> /
    <?php } ?>

    ROZPOZNANE CHOROBY: <?= $data['sf_disease_yes_no']==="1" ? 'Tak' : 'Brak' ?> <?= $data['sf_medicines_season_w']!='' && $data['sf_disease_yes_no']==="1" ? "( {$data['sf_medicines_season_w']} )" : '' ?> /
     CIŚNIENIE TĘTNICZE: <?= $pressure[$data['sf_pressure']] ?>
     <?php if( !empty($data['sf_history_weight']) ) { ?>
     <br>
     Historia wagi: <?= esc_html($data['sf_history_weight']) ?>
     <?php } ?>
  </div>

  <div class="form__row">
    <div class="form__hr"></div>
    <div class="form__col50">
      <div class="form__rowTitle -red">Wywiad zdrowotny</div>
      <div class="form__info"><strong>PRZEWÓD POKARMOWY:</strong></div>
      <div class="form__info">
		  <?= implode(', ',$problemTummyAr) ?>
      </div>
      <div class="form__info"><strong>SKÓRA:</strong></div>
      <div class="form__info">
		    <?= showSkin($data); ?>
      </div>
      <div class="form__info"><strong>ENERGIA – uczucie zmęczenia:</strong></div>
      <div class="form__info"><?= $energy ?></div>
      <div class="form__info"><strong>SEN:</strong></div>
      <div class="form__info"><?= $sleep ?></div>
      <div class="form__info"><strong>ALERGIE:</strong></div>
      <div class="form__info"><?= ($data['sf_5_problem_0']==="1") ? 'Tak' : 'Nie' ?>
		  <?php if(($data['sf_5_problem_0']==="1")){ ?>
            - <?= showAllergy($data); } ?></div>
      <div class="form__info"><strong>ODPORNOŚĆ:</strong></div>
      <div class="form__info">
        <?= $resistance ?>
      </div>
      <div class="form__info">
        CZY POJAWIAJĄ SIĘ U PANA/I CZĘSTE INFEKCJE? : <?= $data['sf_6_problem_0a']==="1" ? 'Tak' : 'Nie' ?>
        <?php if($data['sf_6_problem_0a']==="1") { implode(', ',$infections); } ?>
      </div>
      <div class="form__info">
        CZY BIERZESZ/BRAŁEŚ ANTYBIOTYKI : <?= $data['sf_6_problem_7']==="1" ? 'Tak' : 'Nie' ?>
         <?= ($data['sf_6_problem_7']==="1") ? ( '(' . esc_html($data['sf_6_problem_8']) . ')' ) : ''; ?>
      </div>
      <div class="form__info"><strong>STAWY:</strong></div>
      <div class="form__info">Bóle: <?= $data['sf_7_problem_0']==="1" ? 'Tak' : 'Nie'; ?>  / Zwyrodnienia: <?= $data['sf_7_problem_2']==="1" ? 'Tak' : 'Nie'; ?> <?= $data['sf_7_problem_3']==="1" ? '/ Osteoporoza' : '' ?></div>
      <?php if($swelling!=''){ ?>
      <div class="form__info">Obrzęki : <?= $swelling ?></div>
      <?php } ?>
      <?php if($data['sf_8_problem_0'] !== false && $data['sf_8_problem_0'] !== null){ ?>
      <div class="form__info"><strong>WYWIAD GINEKOLOGICZNY:</strong></div>
      <div class="form__info">
        Miesiączkuje: <?= ($data['sf_8_problem_0']==='1') ? 'Tak' : 'Nie' ?> /
        Bolesne miesiączki: <?= ($data['sf_8_problem_1']==='1') ? 'Tak' : 'Nie' ?> /
        Rodziła: <?= ($data['sf_8_problem_2']==='1') ? 'Tak' : 'Nie' ?> /
        Poronienia: <?= ($data['sf_8_problem_3']==='1') ? 'Tak' : 'Nie' ?> / <br>
        Zespół Napięcia Przedmiesiączkowego: <?= ($data['sf_8_problem_4']==='1') ? 'Tak' : 'Nie' ?> /
        Regularnie: <?= ($data['sf_8_problem_5']==='1') ? 'Tak' : 'Nie' ?> / <br>
        Antykoncepcja: <?= ($data['sf_8_problem_6']==='1') ? 'Tak' : 'Nie' ?> /
        Hormonalne leczenie: <?= ($data['sf_8_problem_7']==='1') ? 'Tak' : 'Nie' ?></div>
        <?php } ?>
      <div class="form__info"><strong>SKURCZE</strong></div>
      <div class="form__info">
      Skurcze łydek: <?= $data['sf_9_problem_1']==='1' ? 'Tak' : 'Nie' ?> /
      Skurcze powiek: <?= $data['sf_9_problem_2']==='1' ? 'Tak' : 'Nie' ?>
      <?php if($data['sf_9_problem_3']!='') { ?>
      / Skurcze inne: <?= esc_html($data['sf_9_problem_3']) ?>
      <?php } ?>
      </div>

      <div class="form__info"><strong>WYWIAD RODZINNY</strong></div>
      <div class="form__info"><?= showWywiad($data); ?></div>
      <div class="form__info">Stosowano diety: <?= ($data['sf_12_problem_0']==='1') ? 'Tak' : 'Nie' ?></div>
		<?php if($data['sf_12_problem_0']==='1') { ?>
          <div class="form__info">Kiedy? Jaki efekt? Jakie samopoczucie?: <?= esc_html($data['sf_12_problem_1']) ?></div>
		<?php } ?>
		  <div class="form__info">Czy jesteś lub byłeś pacjentem Revitadiet? (Dr Arendarczyk, Dr Sobkowiak lub Dr Krynicka) ? <?= ($data['sf_12_problem_3']==='1') ? 'Tak' : 'Nie' ?> <?= ($data['sf_12_problem_3']==='1' && $data['sf_12_problem_3_w']!='' ? ('( '. $data['sf_12_problem_3_w']. ' )') : '') ?></div>
    </div>
    <div class="form__col50">
      <div class="form__rowTitle -red">Wywiad żywieniowy</div>
      <div class="form__info"><strong>PROSZĘ PODAĆ GODZINY POSIŁKÓW I PRZYKŁADOWE MENU Z KILKU DNI LUB NA PRZESTRZENI 2 MIESIĘCY</strong></div>
      <div class="form__info">I Śniadanie: <?= ($data['sf_13_problem_0']==='1') ? 'Tak' : 'Nie' ?></div>
      <?= ($data['sf_13_problem_0']==='1') ? "<div class=\"form__info\">{$data['sf_13_problem_0_t']}</div>" : '' ?>
      <?= ($data['sf_13_problem_0']==='1' && !empty($data['sf_13_problem_0_w'])) ? "<div class=\"form__info\">Czas: {$data['sf_13_problem_0_w']}</div>" : '' ?>
       <div class="form__info">II Śniadanie: <?= ($data['sf_13_problem_1']==='1') ? 'Tak' : 'Nie' ?></div>
      <?= ($data['sf_13_problem_1']==='1') ? "<div class=\"form__info\">{$data['sf_13_problem_1_t']}</div>" : '' ?>
       <?= ($data['sf_13_problem_1']==='1'  && !empty($data['sf_13_problem_1_w'])) ? "<div class=\"form__info\">Czas: {$data['sf_13_problem_1_w']}</div>" : '' ?>
        <div class="form__info">Obiad: <?= ($data['sf_13_problem_2']==='1') ? 'Tak' : 'Nie' ?></div>
      <?= ($data['sf_13_problem_2']==='1') ? "<div class=\"form__info\">{$data['sf_13_problem_2_t']}</div>" : '' ?>
       <?= ($data['sf_13_problem_2']==='1'  && !empty($data['sf_13_problem_1_w'])) ? "<div class=\"form__info\">Czas: {$data['sf_13_problem_2_w']}</div>" : '' ?>
        <div class="form__info">Kolacja: <?= ($data['sf_13_problem_4']==='1') ? 'Tak' : 'Nie' ?></div>
      <?= ($data['sf_13_problem_4']==='1') ? "<div class=\"form__info\">{$data['sf_13_problem_4_t']}</div>" : '' ?>
       <?= ($data['sf_13_problem_4']==='1'  && !empty($data['sf_13_problem_4_w'])) ? "<div class=\"form__info\">Czas: {$data['sf_13_problem_4_w']}</div>" : '' ?>
      <div class="form__info"><strong>PRZEKĄSKI:</strong></div>
      <div class="form__info">
		  <?php if($data['sf_14_problem_0']==="1"){
		    echo ($data['sf_14_problem_0_t']!='') ? ( 'Owoce: ' .esc_html($data['sf_14_problem_0_t']). '<br>') : '';
		   }

		   if($data['sf_14_problem_1']==="1"){
		    echo ($data['sf_14_problem_1_t']!='') ? ( 'Orzeszki: '.esc_html($data['sf_14_problem_1_t']). '<br>') : '';
		  }
		  if($data['sf_14_problem_2']==="1"){
		    echo ($data['sf_14_problem_2_t']!='') ? ( 'Pestki słonecznika, dyni: ' .esc_html($data['sf_14_problem_2_t']). '<br>') : '';
		  }
		  if($data['sf_14_problem_3']==="1"){
		    echo ($data['sf_14_problem_3_t']!='') ? ( 'Owoce suszone:' . esc_html($data['sf_14_problem_3_t']). '<br>') : '';
		  }
		  if($data['sf_14_problem_4']==="1"){
		    echo ($data['sf_14_problem_4_t']!='') ? ( 'Słodycze, ciasta, batony:' . esc_html($data['sf_14_problem_4_t']). '<br>') : '';
		  } ?>
      </div>
      <div class="form__info">NABIAŁ: <?= ($data['sf_14_problem_5']==='1') ? 'Tak,' : 'Nie' ?> <?= ($data['sf_14_problem_5']==='1' && $data['sf_14_problem_5_t']!=='' ) ?  $data['sf_14_problem_5_t'] : '' ?></div>
      <div class="form__info"><strong>CZY WYSTĘPUJĄ  PRODUKTY ŻYWIENIOWE, KTÓRYCH PAN/I ZDECYDOWNIE  NIE ŻYCZY SOBIE W DIECIE?</strong></div>
      <div class="form__info"><?= ($data['sf_15_problem_0']==='1') ? ('Tak ' .( empty($data['sf_15_problem_1']) ? '' : esc_html($data['sf_15_problem_1']) ) ): 'Nie' ?> </div>

    <div class="form__info"><strong>JAKIEGO RODZAJU POSIŁKI SPOŻYWA PAN/I?</strong></div>
    <div class="form__info"><?= ( !empty($meals) ) ?  implode(', ',$meals) : '' ?></div>

    <?php  $texts = array();
     foreach($flowerProducts as $single){
      if($single['show'])
        $texts[] = $single['text'] . ' (' .$single['value'] . ')';
    } ?>
    <div class="form__info"><strong>MĄCZNE PRODUKTY</strong></div>
    <div class="form__info">
    <?php echo empty($texts) ? 'NIE' : implode(', ',$texts);?>
    </div>

       <?php  $texts = array();
     foreach($drinks as $single){
        $texts[] = $single['text'] . ' : ' .$single['value'];
    } ?>
    <div class="form__info"><strong>Napoje</strong></div>
    <div class="form__info">
      <?php echo empty($texts) ? 'NIE' : implode('<br>',$texts);?>
    </div>

 <?php  $texts = array();
     foreach($drugs as $single){
        $texts[] = $single['text'] . ' : ' .$single['value'];
    } ?>

     <div class="form__info"><strong>Używki</strong><br>
    <?= empty($texts) ? 'NIE' : (implode('<br>',$texts)) ?>
    </div>

    </div>

    <div class="form__col100">
      <div class="form__rowTitle -red">Preferencje żywieniowe</div>
      <div class="form__info"><strong><?= ($data['sf_20_problem_0']==='1') ? 'Chcę, żeby moje preferencje żywieniowe zostały uwzględnione (np. mięso tylko 3 x w tyg)' : 'Jem wszystko ALBO po prostu chcę zrobić wszystko co możliwe dla mojego zdrowia niezależnie od moich preferencji żywieniowych' ?></strong></div>

      <?php if(!empty($data['sf_20_problem_1'])){ ?>
      <div class="form__info">Preferencje : <?= esc_html($data['sf_20_problem_1']) ?></div>
      <?php } ?>

      <div class="form__info"><strong>CHARAKTER PRACY :</strong></div>
          <div class="form__info"><?= implode(', ',$workA) ?></div>


	  <div class="form__info"><strong>Ćwiczenia</strong></div>
		  <div class="form__info"> <?= ($data['sf_22_problem_0']==='0') ? 'NIE' : 'TAK' ?></div>
		  <?php if($data['sf_22_problem_0']==='1'){

        $ex = array(
          'sf_22_problem_0_w' => 'Jak często w tygodniu ćwiczysz ',
          'sf_22_problem_2' => 'W jakie dni tygodnia?',
          'sf_22_problem_1' => 'Ile trwają ćwiczenia?',
          'sf_22_problem_3' => 'Od kiedy ćwiczysz?',
        );
        foreach($ex as $key => $label)
        {
          if(!empty($data[$key]))
          printf('<div class="form__info">%s %s</div>',$label,esc_html($data[$key]));
        }
           } ?>
        <div class="form__info"><strong>Suplementy</strong></div>
        <div class="form__info"> <?= ($data['sf_22_problem_4']==='0') ? 'NIE' : 'TAK' ?></div>
      <?php if($data['sf_22_problem_4']==='1' && !empty($data['sf_22_problem_5'])){ ?>
        <div class="form__info">Suplementuje się w związku z tym następującymi produktami: <?= esc_html($data['sf_22_problem_5']) ?></div>
      <?php } ?>

      <div class="form__info"><strong>Preferencje szczegółowe</strong></div>

      <?php if($wayOfEating !== null) { ?>
		  <div class="form__info"> JAKI JEST PANA/I STYL ŻYWIENIA </div>
		  <div class="form__info"> <?= $wayOfEating ?></div>
		  <?php } ?>

		  <div class="form__info"> CZY SĄ PROUKTY, PO KTÓRYCH PAN/I ŹLE SIĘ CZUJE?  </div>
		  <div class="form__info"> <?= ($data['sf_24_problem_0']==='0') ? 'NIE' : 'TAK' ?> <?= (!empty($data['sf_24_problem_1'])) ? ('( '. $data['sf_24_problem_1'] .' )') : '' ?></div>

    </div>
  </div>

  <?php $files =  getFilesFromSection($idUser,'research');  if(!empty($files)){
    ?>
  <div class="form__row">
    <div class="form__rowTitle">BADANIA</div>
    <div class="fileUploader">
      <div class="fileUploader__filebox">

	      <?php foreach($files as $file){ ?>
            <a <?= isFileImage(get_post_meta($file->ID,'sf_path_var',true)) ? 'data-fancybox="group"' : '' ?>  download="<?= get_post_meta($file->ID,'sf_name',true) ?>" href="<?= home_url('/'). get_post_meta($file->ID,'sf_path_url',true); ?>" class="fileUploader__uploaded">
              <div class="fileUploader__uploadedIcon">
                <i class="fa fa-file-text-o"></i>
              </div>
              <div class="fileUploader__uploadedName"><?= $file->post_title ?></div>
            </a>
	      <?php } ?>

    </div>
  </div>
  <?php } ?>


  <?php $files =  getFilesFromSection($idUser,'research-fdt');  if(!empty($files)){
    ?>
  <div class="form__row">
    <div class="form__rowTitle">BADANIE FDT</div>
    <div class="fileUploader">
      <div class="fileUploader__filebox">

	      <?php foreach($files as $file){ ?>
            <a <?= isFileImage(get_post_meta($file->ID,'sf_path_var',true)) ? 'data-fancybox="group"' : '' ?> download="<?= get_post_meta($file->ID,'sf_name',true) ?>" href="<?= home_url('/'). get_post_meta($file->ID,'sf_path_url',true); ?>" class="fileUploader__uploaded">
              <div class="fileUploader__uploadedIcon">
                <i class="fa fa-file-text-o"></i>
              </div>
              <div class="fileUploader__uploadedName"><?= $file->post_title ?></div>
            </a>
	      <?php } ?>

    </div>
  </div>
  <?php } ?>

 <?php $files =  getFilesFromSection($idUser,'research-beta');  if(!empty($files)){
    ?>
  <div class="form__row">
    <div class="form__rowTitle">WYNIKI BADANIA BETA</div>
    <div class="fileUploader">
      <div class="fileUploader__filebox">

	      <?php foreach($files as $file){ ?>
            <a <?= isFileImage(get_post_meta($file->ID,'sf_path_var',true)) ? 'data-fancybox="group"' : '' ?> download="<?= get_post_meta($file->ID,'sf_name',true) ?>" href="<?= home_url('/'). get_post_meta($file->ID,'sf_path_url',true); ?>" class="fileUploader__uploaded">
              <div class="fileUploader__uploadedIcon">
                <i class="fa fa-file-text-o"></i>
              </div>
              <div class="fileUploader__uploadedName"><?= $file->post_title ?></div>
            </a>
	      <?php } ?>

    </div>
  </div>
  <?php } ?>

  <?php
}

function getDataSurvey($id)
{
	$rawData = get_post_meta($id);
	$data = [];
	foreach($rawData as $key => $element)
	{
		if(is_array($element))
		{
			if(count($element)<=1)
				{$data[$key]=$element[0];}
			else
				{$data[$key]=$element;}
		}
		else
			{$data[$key] = $element;}
	}
  return $data;
}


function updateProductsOfDiet( $days, $dietID ) {

	if ( get_post_type( $dietID ) == 'diet' ||  get_post_type( $dietID ) == 'template_diet') {
    $nthDay = 0;

		foreach($days as $data)
		  {
		    $nthDay++;
		    $nthMeal = 0;
		    foreach($data as $meal)
		      {
		        $nthMeal++;
		        update_post_meta($dietID,"sf_recipe_{$nthDay}_{$nthMeal}",$meal['ID']);
		        update_post_meta($dietID, "sf_ing_{$nthDay}_{$nthMeal}",filterFormIng($meal['ing']));

		      }
		  }

	}
}

function filterFormIng($meal){
  $res = array();
  if(is_array($meal))
  {
    foreach($meal as $ing => $amount)
    {
      $res[] = array(
      'ing' => (int)$ing,
      'amount' => $amount
       );
    }
  }
  return $res;
}

function getUserWeights($userID)
{
  $measures = array_reverse(getUserMeasures($userID),false);
  $weights = [];
  foreach($measures as $measure)
    {if(get_post_meta($measure->ID,'sf_weight',true)!= 0)
	    {$weights[] = get_post_meta($measure->ID,'sf_weight',true);}}


  return $weights;
}
function getUserWaist($userID)
{
	$measures = array_reverse(getUserMeasures($userID),false);
	$weights = [];
	foreach($measures as $measure)
		{if(get_post_meta($measure->ID,'sf_talia',true)!= 0)
			{$weights[] = get_post_meta($measure->ID,'sf_talia',true);}}


	return $weights;
}
function getUserStomach($userID)
{
	$measures = array_reverse(getUserMeasures($userID),false);
	$weights = [];
	foreach($measures as $measure)
		{if(get_post_meta($measure->ID,'sf_brzucha',true)!= 0)
			{$weights[] = get_post_meta($measure->ID,'sf_brzucha',true);}}


	return $weights;
}
function getUserHips($userID)
{
	$measures = array_reverse(getUserMeasures($userID),false);
	$weights = [];
	foreach($measures as $measure)
		{if(get_post_meta($measure->ID,'sf_biodra',true)!= 0)
			{$weights[] = get_post_meta($measure->ID,'sf_biodra',true);}}


	return $weights;
}
function getCurrentPartOfPlan($currentDay,$dietID)
{
  $date = ($currentDay);
  $realizations = getAllRealizationByDateUser($date,$dietID) ;
//  $amountOfMeals = get_post_meta($dietID,'sf_quantity_meal',true);

  $day = getDietInfoDay($dietID,strtotime($currentDay));
  $day['meals'] = array_filter($day['meals'],function($single_meal){
                    return  !($single_meal['recipe']=='' || $single_meal['recipe']==0);
                  });

  $amountOfMeals = max(count($day['meals']),1);
  $eaten = 0;

  foreach($realizations as $realization)
  {
    if(get_post_meta($realization->ID,'sf_eaten',true)==1)
	    {$eaten++;}
  }


  return formatNumberToPolish(($eaten/$amountOfMeals)*100,0);

}
function BooleanVal($a)
{
  if($a===null) {return null;}
  else
    {return ($a)?  true : false;}
}

/**
* @param $dietID
 * TODO
*
* @return mixed|string
 */
function getJSONCreateCalendar($dietID)
{
  $dateFrom = get_post_meta($dietID,'sf_date_from',true);
  $dateTo = getEndTimestampDiet($dietID);
  $meals = getDietMeals($dietID);
  $i_days = 0;
  $result = [];

  while($dateTo >= strtotime("+$i_days days",$dateFrom))
  {
	  $item = [
		  'date' => date('d.m.Y',strtotime("+$i_days days",$dateFrom)),

	  ];
    foreach($meals as $meal)
       {$item[$meal['calendar-menu']] = BooleanVal(getMealEaten($dietID,$meal['id'], date('d.m.Y',strtotime("+$i_days days",$dateFrom))));}


	  $result[] = $item;
	  $i_days++;
  }
  return json_encode($result);
}

function getEndTimestampDiet($dietID)
{
  return get_post_meta($dietID,'sf_date_to',true);
}

function getDailyIngOfDiet($dietID)
{
  $result = [];
  for($i_meal = 1; $i_meal<6; $i_meal++)
  {
    $meal = get_post_meta($dietID,'sf_ing_meal_'.$i_meal,true);
    if(!empty($meal))
      {foreach($meal as $ind)
	     {$result[$ind['ing']]+=$ind['amount'];}}
  }

    return $result;
}

function getCalendarHTML($userID,$from,$to,$full){
  $resJson = [];
  $res = '';
  $from = strtotime($from);
  $to = strtotime($to);
  $limit = 100;


  for($i_time = $from; $i_time<=$to; $i_time+=86400){

    if($limit--==0)
      break;

    $diet = getDietTime($userID,$i_time);

    if($diet===null) continue;

    $dietID =  $diet->ID;

    $day = getDietInfoDay($dietID,$i_time);
    $day['meals'] = array_filter($day['meals'],function($single_meal){
                    return  !($single_meal['recipe']=='' || $single_meal['recipe']==0);
                  });

    if(empty($day['meals']))
      {continue;}

  $dayT = __(date('l',$i_time));
  $dateT = date('d.m.Y',$i_time);
  $res .=  "<tr class='day-row unmarked -onlyForPrint'>
              <td class='day-col'><strong>$dayT</strong><br>$dateT</td>
              <td class='day-wrap' colspan='4'>
                <table class='inner-day-table'>
                  <tbody>";

  $nthMeal = 0;

  foreach($day['meals'] as $single_meal){


  $nameMeal = ($nthMeal + 1) . ' Posiłek';

  $recipe = get_post($single_meal['recipe']);
   $ing= array();
  foreach($single_meal['ing'] as $el)
                {$ing []= $el['amount']. ' ' . get_unit_name(getUnitByIng($el['ing']),$el['amount']) . ' ' . lcfirst(get_the_title($el['ing']));}

  $ings =  implode(', ',$ing);


        if($full)
          {
              $res .="<tr class='meal-row'> <td class='meal-col'>$nameMeal<div style='color: #565656; font-weight: 400;'>{$recipe->post_title}</div> <div>Składniki: $ings</div></td></tr>";
          }
          else
          {
              $res .="<tr class='meal-row'> <td class='meal-col'>$nameMeal<div style='color: #565656; font-weight: 400;'>{$recipe->post_title}</div></td></tr>";

          }

         $nthMeal++;
        }

        $res .= "</tbody></table></td></tr>";
  }


  return $res;
}
/**
* @param $userID
* @param $dataStart
* @param $dataStop
 *
 * @return array
 */
function getNDaysIngOfDietSortedByTaxonomy($userID,$dataStart,$dataStop)
{

  $result = [];
  $list = [];
  for($time = $dataStart; $time<=$dataStop ; $time = strtotime('+1 day',$time)){

      $diet = getDietTime($userID,$time);
      $dietID = $diet->ID;
      if($dietID === null) continue;

      $data = getProperDietInfo($dietID,$time);

      foreach($data as $index => $item)
        {
          if(is_array($item))
            {
              foreach($item as $item2)
                {
                  $list[$item2['ing']] = (isset($list[$item2['ing']])) ? $list[$item2['ing']] : 0;
                  $list[$item2['ing']] +=  $item2['amount'];
                }
            }
        }
  }

   foreach($list as $ing => $amount)
      {
        $tax = wp_get_post_terms($ing,'grupa');

        if(!empty($tax))
          {$result [$tax[0]->name][$ing]=$amount;}
        else
          {$result ['Reszta'][$ing]=$amount;}

      }

	return $result;
}
function getNDaysIngOfDietFormat($userID,$dataStart,$dataStop){
	$result = [];
	$nDaysInd = getNDaysIngOfDietSortedByTaxonomy($userID,$dataStart,$dataStop);
  foreach($nDaysInd as $name=>$group)
  {
    $item = [];
    foreach($group as $ing=>$amount)
    {
	    $item[get_the_title($ing)] =  $amount . ' ' .get_unit_name(getUnitByIng($ing),$amount);
    }

	  $result[$name] = $item;
  }
	return $result;

}

function getClosestMeasurement($userID,$timestamp)
{
  $measurements = getUserMeasures($userID);

  if(empty($measurements))
    {return null;}
  else
    {
      $result = $measurements[0];
      $distance = INF;
      foreach($measurements as $measurement)
        {

          $newDistance = abs(strtotime($measurement->post_date)-$timestamp);
          if($distance > $newDistance)
            {
              $distance = $newDistance;
              $result = $measurement;
            }

        }

      return $result;
    }
}

function addTimeToCurrentPlan($userID,$time){
  $plans = get_post_meta($userID,'sf_subs_history',true);
  $planID = getCurrentSubPlanID($userID);
  $start = getUserLastActiveDay($userID);
  $end = $start + $time;
  $newPlan = array(
    'cennik' => $planID,
          'date_from' => [
            'timestamp'=> $start,
            'formatted'=> date('Y-m-d',$start),
          ],
          'date_to' => [
            'timestamp'=> $end,
            'formatted'=> date('Y-m-d',$end),
            ]
  ) ;


  $plans[] = $newPlan;
  update_post_meta($userID,'sf_subs_history',$plans);
  update_post_meta($userID,'sf_date_sub',$end);

}

function getClosestWeight($userID,$timestamp)
{
  $measurement = getClosestMeasurement($userID,$timestamp);

  if($measurement==null)
    {return null;}
  else
    {return get_post_meta($measurement->ID,'sf_weight',true);}
}


function trigger404() {
	status_header( 404 );
	nocache_headers();
	include( get_query_template( '404' ) );
	die();
}

function saveTemplateDiet()
{
      $templateID = $_POST['sf_template'];
			$products = $_POST['meal'];
			$text_1 = $_POST['forbidden'];
			$text_2 = $_POST['daily-supplementation'];
			$text_3 = $_POST['comments'];
			update_post_meta($templateID,'sf_forbidden',$text_1);
			update_post_meta($templateID,'sf_supplements',$text_2);
			update_post_meta($templateID,'sf_warnings',$text_3);
      // if is diet and active
      if(get_post_type($templateID) && get_post_meta($templateID,'sf_active',true))
      {
        $dietID = $templateID;
        $start = get_post_meta($dietID,'sf_date_from',true);
        $currentArchive = get_post_meta($dietID,'sf_archive_meals');

        if(!empty($currentArchive))
        foreach($currentArchive as $item)
          {
            if(isset($item['stop']))
              {
                $start = max($start,$item['stop']);
              }
          }

        $amountOfMeals = get_post_meta($templateID,'sf_quantity_meal',true);
        if(empty($amountOfMeals)) {$amountOfMeals = 5;}
        $archiveData = [
          'start' => $start, //get start of archive
          'stop' => time(), //get start of archive
        ];
        for($i_day = 1; $i_day <= 7 ;$i_day++)
        {
          for($i_meal = 1; $i_meal <= $amountOfMeals ;$i_meal++)
          {
            $archiveData["sf_recipe_{$i_day}_{$i_meal}"]= get_post_meta($dietID,"sf_recipe_{$i_day}_{$i_meal}",true);
            $archiveData["sf_ing_{$i_day}_{$i_meal}"]= get_post_meta($dietID,"sf_ing_{$i_day}_{$i_meal}",true);
          }

        }
        add_post_meta($dietID,'sf_archive_meals',$archiveData);
      }

			updateProductsOfDiet( $products, $templateID );
}


if(!function_exists('fastcgi_finish_request'))
  {include 'libs/polyfill.php';}
function getUnitByIng($ing){
  return get_post_meta($ing,'rm_unit',true);
}
function get_unit_name($unit, $amount){

  if($unit=='') {return 'g';}
  $amount = floatval($amount);

    if( ($amount - floor($amount)) > 0.0001)
    {
      $name = get_post_meta($unit,'rm_lm_1',true);
      return $name == '' ? get_the_title($unit) : $name;
    }
    else
    {
      switch($amount){
        case 1:
          $name = get_post_meta($unit,'rm_lp',true);
          return $name == '' ? get_the_title($unit) : $name;
        case 2:
        case 3:
        case 4:
           $name = get_post_meta($unit,'rm_lm_1',true);
          return $name == '' ? get_the_title($unit) : $name;
        default:
          $name = get_post_meta($unit,'rm_lm_2',true);
          return $name == '' ? get_the_title($unit) : $name;
      }
    }
}
/**
* @param $id
* @param $timestamp
 *
* @return array
 */
function getDietInfo($id,$timestamp= null){


  $res = [];

  $amountOfMeals = get_post_meta($id,'sf_quantity_meal',true);
  if(empty($amountOfMeals)) {$amountOfMeals = 5;}
  $amountOfDays = 7;

  $data = getProperDietInfo($id,$timestamp);

   for($i_day = 1; $i_day<=$amountOfDays;$i_day++)
    {
      $day = array();
      for($i_meal = 1;$i_meal<=$amountOfMeals; $i_meal++)
      {
        $day[] = array(
        'recipe' => $data["sf_recipe_{$i_day}_{$i_meal}"],
        'ing' => $data["sf_ing_{$i_day}_{$i_meal}"],
        );
      }
      $i = $i_day - 1;
      $res[]= array(
        'number' => $i,
        'name' => ucfirst(__(date('l',86400*($i+4)))),
        'meals' => $day
        );
    }

  return $res;
}

function getProperDietInfo($id,$timestamp){
  $archive = get_post_meta($id,'sf_archive_meals');
  $data = [];
  if(!empty($archive))
    foreach($archive as $item)
      {
        if($timestamp >= $item['start'] && $timestamp <=$item['stop'] )
          {
            return $item;
          }
      }

    $amountOfMeals = get_post_meta($id,'sf_quantity_meal',true);
  if(empty($amountOfMeals)) {$amountOfMeals = 5;}



  for($i_day = 1; $i_day <= 7 ;$i_day++)
        {
          for($i_meal = 1; $i_meal <= $amountOfMeals ;$i_meal++)
          {
            $data["sf_recipe_{$i_day}_{$i_meal}"]= get_post_meta($id,"sf_recipe_{$i_day}_{$i_meal}",true);
            $data["sf_ing_{$i_day}_{$i_meal}"]= get_post_meta($id,"sf_ing_{$i_day}_{$i_meal}",true);
          }

        }

      return $data;
}


/**
* @param $id
* @param $timestamp
 *
* @return array
 */
function getDietInfoDay($id,$timestamp){

  $res = getDietInfo($id,$timestamp);
  $currentDay = ucfirst(__(date('l',$timestamp)));
  foreach($res as $day)
  {
    if($day['name'] == $currentDay)
    {
      return $day;
    }
  }

  return [];
}


function createEmptyTemplate($title,$quantity){
  $createPost = array(
      'post_title'=> esc_html($title),
      'post_status'=> 'publish',
      'post_content'=> '',
      'post_type'=> 'template_diet',
  );
  $id = wp_insert_post($createPost);
  update_post_meta($id,'sf_quantity_meal',$quantity);
  return $id;
}

function displayDietPlanEdit($days){
$dataDayShow = 0;
    foreach($days as  $day){ ?>
                <div class="dietPicker__option -active" data-day-show="<?php echo $dataDayShow; ?>">
                  <div class="dietPicker__header" data-header>
                    <img class="dietPicker__icon"
                         src="<?php echo get_template_directory_uri(); ?>/components/dietPicker/src/img-1.svg" alt="">
					  <?= ($day['name']=='środa') ? 'Środa' : $day['name'] ?>
                  </div>
                  <div class="dietPicker__content">

                    <!-- Product Main -->
                    <div class="dietPicker__main" data-product_creator="main"><?php

						$number = 0;
						foreach($day['meals'] as $meal){
							$title = ($meal['recipe']) ? get_the_title($meal['recipe']) : '';
							?>
                          <div class="dietPicker-form" data-day="<?= $day['number'] ?>" data-meal="<?= $number ?>">
                            <div class="dietPicker-form__item">
                              <label class="dietPicker-form__label"><?= $number+1;  ?> POSIŁEK</label>
                              <input class="dietPicker-form__input" value="<?= $title ?>" data-product_creator_name>
                              <input type="hidden" name="meal[<?= $day['number'] ?>][<?= $number ?>][ID]" value="<?= $meal['recipe'] ?>">
                            </div>
                            <div class="dietPicker-form__addholder">
                              <span class="dietPicker-form__add" data-id="0" data-product_creator_create><i class="fa fa-plus-circle"></i> ZMIEŃ DANIE NA INNE</span>
                            </div>
                            <div class="dietPicker-form__ingredients "><?php
                            if(is_array($meal['ing']))
                            {
                              foreach($meal['ing'] as $ing){
									printf('<div class="dietPicker-form__ingredient"><input name="meal[%d][%d][ing][%d]" value="%s"> %s <strong>%s</strong></div>',$day['number'],$number,$ing['ing'],$ing['amount'],get_unit_name(getUnitByIng($ing['ing']),$ing['amount']),get_the_title($ing['ing']));
								}} ?></div>

                          </div>
						<?php $number++; } ?></div>
                  </div>

                </div>

			  <?php $dataDayShow++; }
}

function displayDietTextFields($forbidden,$supp,$comments_diet){
  ?>
              <div class="dietAdditional__content" data-product_creator>
              <div class="dietAdditional__item">
                <label class="dietAdditional__label">NIE WOLNO</label>
                <div class="dietAdditional__group">
                  <textarea class="dietAdditional__textarea" name="forbidden"><?= esc_textarea($forbidden); ?></textarea>
                </div>
                <div class="dietAdditional__collection" data-product_list>
                </div>
              </div>
              <div class="dietAdditional__item">
                <label class="dietAdditional__label">CODZIENNA SUPLEMENTACJA</label>
                <textarea class="dietAdditional__textarea" name="daily-supplementation"><?= esc_textarea($supp); ?></textarea>
              </div>
              <div class="dietAdditional__item">
                <label class="dietAdditional__label">UWAGI</label>
                <textarea class="dietAdditional__textarea" name="comments"><?= esc_textarea($comments_diet); ?></textarea>
              </div>
            </div>

  <?php
}

function createDraftPartialSurvey($userID,$dietID){

  $args = array(
    'post_status' => 'publish',
    'post_type' => 'part_ankieta',
    'post_title' => getUserName($userID),
    'post_content' => '',
    'post_name' => generateRandomString(),
    'meta_input' => array(
      'sf_client_name' => $userID,
      'sf_related_diet' => $dietID
    ),
  );

  return wp_insert_post($args);
}

function generatePartialSurveyForUsers(){
  $titan = TitanFramework::getInstance( 'revita' );
  foreach(getAllClient() as $clientID)
    {
      $currentDiet = getCurrentActiveDiet($clientID);
      if(empty($currentDiet)) {continue;}


      $partSurveys = get_posts(array(
        'post_type' => 'part_ankieta',
        'post_status' => 'all',
		    'nopaging'    => true,
		    'meta_query' => array(
			    'relation' => 'AND',
			    array(
			      'key'     => 'sf_related_diet',
			      'value'   => $currentDiet->ID,
			      'compare' => '='
			      ),
			    )
      ));
      $block = get_post_meta($currentDiet->ID,'sf_block_partial',true);
      if(empty($partSurveys) && !$block)
        {
          $time = (get_post_meta($currentDiet->ID,'sf_date_to',true)-7*(60*60*24));

          if(time() > $time)
          {
            // New partial survey e
            $id = createDraftPartialSurvey($clientID,$currentDiet->ID);
            $title = $titan->getOption('email_short_survey_title');
            $text = $titan->getOption('email_short_survey_text');
            $text.= '<a href="' . get_the_permalink($id) .'">Kliknij tutaj</a>';
            $res = revitamed_send_mail(get_post_meta($clientID,'sf_email',true),$text,$title);
          }


        }

    }

}

function getRelatedMealDiet($id)
{
  $res = null;
  $user = new user;
  if($user->isActive())
    {
      $diet = getLastDiet($user->getId());

      for($i_day = 1; $i_day<=7;$i_day++)
      {
       for($i_meal = 1;$i_meal<=5; $i_meal++)
        {
          if(get_post_meta($diet->ID,"sf_recipe_{$i_day}_{$i_meal}",true)==$id)
            {return get_post_meta($diet->ID,"sf_ing_{$i_day}_{$i_meal}",true);}
        }
      }
    }

    return $res;
}

/**
* @param $userID int
 */
function fixUserToUserRelation($userID)
{

    if(get_post_meta($userID,'sf_user',true)==0 || true)
		{

		  $email = get_post_meta($userID,'sf_email',true);
		  global $wpdb;
			//find account with this email

			$res = $wpdb->get_results($wpdb->prepare("SELECT * FROM $wpdb->users WHERE user_email=%s",$email),ARRAY_A);
			if(!empty($res))
      {
        $res = $res[0];
        //set it to this account
        update_post_meta($userID,'sf_user',$res['ID']);
      }
      else
      {
            $name = get_post_meta($userID,'sf_name',true);
            $surname = get_post_meta($userID,'sf_surname',true);
            $user_id = wp_create_user($email,'',$email);
            if(is_wp_error($user_id))
              $user_id = 0;
            wp_update_user(array(
                    'ID' => $user_id,
                    'display_name' => ($name. ' ' . $surname),
            ));
            update_user_meta($user_id,'first_name',$name);
            update_user_meta($user_id,'last_name',$surname);
            update_user_meta($user_id,'nickname',$name. ' ' . $surname);

            update_post_meta($userID,'sf_user',$user_id);
      }

		}

}


function comment_form_custom( $args = array(), $post_id = null ) {
  if ( null === $post_id )
    {$post_id = get_the_ID();}

  // Exit the function when comments for the post are closed.
  if ( ! comments_open( $post_id ) ) {
    /**
     * Fires after the comment form if comments are closed.
     *
     * @since 3.0.0
     */
    do_action( 'comment_form_comments_closed' );

    return;
  }

  $commenter = wp_get_current_commenter();
  $user = wp_get_current_user();
  $user_identity = $user->exists() ? $user->display_name : '';

  $args = wp_parse_args( $args );
  if ( ! isset( $args['format'] ) )
    {$args['format'] = current_theme_supports( 'html5', 'comment-form' ) ? 'html5' : 'xhtml';}

  $req      = get_option( 'require_name_email' );
  $aria_req = ( $req ? " aria-required='true'" : '' );
  $html_req = ( $req ? " required='required'" : '' );
  $html5    = 'html5' === $args['format'];
  $fields   =  array(
    'author' => '<p class="comment-form-author">' . '<label for="author">' . __( 'Name' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
      '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" maxlength="245"' . $aria_req . $html_req . ' /></p>',
    'email'  => '<p class="comment-form-email"><label for="email">' . __( 'Email' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
      '<input id="email" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" maxlength="100" aria-describedby="email-notes"' . $aria_req . $html_req  . ' /></p>',
    'url'    => '<p class="comment-form-url"><label for="url">' . __( 'Website' ) . '</label> ' .
      '<input id="url" name="url" ' . ( $html5 ? 'type="url"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" maxlength="200" /></p>',
  );

  $required_text = sprintf( ' ' . __('Required fields are marked %s'), '<span class="required">*</span>' );

  /**
   * Filters the default comment form fields.
   *
   * @since 3.0.0
   *
   * @param array $fields The default comment fields.
   */
  $fields = apply_filters( 'comment_form_default_fields', $fields );
  $defaults = array(
    'fields'               => $fields,
    'comment_field'        => '<p class="comment-form-comment"><label for="comment">' . _x( 'Comment', 'noun' ) . '</label> <textarea id="comment" name="comment" cols="45" rows="8" maxlength="65525" aria-required="true" required="required"></textarea></p>',
    /** This filter is documented in wp-includes/link-template.php */
    'must_log_in'          => '<p class="must-log-in">' . sprintf(
      /* translators: %s: login URL */
        __( 'You must be <a href="%s">logged in</a> to post a comment.' ),
        get_permalink_template('page-tpl-konto-10-logowanie.php')
      ) . '</p>',
    /** This filter is documented in wp-includes/link-template.php */
    'logged_in_as'         => '<p class="logged-in-as">' . sprintf(
      /* translators: 1: edit user link, 2: accessibility text, 3: user name, 4: logout URL */
        __( '<a href="%1$s" aria-label="%2$s">Logged in as %3$s</a>. <a href="%4$s">Log out?</a>' ),
        get_edit_user_link(),
        /* translators: %s: user name */
        esc_attr( sprintf( __( 'Logged in as %s. Edit your profile.' ), $user_identity ) ),
        $user_identity,
        '?logout=true'
      ) . '</p>',
    'comment_notes_before' => '<p class="comment-notes"><span id="email-notes">' . __( 'Your email address will not be published.' ) . '</span>'. ( $req ? $required_text : '' ) . '</p>',
    'comment_notes_after'  => '',
    'action'               => site_url( '/wp-comments-post.php' ),
    'id_form'              => 'commentform',
    'id_submit'            => 'submit',
    'class_form'           => 'comment-form',
    'class_submit'         => 'submit',
    'name_submit'          => 'submit',
    'title_reply'          => '',
    'title_reply_to'       => __( 'Leave a Reply to %s' ),
    'title_reply_before'   => '<h3 id="reply-title" class="comment-reply-title">',
    'title_reply_after'    => '</h3>',
    'cancel_reply_before'  => ' <small>',
    'cancel_reply_after'   => '</small>',
    'cancel_reply_link'    => __( 'Cancel reply' ),
    'label_submit'         => 'Dodaj komentarz',
    'submit_button'        => '<input name="%1$s" type="submit" id="%2$s" class="%3$s" value="%4$s" />',
    'submit_field'         => '<p class="form-submit">%1$s %2$s</p>',
    'format'               => 'xhtml',
  );

  /**
   * Filters the comment form default arguments.
   *
   * Use {@see 'comment_form_default_fields'} to filter the comment fields.
   *
   * @since 3.0.0
   *
   * @param array $defaults The default comment form arguments.
   */
  $args = wp_parse_args( $args, apply_filters( 'comment_form_defaults', $defaults ) );

  // Ensure that the filtered args contain all required default values.
  $args = array_merge( $defaults, $args );

  /**
   * Fires before the comment form.
   *
   * @since 3.0.0
   */
  do_action( 'comment_form_before' );
  ?>
  <div id="respond" class="comment-respond">
    <?php
    echo $args['title_reply_before'];

    comment_form_title( $args['title_reply'], $args['title_reply_to'] );

    echo $args['cancel_reply_before'];

    cancel_comment_reply_link( $args['cancel_reply_link'] );

    echo $args['cancel_reply_after'];

    echo $args['title_reply_after'];

    if ( get_option( 'comment_registration' ) && !is_user_logged_in() ) :
      echo $args['must_log_in'];
      /**
       * Fires after the HTML-formatted 'must log in after' message in the comment form.
       *
       * @since 3.0.0
       */
      do_action( 'comment_form_must_log_in_after' );
    else : ?>
      <form action="<?php echo esc_url( $args['action'] ); ?>" method="post" id="<?php echo esc_attr( $args['id_form'] ); ?>" class="<?php echo esc_attr( $args['class_form'] ); ?>"<?php echo $html5 ? ' novalidate' : ''; ?>>
        <?php
        /**
         * Fires at the top of the comment form, inside the form tag.
         *
         * @since 3.0.0
         */
        do_action( 'comment_form_top' );

        if ( is_user_logged_in() ) :
          /**
           * Filters the 'logged in' message for the comment form for display.
           *
           * @since 3.0.0
           *
           * @param string $args_logged_in The logged-in-as HTML-formatted message.
           * @param array  $commenter      An array containing the comment author's
           *                               username, email, and URL.
           * @param string $user_identity  If the commenter is a registered user,
           *                               the display name, blank otherwise.
           */
          echo apply_filters( 'comment_form_logged_in', $args['logged_in_as'], $commenter, $user_identity );

          /**
           * Fires after the is_user_logged_in() check in the comment form.
           *
           * @since 3.0.0
           *
           * @param array  $commenter     An array containing the comment author's
           *                              username, email, and URL.
           * @param string $user_identity If the commenter is a registered user,
           *                              the display name, blank otherwise.
           */
          do_action( 'comment_form_logged_in_after', $commenter, $user_identity );

        else :

          echo $args['comment_notes_before'];

        endif;

        // Prepare an array of all fields, including the textarea
        $comment_fields = array( 'comment' => $args['comment_field'] ) + (array) $args['fields'];

        /**
         * Filters the comment form fields, including the textarea.
         *
         * @since 4.4.0
         *
         * @param array $comment_fields The comment fields.
         */
        $comment_fields = apply_filters( 'comment_form_fields', $comment_fields );

        // Get an array of field names, excluding the textarea
        $comment_field_keys = array_diff( array_keys( $comment_fields ), array( 'comment' ) );

        // Get the first and the last field name, excluding the textarea
        $first_field = reset( $comment_field_keys );
        $last_field  = end( $comment_field_keys );

        foreach ( $comment_fields as $name => $field ) {

          if ( 'comment' === $name ) {

            /**
             * Filters the content of the comment textarea field for display.
             *
             * @since 3.0.0
             *
             * @param string $args_comment_field The content of the comment textarea field.
             */
            echo apply_filters( 'comment_form_field_comment', $field );

            echo $args['comment_notes_after'];

          } elseif ( ! is_user_logged_in() ) {

            if ( $first_field === $name ) {
              /**
               * Fires before the comment fields in the comment form, excluding the textarea.
               *
               * @since 3.0.0
               */
              do_action( 'comment_form_before_fields' );
            }

            /**
             * Filters a comment form field for display.
             *
             * The dynamic portion of the filter hook, `$name`, refers to the name
             * of the comment form field. Such as 'author', 'email', or 'url'.
             *
             * @since 3.0.0
             *
             * @param string $field The HTML-formatted output of the comment form field.
             */
            echo apply_filters( "comment_form_field_{$name}", $field ) . "\n";

            if ( $last_field === $name ) {
              /**
               * Fires after the comment fields in the comment form, excluding the textarea.
               *
               * @since 3.0.0
               */
              do_action( 'comment_form_after_fields' );
            }
          }
        }

        $submit_button = sprintf(
          $args['submit_button'],
          esc_attr( $args['name_submit'] ),
          esc_attr( $args['id_submit'] ),
          esc_attr( $args['class_submit'] ),
          esc_attr( $args['label_submit'] )
        );

        /**
         * Filters the submit button for the comment form to display.
         *
         * @since 4.2.0
         *
         * @param string $submit_button HTML markup for the submit button.
         * @param array  $args          Arguments passed to `comment_form()`.
         */
        $submit_button = apply_filters( 'comment_form_submit_button', $submit_button, $args );

        $submit_field = sprintf(
          $args['submit_field'],
          $submit_button,
          get_comment_id_fields( $post_id )
        );

        /**
         * Filters the submit field for the comment form to display.
         *
         * The submit field includes the submit button, hidden fields for the
         * comment form, and any wrapper markup.
         *
         * @since 4.2.0
         *
         * @param string $submit_field HTML markup for the submit field.
         * @param array  $args         Arguments passed to comment_form().
         */
        echo apply_filters( 'comment_form_submit_field', $submit_field, $args );

        /**
         * Fires at the bottom of the comment form, inside the closing </form> tag.
         *
         * @since 1.5.0
         *
         * @param int $post_id The post ID.
         */
        do_action( 'comment_form', $post_id );
        ?>
      </form>
    <?php endif; ?>
  </div><!-- #respond -->
  <?php

  /**
   * Fires after the comment form.
   *
   * @since 3.0.0
   */
  do_action( 'comment_form_after' );
}
