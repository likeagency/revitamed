<?php


/**
 * @param $dietID
 *
 * @deprecated
 * @return array
 */
function getDietMeals( $dietID ) {
	$amount = ( get_post_type( $dietID ) == 'diet' ) ? get_post_meta( $dietID, 'sf_quantity_meal', true ) : 5;
	$meal0  = ( is_array( get_post_meta( $dietID, 'sf_ing_meal_1', true ) ) ) ? get_post_meta( $dietID, 'sf_ing_meal_1', true ) : [];
	$meal1  = ( is_array( get_post_meta( $dietID, 'sf_ing_meal_2', true ) ) ) ? get_post_meta( $dietID, 'sf_ing_meal_2', true ) : [];
	$meal2  = ( is_array( get_post_meta( $dietID, 'sf_ing_meal_3', true ) ) ) ? get_post_meta( $dietID, 'sf_ing_meal_3', true ) : [];
	$meal3  = ( is_array( get_post_meta( $dietID, 'sf_ing_meal_4', true ) ) ) ? get_post_meta( $dietID, 'sf_ing_meal_4', true ) : [];
	$meal4  = ( is_array( get_post_meta( $dietID, 'sf_ing_meal_5', true ) ) ) ? get_post_meta( $dietID, 'sf_ing_meal_5', true ) : [];

	$meal0extra = ( is_array( get_post_meta( $dietID, 'sf_ing_meal_extra_1', true ) ) ) ? get_post_meta( $dietID, 'sf_ing_meal_extra_1', true ) : [];
	$meal1extra = ( is_array( get_post_meta( $dietID, 'sf_ing_meal_extra_2', true ) ) ) ? get_post_meta( $dietID, 'sf_ing_meal_extra_2', true ) : [];
	$meal2extra = ( is_array( get_post_meta( $dietID, 'sf_ing_meal_extra_3', true ) ) ) ? get_post_meta( $dietID, 'sf_ing_meal_extra_3', true ) : [];
	$meal3extra = ( is_array( get_post_meta( $dietID, 'sf_ing_meal_extra_4', true ) ) ) ? get_post_meta( $dietID, 'sf_ing_meal_extra_4', true ) : [];
	$meal4extra = ( is_array( get_post_meta( $dietID, 'sf_ing_meal_extra_5', true ) ) ) ? get_post_meta( $dietID, 'sf_ing_meal_extra_5', true ) : [];
	switch ( $amount ) {
		case 2:
			return array(
				0 => array(
					'name'       => 'ŚNIADANIE',
					'id'         => 1,
					'recipes'=> get_post_meta($dietID,'sf_meal_1_recipes',true),
					'icon'       => 'img-1.svg',
					'data-menu'  => 'breakfast',
					'calendar-menu'  => 'breakfast',
					'meal'       => $meal0,
					'meal_extra' => $meal0extra,
					'percent'    => [ 'fat'     => get_post_meta( $dietID, 'sf_percent_1_fat', true ),
					                  'protein' => get_post_meta( $dietID, 'sf_percent_1_protein', true ),
					                  'carbs'   => get_post_meta( $dietID, 'sf_percent_1_carbs', true )
					],
				),
				1 => array(
					'name'       => 'OBIADOKOLACJA',
					'id'         => 5,
					'recipes'=> get_post_meta($dietID,'sf_meal_5_recipes',true),
					'icon'       => 'img-5.svg',
					'data-menu'  => 'supper',
					'calendar-menu'  => 'supper',
					'meal'       => $meal4,
					'meal_extra' => $meal4extra,
					'percent'    => [ 'fat'     => get_post_meta( $dietID, 'sf_percent_5_fat', true ),
					                  'protein' => get_post_meta( $dietID, 'sf_percent_5_protein', true ),
					                  'carbs'   => get_post_meta( $dietID, 'sf_percent_5_carbs', true )
					]
				),
			);
		case 3:
			return array(
				0 => array(
					'name'       => 'ŚNIADANIE',
					'id'         => 1,
					'recipes'=> get_post_meta($dietID,'sf_meal_1_recipes',true),
					'icon'       => 'img-1.svg',
					'data-menu'  => 'breakfast',
					'calendar-menu'  => 'breakfast',
					'meal'       => $meal0,
					'meal_extra' => $meal0extra,
					'percent'    => [ 'fat'     => get_post_meta( $dietID, 'sf_percent_1_fat', true ),
					                  'protein' => get_post_meta( $dietID, 'sf_percent_1_protein', true ),
					                  'carbs'   => get_post_meta( $dietID, 'sf_percent_1_carbs', true )
					],
				),
				1 => array(
					'name'       => 'OBIAD',
					'id'         => 3,
					'recipes'=> get_post_meta($dietID,'sf_meal_3_recipes',true),
					'icon'       => 'img-3.svg',
					'data-menu'  => 'dinner',
					'calendar-menu'  => 'dinner',
					'meal'       => $meal2,
					'meal_extra' => $meal2extra,
					'percent'    => [ 'fat'     => get_post_meta( $dietID, 'sf_percent_3_fat', true ),
					                  'protein' => get_post_meta( $dietID, 'sf_percent_3_protein', true ),
					                  'carbs'   => get_post_meta( $dietID, 'sf_percent_3_carbs', true )
					]
				),
				2 => array(
					'name'       => 'KOLACJA',
					'id'         => 5,
					'recipes'=> get_post_meta($dietID,'sf_meal_5_recipes',true),
					'icon'       => 'img-5.svg',
					'data-menu'  => 'supper',
					'calendar-menu'  => 'supper',
					'meal'       => $meal4,
					'meal_extra' => $meal4extra,
					'percent'    => [ 'fat'     => get_post_meta( $dietID, 'sf_percent_5_fat', true ),
					                  'protein' => get_post_meta( $dietID, 'sf_percent_5_protein', true ),
					                  'carbs'   => get_post_meta( $dietID, 'sf_percent_5_carbs', true )
					]
				),
			);
		case 4:
			return array(
				0 => array(
					'name'       => 'ŚNIADANIE',
					'id'         => 1,
					'recipes'=> get_post_meta($dietID,'sf_meal_1_recipes',true),
					'icon'       => 'img-1.svg',
					'data-menu'  => 'breakfast',
					'calendar-menu'  => 'breakfast',
					'meal'       => $meal0,
					'meal_extra' => $meal0extra,
					'percent'    => [ 'fat'     => get_post_meta( $dietID, 'sf_percent_1_fat', true ),
					                  'protein' => get_post_meta( $dietID, 'sf_percent_1_protein', true ),
					                  'carbs'   => get_post_meta( $dietID, 'sf_percent_1_carbs', true )
					],
				),
				1 => array(
					'name'       => 'OBIAD',
					'id'         => 3,
					'recipes'=> get_post_meta($dietID,'sf_meal_3_recipes',true),
					'icon'       => 'img-3.svg',
					'data-menu'  => 'dinner',
					'calendar-menu'  => 'dinner',
					'meal'       => $meal2,
					'meal_extra' => $meal2extra,
					'percent'    => [ 'fat'     => get_post_meta( $dietID, 'sf_percent_3_fat', true ),
					                  'protein' => get_post_meta( $dietID, 'sf_percent_3_protein', true ),
					                  'carbs'   => get_post_meta( $dietID, 'sf_percent_3_carbs', true )
					]
				),
				2 => array(
					'name'       => 'PRZEKĄSKA',
					'id'         => 4,
					'recipes'=> get_post_meta($dietID,'sf_meal_4_recipes',true),
					'icon'       => 'img-4.svg',
					'data-menu'  => 'snack',
					'calendar-menu'  => 'snack',
					'meal'       => $meal3,
					'meal_extra' => $meal3extra,
					'percent'    => [ 'fat'     => get_post_meta( $dietID, 'sf_percent_4_fat', true ),
					                  'protein' => get_post_meta( $dietID, 'sf_percent_4_protein', true ),
					                  'carbs'   => get_post_meta( $dietID, 'sf_percent_4_carbs', true )
					]
				),
				3 => array(
					'name'       => 'KOLACJA',
					'id'         => 5,
					'recipes'=> get_post_meta($dietID,'sf_meal_5_recipes',true),
					'icon'       => 'img-5.svg',
					'data-menu'  => 'supper',
					'calendar-menu'  => 'supper',
					'meal'       => $meal4,
					'meal_extra' => $meal4extra,
					'percent'    => [ 'fat'     => get_post_meta( $dietID, 'sf_percent_5_fat', true ),
					                  'protein' => get_post_meta( $dietID, 'sf_percent_5_protein', true ),
					                  'carbs'   => get_post_meta( $dietID, 'sf_percent_5_carbs', true )
					]
				),
			);
		case 5:
			return array(
				0 => array(
					'name'       => 'ŚNIADANIE',
					'id'         => 1,
					'recipes'=> get_post_meta($dietID,'sf_meal_1_recipes',true),
					'icon'       => 'img-1.svg',
					'data-menu'  => 'breakfast',
					'calendar-menu'  => 'breakfast',
					'meal'       => $meal0,
					'meal_extra' => $meal0extra,
					'percent'    => [ 'fat'     => get_post_meta( $dietID, 'sf_percent_1_fat', true ),
					                  'protein' => get_post_meta( $dietID, 'sf_percent_1_protein', true ),
					                  'carbs'   => get_post_meta( $dietID, 'sf_percent_1_carbs', true )
					],
				),
				1 => array(
					'name'       => 'II ŚNIADANIE',
					'id'         => 2,
					'recipes'=> get_post_meta($dietID,'sf_meal_2_recipes',true),
					'icon'       => 'img-2.svg',
					'data-menu'  => 'second-breakfast',
					'calendar-menu'  => 'breakfast_2',
					'meal'       => $meal1,
					'meal_extra' => $meal1extra,
					'percent'    => [ 'fat'     => get_post_meta( $dietID, 'sf_percent_2_fat', true ),
					                  'protein' => get_post_meta( $dietID, 'sf_percent_2_protein', true ),
					                  'carbs'   => get_post_meta( $dietID, 'sf_percent_2_carbs', true )
					]
				),
				2 => array(
					'name'       => 'OBIAD',
					'id'         => 3,
					'recipes'=> get_post_meta($dietID,'sf_meal_3_recipes',true),
					'icon'       => 'img-3.svg',
					'data-menu'  => 'dinner',
					'calendar-menu'  => 'dinner',
					'meal'       => $meal2,
					'meal_extra' => $meal2extra,
					'percent'    => [ 'fat'     => get_post_meta( $dietID, 'sf_percent_3_fat', true ),
					                  'protein' => get_post_meta( $dietID, 'sf_percent_3_protein', true ),
					                  'carbs'   => get_post_meta( $dietID, 'sf_percent_3_carbs', true )
					]
				),
				3 => array(
					'name'       => 'PRZEKĄSKA',
					'id'         => 4,
					'recipes'=> get_post_meta($dietID,'sf_meal_4_recipes',true),
					'icon'       => 'img-4.svg',
					'data-menu'  => 'snack',
					'calendar-menu'  => 'snack',
					'meal'       => $meal3,
					'meal_extra' => $meal3extra,
					'percent'    => [ 'fat'     => get_post_meta( $dietID, 'sf_percent_4_fat', true ),
					                  'protein' => get_post_meta( $dietID, 'sf_percent_4_protein', true ),
					                  'carbs'   => get_post_meta( $dietID, 'sf_percent_4_carbs', true )
					]
				),
				4 => array(
					'name'       => 'KOLACJA',
					'id'         => 5,
					'recipes'=> get_post_meta($dietID,'sf_meal_5_recipes',true),
					'icon'       => 'img-5.svg',
					'data-menu'  => 'supper',
					'calendar-menu'  => 'supper',
					'meal'       => $meal4,
					'meal_extra' => $meal4extra,
					'percent'    => [ 'fat'     => get_post_meta( $dietID, 'sf_percent_5_fat', true ),
					                  'protein' => get_post_meta( $dietID, 'sf_percent_5_protein', true ),
					                  'carbs'   => get_post_meta( $dietID, 'sf_percent_5_carbs', true )
					]
				),
			);
		default:
			return [];
	}


}