<?php

if(!class_exists('TitanFrameworkPlugin')) return;

add_action( 'init', 'theme_settings' );

function theme_settings() {
    $titan = TitanFramework::getInstance( 'revita' );
    /**
     * Strona z komunikatami
     */
    $mainAdminPanel = $titan->createAdminPage( array(
        'name' => 'Treści komunikatów',
        'id' => sanitize_title('Treści komunikatów')
    ) );
    /**
     * Logowanie / Rejestaracja
     */
    $layoutPanel = $mainAdminPanel->createTab( array(
        'name' => 'Logowanie / Rejestracja',
        'id' => sanitize_title('Logowanie')
    ) );
    $layoutPanel->createOption( array(
        'name' => 'Błędne dane logowania',
        'id' => 'login-wrong-data',
        'type' => 'textarea',
        'default'=> 'Nieprawidłowy login lub hasło.'
    ) );
    $layoutPanel->createOption( array(
        'name' => 'Konto nie było aktywowane',
        'id' => 'login-not-activated',
        'type' => 'textarea',
        'default'=> 'Konto nie zostało jeszcze aktywowane – kliknij aktywacyjnego'
    ) );
    $layoutPanel->createOption( array(
        'name' => 'Rejestracja - poprawnie wypełniony formularz',
        'id' => 'register-success',
        'type' => 'textarea',
        'default'=> 'Dziękujemy! Formularz został wysłany poprawnie. Na twój adres email wysłaliśmy link aktywacyjny - kliknij go w celu dokoczenia rejestracji.'
    ) );
    $layoutPanel->createOption( array(
        'name' => 'Rejestracja - podany email jest niepoprawny',
        'id' => 'register-wrong-email',
        'type' => 'textarea',
        'default'=> 'Podany email jest niepoprawny.'
    ) );
    $layoutPanel->createOption( array(
        'name' => 'Rejestracja - email jest już zajęty',
        'id' => 'register-taken-email',
        'type' => 'textarea',
        'default'=> 'Podany email jest już zajęty.'
    ) );
    $layoutPanel->createOption( array(
        'name' => 'Rejestracja - hasła się różnią',
        'id' => 'register-diff-passwords',
        'type' => 'textarea',
        'default'=> 'Podane hasła się różnią.'
    ) );
    $layoutPanel->createOption( array(
        'name' => 'Rejestracja - hasło jest zbyt krótkie',
        'id' => 'register-short-password',
        'type' => 'textarea',
        'default'=> 'Hasła jest zbyt krótkie. Musi posiadać przynajmniej 5 znaków.'
    ) );
    $layoutPanel->createOption( array(
        'name' => 'Rejestracja - pole imię jest puste',
        'id' => 'register-empty-name',
        'type' => 'textarea',
        'default'=> 'Pole imię musi być wypełnione.'
    ) );
    $layoutPanel->createOption( array(
        'name' => 'Rejestracja - pole nazwisko jest puste',
        'id' => 'register-empty-surname',
        'type' => 'textarea',
        'default'=> 'Pole nazwisko musi być wypełnione.'
    ) );
    $layoutPanel->createOption( array(
        'name' => 'Rejestracja - tytuł emailu z linkiem aktywacyjnym',
        'id' => 'register-activation-email-title',
        'type' => 'text',
        'default'=> 'Aktywacja konta - Revitamed'
    ) );
    $layoutPanel->createOption( array(
        'name' => 'Rejestracja - treść emailu z linkiem aktywacyjnym',
        'id' => 'register-activation-email',
        'type' => 'textarea',
        'default'=> 'Aby aktywować swoje konto kliknij na link poniżej'
    ) );
    $layoutPanel->createOption( array(
        'name' => 'Rejestracja - treść emailu z linkiem aktywacyjnym - podgląd',
        'custom' => viewEmailActivation(),
        'type' => 'custom',
    ) );

    $layoutPanel->createOption( array(
        'type' => 'save',
        'save' => 'Zapisz zmiany',
        'reset' => 'Reset ustawień'
    ) );
    /**
     * Zapomniałem hasła
     */
    $layoutPanel = $mainAdminPanel->createTab( array(
        'name' => 'Zapomniałem hasła',
        'id' => sanitize_title('Zapomniałem hasła')
    ) );
    $layoutPanel->createOption( array(
        'name' => 'Pomyślne dane dla formularza',
        'id' => 'forgot-password-success',
        'type' => 'textarea',
        'default'=> 'E-mail z dalszymi instrukcjami został wysłany'
    ) );
    $layoutPanel->createOption( array(
        'name' => 'Błąd - Nie ma użytkownika o podanym emailu',
        'id' => 'forgot-password-no-email',
        'type' => 'textarea',
        'default'=> 'Nie znaleziono użytkownika o takim emailu.'
    ) );
    $layoutPanel->createOption( array(
        'name' => 'Błąd - konto jest nieaktywne',
        'id' => 'forgot-password-not-active',
        'type' => 'textarea',
        'default'=> 'Dane konto użytkownika jest nieaktywne. Email aktywacyjny został wysłany ponownie.'
    ) );
    $layoutPanel->createOption( array(
        'name' => 'Tytuł emaila dla resetu hasła',
        'id' => 'forgot-password-email-title',
        'type' => 'text',
        'default'=> 'Reset hasła - Revitamed'
    ) );
    $layoutPanel->createOption( array(
        'name' => 'Zawartość emaila dla resetu hasła',
        'id' => 'forgot-password-email-content',
        'type' => 'textarea',
        'default'=> 'Twoje nowe hasło to'
    ) );
    $layoutPanel->createOption( array(
        'name' => 'Zawartość emaila dla resetu hasła po haśle',
        'id' => 'forgot-password-email-content-2',
        'type' => 'textarea',
        'default'=> ''
    ) );
    $layoutPanel->createOption( array(
        'name' => 'Zapomniałem hasła - treść emailu z linkiem aktywacyjnym - podgląd',
        'custom' => viewForgottenPassword(),
        'type' => 'custom',
    ) );
	  $layoutPanel->createOption( array(
	    'type' => 'save',
	    'save' => 'Zapisz zmiany',
	    'reset' => 'Reset ustawień'
	  ) );

  /**
   * Komunikaty z linka aktywacyjnego
   */
  $layoutPanel = $mainAdminPanel->createTab( array(
    'name' => 'Komunikaty z linka aktywacyjnego',
    'id' => sanitize_title('kom_aktywacyjny')
  ) );
  $layoutPanel->createOption( array(
    'name' => 'Komunikat po aktywacji konta - pomyślny',
    'id' => 'activation_success',
    'type' => 'textarea',
    'default'=> 'Pomyślnie aktywowano konto'
  ) );
  $layoutPanel->createOption( array(
    'name' => 'Komunikat po aktywacji konta - błąd',
    'id' => 'activation_error',
    'type' => 'textarea',
    'default'=> 'Błąd. Być konto już jest aktywne'
  ) );
  $layoutPanel->createOption( array(
    'type' => 'save',
    'save' => 'Zapisz zmiany',
    'reset' => 'Reset ustawień'
  ) );
  
	/**
	 * Panel użytkownika
	 */
	$layoutPanel = $mainAdminPanel->createTab( array(
		'name' => 'Panel użytkownika',
		'id' => sanitize_title('Panel użytkownika')
	) );

	$layoutPanel->createOption( array(
		'name' => 'Komunikat wysłaniu pomiarów',
		'id' => 'measure_success',
		'type' => 'textarea',
		'default'=> 'Pomyślnie zapisano pomiary'
	) );

	$layoutPanel->createOption( array(
		'name' => 'Komunikat przy braku diety, ponieważ konto nie będzie aktywne przez przynajmniej kolejne 21 dni',
		'id' => 'text-no-diet-21-days',
		'type' => 'textarea',
		'default'=> 'Twoje konto nie jest opłacone na 21 dni do przodu, więc nie otrzymasz nowej diety. Przedłuż abonament, aby otrzymać nową dietę.'
	) );

	$layoutPanel->createOption( array(
		'name' => 'Komunikat prośby wypełnienia ankiety cząstkowej',
		'id' => 'part_survey_user',
		'type' => 'textarea',
		'default'=> 'Wypełnij ankietę cząstkową'
	) );

	$layoutPanel->createOption( array(
		'type' => 'save',
		'save' => 'Zapisz zmiany',
		'reset' => 'Reset ustawień'
	) );
	/**
	 * Email z powiadomieniem ankiety cząstkowej
	 */
	$layoutPanel = $mainAdminPanel->createTab( array(
		'name' => 'Ankieta',
		'id' => sanitize_title('Ankieta')
	) );
	$layoutPanel->createOption( array(
		'name' => 'Klauzula wymaga przez RODO',
		'id' => 'rodo_clause_1',
		'type' => 'editor',
		'default'=> 'Zapoznałem się z klauzulą informacyjną RODO dotyczącą pozyskiwania danych, zamieszczoną na niniejszym portalu..'
	) );

	$layoutPanel->createOption( array(
		'name' => 'Klauzula wymaga przez RODO',
		'id' => 'rodo_clause_2',
		'type' => 'editor',
		'default'=> 'Wyrażam zgodę na przetwarzanie moich danych osobowych podanych w niniejszym formularzu przez Należy wskazać nazwę firmy i siedzibę np. ABC sp. z o.o. z siedzibą w z siedzibą w [miejscowość] ulica [ulica, numer lokalu, kod pocztowy, miejscowość] zarejestrowana w Krajowym Rejestrze Sądowym przez Sąd Rejonowy [Sąd i Wydział dokonujący rejestracji], pod numerem KRS:[numer KRS], NIP:[numer NIP] , REGON: [numer REGON], kapitał zakładowy w wysokości [wysokość kapitału zakładowego] w celu Udostępniania Panu/Pani naszych Usług i realizacji zawartej umowy'
	) );

	$layoutPanel->createOption( array(
		'name' => 'Obrazek podglądowy mężczyzna',
		'id' => 'image_measure_m',
		'type' => 'upload',
	) );

	$layoutPanel->createOption( array(
		'name' => 'Obrazek podglądowy kobieta',
		'id' => 'image_measure_k',
		'type' => 'upload',
	) );

	$layoutPanel->createOption( array(
		'name' => 'Obrazek podglądowy dziecko',
		'id' => 'image_measure_d',
		'type' => 'upload',
	) );

	$layoutPanel->createOption( array(
		'name' => 'Wskazówka mężczyzna',
		'id' => 'text_measure_m',
		'type' => 'textarea',
		'default' => 'Panowie: <br>
              brzucha, na pomiar pod żebrami',
	) );

	$layoutPanel->createOption( array(
		'name' => 'Wskazówka kobieta',
		'id' => 'text_measure_k',
		'type' => 'textarea',
		'default' => 'Panie : <br>
              talia, pomiar pod żebrami <br>
              brzucha, pomiar na wysokości pępka <br>
              biodra, pomiar na wysokości kolców biodrowych',
	) );

	$layoutPanel->createOption( array(
		'name' => 'Wskazówka dziecko',
		'id' => 'text_measure_d',
		'type' => 'textarea',
		'default' => '',
	) );

	$layoutPanel->createOption( array(
		'type' => 'save',
		'save' => 'Zapisz zmiany',
		'reset' => 'Reset ustawień'
	) );
	/**
	 * Email z powiadomieniem ankiety cząstkowej
	 */
	$layoutPanel = $mainAdminPanel->createTab( array(
		'name' => 'Email z powiadomieniem o wypełnieniu ankiety cząstkowej',
		'id' => sanitize_title('Email z powiadomieniem')
	) );
	$layoutPanel->createOption( array(
		'name' => 'Tytuł',
		'id' => 'email_short_survey_title',
		'type' => 'text',
		'default'=> 'Krótka ankieta - Revitamed'
	) );

	$layoutPanel->createOption( array(
		'name' => 'Tekst email',
		'id' => 'email_short_survey_text',
		'type' => 'editor',
		'default'=> 'Prosimy cię o wypełnienie krótkiej ankiety na temat twojej diety.<br>'
	) );
	$layoutPanel->createOption( array(
		'type' => 'save',
		'save' => 'Zapisz zmiany',
		'reset' => 'Reset ustawień'
	) );
	/**
	 * Email z powiadomieniem
	 */
	$layoutPanel = $mainAdminPanel->createTab( array(
		'name' => 'Email z powiadomieniem o braku wykupionego abonamenty na 21 dni po zakończeniu obecnej diety',
		'id' => sanitize_title('Email z powiadomieniem braku diety')
	) );
	$layoutPanel->createOption( array(
		'name' => 'Tytuł',
		'id' => 'email_not_payed_title',
		'type' => 'text',
		'default'=> 'Twój abonament niedługo wygasa - Revitamed'
	) );

	$layoutPanel->createOption( array(
		'name' => 'Tekst email',
		'id' => 'email_not_payed_text',
		'type' => 'editor',
		'default'=> 'Jeśli chcesz kontynuować dietę, opłać abonament dostępowy, przedłużenia powinieneś dokonać co najmniej 5 dni przed upływem abonamentu, aby nasi lekarze mieli czas na przygotowanie nowego jadłospisu!'
	) );
	$layoutPanel->createOption( array(
		'type' => 'save',
		'save' => 'Zapisz zmiany',
		'reset' => 'Reset ustawień'
	) );

	/**
	 * Email z powiadomieniem
	 */
	$layoutPanel = $mainAdminPanel->createTab( array(
		'name' => 'Email z powiadomieniem o nowej diecie',
		'id' => sanitize_title('Email z powiadomieniem o nowej diecie')
	) );
	$layoutPanel->createOption( array(
		'name' => 'Tytuł',
		'id' => 'email_new_diet_title',
		'type' => 'text',
		'default'=> 'Twoje dieta oczekuje na ciebie - Revitamed'
	) );

	$layoutPanel->createOption( array(
		'name' => 'Tekst email',
		'id' => 'email_new_diet_text',
		'type' => 'editor',
		'default'=> 'Twoja dieta czeka na ciebie. Zaloguj się na swoje konto, aby sprawdzić ją.'
	) );


	$layoutPanel->createOption( array(
		'type' => 'save',
		'save' => 'Zapisz zmiany',
		'reset' => 'Reset ustawień'
	) );
	/**
	 * Email z powiadomieniem o jutrzejszej
	 */
	$layoutPanel = $mainAdminPanel->createTab( array(
		'name' => 'Email z powiadomieniem o jutrzejszej diecie',
		'id' => sanitize_title('Email z powiadomieniem o jutrzejszej diecie')
	) );
	$layoutPanel->createOption( array(
		'name' => 'Tytuł (nowa dieta)',
		'id' => 'email_tomorrow_diet_title',
		'type' => 'text',
		'default'=> 'Twoje dieta gotowa i rozpoczyna się jutro - Revitamed'
	) );

	$layoutPanel->createOption( array(
		'name' => 'Tekst email (nowa dieta)',
		'id' => 'email_tomorrow_diet_text',
		'type' => 'editor',
		'default'=> 'Twoja dieta jest gotowa - rozpoczyna się jutro! Przygotuj się do niej.'
	) );
	$layoutPanel->createOption( array(
		'name' => 'Tytuł (kontynuacja)',
		'id' => 'email_tomorrow_diet_title_continuation',
		'type' => 'text',
		'default'=> 'Twoje dieta gotowa i rozpoczyna się jutro - Revitamed'
	) );

	$layoutPanel->createOption( array(
		'name' => 'Tekst email (kontynuacja)',
		'id' => 'email_tomorrow_diet_text_continuation',
		'type' => 'editor',
		'default'=> 'Twoja dieta jest gotowa - rozpoczyna się jutro! Przygotuj się do niej.'
	) );


	$layoutPanel->createOption( array(
		'type' => 'save',
		'save' => 'Zapisz zmiany',
		'reset' => 'Reset ustawień'
	) );

	/**
	 * Email z powiadomieniem o końcu subskrypcji
	 */
	$layoutPanel = $mainAdminPanel->createTab( array(
		'name' => 'Powiadomienia o końcu subskrypcji',
		'id' => sanitize_title('Email z powiadomieniem o końcu subskrypcji')
	) );
	$days = array(10,6);

	foreach($days as $day){
		$layoutPanel->createOption( array(
			'name' => 'Tytuł email - powiadomienie '.$day.' dni przed końcem subskrypcji',
			'id' => 'email_end_subs_title_day_'.$day,
			'type' => 'textarea',
			'default'=> 'Twoja subskrypcja niedługo wygasa - Revitamed'
		) );

		$layoutPanel->createOption( array(
			'name' => 'Tekst email - powiadomienie '.$day.' dni przed końcem subskrypcji',
			'id' => 'email_end_subs_text_day_'.$day,
			'type' => 'editor',
			'default'=> 'Twoja subskrypcja niedługo wygasa, przedłuż ją <a href="http://revitamed.like.energy/moje-konto/platnosci">tutaj</a>. Pamiętaj aby kolejna dieta była ułożona musisz opłacić dostęp na co najmniej 3 dni robocze przed upływem dostępu'
		) );
	}


	$layoutPanel->createOption( array(
		'name' => 'Komunikat o zbliżającym się końcu subskrypcji w panelu użytkownika',
		'id' => 'sub_end_is_coming_user_view',
		'type' => 'textarea',
		'default'=> 'Pamietaj - aby dostać swoja dietę w terminie - opłać abonament conajmniej 3 dni przed jego upływem.'
	) );



	$layoutPanel->createOption( array(
		'type' => 'save',
		'save' => 'Zapisz zmiany',
		'reset' => 'Reset ustawień'
	) );

	/**
	 * Email z powiadomieniem o dalszym niewypełnieniu ankiety cząstkowej
	 */
	$layoutPanel = $mainAdminPanel->createTab( array(
		'name' => 'Email z powiadomieniem o dalszym niewypełnieniu ankiety cząstkowej',
		'id' => sanitize_title('Email z powiadomieniem o dalszym niewypełnieniu ankiety cząstkowej')
	) );

	$days = array(4,5);

	foreach($days as $day){

		$layoutPanel->createOption( array(
			'name' => "Tytuł - $day dni przed zakończeniem etapu",
			'id' => 'email_no_partial_survey_title_day_'.$day,
			'type' => 'text',
			'default'=> 'Twoja ankieta cząstkowa czeka na wypełnienie - Revitamed'
		) );

		$layoutPanel->createOption( array(
			'name' => "Tekst - $day dni przed zakończeniem etapu",
			'id' => 'email_no_partial_survey_text_day_'.$day,
			'type' => 'editor',
			'default'=> 'Twoja ankieta cząstkowa czeka na wypełnienie. Zaloguj się i wypełnij ją.'
		) );

	}





	$layoutPanel->createOption( array(
		'type' => 'save',
		'save' => 'Zapisz zmiany',
		'reset' => 'Reset ustawień'
	) );

	$days = array(1,3,7,30);

	/**
	 * Email - nie ma wypełnionej ankiety
	 */
	$layoutPanel = $mainAdminPanel->createTab( array(
		'name' => 'Email - nie ma wypełnionej ankiety',
		'id' => sanitize_title('Email - nie ma wypełnionej ankiety')
	) );

	foreach($days as $day)
	{
		$layoutPanel->createOption( array(
			'name' => "Tytuł wiadomości - ilość dni od zapłaty - $day ",
			'id' => 'email_no_survey_days_'.$day,
			'type' => 'text',
			'default' => 'Przypomienie - revita',
		) );

		$layoutPanel->createOption( array(
			'name' => "Zawartość wiadomości - ilość dni od zapłaty - $day ",
			'id' => 'email_no_survey_days_content_'.$day,
			'type' => 'editor',
			'default' => 'Pamiętaj aby otrzymać indywidualną dietę - wypełnij ankietę.',
		) );


	}

	$layoutPanel->createOption( array(
		'type' => 'save',
		'save' => 'Zapisz zmiany',
		'reset' => 'Reset ustawień'
	) );

	/**
	 * Email - nie ma zakupionej subskrypcji
	 */
	$layoutPanel = $mainAdminPanel->createTab( array(
		'name' => 'Email - nie ma zakupionej subskrypcji',
		'id' => sanitize_title('Email - nie ma zakupionej subskrypcji')
	) );

	foreach($days as $day)
	{
		$layoutPanel->createOption( array(
			'name' => "Tytuł wiadomości - ilość dni od wypełnienia ankiety - $day ",
			'id' => 'email_no_payment_days_'.$day,
			'type' => 'text',
			'default' => 'Przypomienie - revita',
		) );

		$layoutPanel->createOption( array(
			'name' => "Zawartość wiadomości - ilość dni od wypełnienia ankiety - $day ",
			'id' => 'email_no_payment_days_content_'.$day,
			'type' => 'editor',
			'default' => 'Pamiętaj aby otrzymać indywidualną dietę - opłać abonament.',
		) );


	}

	$layoutPanel->createOption( array(
		'type' => 'save',
		'save' => 'Zapisz zmiany',
		'reset' => 'Reset ustawień'
	) );

	/**
	 * Email - nie ma wypełnionej ankiety i zakupionej subskrypcji
	 */
	$layoutPanel = $mainAdminPanel->createTab( array(
		'name' => 'Email - nie ma  wypełnionej ankiety i zakupionej subskrypcji ',
		'id' => sanitize_title('Email - nie ma  wypełnionej ankiety i zakupionej subskrypcji')
	) );

	foreach($days as $day)
	{
		$layoutPanel->createOption( array(
			'name' => "Tytuł wiadomości - ilość dni od stworzenia konta - $day ",
			'id' => 'email_no_everything_days_'.$day,
			'type' => 'text',
			'default' => 'Przypomienie - revita',
		) );

		$layoutPanel->createOption( array(
			'name' => "Zawartość wiadomości - ilość dni od stworzenia konta - $day ",
			'id' => 'email_no_everything_days_content_'.$day,
			'type' => 'editor',
			'default' => 'Pamiętaj aby otrzymać indywidualną dietę - wypełnij ankietę i opłać abonament.',
		) );


	}

	$layoutPanel->createOption( array(
		'type' => 'save',
		'save' => 'Zapisz zmiany',
		'reset' => 'Reset ustawień'
	) );


	$layoutPanel = $mainAdminPanel->createTab( array(
		'name' => 'Inne',
		'id' => sanitize_title('inne')
	) );

	$layoutPanel->createOption( array(
		'name' => 'Tekst potwierdzenia ankiety',
		'id' => 'survey_accept',
		'type' => 'textarea',
		'default'=> 'PAMIĘTAJ, DO MOMENTU WYSŁANIA ANKIETY DIETETYKOWI, MOŻESZ JĄ  MODYFIKOWAĆ W DOWOLNYM MOMENCIE JAK RÓWNIEŻ DOŁĄCZAĆ NOWE SKANY BADAŃ. PO WYSŁANIU ANKIETY NIE BĘDZIESZ MIAŁ JUŻ TAKIEJ MOŻLIWOŚCI. UPEWNIJ SIĘ, ŻE PODAŁEŚ WSZYSTKIE NIEZBĘDNE INFORMACJE ORAZ UMIEŚCIŁEŚ AKTUALNE WYNIKI BADAŃ.'
	) );

	$layoutPanel->createOption( array(
		'name' => 'Tekst komunikatu potwierdzenia ankiety cząstkowej - dla nieopłaconego konta',
		'id' => 'partial_survey_accept_unpaid',
		'type' => 'textarea',
		'default'=> 'Ankieta cząstkowa została zapisana i zostanie przesłana do lekarza po przedłużeniu abonamentu.'
	) );

	$layoutPanel->createOption( array(
		'name' => 'Tekst komunikatu potwierdzenia ankiety cząstkowej',
		'id' => 'partial_survey_accept',
		'type' => 'textarea',
		'default'=> 'Ankieta cząstkowa została wysłana do lekarza, Twoje preferencje zostaną uwzględnione przy układaniu kolejnego etapu diety.'
	) );

	$layoutPanel->createOption( array(
		'name' => 'Tekst komunikatu po zatwierdzeniu ankiety dla nieopłaconego konta',
		'id' => 'post_survey_text_unpaid_account',
		'type' => 'textarea',
		'default'=> 'Ankieta została wysłana do lekarza'
	) );
	$layoutPanel->createOption( array(
		'name' => 'Tekst komunikatu po opłaceniu konta pierwszy raz po wypełnieniu ankiety',
		'id' => 'post_pay_account_first_time',
		'type' => 'textarea',
		'default'=> 'Dziękujemy za dokonanie płatności. Ankieta została przesłana do lekarza. Gdy dieta będzie gotowa dostanie Pan/i wiadomość e-mail'
	) );
	$layoutPanel->createOption( array(
		'name' => 'Tekst komunikatu po opłaceniu konta pierwszy raz przed wypełnieniem ankiety',
		'id' => 'post_pay_account_first_time_pre_survey',
		'type' => 'textarea',
		'default'=> 'Dziekujemy za dokonanie płatności. aby otrzymać diete wypełnij ankiete'
	) );

	$layoutPanel->createOption( array(
		'name' => 'Tekst komunikatu po opłaceniu konta',
		'id' => 'post_pay_account_text',
		'type' => 'textarea',
		'default'=> 'Dziękujemy za dokonanie płatności.'
	) );

	$layoutPanel->createOption( array(
		'name' => 'Tekst komunikatu po zatwierdzeniu ankiety dla opłaconego konta',
		'id' => 'post_survey_text_paid_account',
		'type' => 'textarea',
		'default'=> 'Ankieta została wysłana do lekarza'
	) );


	$layoutPanel->createOption( array(
		'type' => 'save',
		'save' => 'Zapisz zmiany',
		'reset' => 'Reset ustawień'
	) );

}