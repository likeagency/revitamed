<?php

function register_sf_jednostki()
{
	$labels = array(
		'name' => _x('Jednostki', 'evg'),
		'singular_name' => _x('Jednostki', 'evg'),
		'add_new' => _x('Dodaj', 'evg'),
		'add_new_item' => _x('Dodaj', 'evg'),
		'edit_item' => _x('Edytuj', 'evg'),
		'new_item' => _x('Nowy', 'evg'),
		'view_item' => _x('Zobacz', 'evg'),
		'search_items' => _x('Szukaj', 'evg'),
		'not_found' => _x('Brak obiektów', 'evg'),
		'not_found_in_trash' => _x('Brak obiektów w koszu', 'evg'),
		'parent_item_colon' => _x('Rodzic:', 'evg'),
		'menu_name' => _x('Jednostki', 'evg'),
	);

	$args = array(
		'labels' => $labels,
		'hierarchical' => false,
		'supports' => array('title'),
		'public' => false,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 28,
		'menu_icon' => 'dashicons-chart-bar',
		'show_in_nav_menus' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => false,
		'has_archive' => false,
		'query_var' => false,
		'can_export' => true,
		'rewrite' => true,
		'capability_type' => 'post'
	);

	register_post_type('jednostki', $args);

}
add_action( 'init', 'register_sf_jednostki' );

function register_sf_protocol()
{
    $labels = array(
        'name' => _x('Protokoły żywieniowe', 'revita'),
        'singular_name' => _x('Protokoły żywieniowe', 'revita'),
        'add_new' => _x('Dodaj', 'revita'),
        'add_new_item' => _x('Dodaj', 'revita'),
        'edit_item' => _x('Edytuj', 'revita'),
        'new_item' => _x('Nowy', 'revita'),
        'view_item' => _x('Zobacz', 'revita'),
        'search_items' => _x('Szukaj', 'revita'),
        'not_found' => _x('Brak obiektów', 'revita'),
        'not_found_in_trash' => _x('Brak obiektów w koszu', 'revita'),
        'parent_item_colon' => _x('Rodzic:', 'revita'),
        'menu_name' => _x('Protokoły żywieniowe', 'revita'),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => array('title', 'page-attributes', 'thumbnail' , 'editor'),
        'public' => false,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 28,
        'menu_icon' => 'dashicons-images-alt',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => true,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );

    register_post_type('protokol', $args);


}
add_action( 'init', 'register_sf_protocol' );

function register_sf_user()
{
    $labels = array(
        'name' => _x('Konta klientów', 'revita'),
        'singular_name' => _x('Konta klientów', 'revita'),
        'add_new' => _x('Dodaj', 'revita'),
        'add_new_item' => _x('Dodaj', 'revita'),
        'edit_item' => _x('Edytuj', 'revita'),
        'new_item' => _x('Nowy', 'revita'),
        'view_item' => _x('Zobacz', 'revita'),
        'search_items' => _x('Szukaj', 'revita'),
        'not_found' => _x('Brak obiektów', 'revita'),
        'not_found_in_trash' => _x('Brak obiektów w koszu', 'revita'),
        'parent_item_colon' => _x('Rodzic:', 'revita'),
        'menu_name' => _x('Konta klientów', 'revita'),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => array('title'),
        'public' => false,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 28,
        'menu_icon' => 'dashicons-businessman',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => true,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );

    register_post_type('user_client', $args);


}
add_action( 'init', 'register_sf_user' );

function register_measurement()
{
    $labels = array(
        'name' => _x('Historia pomiarów', 'revita'),
        'singular_name' => _x('Historia pomiarów', 'revita'),
        'add_new' => _x('Dodaj', 'revita'),
        'add_new_item' => _x('Dodaj', 'revita'),
        'edit_item' => _x('Edytuj', 'revita'),
        'new_item' => _x('Nowy', 'revita'),
        'view_item' => _x('Zobacz', 'revita'),
        'search_items' => _x('Szukaj', 'revita'),
        'not_found' => _x('Brak obiektów', 'revita'),
        'not_found_in_trash' => _x('Brak obiektów w koszu', 'revita'),
        'parent_item_colon' => _x('Rodzic:', 'revita'),
        'menu_name' => _x('Historia pomiarów', 'revita'),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => array('title'),
        'public' => false,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 28,
        'menu_icon' => 'dashicons-chart-line',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => true,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );

    register_post_type('measurement', $args);


}
add_action( 'init', 'register_measurement' );

function register_doc()
{
    $labels = array(
        'name' => _x('Dietetyk / Lekarz', 'revita'),
        'singular_name' => _x('Dietetyk / Lekarz', 'revita'),
        'add_new' => _x('Dodaj', 'revita'),
        'add_new_item' => _x('Dodaj', 'revita'),
        'edit_item' => _x('Edytuj', 'revita'),
        'new_item' => _x('Nowy', 'revita'),
        'view_item' => _x('Zobacz', 'revita'),
        'search_items' => _x('Szukaj', 'revita'),
        'not_found' => _x('Brak obiektów', 'revita'),
        'not_found_in_trash' => _x('Brak obiektów w koszu', 'revita'),
        'parent_item_colon' => _x('Rodzic:', 'revita'),
        'menu_name' => _x('Dietetyk / Lekarz', 'revita'),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => array('title' , 'thumbnail'),
        'public' => false,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 28,
        'menu_icon' => 'dashicons-id',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => true,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );

    register_post_type('doc', $args);


}
add_action( 'init', 'register_doc' );

function register_ankieta()
{
    $labels = array(
        'name' => _x('Ankieta', 'revita'),
        'singular_name' => _x('Ankieta', 'revita'),
        'add_new' => _x('Dodaj', 'revita'),
        'add_new_item' => _x('Dodaj', 'revita'),
        'edit_item' => _x('Edytuj', 'revita'),
        'new_item' => _x('Nowy', 'revita'),
        'view_item' => _x('Zobacz', 'revita'),
        'search_items' => _x('Szukaj', 'revita'),
        'not_found' => _x('Brak obiektów', 'revita'),
        'not_found_in_trash' => _x('Brak obiektów w koszu', 'revita'),
        'parent_item_colon' => _x('Rodzic:', 'revita'),
        'menu_name' => _x('Ankieta', 'revita'),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => array('title'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 28,
        'menu_icon' => 'dashicons-list-view',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => true,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );

    register_post_type('ankieta', $args);


}
add_action( 'init', 'register_ankieta' );

function register_diet()
{
    $labels = array(
        'name' => _x('Dieta', 'revita'),
        'singular_name' => _x('Dieta', 'revita'),
        'add_new' => _x('Dodaj', 'revita'),
        'add_new_item' => _x('Dodaj', 'revita'),
        'edit_item' => _x('Edytuj', 'revita'),
        'new_item' => _x('Nowy', 'revita'),
        'view_item' => _x('Zobacz', 'revita'),
        'search_items' => _x('Szukaj', 'revita'),
        'not_found' => _x('Brak obiektów', 'revita'),
        'not_found_in_trash' => _x('Brak obiektów w koszu', 'revita'),
        'parent_item_colon' => _x('Rodzic:', 'revita'),
        'menu_name' => _x('Dieta', 'revita'),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => array('title'),
        'public' => false,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 28,
        'menu_icon' => 'dashicons-media-spreadsheet',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => true,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' =>  array( 'slug' => 'dieta' ),
        'capability_type' => 'post'
    );

    register_post_type('diet', $args);
    $labels = array(
      'name'              => _x( 'Etap diety', 'taxonomy general name' ),
      'singular_name'     => _x( 'Etap diety', 'taxonomy singular name' ),
      'search_items'      => __( 'Szukaj' ),
      'all_items'         => __( 'Wszystkie' ),
      'parent_item'       => __( 'Nadrzędny' ),
      'parent_item_colon' => __( 'Nadrzędny:' ),
      'edit_item'         => __( 'Edytuj' ),
      'update_item'       => __( 'Zapisz' ),
      'add_new_item'      => __( 'Dodaj' ),
      'new_item_name'     => __( 'Nazwa' ),
      'menu_name'         => __( 'Etap diety' ),
    );
    $args = array(
      'hierarchical'      => true,
      'labels'            => $labels,
      'show_ui'           => true,
      'show_admin_column' => true,
      'query_var'         => false,
    );

  register_taxonomy('etap_diety',array('diet'),$args);
  $labels = array(
      'name'              => _x( 'Długość pełnej diety', 'taxonomy general name' ),
      'singular_name'     => _x( 'Długość pełnej diety', 'taxonomy singular name' ),
      'search_items'      => __( 'Szukaj' ),
      'all_items'         => __( 'Wszystkie' ),
      'parent_item'       => __( 'Nadrzędny' ),
      'parent_item_colon' => __( 'Nadrzędny:' ),
      'edit_item'         => __( 'Edytuj' ),
      'update_item'       => __( 'Zapisz' ),
      'add_new_item'      => __( 'Dodaj' ),
      'new_item_name'     => __( 'Nazwa' ),
      'menu_name'         => __( 'Długość pełnej diety' ),
    );
    $args = array(
      'hierarchical'      => true,
      'labels'            => $labels,
      'show_ui'           => true,
      'show_admin_column' => true,
      'query_var'         => false,
    );

  register_taxonomy('dl_diety',array('diet'),$args);

}
add_action( 'init', 'register_diet' );

function register_r_diet()
{
    $labels = array(
        'name' => _x('Realizacja diety', 'revita'),
        'singular_name' => _x('Realizacja diety', 'revita'),
        'add_new' => _x('Dodaj', 'revita'),
        'add_new_item' => _x('Dodaj', 'revita'),
        'edit_item' => _x('Edytuj', 'revita'),
        'new_item' => _x('Nowy', 'revita'),
        'view_item' => _x('Zobacz', 'revita'),
        'search_items' => _x('Szukaj', 'revita'),
        'not_found' => _x('Brak obiektów', 'revita'),
        'not_found_in_trash' => _x('Brak obiektów w koszu', 'revita'),
        'parent_item_colon' => _x('Rodzic:', 'revita'),
        'menu_name' => _x('Realizacja diety', 'revita'),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => array('title'),
        'public' => false,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 28,
        'menu_icon' => 'dashicons-clipboard',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => true,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );

    register_post_type('realization_diet', $args);


}
add_action( 'init', 'register_r_diet' );

function register_template_diet()
{
	$labels = array(
		'name' => _x('Szablon diety', 'revita'),
		'singular_name' => _x('Szablon diety', 'revita'),
		'add_new' => _x('Dodaj', 'revita'),
		'add_new_item' => _x('Dodaj', 'revita'),
		'edit_item' => _x('Edytuj', 'revita'),
		'new_item' => _x('Nowy', 'revita'),
		'view_item' => _x('Zobacz', 'revita'),
		'search_items' => _x('Szukaj', 'revita'),
		'not_found' => _x('Brak obiektów', 'revita'),
		'not_found_in_trash' => _x('Brak obiektów w koszu', 'revita'),
		'parent_item_colon' => _x('Rodzic:', 'revita'),
		'menu_name' => _x('Szablon diety', 'revita'),
	);

	$args = array(
		'labels' => $labels,
		'hierarchical' => false,
		'supports' => array('title'),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 28,
		'menu_icon' => 'dashicons-list-view',
		'show_in_nav_menus' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => true,
		'has_archive' => false,
		'query_var' => true,
		'can_export' => true,
		'rewrite' => [
			'slug' => 'jadlospis',
			'with_front' => true,
		],
		'capability_type' => 'post'
	);

	register_post_type('template_diet', $args);


}
add_action( 'init', 'register_template_diet' );

function register_msg()
{
  $labels = array(
    'name' => _x('Wiadomości', 'revita'),
    'singular_name' => _x('Wiadomości', 'revita'),
    'add_new' => _x('Dodaj', 'revita'),
    'add_new_item' => _x('Dodaj', 'revita'),
    'edit_item' => _x('Edytuj', 'revita'),
    'new_item' => _x('Nowy', 'revita'),
    'view_item' => _x('Zobacz', 'revita'),
    'search_items' => _x('Szukaj', 'revita'),
    'not_found' => _x('Brak obiektów', 'revita'),
    'not_found_in_trash' => _x('Brak obiektów w koszu', 'revita'),
    'parent_item_colon' => _x('Rodzic:', 'revita'),
    'menu_name' => _x('Wiadomości', 'revita'),
  );

  $args = array(
    'labels' => $labels,
    'hierarchical' => false,
    'supports' => array('title'),
    'public' => false,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 28,
    'menu_icon' => 'dashicons-email',
    'show_in_nav_menus' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => true,
    'has_archive' => false,
    'query_var' => true,
    'can_export' => true,
    'rewrite' => true,
    'capability_type' => 'post'
  );

  register_post_type('msg', $args);


}
add_action( 'init', 'register_msg' );

function register_cennik()
{
  $labels = array(
    'name' => _x('Cennik', 'revita'),
    'singular_name' => _x('Cennik', 'revita'),
    'add_new' => _x('Dodaj', 'revita'),
    'add_new_item' => _x('Dodaj', 'revita'),
    'edit_item' => _x('Edytuj', 'revita'),
    'new_item' => _x('Nowy', 'revita'),
    'view_item' => _x('Zobacz', 'revita'),
    'search_items' => _x('Szukaj', 'revita'),
    'not_found' => _x('Brak obiektów', 'revita'),
    'not_found_in_trash' => _x('Brak obiektów w koszu', 'revita'),
    'parent_item_colon' => _x('Rodzic:', 'revita'),
    'menu_name' => _x('Cennik', 'revita'),
  );

  $args = array(
    'labels' => $labels,
    'hierarchical' => false,
    'supports' => array('title'),
    'public' => false,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 28,
    'menu_icon' => 'dashicons-cart',
    'show_in_nav_menus' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => true,
    'has_archive' => false,
    'query_var' => true,
    'can_export' => true,
    'rewrite' => true,
    'capability_type' => 'post'
  );

  register_post_type('cennik', $args);


}
add_action( 'init', 'register_cennik' );

function register_faq()
{
  $labels = array(
    'name' => _x('FAQ', 'revita'),
    'singular_name' => _x('FAQ', 'revita'),
    'add_new' => _x('Dodaj', 'revita'),
    'add_new_item' => _x('Dodaj', 'revita'),
    'edit_item' => _x('Edytuj', 'revita'),
    'new_item' => _x('Nowy', 'revita'),
    'view_item' => _x('Zobacz', 'revita'),
    'search_items' => _x('Szukaj', 'revita'),
    'not_found' => _x('Brak obiektów', 'revita'),
    'not_found_in_trash' => _x('Brak obiektów w koszu', 'revita'),
    'parent_item_colon' => _x('Rodzic:', 'revita'),
    'menu_name' => _x('FAQ', 'revita'),
  );

  $args = array(
    'labels' => $labels,
    'hierarchical' => false,
    'supports' => array('title' , 'editor' , 'page-attributes'),
    'public' => false,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 28,
    'menu_icon' => 'dashicons-groups',
    'show_in_nav_menus' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => true,
    'has_archive' => false,
    'query_var' => true,
    'can_export' => true,
    'rewrite' => true,
    'capability_type' => 'post'
  );

  register_post_type('faq', $args);


  $labels = array(
    'name'              => _x( 'Kategoria', 'taxonomy general name' ),
    'singular_name'     => _x( 'Kategoria', 'taxonomy singular name' ),
    'search_items'      => __( 'Szukaj' ),
    'all_items'         => __( 'Wszystkie' ),
    'parent_item'       => __( 'Nadrzędny' ),
    'parent_item_colon' => __( 'Nadrzędny:' ),
    'edit_item'         => __( 'Edytuj' ),
    'update_item'       => __( 'Zapisz' ),
    'add_new_item'      => __( 'Dodaj' ),
    'new_item_name'     => __( 'Nazwa' ),
    'menu_name'         => __( 'Kategoria' ),
  );
  $args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => false,
  );

  register_taxonomy('kategoria',array('faq'),$args);

}
add_action( 'init', 'register_faq' );


function register_custom_files()
{
	$labels = array(
		'name' => _x('Pliki od użytkownika', 'revita'),
		'singular_name' => _x('Plik od użytkownika', 'revita'),
		'add_new' => _x('Dodaj', 'revita'),
		'add_new_item' => _x('Dodaj', 'revita'),
		'edit_item' => _x('Edytuj', 'revita'),
		'new_item' => _x('Nowy', 'revita'),
		'view_item' => _x('Zobacz', 'revita'),
		'search_items' => _x('Szukaj', 'revita'),
		'not_found' => _x('Brak obiektów', 'revita'),
		'not_found_in_trash' => _x('Brak obiektów w koszu', 'revita'),
		'parent_item_colon' => _x('Rodzic:', 'revita'),
		'menu_name' => _x('Pliki od użytkownika', 'revita'),
	);

	$args = array(
		'labels' => $labels,
		'hierarchical' => false,
		'supports' => array('title' ),
		'public' => false,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 28,
		'menu_icon' => 'dashicons-media-interactive',
		'show_in_nav_menus' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => true,
		'has_archive' => false,
		'query_var' => true,
		'can_export' => true,
		'rewrite' => true,
		'capability_type' => 'post'
	);

	register_post_type('custom_file', $args);

}
add_action( 'init', 'register_custom_files' );

function register_cennik_doc()
{
	$labels = array(
		'name' => _x('Cennik komunikacji z lekarzem', 'revita'),
		'singular_name' => _x('Cennik komunikacji z lekarzem', 'revita'),
		'add_new' => _x('Dodaj', 'revita'),
		'add_new_item' => _x('Dodaj', 'revita'),
		'edit_item' => _x('Edytuj', 'revita'),
		'new_item' => _x('Nowy', 'revita'),
		'view_item' => _x('Zobacz', 'revita'),
		'search_items' => _x('Szukaj', 'revita'),
		'not_found' => _x('Brak obiektów', 'revita'),
		'not_found_in_trash' => _x('Brak obiektów w koszu', 'revita'),
		'parent_item_colon' => _x('Rodzic:', 'revita'),
		'menu_name' => _x('Cennik komunikacji z lekarzem', 'revita'),
	);

	$args = array(
		'labels' => $labels,
		'hierarchical' => false,
		'supports' => array('title'),
		'public' => false,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 28,
		'menu_icon' => 'dashicons-phone',
		'show_in_nav_menus' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => true,
		'has_archive' => false,
		'query_var' => true,
		'can_export' => true,
		'rewrite' => true,
		'capability_type' => 'post'
	);

	register_post_type('cennik_doc', $args);


}
add_action( 'init', 'register_cennik_doc' );

function register_ankieta_czastkowa()
{
	$labels = array(
		'name' => _x('Ankieta cząstkowa', 'revita'),
		'singular_name' => _x('', 'revita'),
		'add_new' => _x('Dodaj', 'revita'),
		'add_new_item' => _x('Dodaj', 'revita'),
		'edit_item' => _x('Edytuj', 'revita'),
		'new_item' => _x('Nowy', 'revita'),
		'view_item' => _x('Zobacz', 'revita'),
		'search_items' => _x('Szukaj', 'revita'),
		'not_found' => _x('Brak obiektów', 'revita'),
		'not_found_in_trash' => _x('Brak obiektów w koszu', 'revita'),
		'parent_item_colon' => _x('Rodzic:', 'revita'),
		'menu_name' => _x('Ankieta cząstkowa', 'revita'),
	);

	$args = array(
		'labels' => $labels,
		'hierarchical' => false,
		'supports' => array('title'),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 28,
		'menu_icon' => 'dashicons-book-alt',
		'show_in_nav_menus' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => true,
		'has_archive' => false,
		'query_var' => true,
		'can_export' => true,
		'rewrite' => array(
			'slug' => 'czastkowa-ankieta'
		),
		'capability_type' => 'post'
	);

	register_post_type('part_ankieta', $args);


}
add_action( 'init', 'register_ankieta_czastkowa' );