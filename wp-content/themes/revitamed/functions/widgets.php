<?php

add_action( 'widgets_init', create_function( '', 'register_widget("sf_popular_recipes");' ) );
add_action( 'widgets_init', create_function( '', 'register_widget("sf_button_widgets");' ) );

class sf_popular_recipes extends WP_Widget {


    public function __construct() {
        $widget_id = 'sf_popular_recipes';
        $widget_name = 'Widget: popularne przepisy';
        $widget_opt = array('description'=>'Wyświetla popularne przepisy');

        parent::__construct($widget_id, $widget_name, $widget_opt);
    }


    public function widget( $args, $instance ) {
        // outputs the content of the widget

        $sf_args = array(
            'orderby' => 'menu_order',
            'post_type' => 'przepisy',
            'nopaging'    => true,
            'post_status' => 'publish',
            'posts_per_page' => intval($instance['max_post']),
            'meta_query' => array(
                    'relation' => 'AND',
                array(
                        'key'     => 'rm_featured',
                        'value'   => true,
                        'compare' => '='
                    ),
                )
        );
        $posts_przepisy = get_posts($sf_args);
        if(empty($posts_przepisy)) return;
        echo $args['before_widget'];

        echo $args['before_title'] . esc_html($instance['title']) . $args['after_title'];?>
        <div class="popular-posts">


            <?php foreach(get_posts($sf_args) as $przepis){ ?>
                <div class="recommended__item">
                  <a class="recommended__imageHolderLink" href="<?= get_the_permalink($przepis->ID) ?>" rel="bookmark">
                      <?php if (get_the_post_thumbnail_url($przepis->ID,'popular-przepisy-thumb2')) { ?>
                        <img class="recommended__image" src="<?= get_the_post_thumbnail_url($przepis->ID,'popular-przepisy-thumb2') ?>" alt="<?= get_the_title(get_post_thumbnail_id($przepis->ID)) ?>">
                      <?php } else { ?>

                          <img class="recommended__image" src="<?= TEMP_URI ?>/components/vendor/src/placeholder-new.png" alt="<?= get_the_title(get_post_thumbnail_id($przepis->ID)) ?>">

                      <?php } ?>
                    </a>

                    <div class="recommended__title"><?= $przepis->post_title ?></div>
                    <a class="recommended__more" href="<?= get_the_permalink($przepis->ID) ?>" rel="bookmark">ZOBACZ PRZEPIS</a>
                </div>

            <?php } ?>

        </div>

        <?php
        echo $args['after_widget'];
    }


    public function form( $instance ) {

        $my_defaults = array(
            'title' => "Polecane przepisy",
            'max_post' => 3,
        );

        $instance = wp_parse_args( (array) $instance, $my_defaults );
        ?>
        <div class="text-widget-fields">
        <br>
        <label for="<?= $this->get_field_id('title'); ?>">Tytuł</label>
            <br>
        <input value="<?= $instance['title'] ?>" type="text" class="widefat" id="<?= $this->get_field_id('title'); ?>" name="<?= $this->get_field_name('title'); ?>">
        <br>
        <br>
        <label for="<?php echo $this->get_field_id('max_post'); ?>">Maksymalna ilość postów</label><br>
        <input value="<?= $instance['max_post'] ?>" type="number" id="<?php echo $this->get_field_id('max_post'); ?>" name="<?php echo $this->get_field_name('max_post'); ?>">
        <br>
        <br>
        </div>

        <?php
    }


    public function update( $new_instance, $old_instance ) {
        return $new_instance;
    }


}

class sf_button_widgets extends WP_Widget {


    public function __construct() {
        $widget_id = 'sf_button_widgets';
        $widget_name = 'Przycisk - porady';
        $widget_opt = array('description'=>'Wyświetla przycisk');

        parent::__construct($widget_id, $widget_name, $widget_opt);
        add_action('admin_enqueue_scripts', array($this, 'upload_scripts'));
    }

    public function upload_scripts()
    {
        wp_enqueue_media();
        wp_register_script('my_very_own_script',TEMP_URI."/js/upload-media.js",array( 'jquery', 'media-upload', 'media-views'), null, true);
        wp_enqueue_script('my_very_own_script');
    }


    public function widget( $args, $instance ) {
        // outputs the content of the widget
        $color_flag = [
            'Czerwony' => '-shop',
            'Zielony' => '-order'
        ];
        $showBG = (wp_get_attachment_image_url($instance['sf_img_id'],'')!=''); ?>
            <?php if(!empty($instance['link'])){?>
            <div class="buttonWidget__item">
              <a <?= ($instance['new_window']!='') ? 'target="_blank"' : '' ?> href="<?= $instance['link'] ?>" class="buttonWidget__link <?= $color_flag[$instance['color']]  ?>" <?php if($showBG){?>style="background-image: url('<?= wp_get_attachment_image_url($instance['sf_img_id'],'') ?>')"<?php } ?>>
                <div class="buttonWidget__text"><?= $instance['title'] ?></div>
              </a>
            </div>
                <?php }else{ ?>
                  <div class="buttonWidget__item">
                    <div class="buttonWidget__link <?= $color_flag[$instance['color']] ?>" <?php if($showBG){?>style="background-image: url('<?= wp_get_attachment_image_url($instance['sf_img_id'],'') ?>')"<?php } ?>>
                      <div class="buttonWidget__text"><?= $instance['title'] ?></div>
                    </div>
                  </div>
            <?php }



    }


    public function form( $instance ) {

        $my_defaults = array(
                'title' => '',
                'link' => '',
                'new_window' => '',
                'color' => '',
                'sf_img_id' => 31,
                'default-url' => wp_get_attachment_url(31),
                'default-id' => 31,
        );
        $instance = wp_parse_args( (array) $instance, $my_defaults );
        ?>
        <label for="<?= $this->get_field_id('title'); ?>">Tekst na przycisku:</label><br><input value="<?= $instance['title'] ?>" name="<?php echo $this->get_field_name('title'); ?>" id="<?= $this->get_field_id('title'); ?>" type="text"><br>
        <label for="<?= $this->get_field_id('link'); ?>">Link:</label><br><input value="<?= $instance['link'] ?>" name="<?php echo $this->get_field_name('link'); ?>" id="<?= $this->get_field_id('link'); ?>" type="text"><br><br>
        <input <?= ($instance['new_window']=='on') ? 'checked' : '' ?> name="<?php echo $this->get_field_name('new_window'); ?>" id="<?= $this->get_field_id('new_window'); ?>" type="checkbox"><label for="<?= $this->get_field_id('new_window'); ?>">W nowym oknie ?</label><br>
        <label for="<?= $this->get_field_id('color'); ?>">Kolor: </label>
        <select name="<?php echo $this->get_field_name('color'); ?>" id="<?= $this->get_field_id('color'); ?>">
            <option <?= ($instance['color']=='Czerwony') ? 'selected' : '' ?>>Czerwony</option>
            <option <?= ($instance['color']=='Zielony') ? 'selected' : '' ?>>Zielony</option>
        </select><br>

        <a class="button-secondary sf-attachment-add-images" href="#">Zmień obrazek</a>
        <a 	<?php if(($instance['sf_img_id']==$instance['default-id'])) echo 'style="display:none"'; ?> class="button-secondary sf-attachment-remove-images" href="#">Usuń</a>
        <br>
       <input type="text" hidden class="input_id_of_my_image" id="<?php echo $this->get_field_id('sf_img_id'); ?>" name="<?php echo $this->get_field_name('sf_img_id') ?>" value="<?php echo $instance['sf_img_id']; ?>">

        <br><div style="background-color : #F1F1F1; text-align: center;"><img class="input_my_image_widget" data-defaultid="<?php echo $instance['default-id'] ?>" data-default="<?php echo $instance['default-url']?>" style="width:auto;max-width:200px;clear:both; " src="<?php echo wp_get_attachment_url($instance['sf_img_id']);?>" alt="Brak obrazka" ></div>
        <?php
    }


    public function update( $new_instance, $old_instance ) {
        return $new_instance;
    }


}
