<?php

function custom_redirects_for_skladniki() {
    global $wp,$wp_rewrite;
    $wp->add_query_var('skladnik');

    add_rewrite_rule('^skladnik/([^/]+)\/?$','index.php?s=&skladnik=$matches[1]', 'top');
    add_rewrite_rule('^skladnik/([^/]+)/page/([0-9]{1,})/?$','index.php?s=&skladnik=$matches[1]&paged=$matches[2]', 'top');

   // $wp_rewrite->flush_rules(false); //comment on PROD
}
add_action("init", "custom_redirects_for_skladniki");
