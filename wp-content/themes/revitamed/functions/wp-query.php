<?php


function getAllDiagnostyka($nopePost = 0)
{
    $args = array(
        'orderby' => 'menu_order',
        'post_type' => 'badania',
        'nopaging'    => true,
        'post_status' => 'publish',
        'post__not_in' =>  [$nopePost],

    );
    return get_posts($args);
}

function getAllProtocols()
{
    $args = array(
        'orderby' => 'menu_order',
        'post_type' => 'protokol',
        'nopaging'    => true,
        'post_status' => 'publish',

    );
    return get_posts($args);

}
function getAllCatFAQ()
{
  return get_terms( array('kategoria'), array(
    'order' => 'DSC',
  ));
}

function getFAQ($term_slug='')
{
  $args = array(
    'orderby' => 'menu_order',
    'post_type' => 'faq',
    'nopaging'    => true,
    'post_status' => 'publish',

  );
  if($term_slug!='')
  {
    $args['tax_query']= array(
      'relation' => 'AND',
      array(
        'taxonomy' => 'kategoria',
        'field'    => 'slug',
        'terms'    => $term_slug,
        'operator' => 'IN',
      )
    );
  }

  return get_posts($args);
}
function getTodayMeasure($userID)
{
  $result = array ();
  $args = array(
    'orderby' => 'menu_order',
    'post_type' => 'measurement',
    'nopaging'    => true,
    'post_status' => 'publish',
    'year' => date('Y'),
    'monthnum' => date('m'),
    'day' => date('d'),
    'meta_query' => array(
      'relation' => 'AND',
      array(
        'key'     => 'sf_user_relation',
        'value'   => $userID,
        'compare' => '='
      )
    )
  );
  $posts = get_posts($args);
  if(empty($posts))
  {
    $result['user_weight'] = '';
    $result['ob-talia'] = '';
    $result['ob-brzuch'] = '';
    $result['ob-biodra'] = '';
  }
  else
  {
    $id = $posts[0]->ID;
    $result['user_weight'] = get_post_meta($id,'sf_weight',true);
    $result['ob-talia'] = get_post_meta($id,'sf_talia',true);
    $result['ob-brzuch'] = get_post_meta($id,'sf_brzucha',true);
    $result['ob-biodra'] = get_post_meta($id,'sf_biodra',true);
  }

  return $result;
}

function getTodayMeasurePost($userID)
{

  $args = array(
    'orderby' => 'menu_order',
    'post_type' => 'measurement',
    'nopaging'    => true,
    'post_status' => 'publish',
    'year' => date('Y'),
    'monthnum' => date('m'),
    'day' => date('d'),
    'meta_query' => array(
      'relation' => 'AND',
      array(
        'key'     => 'sf_user_relation',
        'value'   => $userID,
        'compare' => '='
      )
    )
  );

  return get_posts($args);
}

/**
 * @param $id int
 *
 * @return array
 */
function getUserSurvey($id)
{
  $args = array(
    'orderby' => 'menu_order',
    'post_type' => 'ankieta',
    'nopaging'    => true,
    'meta_query' => array(
      'relation' => 'AND',
      array(
        'key'     => 'sf_client_name',
        'value'   => $id,
        'compare' => '='
      )
    )
  );
  return get_posts($args);

}

/**
 * @param $id
 * @return null|WP_Post
 */
function getUserActiveDiet($id)
{
  $args = array(
    'orderby' => 'menu_order',
    'post_type' => 'diet',
    'nopaging'    => true,
    'meta_query' => array(
      'relation' => 'AND',
      array(
        'key'     => 'sf_user',
        'value'   => $id,
        'compare' => '='
      ),
      array(
        'key'     => 'sf_current',
        'value'   => 1,
        'compare' => '='
      )
    )
  );
  $posts = get_posts($args);
  return (empty($posts)) ? null : $posts[0];

}

/**
 * @param $idUser
 * @param $time
 *
 * @return int
 * 0 - didn't payed
 * 1 - active account
 * 2 - can send message to doc
 */
function getCurrentUserPlan( $idUser , $time = null) {
	$time = $time === null ? time() : $time;
	$result = 0;
	if ( $idUser == '' ) {
		return $result;
	}

	$plans    = get_post_meta( $idUser, 'sf_subs_history', true );
	$plansDoc = get_post_meta( $idUser, 'sf_subs_doc_history', true );
	if ( is_array( $plans ) ) {
		foreach ( $plans as $plan ) {
			if ( $time >= $plan['date_from']['timestamp'] && $time <= $plan['date_to']['timestamp'] ) {
				$result = 1;
				break;
			}
		}
	}

	if ( is_array( $plansDoc ) && $result === 1 ) {
		foreach ( $plansDoc as $plan ) {
			if ( $time >= $plan['date_from']['timestamp'] && $time <= $plan['date_to']['timestamp'] ) {
				$result = 2;
				break;
			}
		}
	}

	return $result;
}

/**
 * @param $idUser
 * @param null $time
 *
 * @return bool
 */
function canSendDocMsg( $idUser , $time = null) {
	$time = $time === null ? time() : $time;
	$result = false;
	if ( $idUser == '' ) {
		return $result;
	}

	$plansDoc = get_post_meta( $idUser, 'sf_subs_doc_history', true );

	if ( is_array( $plansDoc ) ) {
		foreach ( $plansDoc as $plan ) {
			if ( $time >= $plan['date_from']['timestamp'] && $time <= $plan['date_to']['timestamp'] ) {
				return true;
			}
		}
	}

	return $result;
}
function getCurrentSubPlanID( $idUser , $time = null) {
	$time = $time === null ? time() : $time;
	$result = 0;
	if ( $idUser == '' ) {
		return $result;
	}

	$plans    = get_post_meta( $idUser, 'sf_subs_history', true );
	if ( is_array( $plans ) ) {
		foreach ( $plans as $plan ) {
			if ( $time >= $plan['date_from']['timestamp'] && $time <= $plan['date_to']['timestamp'] ) {
				$result = $plan['cennik'];
				break;
			}
		}
	}


	return $result;
}

/**
 * @param $idUser
 *
 * @return int|mixed
 */
function getUserLastActiveDay($idUser)
{
	if($idUser=='') return 0;

	$maxEnd = 0;

	$plans = get_post_meta($idUser,'sf_subs_history',true);
	if(is_array($plans))
		foreach($plans as $plan)
		{
			$maxEnd = max($maxEnd,$plan['date_to']['timestamp']);
		}

	return $maxEnd;

}
function getUserLastActiveDayPremium($idUser)
{
	if($idUser=='') return 0;

	$maxEnd = 0;

	$plans = get_post_meta($idUser,'sf_subs_doc_history',true);
	if(is_array($plans))
		foreach($plans as $plan)
		{
			$maxEnd = max($maxEnd,$plan['date_to']['timestamp']);
		}

	return $maxEnd;

}

function getSortedDocsByAccessibility()
{
  global $wpdb;
  $doctors = $wpdb->get_results("SELECT wp_p.ID, sum(case when wp_pm.meta_key = 'sf_doc_diet' then 1 else 0 end) counter  from (SELECT wp_posts.* from {$wpdb->posts} as wp_posts INNER JOIN {$wpdb->postmeta} as wp_postmeta ON ID=post_id WHERE meta_key='sf_doc' and meta_value='0' ) AS wp_p LEFT JOIN {$wpdb->postmeta} wp_pm ON wp_p.ID=meta_value GROUP BY wp_p.ID ORDER BY counter ASC");
  return $doctors;

}

/**
 * really unsafe, pls don't use it
 * @param $id
 */
function deleteAllMetaPost($id)
{
  global $wpdb;
  $id = intval($id);

  if($id=='' || $id==0)
      return;


  $wpdb->query($wpdb->prepare("DELETE FROM {$wpdb->postmeta} WHERE post_id=%d",$id));
}

function getAllCennik()
{
  $args = array(
    'orderby' => 'meta_value',
    'post_type' => 'cennik',
    'nopaging'    => true,
    'post_status' => 'publish',
    'meta_key' => 'sf_days',
    'order' => 'ASC',
    'meta_type'=>'NUMERIC'
  );
  return get_posts($args);

}

function getAllDietaryForDoc($idDoc)
{

	$args = array(
		'orderby' => 'menu_order',
		'post_type' => 'doc',
		'nopaging'    => true,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sf_controlling_doc',
				'value'   => $idDoc,
				'compare' => '='
			)
		)
	);
	$array =  array_map(function($postDoc){ return $postDoc->ID; },get_posts($args));
	return $array;
}

/**
 *
 * @param int|array $dietaryID
 *
 * @return array
 */
function getAllClientForDietary($dietaryID)
{

	if(!is_array($dietaryID))
		$dietaryIDs = [$dietaryID];
	else
		$dietaryIDs = $dietaryID;

	$args = array(
		'orderby' => 'menu_order',
		'post_type' => 'user_client',
		'nopaging'    => true,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sf_doc_diet',
				'value'   => $dietaryIDs, // they have survey
				'compare' => 'IN'
			)
		)
	);

	return get_posts($args);

}

function getAllClient()
{

	$args = array(
		'orderby' => 'menu_order',
		'post_type' => 'user_client',
		'nopaging'    => true,
		'fields'    => 'ids',
	);

	return get_posts($args);

}

function getAllDietUser($userID, $order='DESC')
{

	$args = array(
		'orderby' => 'date',
		'order' => $order,
		'post_type' => 'diet',
		'fields' => 'id',
		'nopaging'    => true,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sf_user',
				'value'   => $userID,
				'compare' => '='
			),
		)
	);
	return get_posts($args);
}
function getAllDietUserToShow($userID)
{

	$args = array(
		'orderby' => 'date',
		'order' => 'DESC',
		'post_type' => 'diet',
		'fields' => 'id',
		'nopaging'    => true,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sf_user',
				'value'   => $userID,
				'compare' => '='
			),
			array(
				'key'     => 'sf_active',
				'value'   => 1,
				'compare' => '='
			),
		)
	);
	return get_posts($args);
}
/**
 * @param int/null $id
 * @return int $timestamp
 */
function getLastBeginningProlongingDate($id=null) {

	$plans = get_post_meta( $id, 'sf_subs_history', true );
	$max = 0;
	if ( is_array( $plans ) ) {
		foreach ( $plans as $plan )
			$max = max($max,$plan['date_from']['timestamp']);
	}

		return $max;
}

function getLastDiet($userID)
{
	//it's last because of order by date
	$args = array(
		'orderby' => 'date',
		'order' => 'DESC',
		'post_type' => 'diet',
		'nopaging'    => true,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sf_user',
				'value'   => $userID,
				'compare' => '='
			),
		)
	);
	$posts = get_posts($args);
	return (!empty($posts)) ? $posts[0] : null;
}
function getFirstDiet($userID)
{
	//it's first because of order by date
	$args = array(
		'orderby' => 'date',
		'order' => 'ASC',
		'post_type' => 'diet',
		'nopaging'    => true,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sf_user',
				'value'   => $userID,
				'compare' => '='
			),
		)
	);
	$posts = get_posts($args);
	return (!empty($posts)) ? $posts[0] : null;
}

function getLastStartingDiet($userID)
{
	//it's first because of order by date
	$args = array(
		'orderby' => 'date',
		'order' => 'ASC',
		'post_type' => 'diet',
		'nopaging'    => true,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sf_user',
				'value'   => $userID,
				'compare' => '='
			),
			array(
				'key'     => 'sf_new_diet',
				'value'   => 1,
				'compare' => '='
			),
		)
	);
	$posts = get_posts($args);
	return (!empty($posts)) ? $posts[0] : null;
}

function getUserMeasures($userID)
{
	$args = array(
		'orderby' => 'date',
		'order' => 'DESC',
		'post_type' => 'measurement',
		'nopaging'    => true,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sf_user_relation',
				'value'   => $userID,
				'compare' => '='
			),
		)
	);

	return get_posts($args);
}

function getAllRecipies()
{
	global $wpdb;
	return $wpdb->get_results("SELECT ID,post_title FROM {$wpdb->posts} WHERE post_type='przepisy' AND post_status='publish';",ARRAY_N);
}

/**
 * @param $idING
 *
 * @return array
 */
function findRecipeByMainIngredient($idING)
{
	$results = [];
	$args = array(
		'post_status' => 'publish',
		'post_type' => 'przepisy',
		'nopaging'    => true,
	);
	foreach( get_posts($args) as $recipe)
	{
		$ings = get_post_meta($recipe->ID,'rm_ing',true);
		if(is_array($ings))
			foreach($ings as $ing)
			{
				if($ing['main'] && $ing['ing'] ==$idING)
				{
					$results[]=$recipe->ID;
					break;
				}

			}

	}

	return $results;
}

function getEndedDiets($client)
{

	if(is_object($client))
		$id = $client->ID;
	else
		$id = $client;

	$args = array(
		'post_status' => 'publish',
		'post_type' => 'diet',
		'nopaging'    => true,
		'orderby' => 'date',
		'order' => 'DESC',
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sf_user',
				'value'   => $id,
				'compare' => '='
			),
			array(
				'key'     => 'sf_status',
				'value'   => 3,
				'compare' => '='
			),
		)
	);


	return get_posts($args);
}

/**
 * @param $client WP_Post|int
 *
 * @return WP_Post|null
 */
function getLastEndedOrActiveDiet($client)
{

	if(is_object($client))
		$id = $client->ID;
	else
		$id = $client;

	$args = array(
		'post_status' => 'publish',
		'post_type' => 'diet',
		'nopaging'    => true,
		'orderby' => 'date',
		'order' => 'DESC',
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sf_user',
				'value'   => $id,
				'compare' => '='
			),
			array(
				'relation' => 'OR',
				array(
					'key'     => 'sf_status',
					'value'   => 1,
					'compare' => '='
				),
				array(
					'key'     => 'sf_status',
					'value'   => 3,
					'compare' => '='
				),
			),

		)
	);
	$posts = get_posts($args);

	return (empty($posts)) ? null : $posts[0];
}

function realizationOfDietExists($date,$meal,$diet)
{
	$args = array(
		'post_status' => 'publish',
		'post_type' => 'realization_diet',
		'nopaging'    => true,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sf_date',
				'value'   => strtotime($date),
				'compare' => '='
			),
			array(
				'key'     => 'sf_meal',
				'value'   => intval($meal),
				'compare' => '='
			),
			array(
				'key'     => 'sf_diet',
				'value'   => intval($diet),
				'compare' => '='
			),
		)
	);

	$posts = get_posts($args);
	return !empty($posts);
}

function getAllRealizationByDateUser($date,$dietID)
{
	$args = array(
		'post_status' => 'publish',
		'post_type' => 'realization_diet',
		'nopaging'    => true,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sf_date',
				'value'   => strtotime($date),
				'compare' => '='
			),

			array(
				'key'     => 'sf_diet',
				'value'   => intval($dietID),
				'compare' => '='
			),
		)
	);
	return get_posts($args);
}

/**
 * @param $dietID
 * @param $meal
 * @param $date
 * @return boolean|null
 */
function getMealEaten($dietID,$meal,$date)
{
	$args = array(
		'post_status' => 'publish',
		'post_type' => 'realization_diet',
		'nopaging'    => true,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sf_date',
				'value'   => strtotime($date),
				'compare' => '='
			),

			array(
				'key'     => 'sf_diet',
				'value'   => intval($dietID),
				'compare' => '='
			),
			array(
				'key'     => 'sf_meal',
				'value'   => intval($meal),
				'compare' => '='
			),
		)
	);
	$posts = get_posts($args);
	//print_r_e(new WP_Query($args));

	return empty($posts) ? null : get_post_meta($posts[0]->ID,'sf_eaten',true);
}
function getAllDiet(){
	$args = array(
		'post_status' => 'publish',
		'post_type' => 'diet',
		'nopaging'    => true,
	);

	return  get_posts($args);
}
/**
 * @param $userID
 * @param null $time
 *
 * @return null|WP_Post
 */
function getCurrentActiveDiet($userID,$time=null)
{
	if($time===null)
		$time = strtotime('now');

	$args = array(
		'post_status' => 'publish',
		'post_type' => 'diet',
		'nopaging'    => true,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sf_date_from',
				'value'   => $time,
				'compare' => '<'
			),

			array(
				'key'     => 'sf_date_to',
				'value'   => $time,
				'compare' => '>'
			),
			array(
				'key'     => 'sf_user',
				'value'   => $userID,
				'compare' => '='
			),
		)
	);
	$posts = get_posts($args);
	return  (!empty($posts)) ? $posts[0] : null;
}
function getFutureDiet($userID,$time=null){
	if($time===null)
		$time = strtotime('now');

	$args = array(
		'post_status' => 'publish',
		'post_type' => 'diet',
		'nopaging'    => true,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sf_date_from',
				'value'   => $time,
				'compare' => '>'
			),

			array(
				'key'     => 'sf_date_to',
				'value'   => $time,
				'compare' => '>'
			),
			array(
				'key'     => 'sf_user',
				'value'   => $userID,
				'compare' => '='
			),
		)
	);
	$posts = get_posts($args);
	return  (!empty($posts)) ? $posts[0] : null;
}

function getDietTime($userID,$time=null){
	if($time===null)
		$time = strtotime('now');

	$args = array(
		'post_status' => 'publish',
		'post_type' => 'diet',
		'nopaging'    => true,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sf_date_from',
				'value'   => $time,
				'compare' => '<='
			),

			array(
				'key'     => 'sf_date_to',
				'value'   => $time,
				'compare' => '>='
			),
			array(
				'key'     => 'sf_user',
				'value'   => $userID,
				'compare' => '='
			),
		)
	);
	$posts = get_posts($args);
	//print_r(new WP_Query($args));
	return  (!empty($posts)) ? $posts[0] : null;
}

function getMaybeEndedDiet($userID)
{
	$args = array(
		'post_status' => 'publish',
		'post_type' => 'diet',
		'nopaging'    => true,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sf_user',
				'value'   => $userID,
				'compare' => '='
			),
			array(
				'key'     => 'sf_status',
				'value'   => 4,
				'compare' => '='
			),
		)
	);
	return  get_posts($args);
}
function getAllUsers(){
	$args = array(
		'post_status' => 'publish',
		'post_type' => 'user_client',
		'nopaging'    => true,
	);
	return  get_posts($args);
}
function getFilesFromSection($userID,$section)
{
	$args = array(
		'post_status' => 'publish',
		'post_type' => 'custom_file',
		'nopaging'    => true,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sf_user',
				'value'   => $userID,
				'compare' => '='
			),

			array(
				'key'     => 'sf_section',
				'value'   => $section,
				'compare' => '='
			),
		)
	);
	return get_posts($args);
}
function getAllFiles($userID)
{
	$args = array(
		'post_status' => 'publish',
		'post_type' => 'custom_file',
		'nopaging'    => true,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sf_user',
				'value'   => $userID,
				'compare' => '='
			),

		)
	);
	return get_posts($args);
}

function getAllCennikDoc(){

	$args = array(
		'orderby' => 'meta_value',
		'post_type' => 'cennik_doc',
		'nopaging'    => true,
		'post_status' => 'publish',
		'meta_key' => 'sf_days',
		'order' => 'ASC',
		'meta_type'=>'NUMERIC'
	);
	return get_posts($args);

}

function getDietsTemplates()
{
	$args = array(
		'post_status' => 'publish',
		'post_type' => 'template_diet',
		'nopaging'    => true,

	);
	return get_posts($args);


}

/**
 * @param $userID
 * @param $postStatus
 *
 * @return null|WP_Post
 */
function getLastPartialSurvey($userID,$postStatus='private')
{
	$args = array(
		'post_status' => $postStatus,
		'post_type' => 'part_ankieta',
		'orderby' => 'date',
		'order' => 'DESC',
		'limit'    => 1,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sf_client_name',
				'value'   => $userID,
				'compare' => '='
			),
			array(
				'key'     => 'sf_blocked',
				'compare' => 'NOT EXISTS'
			),
		)
	);
	$posts = get_posts($args);
	return empty($posts) ? null : $posts[0];
}
function getUserPartialSurvey($userID,$postStatus='public')
{
	$args = array(
		'post_status' => $postStatus,
		'post_type' => 'part_ankieta',
		'orderby' => 'date',
		'order' => 'DESC',
		'limit'    => 1,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sf_client_name',
				'value'   => $userID,
				'compare' => '='
			),
		)
	);
	$posts = get_posts($args);
	return $posts;
}

/**
 * @param $dietID
 *
 * @return null|WP_Post
 */
function getPreviousDiet($dietID)
{
	$post = get_post($dietID);
	$userID = get_post_meta($dietID,'sf_user',true);
	$args = array(
		'post_status' => 'publish',
		'post_type' => 'diet',
		'orderby' => 'date',
		'order' => 'DESC',
		'limit'    => 1,
		'date_query' => array(
			array(
				'before'    => $post->post_date,
				'inclusive' => false,
			),
		),
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sf_user',
				'value'   => $userID,
				'compare' => '='
			),
		)
	);
	$posts = get_posts($args);
	return  empty($posts) ? null : $posts[0];
}
function getPreviousDiets($dietID,$inclusive=true)
{
	$post = get_post($dietID);
	$userID = get_post_meta($dietID,'sf_user',true);
	$args = array(
		'post_status' => 'publish',
		'post_type' => 'diet',
		'orderby' => 'date',
		'order' => 'DESC',
		'nopaging' => true,
		'date_query' => array(
			array(
				'before'    => $post->post_date,
				'inclusive' => $inclusive,
			),
		),
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sf_user',
				'value'   => $userID,
				'compare' => '='
			),
		)
	);
	$posts = get_posts($args);
	return $posts;
}

function getPartialSurveyForDiet($dietID,$postStatus = 'private'){

	$previousDiet = getPreviousDiet($dietID);
	if($previousDiet === null) return null;

	$args = array(
		'post_status' => $postStatus,
		'post_type' => 'part_ankieta',
		'orderby' => 'date',
		'order' => 'DESC',
		'limit'    => 1,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sf_related_diet',
				'value'   => $previousDiet->ID,
				'compare' => '='
			),
			array(
				'key'     => 'sf_blocked',
				'compare' => 'NOT EXISTS'
			),

		)
	);
	$posts = get_posts($args);
	return  empty($posts) ? null : $posts[0];
}

function getPartialSurveyForDietLocked ($dietID,$postStatus = 'private'){

	$previousDiet = getPreviousDiet($dietID);
	if($previousDiet === null) return null;

	$args = array(
		'post_status' => $postStatus,
		'post_type' => 'part_ankieta',
		'orderby' => 'date',
		'order' => 'DESC',
		'limit'    => 1,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sf_related_diet',
				'value'   => $previousDiet->ID,
				'compare' => '='
			),

		)
	);
	$posts = get_posts($args);
	return  empty($posts) ? null : $posts[0];
}

function getPartialSurveyForThisDiet($dietID,$postStatus = 'private'){


	$args = array(
		'post_status' => $postStatus,
		'post_type' => 'part_ankieta',
		'orderby' => 'date',
		'order' => 'DESC',
		'limit'    => 1,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sf_related_diet',
				'value'   => $dietID,
				'compare' => '='
			),
			array(
				'key'     => 'sf_blocked',
				'compare' => 'NOT EXISTS'
			),
		)
	);
	$posts = get_posts($args);
	return  empty($posts) ? null : $posts[0];
}

function getUserDieterEmail($userID){
	return get_post_meta(get_post_meta($userID,'doc_diet',true),'sf_email',true);
}

/**
 * @param $userID int
 *
 * @return false|string
 */
function getUserDocEmail($userID)
{
	$docID = get_post_meta($userID,'doc_diet',true);

	$realDoc =  get_post_meta($docID,'controlling_doc',true);

	return get_post_meta($realDoc,'sf_email',true);
}

/**
 * @param int $idUser
 * @param null|int $time
 *
 * @return null|int
 */
function getStartOfSubscription($idUser,$time=null){
	$time = ($time===null)? time() : $time;
	$plans = get_post_meta($idUser,'sf_subs_history',true);
	if(is_array($plans))
	{
		foreach($plans as $plan)
		{
			if( $time >= $plan['date_from']['timestamp'] && $time <= $plan['date_to']['timestamp'] )
				return $plan['date_from']['timestamp'];
		}
	}

	return null;

}

