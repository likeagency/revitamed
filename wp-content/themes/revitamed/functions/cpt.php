<?php

if(! defined( 'ABSPATH' )) return;

function skladniki() {

    $labels = array(
        'name'                  => _x( 'Składniki', 'Post Type General Name', 'like' ),
        'singular_name'         => _x( 'Składnik', 'Post Type Singular Name', 'like' ),
        'menu_name'             => __( 'Składniki', 'like' ),
        'name_admin_bar'        => __( 'Składniki', 'like' ),
        'archives'              => __( 'Składniki Archives', 'like' ),

        'parent_item_colon'     => __( 'Parent Składniki:', 'like' ),
        'all_items'             => __( 'Wszystkie', 'like' ),
        'add_new_item'          => __( 'Dodaj nowy', 'like' ),
        'add_new'               => __( 'Dodaj nowy', 'like' ),
        'new_item'              => __( 'Nowy element', 'like' ),
        'edit_item'             => __( 'Edytuj', 'like' ),
        'update_item'           => __( 'Aktualizuj', 'like' ),
        'view_item'             => __( 'Zobacz Składniki', 'like' ),
        'view_items'            => __( 'Zobacz Składniki', 'like' ),
        'search_items'          => __( 'Search Item', 'like' ),
        'not_found'             => __( 'Not found', 'like' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'like' ),
        'featured_image'        => __( 'Featured Image', 'like' ),
        'set_featured_image'    => __( 'Set featured image', 'like' ),
        'remove_featured_image' => __( 'Remove featured image', 'like' ),
        'use_featured_image'    => __( 'Use as featured image', 'like' ),
        'insert_into_item'      => __( 'Insert into item', 'like' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'like' ),
        'items_list'            => __( 'Items list', 'like' ),
        'items_list_navigation' => __( 'Items list navigation', 'like' ),
        'filter_items_list'     => __( 'Filter items list', 'like' ),
        );
    $args = array(
        'label'                 => __( 'Składnik', 'like' ),
        'description'           => __( 'Post Type Description', 'like' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'page-attributes' ),
        'taxonomies'            => array( 'grupa' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 8,
        'menu_icon'             => 'dashicons-clipboard',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,        
        'exclude_from_search'   => false,
        'publicly_queryable'    => false,
        'capability_type'       => 'page',
        );
    register_post_type( 'skladnik', $args );

}
add_action( 'init', 'skladniki', 0 );

$labels = array(
    'name' => _x( 'Grupa', 'taxonomy general name' ),
    'singular_name' => _x( 'Grupa', 'taxonomy singular name' ),
    'search_items' =>  __( 'Szukaj' ),
    'all_items' => __( 'Wszystkie grupy' ),
    'parent_item' => __( 'Nadrzędne grupa' ),
    'parent_item_colon' => __( 'Nadrzędny grupa:' ),
    'edit_item' => __( 'Edytuj' ),
    'update_item' => __( 'Zaktualizuj' ),
    'add_new_item' => __( 'Dodaj' ),
    'menu_name' => __( 'Grupa' ),
);



register_taxonomy('grupa',array('skladnik'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'query_var' => true,
    'show_admin_column' => true,
  //  'rewrite' => array( 'slug' => 'grupa' ),
));

function lekarz() {

    $labels = array(
        'name'                  => _x( 'Lekarze', 'Post Type General Name', 'like' ),
        'singular_name'         => _x( 'Lekarz', 'Post Type Singular Name', 'like' ),
        'menu_name'             => __( 'Lekarze', 'like' ),
        'name_admin_bar'        => __( 'Lekarze', 'like' ),
        'archives'              => __( 'Lekarz Archives', 'like' ),
        'attributes'            => __( 'Lekarze Attributes', 'like' ),
        'parent_item_colon'     => __( 'Paren tLekarze:', 'like' ),
        'all_items'             => __( 'Wszyscy', 'like' ),
        'add_new_item'          => __( 'Dodaj nowy', 'like' ),
        'add_new'               => __( 'Dodaj nowy', 'like' ),
        'new_item'              => __( 'Nowy element', 'like' ),
        'edit_item'             => __( 'Edytuj', 'like' ),
        'update_item'           => __( 'Aktualizuj', 'like' ),
        'view_item'             => __( 'Zobacz lekarza', 'like' ),
        'view_items'            => __( 'Zobacz lekarzy', 'like' ),
        'search_items'          => __( 'Search Item', 'like' ),
        'not_found'             => __( 'Not found', 'like' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'like' ),
        'featured_image'        => __( 'Featured Image', 'like' ),
        'set_featured_image'    => __( 'Set featured image', 'like' ),
        'remove_featured_image' => __( 'Remove featured image', 'like' ),
        'use_featured_image'    => __( 'Use as featured image', 'like' ),
        'insert_into_item'      => __( 'Insert into item', 'like' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'like' ),
        'items_list'            => __( 'Items list', 'like' ),
        'items_list_navigation' => __( 'Items list navigation', 'like' ),
        'filter_items_list'     => __( 'Filter items list', 'like' ),
        );
    $args = array(
        'label'                 => __( 'Lekarz', 'like' ),
        'description'           => __( 'Post Type Description', 'like' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'author', 'thumbnail', ),
        'taxonomies'            => array( 'post_tag' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-universal-access-alt',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,        
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
        );
    register_post_type( 'lekarz', $args );

}
add_action( 'init', 'lekarz', 0 );

function przepisy() {

    $labels = array(
        'name'                  => _x( 'Przepisy', 'Post Type General Name', 'like' ),
        'singular_name'         => _x( 'Przepis', 'Post Type Singular Name', 'like' ),
        'menu_name'             => __( 'Przepisy', 'like' ),
        'name_admin_bar'        => __( 'Przepisy', 'like' ),
        'archives'              => __( 'Przepis Archives', 'like' ),
        'attributes'            => __( 'Przepisy Attributes', 'like' ),
        'parent_item_colon'     => __( 'Parent Przepis:', 'like' ),
        'all_items'             => __( 'Wszystkie Przepisy', 'like' ),
        'add_new_item'          => __( 'Dodaj nowy', 'like' ),
        'add_new'               => __( 'Dodaj nowy', 'like' ),
        'new_item'              => __( 'Nowy element', 'like' ),
        'edit_item'             => __( 'Edytuj', 'like' ),
        'update_item'           => __( 'Aktualizuj', 'like' ),
        'view_item'             => __( 'Zobacz Przepis', 'like' ),
        'view_items'            => __( 'Zobacz Przepisy', 'like' ),
        'search_items'          => __( 'Search Item', 'like' ),
        'not_found'             => __( 'Not found', 'like' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'like' ),
        'featured_image'        => __( 'Featured Image', 'like' ),
        'set_featured_image'    => __( 'Set featured image', 'like' ),
        'remove_featured_image' => __( 'Remove featured image', 'like' ),
        'use_featured_image'    => __( 'Use as featured image', 'like' ),
        'insert_into_item'      => __( 'Insert into item', 'like' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'like' ),
        'items_list'            => __( 'Items list', 'like' ),
        'items_list_navigation' => __( 'Items list navigation', 'like' ),
        'filter_items_list'     => __( 'Filter items list', 'like' ),
        );
    $args = array(
        'label'                 => __( 'Przepis', 'like' ),
        'description'           => __( 'Post Type Description', 'like' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'author', 'thumbnail', ),
        //'taxonomies'            => array( 'category', 'post_tag' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 6,
        'menu_icon'             => 'dashicons-carrot',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,        
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
        );
    register_post_type( 'przepisy', $args );

    $labels = array(
        'name' => _x( 'Rodzaje', 'taxonomy general name' ),
        'singular_name' => _x( 'Rodzaje', 'taxonomy singular name' ),
        'search_items' =>  __( 'Szukaj Rodzaje' ),
        'all_items' => __( 'Wszystkie Rodzaje' ),
        'parent_item' => __( 'Nadrzędne Rodzaj' ),
        'parent_item_colon' => __( 'Nadrzędny Rodzaj:' ),
        'edit_item' => __( 'Edytuj Rodzaj' ),
        'update_item' => __( 'Zaktualizuj Rodzaj' ),
        'add_new_item' => __( 'Dodaj Rodzaj' ),
        'new_item_name' => __( 'New Rodzaj Name' ),
        'menu_name' => __( 'Rodzaje' ),
        );

    register_taxonomy('rodzaj',array('przepisy'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'rodzaj' ),
        ));

}
add_action( 'init', 'przepisy', 0 );
