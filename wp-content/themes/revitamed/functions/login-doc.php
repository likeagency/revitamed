<?php
define('USER_CLASS_DOC','doc');
class userDoc
{
    private $_login;
    private $_id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->_login;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        if ($this->getId() == $_SESSION['user'] && $_SESSION['user'] !== NULL && $_SESSION['agent'] == $_SERVER['HTTP_USER_AGENT'])
            return true;
        else
            return false;
    }


    function __construct()
    {
        $id = $_SESSION['user'];
        if (get_post_type($id) == USER_CLASS_DOC) {
            $this->_id = ($id);
            $this->regenerate();
        }

    }
    public function isDoc(){
	    return get_post_meta($this->getId(),'sf_doc',true) == 1;
    }

    private function regenerate()
    {
        $this->_login = (get_post_meta($this->_id, 'sf_email', true));
    }
    private function regenerateSession()
    {
        $this->regenerate();
        $_SESSION['user'] = $this->_id;
        $_SESSION['agent'] = $_SERVER['HTTP_USER_AGENT'];
    }

    /**
     * @param $login string
     * @param $password string
     * @return bool
     */
    public function logon($login, $password)
    {
        if ($login == '' || $password == '') return false;

        $the_query = new WP_Query(
            array(
                'post_status' => 'publish',
                'post_type' => USER_CLASS_DOC,
                'meta_query' => array(
                    'relation' => 'AND',
                    array(
                        'key' => 'sf_email',
                        'value' => $login,
                        'compare' => '=',
                    ),
                ),
            )
        );
        if ($the_query->post_count == 1 && get_post_meta($the_query->posts[0]->ID, 'sf_password_hash', true) == create_hash_password($password, get_post_meta($the_query->posts[0]->ID, 'sf_salt', true))) {
            $this->_id = $the_query->posts[0]->ID;
            $this->regenerateSession();
            return true;
        }
        return false;


    }

    public function logon_no_password($login)
    {
        $the_query = new WP_Query(
            array(
                'post_status' => 'publish',
                'post_type' => USER_CLASS_DOC,
                'meta_query' => array(
                    'relation' => 'AND',
                    array(
                        'key' => 'sf_email',
                        'value' => $login,
                        'compare' => '=',
                    ),
                ),
            )
        );
        if ($the_query->post_count == 1) {
            $this->_id = $the_query->posts[0]->ID;
            $this->regenerateSession();
            return true;
        }
        return false;
    }


    public function logout()
    {
        unset ($_SESSION['user']);
        unset ($_SESSION['agent']);
    }
}


function login_user_doc()
{
    $user_fiberFox = new userDoc;
    $user_fiberFox->logon($_POST['user_email'], $_POST['user_pass']);
    if ($user_fiberFox->isActive()) {
        if($_GET['redirect']=='')
        {
            wp_redirect(home_url('/panel-dietetyka/'), 302);
        }
        else
            wp_redirect($_GET['redirect'], 302);
        die();
    } else
        return false;

}

function doc_page($alsoAdmin = false)
{
	$user_fiberFox = new userDoc;
	if (!$user_fiberFox->isActive() && !$alsoAdmin) {
		wp_redirect(home_url('/dietetyk/').'?redirect='.get_the_permalink());
	}
}

function logout_account_doc()
{
    if (isset($_GET['logout']) && $_GET['logout'] == 'true') {
        $user = new userDoc();
        $user->logout();
        wp_redirect(home_url('/'), 301);
        exit;
    }

}
add_action('wp_loaded', 'logout_account_doc');

class ActionHookEditPasswordDoc {
    public function __construct(){
        add_action( 'save_post', array( $this, 'save_post' ) );

    }

    public function save_post( $post_id ) {

        if ( USER_CLASS_DOC == get_post_type($post_id)) {

            if ( get_post_meta( $post_id, 'sf_password_admin_create', true ) != '' ) {
                $password = get_post_meta( $post_id, 'sf_password_admin_create', true );
                update_post_meta( $post_id, 'sf_password_admin_create', '' );
                update_post_meta( $post_id, 'sf_password_hash', create_hash_password( $password,get_post_meta($post_id,'sf_salt',true) ) );
                add_filter( 'redirect_post_location', array( $this, 'add_notice_query_var' ), 99 );
            }
        }

    }

    public function add_notice_query_var( $location ) {
        remove_filter( 'redirect_post_location', array( $this, 'add_notice_query_var' ), 99 );
        return add_query_arg( array( 'password_create' => 'true' ), $location );
    }


}

function reset_password_for_doc()
{
	$titan = TitanFramework::getInstance( 'revita' );
	$result = array('result' => null, 'text' => null);
	$email = $_POST['user_email'];



	if (!is_email($email)) {
		$result['result'] = 'error';
		$result['text'] = $titan->getOption('register-wrong-email');
	} elseif (checkIfThereIsDocWithThisEmail($email, false) != 1) {
		$result['result'] = 'error';
		$result['text'] =  $titan->getOption('forgot-password-no-email');
	}
	else{
		$result['result'] = 'success';
		$result['text'] = $titan->getOption('forgot-password-success');
		$id_user = getDocIDafterEmail($email);

		$new_password = generateRandomString(10);
		update_post_meta($id_user, 'sf_password_hash', create_hash_password($new_password, get_post_meta($id_user, 'sf_salt', true)));

		$body = ($titan->getOption('forgot-password-email-content') ).' "' . $new_password . '"';
		$title = $titan->getOption('forgot-password-email-title');
		revitamed_send_mail($email, $body, $title);
	}


	return $result;

}
/**
 * @param $email
 * @param bool $returnBoolean
 * @return bool|int
 */
function checkIfThereIsDocWithThisEmail($email,$returnBoolean=true)
{

	$args        = array(
		'post_type'   => USER_CLASS_DOC,
		'post_status' => array('publish','draft'),
		'nopaging'    => true,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sf_email',
				'value'   => $email,
				'compare' => '='
			)
		)
	);
	$user_list_count = count(get_posts( $args ));
	if($returnBoolean)
	{
		if($user_list_count==0) return false;
		else return true;
	}
	else
		return $user_list_count;

}

/**
 * @param $email string
 * @return int
 */
function getDocIDafterEmail($email)
{
	if(!is_email($email)) return 0;

	$args        = array(
		'post_type'   => USER_CLASS_DOC,
		'post_status' => array('publish','draft'),
		'nopaging'    => true,
		'meta_query' => array(
			'relation' => 'AND',
			array(
				'key'     => 'sf_email',
				'value'   => $email,
				'compare' => '='
			)
		)
	);
	$posts = get_posts($args);
	if(count($posts)==0) return 0;
	else return $posts[0]->ID;
}
function getDocOfDieter($id)
{
	if(get_post_meta($id,'sf_doc',true)==1) return $id;
	else
		return get_post_meta($id,'sf_controlling_doc',true);

}


function getDocName($userID)
{
	return (get_post_type($userID)=='doc') ?  get_post_meta($userID,'sf_name',true) . ' ' . get_post_meta($userID,'sf_surname',true) :  null;
}
//TODO action hook robiacy title dla doca
$really_unique_name_for_sth = new ActionHookEditPasswordDoc();