<?php
if(defined('DOING_AJAX'))
{
	remove_action('admin_init','DPWP_action_duplicate');
}

function updateMeasureAJAX()
{

  $result = [];
  $userRevitamed = new user;
  if($userRevitamed->isActive()) {

  $weight = formatNumberToPolish(floatval(formatNumberToPHP($_REQUEST['user_weight'])),0);
  $talia = intval($_REQUEST['ob-talia']);
  $brzuch = intval($_REQUEST['ob-brzuch']);
  $biodra = intval($_REQUEST['ob-biodra']);


    if($talia==0)  $talia = '';
    if($brzuch==0) $brzuch = '';
    if($biodra==0) $biodra = '';
    if($weight==0) $weight = '';

    $result = updateMeasure($weight,$brzuch,$talia,$biodra);
	  $titan = TitanFramework::getInstance( 'revita' );
    $result['msg'] = $titan->getOption('measure_success');
  }
  else
  {
    header("HTTP/1.1 401 Unauthorized");
    echo "401 Unauthorized";
  }

  echo json_encode($result);
  die();
}
add_action( 'wp_ajax_nopriv_weight_update', 'updateMeasureAJAX' );
add_action( 'wp_ajax_weight_update', 'updateMeasureAJAX' );
add_action( 'wp_ajax_nopriv_circuit_update', 'updateMeasureAJAX' );
add_action( 'wp_ajax_circuit_update', 'updateMeasureAJAX' );

/**
 * @throws Exception
 */
function generateDataPayment()
{
  include 'libs/apiPayU/Util.php';
  $result = array(
    'ts' => strtotime('now'),
    'sig' => '',
    'session_id' => '',
  );

  $userRevita = new user;
	$idOffer = intval($_POST['ID']);
	$idMsgWithDoc = intval($_POST['ID_2']);
	unset($_POST['ID']);
	unset($_POST['ID_2']);



	//TODO create post buy
	//for now just accept it
	if($userRevita->isActive())
	{
		$timeLast = getUserLastActiveDay($userRevita->getId());
		if($timeLast< strtotime('-1 month') && $timeLast!=0)
		{
			update_post_meta($userRevita->getId(),'sf_survey_part',createDraftPartialSurvey($userRevita->getId(),0));
		}

		if($idOffer!=0)
			addUserSub($idOffer);

		if($idMsgWithDoc!=0)
			addUserSubDoc($idMsgWithDoc);

		if(get_post_meta($userRevita->getId(),'sf_first_payment',true)=='' && $idOffer!=0)
			update_post_meta($userRevita->getId(),'sf_first_payment',time());

		if($idOffer!=0)
			foreach([10,6] as $day)
				update_post_meta($userRevita->getId(),'sf_user_needs_to_pay_day_'.$day,false);
	}


  $result['session_id'] = $session_id = generateRandomString(30);
  $ts = $result['ts'];
  $_POST['session_id'] = $session_id;
  $_POST['ts'] = $ts;
  unset($_POST['sig']);

    $result ['sig'] = $sig = ((new OpenPayU_Util)->generateSignData($_POST, 'SHA-256', get_option('pos_id'), get_option('second_key')));


 echo json_encode($result);
 die();
}
add_action( 'wp_ajax_nopriv_getSessionTsPayU', 'generateDataPayment' );
add_action( 'wp_ajax_getSessionTsPayU', 'generateDataPayment' );


function saveCurrentDiet()
{
	$userDoc = new userDoc();
	if($userDoc->isActive())
	{
		if($_POST['form']=='step_2_diet') {
			stepTwoDietForm();
		}
		elseif($_POST['form']=='step_3_diet')
		{
			stepThreeDietForm(false);
		}
		elseif($_POST['form']=='UPDATE_DIET')
		{
			stepTwoDietForm();
			stepThreeDietForm(true,'productTwo');
		}
		elseif($_POST['form']=='sf_template')
		{
			saveTemplateDiet();
		}

		echo json_encode(array(
				'status'=>'OK' )
		);
	}
	else
	{
		header($_SERVER["SERVER_PROTOCOL"]. " 401 Unauthorized");
		echo json_encode(array(
				'status'=>'Not OK', )
		);
	}


	die();
}
add_action( 'wp_ajax_nopriv_saveCurrentDiet', 'saveCurrentDiet' );
add_action( 'wp_ajax_saveCurrentDiet', 'saveCurrentDiet' );


function dietMealConfirmation()
{

	header('Content-Type: json/application');
	$userRevita = new user;

	$typeEaten = array(
			'unconfirmed' => 0,
			'confirmed' => 1,
			'existed' => 2,
	);

	if($userRevita->isActive() && $userRevita->hasActiveSub() && $userRevita->hasActiveDiet())
	{

		$date = $_POST['date'];
		$meal = $_POST['meal'];
		$diet =  getCurrentActiveDiet($userRevita->getId())->ID;
		$eaten = $typeEaten[$_POST['value']];
		if(strtotime('now') < strtotime($date))
		{
			echo json_encode(array(
					'status'=>'Not OK',
					'msg'=> 'Jest za wcześnie na wykonanie tego posiłku')
			);

		}elseif(realizationOfDietExists($date,$meal,$diet))
		{
			echo json_encode(array(
					'status'=>'Not OK',
					'msg' => 'Już istnieje wykonanie tego posiłku')
			);
		}
		else
		{

			//create realization
			$prefix = 'sf_';
			wp_insert_post(array(
				'post_type' => 'realization_diet',
				'post_status' => 'publish',
				'post_title' => $userRevita->getName() . ' ' . date('Y-m-d'),
				'meta_input' => array(
					"{$prefix}date" => strtotime($date),
					"{$prefix}meal" => $meal,
					"{$prefix}diet" => $diet,
					"{$prefix}eaten" => $eaten,
				)
			));
			echo json_encode(array(
					'status'=>'OK',
					'part' => getCurrentPartOfPlan($date,$diet)
					)
			);
		}

	}
	else
	{
		header($_SERVER["SERVER_PROTOCOL"]. " 401 Unauthorized");
		echo json_encode(array(
				'status'=>'Not OK', )
		);
	}

	die();
}
add_action( 'wp_ajax_nopriv_diet_meal_confirmation', 'dietMealConfirmation' );
add_action( 'wp_ajax_diet_meal_confirmation', 'dietMealConfirmation' );

function generateListShopping()
{
    $dataStart = strtotime($_REQUEST['from']);
    $dataStop = strtotime($_REQUEST['to']);
	$userRevita = new user;
	header('Content-Type: application/json');

	if($userRevita->isActive())
	{

		echo json_encode([
			'data' => getNDaysIngOfDietFormat($userRevita->getId(),$dataStart,$dataStop),
			'error' => false
		]);

	}
	else
	{
		header("HTTP/1.1 401 Unauthorized");
		echo json_encode([
			'error'=> true
		]);
	}

	die();

}
add_action( 'wp_ajax_nopriv_generate_list_shopping', 'generateListShopping' );
add_action( 'wp_ajax_generate_list_shopping', 'generateListShopping' );

function generateCalendarUser()
{
	$userRevita = new user;
	$diet = getCurrentActiveDiet($userRevita->getId());
	$diet = ($diet === null) ? getFutureDiet($userRevita->getId()) : $diet;
	$from = $_REQUEST['from'];
	$to = $_REQUEST['to'];
	$full = $_REQUEST['full']==='true' ? true : false;
	header('Content-Type: application/json');
	header("Expires: ".date('l, d M Y H:i:s e',strtotime('+1 day')),true);

	if($userRevita->isActive())
	{


		if(get_post_meta($diet->ID,'sf_user',true)==$userRevita->getId())
		{

			echo json_encode([
				'html' => getCalendarHTML($userRevita->getId(),$from,$to,$full),
				'error' => false
			]);
		}
		else
		{
			header("HTTP/1.1 401 Unauthorized");
			echo json_encode([
				'error'=> true,
				'diet'=> $diet,
			]);
		}
	}
	else
	{
		header("HTTP/1.1 401 Unauthorized");
		echo json_encode([
			'error'=> true
		]);
	}

	die();

}
add_action( 'wp_ajax_nopriv_generate_calendar_user', 'generateCalendarUser' );
add_action( 'wp_ajax_generate_calendar_user', 'generateCalendarUser' );
function generateListShoppingEmail() {


	$userRevita = new user;
	if($userRevita->isActive())
	{
		$email = $userRevita->getLogin();
		$dataStart = strtotime($_POST['from']);
		$dataStop = strtotime($_POST['to']);
		$content = getMailContent($userRevita->getId(),$dataStart,$dataStop);

		$res = revitamed_send_mail($email,$content,'Lista zakupów - Revitadiet',false);
		header('Content-Type: application/json');
		echo json_encode([
			'error' => !$res
		]);
	}
	else
	{
		header("HTTP/1.1 401 Unauthorized");
		echo json_encode(['error'=> true]);
	}
	die();
}
add_action( 'wp_ajax_nopriv_generate_list_shopping_email', 'generateListShoppingEmail' );
add_action( 'wp_ajax_generate_list_shopping_email', 'generateListShoppingEmail' );


function uploadFileUser(){

	$userNebo = new user;
	$section = $_POST['section'];
	if($userNebo->isActive()) {


		$image = ((add_image_from_user($_FILES['file'], $userNebo->getId(),$section)));

		echo json_encode($image);
	}
	else
	{
		header("HTTP/1.1 401 Unauthorized");
		echo "HTTP/1.1 401 Unauthorized";
	}
	die();
}
add_action( 'wp_ajax_nopriv_upload_file_user', 'uploadFileUser' );
add_action( 'wp_ajax_upload_file_user', 'uploadFileUser' );

function removeFileUser(){

	$userNebo = new user;
	$idFile = (int)($_POST['id']);
	if($userNebo->isActive()) {

		if(get_post_type($idFile)=='custom_file')
		{
			if(get_post_meta($idFile,'sf_user',true)==$userNebo->getId()){

				wp_delete_post($idFile,true);
				echo json_encode(['error'=> false]);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
				echo json_encode(['error'=> true]);
			}
		}
		else
		{
			header("HTTP/1.1 401 Unauthorized");
			echo json_encode(['error'=> true]);
		}

	}
	else
	{
		header("HTTP/1.1 401 Unauthorized");
		echo "HTTP/1.1 401 Unauthorized";
	}
	die();


}
add_action( 'wp_ajax_nopriv_remove_file_user', 'removeFileUser' );
add_action( 'wp_ajax_remove_file_user', 'removeFileUser' );
function saveCurrentQuestionnaire()
{
	$_POST['action'] = $_POST['step'];
	saveDataFromFormToSession();
	echo json_encode(['error'=> 'false']);
	die();

}
add_action( 'wp_ajax_nopriv_saveCurrentQuestionnaire', 'saveCurrentQuestionnaire' );
add_action( 'wp_ajax_saveCurrentQuestionnaire', 'saveCurrentQuestionnaire' );


function removeTemplateDiet()
{

	$userDoc = new userDoc();
	if($userDoc->isActive()){
		$id = $_POST['id'];
		wp_delete_post($id);
		echo json_encode(['error'=> 'false']);
	}
	else
	{
		header("HTTP/1.1 401 Unauthorized");
		echo "HTTP/1.1 401 Unauthorized";
	}
	die();

}
add_action( 'wp_ajax_nopriv_removeTemplateDiet', 'removeTemplateDiet' );
add_action( 'wp_ajax_removeTemplateDiet', 'removeTemplateDiet' );


function getUnitAJAX()
{
	header('Content-Type: Application/json');
	$idProduct = (int)$_GET['id'];
	$unitID = get_post_meta($idProduct,'rm_unit',true);

	$unitName = ($unitID=='') ? 'Gram' : get_the_title($unitID);

	echo json_encode(array(
		'text' => $unitName,
	));
	die();

}
add_action( 'wp_ajax_nopriv_getunit', 'getUnitAJAX' );
add_action( 'wp_ajax_getunit', 'getUnitAJAX' );

function getRecipe()
{
	header('Content-Type: Application/json');
	header("Expires: ".date('l, d M Y H:i:s e',strtotime('+1 day')),true);
	header("Vary: Accept-Encoding" );

	$idRecipe = (int)$_GET['id'];
	$custom = get_post_meta($idRecipe,'rm_ing',true);
	$custom = (is_array($custom)) ? $custom : array();
	foreach($custom as $key =>$ing)
	{
		if(get_post_status($ing['ing'])!='publish') {
			unset($custom[$key]);
			continue;
		}
		$unitID = get_post_meta($ing['ing'],'rm_unit',true);

		$unitName = ($unitID=='') ? 'gram' : mb_strtolower(get_the_title($unitID));
		unset($custom[$key]['amount']);
		$custom[$key]['unit'] = $unitName;
		$custom[$key]['name'] = get_the_title($ing['ing']);
	}
	echo json_encode($custom);
	die();

}
add_action( 'wp_ajax_nopriv_get_recipe', 'getRecipe' );
add_action( 'wp_ajax_get_recipe', 'getRecipe' );

function connectWordpressAccounts(){

	foreach(getAllUsers() as $user)
	{
		//echo $user->post_title . ' ';
		fixUserToUserRelation($user->ID);
	}
}
add_action( 'wp_ajax_nopriv_fix_relation_user', 'connectWordpressAccounts' );
add_action( 'wp_ajax_fix_relation_user', 'connectWordpressAccounts' );
