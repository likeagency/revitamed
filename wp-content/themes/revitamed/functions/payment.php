<?php

define('OPTION_STORE', 'PROD');

/**
 * PayU
 */
update_option("pos_id", 'TEST');
update_option("pos_auth_key", 'TEST2');
update_option("second_key", 'TEST23');
update_option("first_key", 'TEST24');


function createFormPayment()
{
  $user = new user;
  $form_creation_table = array(
    'amount' => '',//created in js
    'client_ip' => $_SERVER['REMOTE_ADDR'],
    'desc' => '',
    'email' => $user->getLogin(),
    'first_name' => $user->getUserName(),
    'js' => '1',
    'last_name' => $user->getUserSurname(),
    'pos_auth_key' => get_option('pos_auth_key'),
    'pos_id' => get_option('pos_id'),
    'session_id' => '', //created in js
    'sig' => '', //created in js
    'ts' => '', //created in js
  );


  return $form_creation_table;
}

function addUserSub($idOffer)
{
	$userRevitamed = new user();


	$days = get_post_meta($idOffer,'sf_days',true);
	$subHistory = get_post_meta($userRevitamed->getId(),'sf_subs_history',true);

	if($userRevitamed->hasActiveSub())
	{
		update_post_meta($userRevitamed->getId(),'sf_date_sub',strtotime("+$days days",get_post_meta($userRevitamed->getId(),'sf_date_sub',true)));
		$subHistory[]=[
			'cennik' => $idOffer,
			'date_from' => [
				'timestamp'=> getUserLastActiveDay($userRevitamed->getId()),
				'formatted'=> date('Y-m-d',getUserLastActiveDay($userRevitamed->getId())),
			],
			'date_to' => [
				'timestamp'=> strtotime("+$days days",getUserLastActiveDay($userRevitamed->getId())),
				'formatted'=> date('Y-m-d',strtotime("+$days days",getUserLastActiveDay($userRevitamed->getId()))),
			],
		];
	}
	else
	{
		update_post_meta($userRevitamed->getId(),'sf_date_sub',strtotime("+$days days"));
		$subHistory[]=[
			'cennik' => $idOffer,
			'date_from' => [
				'timestamp' => strtotime('now'),
				'formatted' => date('Y-m-d',strtotime('now')),
			],
			'date_to' => [
				'timestamp' => strtotime("+$days days"),
				'formatted' => date('Y-m-d',strtotime("+$days days")),
			],
		];
	}
	update_post_meta($userRevitamed->getId(),'sf_subs_history',$subHistory);

}

function addUserSubDoc($idOffer)
{
	$userRevitamed = new user();


	$days = get_post_meta($idOffer,'sf_days',true);
	$subHistory = get_post_meta($userRevitamed->getId(),'sf_subs_doc_history',true);
	if(!is_array($subHistory))
		$subHistory = [];

	if($userRevitamed->hasActiveSubPremium())
	{
		update_post_meta($userRevitamed->getId(),'sf_date_sub_doc',strtotime("+$days days",get_post_meta($userRevitamed->getId(),'sf_date_sub_doc',true)));
		$subHistory[]=[
			'cennik' => $idOffer,
			'date_from' => [
				'timestamp'=> getUserLastActiveDayPremium($userRevitamed->getId()),
				'formatted'=> date('Y-m-d',getUserLastActiveDayPremium($userRevitamed->getId())),
			],
			'date_to' => [
				'timestamp'=> strtotime("+$days days",getUserLastActiveDayPremium($userRevitamed->getId())),
				'formatted'=> date('Y-m-d',strtotime("+$days days",getUserLastActiveDayPremium($userRevitamed->getId()))),
			],
		];
	}
	else
	{
		update_post_meta($userRevitamed->getId(),'sf_date_sub_doc',strtotime("+$days days"));
		$subHistory[]=[
			'cennik' => $idOffer,
			'date_from' => [
				'timestamp' => strtotime('now'),
				'formatted' => date('Y-m-d',strtotime('now')),
			],
			'date_to' => [
				'timestamp' => strtotime("+$days days"),
				'formatted' => date('Y-m-d',strtotime("+$days days")),
			],
		];
	}
	update_post_meta($userRevitamed->getId(),'sf_subs_doc_history',$subHistory);

}