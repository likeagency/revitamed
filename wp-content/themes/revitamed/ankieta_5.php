<?php
/**
 * The template for displaying all pages.
 * Template name: ankieta 5
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */
user_page();
$userRevita = new user();
$titan = TitanFramework::getInstance('revita');
saveDataFromFormToSession();
$data = $_SESSION['individualDiagnostic'];
if(count($data)<15)
  wp_redirect(get_the_permalink(get_id_after_template_filename('ankieta_1.php')));

if($data['sf_3_problem_0']=='') $data['sf_3_problem_0']= '1';
if($data['sf_6_problem_0']=='') $data['sf_6_problem_0']= '1';

$resultSurvey = ($_POST['action']=='saveSurvey') ? (saveSurvey()): null;

get_header(); ?>
<div id="primary" class="content-area  container">
  <div class="col-12">
  <div class="wrapper">
    <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
      <?php if (function_exists('bcn_display')) {
          bcn_display();
      } ?>
    </div>
  </div><!-- ./wrapper -->
</div>
  <main id="main" class="site-main bli" role="main">
    <div class="steps">
      <div class="steps__content">
        <a href="<?= get_permalink_template('ankieta_1.php'); ?>" class="steps__item ">
          <div class="steps__counter">Krok 1</div>
          <div class="steps__name">Wywiad ogólny</div>
        </a>
        <a href="<?= get_permalink_template('ankieta_2.php'); ?>" class="steps__item ">
          <div class="steps__counter">Krok 2</div>
          <div class="steps__name">Wywiad zdrowotny</div>
        </a>
        <a href="<?= get_permalink_template('ankieta_3.php'); ?>" class="steps__item">
          <div class="steps__counter">Krok 3</div>
          <div class="steps__name">Wywiad żywieniowy</div>
        </a>
        <a href="<?= get_permalink_template('ankieta_4.php'); ?>" class="steps__item ">
          <div class="steps__counter">Krok 4</div>
          <div class="steps__name">Preferencje żywieniowe</div>
        </a>
        <div class="steps__item -active">
          <div class="steps__counter">Krok 5</div>
          <div class="steps__name">Podsumowanie</div>
        </div>
      </div>
    </div>
    <form class="form" method="post" data-questionnaire-form>
      <div class="form__content">
        <?php viewSurvey($data,false,$userRevita->getId()); ?>
        <div class="form__row -marginBottom" data-group="vat" data-show>
          <?php if($resultSurvey!==null){ ?>
          <div class="registerForm__alert -fail">
            <?= $resultSurvey['msg'] ?>
          </div>
          <?php } ?>
          <div class="form__hr"></div>
          <div class="form__rowTitle">TWOJE DANE</div>
          <div class="form__col25">
            <label class="form__label">Imię<span>*</span></label>
            <input name="user_name" value="<?= esc_attr($userRevita->getUserName()); ?>" required type="text" class="form__input -left">



            <label class="form__label -hideOnNo">Imię (Faktura VAT)</label>
            <input name="user_name_invoice" value="<?= esc_attr($userRevita->getUserNameVAT()); ?>" type="text" class="form__input -left -hideOnNo">
            <label class="form__label -hideOnNo">Nazwisko (Faktura VAT)</label>
            <input name="user_surname_invoice" value="<?= esc_attr($userRevita->getUserSurnameVAT()); ?>" type="text" class="form__input -left -hideOnNo">

          </div>
          <div class="form__col25">
            <label class="form__label">Nazwisko<span>*</span></label>
            <input name="user_surname" value="<?= esc_attr($userRevita->getUserSurname()); ?>"  type="text" class="form__input -left">

            <label class="form__label -hideOnNo">Ulica / Nr. domu (Faktura VAT)</label>
            <input name="user_street" value="<?= esc_attr($userRevita->getUserStreet()); ?>" type="text" class="form__input -left -hideOnNo">
            <label class="form__label -hideOnNo">Miejscowość (Faktura VAT)</label>
            <input name="user_city" value="<?= esc_attr($userRevita->getUserCity()); ?>" type="text" class="form__input -left -hideOnNo">
          </div>
          <div class="form__col25">
            <label class="form__label">Telefon kontaktowy<span>*</span></label>
            <input name="user_phone" required value="<?= esc_attr($userRevita->getUserPhone()); ?>"  type="text" class="form__input -left">

            <label class="form__label -hideOnNo">Kod pocztowy (Faktura VAT)</label>
            <input name="user_zip" value="<?= esc_attr($userRevita->getUserZipCode()); ?>" type="text" class="form__input -left -hideOnNo">
            <label class="form__label -hideOnNo">NIP firmy (Faktura VAT)</label>
            <input name="user_nip" value="<?= esc_attr($userRevita->getUserNipCode()); ?>" type="text" class="form__input -left -hideOnNo">
          </div>
          <div class="form__col25">
            <div class="form__label">Chcesz otrzymać Fakturę VAT?</div>
            <div class="form__checkBoxHolder">
              <input <?= ($userRevita->getUserInvoice())  ? 'checked' : '' ?> id="checkbox_9" type="checkbox" class="form__checkbox" value="1" name="vat" data-item data-toggle>
              <label for="checkbox_9" class="form__label">Tak</label>
            </div>
            <div class="form__checkBoxHolder -m-b-30">
              <input <?= (!$userRevita->getUserInvoice() && $userRevita->isActive() )  ? 'checked' : '' ?> id="checkbox_10" type="checkbox" class="form__checkbox" value="0" name="vat" data-item >
              <label for="checkbox_10" class="form__label">Nie</label>
            </div>
          </div>
          <div><label><input type="checkbox" required><?= $titan->getOption('rodo_clause_1') ?></label></div>
          <div><label><input type="checkbox" required><?= $titan->getOption('rodo_clause_2') ?></label></div>
        </div>
        <div class="form__nav" data-questionnaire-navigation>
          <button class="btn form__saveDiet" data-questionnaire-save>Zapisz ankietę</button>
          <button class="form__button -pay" data-accept="<?= esc_attr($titan->getOption('survey_accept'))?>">Wyślij ankietę</button>
        </div>
      </div>
      <input type="hidden" value="saveSurvey" name="action">
      <input type="hidden" value="saveSurvey" name="step">
      <?php wp_nonce_field( 'post_nonce', 'post_nonce_field' ); ?>
    </form>
  </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer();
