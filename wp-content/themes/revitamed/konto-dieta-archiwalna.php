<?php
/**
 * The template for displaying all pages.
 * Template name: Konto - dieta archiwalna
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */
user_page(false,true);
$userRevitamed = new user();
$userID = $userRevitamed->getId();

$dietID = $_GET['dietID'];
$diet = get_post($dietID);
if(get_post_meta($dietID,'sf_user',true)==$userID)
{
  include 'konto-aktualna-dieta.php';
}
else
{
	trigger404();
}