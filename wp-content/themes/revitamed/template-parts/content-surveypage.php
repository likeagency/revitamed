<div class="container">
<?php
$etapBoxNumber=0;
if( have_rows('survey_content') ):
	while ( have_rows('survey_content') ) : the_row();
?>
<?php if( get_row_layout() == 'survey_block_image' ){  ?>
		<?php
			if( have_rows('survey_photo_repeater') ):
				while ( have_rows('survey_photo_repeater') ) : the_row();
					if(get_sub_field("survey_pozycja_obrazka")==1){

						echo "<div class='container-img clear'>";
						echo "<div class='col-6 pr'><img src='".get_sub_field("survey_repeater_image")['sizes']['misja_repeater_image']."' alt='".get_sub_field("survey_repeater_image")['alt']."'></div>";
						echo "<div class='col-6'>";
						echo "<div class='text-box'>";
						echo "<h6>";
						echo get_sub_field("survey_repeater_title");
						echo "</h6>";
						echo "<h5>";
						echo get_sub_field("survey_repeater_subtitle");
						echo "</h5>";
						echo get_sub_field("survey_repeater_text");
						echo "</div>";
						echo "</div>";
						echo "</div>";

					}else{
						echo "<div class='container-img clear'>";
						echo "<div class='col-6 pl fr'><img src='".get_sub_field("survey_repeater_image")['sizes']['misja_repeater_image']."' alt='".get_sub_field("survey_repeater_image")['alt']."'></div>";
						echo "<div class='col-6 fl'>";
						echo "<div class='text-box'>";
						echo "<h6>";
						echo get_sub_field("survey_repeater_title");
						echo "</h6>";
						echo "<h5>";
						echo get_sub_field("survey_repeater_subtitle");
						echo "</h5>";
						echo get_sub_field("survey_repeater_text");
						echo "</div>";
						echo "</div>";
						echo "</div>";
					}

				endwhile;
			endif;
		?>
<?php }?>

<?php if( get_row_layout() == 'survey_box' ){  ?>
	<div class="col-4">
	<?php
	echo "<div class='survey-box'>";
	echo "<div class='survey-box-top'>";
		echo	"<img src='".get_sub_field("survey_ikona")['sizes']['etapy-ikona']."' alt='".get_sub_field("survey_ikona")['alt']."'>";
	echo "</div>";
	echo "<div class='survey-box-text'>";
		echo get_sub_field("survey_text");
	echo "</div>";
	echo "<div class='survey-box-btm'>";
		foreach (get_sub_field("survey_buttony") as $przycisk2) {
			if(!get_sub_field("link_w_nowym_oknie")){
				echo "<a class='button' href='".$przycisk2['survey_button_link']."' target='_blank'>";
				echo $przycisk2['survey_button_text'];
				echo "</a>";
			}else{
				echo "<a class='button' href='".$przycisk2['survey_button_link']."'>";
				echo $przycisk2['survey_button_text'];
				echo "</a>";
			}
		}
	echo "</div>";
	echo "</div>";
	?>

	</div>
<?php }?>



<?php if( get_row_layout() == 'sciezki' ){  ?>
	<div class="sciezki">
<div class="col-4">
	<div class="sciezka-s">
		<?php if( get_sub_field('sciezka_szybka_tytul1') ): ?>
			<h4><?php the_sub_field('sciezka_szybka_tytul1'); ?></h4>
		<?php endif; ?>
	<?php
	while ( have_rows('sciezka_szybka')) : the_row();
				echo "<div class='sciezka-white-bg'>";
				echo "<img src='".get_sub_field("sciezka_szybka_ikona")[url]."' alt='".get_sub_field("sciezka_szybka_ikona")['alt']."'>";
				echo "<div class='sciezka-box-text'>";
					echo get_sub_field("sciezka_szybka_tytul");
				echo "</div>";
				echo "<div class='sciezka-box-btm'>";
					foreach (get_sub_field("sciezka_szybka_button") as $sciezkaBtn) {
						if($sciezkaBtn["link_w_nowym_oknie"]){
							echo "<a class='button' href='".$sciezkaBtn['sciezka_szybka_btn_link']."' target='_blank'>";
							echo $sciezkaBtn['sciezka_szybka_btn_nazwa'];
							echo "</a>";
						}else{
							echo "<a class='button' href='".$sciezkaBtn['sciezka_szybka_btn_link']."'>";
							echo $sciezkaBtn['sciezka_szybka_btn_nazwa'];
							echo "</a>";
						}
					}
				echo "</div>";
				echo "</div>";

	endwhile;
	?>
	</div>
</div>
<div class="col-8">
	<div class="sciezka-z cf">
		<?php if( get_sub_field('sciezka_zaawansowana_tytul1') ): ?>
			<h4><?php the_sub_field('sciezka_zaawansowana_tytul1'); ?></h4>
		<?php endif; ?>
	<div class="row">

<?php
	while ( have_rows('sciezka_zawansowana')) : the_row();
		echo "<div class='col-6'>";
		echo "<div class='sciezka-white-bg'>";
			echo "<img src='".get_sub_field("sciezka_zawansowana_ikona")[url]."' alt='".get_sub_field("sciezka_zawansowana_ikona")['alt']."'>";
			echo "<div class='sciezka-box-text'>";
				echo get_sub_field("sciezka_zawansowana_tytul");
			echo "</div>";
			echo "<div class='sciezka-box-btm'>";
				foreach (get_sub_field("sciezka_zawansowana_button") as $sciezkaBtn) {
					if($sciezkaBtn["link_w_nowym_oknie"]){
						echo "<a class='button' href='".$sciezkaBtn['sciezka_zawansowana_btn_link']."' target='_blank'>";
						echo $sciezkaBtn['sciezka_zawansowana_btn_nazwa'];
						echo "</a>";
					}else{
						echo "<a class='button' href='".$sciezkaBtn['sciezka_zawansowana_btn_link']."'>";
						echo $sciezkaBtn['sciezka_zawansowana_btn_nazwa'];
						echo "</a>";
					}
				}
			echo "</div>";
		echo "</div>";
		echo "</div>";
	endwhile;

	?>
</div>
</div>
</div>
</div>
<?php }?>







<?php if( get_row_layout() == 'survey_etap_box' ){  ?>
	<div class="col-12 btm-margin clear">
	<?php
	while ( have_rows('survey_etap_repeater')) : the_row();

	echo "<div class='etap-box survey-etap-box box-".$etapBoxNumber++."'>";
		echo "<div class='etap-box-top'>";
			echo "<div class='top-text'>";
			echo get_sub_field("survey_etap_top_text");
			echo "</div>";
			echo "<img src='".get_sub_field("survey_etap_ikona")['sizes']['etapy-ikona']."' alt='".get_sub_field("survey_etap_ikona")['alt']."'>";
			echo get_sub_field("survey_etap_opis");
		echo "</div>";
	 echo "</div>";

	endwhile;

	?>
	</div>



<?php }?>



<?php if( get_row_layout() == 'survey_titles' ){  ?>
	<div class="col-12">
			<h2 class="survey-title"><?php echo get_sub_field("survey_title"); ?></h2>
	</div>
<?php }?>
<?php if( get_row_layout() == 'survey_text' ){  ?>
			<div class="col-12 text-margin"><?php echo get_sub_field("survey_text_1"); ?></div>
<?php }?>
<?php if( get_row_layout() == 'survey_line' ){  ?>
			<div class="col-12"><hr class="survey-line"></div>

<?php }?>

<?php
endwhile;
endif;
?>


</div>

<?php
if( have_rows('banner_btm', 'option') ):
	while ( have_rows('banner_btm', 'option') ) : the_row();
?>

<?php if( get_row_layout() == 'survey_btm_text_bg' ){  ?>
	<?php
	$image_object = get_sub_field('banner_btm_text_image');
	$image_size = 'misja_btm_bg2';
	$image_url = $image_object['sizes'][$image_size];
	$alt = $image_object['alt'];
	$caption = $image_object['caption'];
	?>
	<?php
	if (get_sub_field('banner_btm_col')==1) {  ?>
		<div class="container-bg-btm clear" style="background: url(<?php echo $image_url; ?>) no-repeat center center / cover">
		<div class="container">
			<?php
				echo "<div class='col-8'>";
					echo get_sub_field("banner_btm_text_text");

						echo "<div class='btns-box'>";
							foreach (get_sub_field("banner_btm_btns") as $przycisk) {
							echo "<a class='button' href='".$przycisk['banner_btm_btn_link']."'>";
							echo $przycisk['banner_btm_btn_text'];
							echo "</a>";
						}
						echo "</div>";

				echo "</div>";

				echo "<div class='col-4'>";
					 echo	"<img src='".get_sub_field("banner_btm_text_img_right")['sizes']['misja_btm_img']."' alt='".get_sub_field("banner_btm_text_img_right")['alt']."'>";
				echo "</div>";
			?>
		</div>
		</div>

<?php } else {?>

	<div class="container-bg-btm clear" style="background: url(<?php echo $image_url; ?>) no-repeat center center / cover">
	<div class="container">
		<?php
					echo "<div class='banner-box'>";
						echo "<div class='col-12'>";
							echo "<h6>".get_sub_field("banner_btm_title")."</h6>";
						echo "<h5>".get_sub_field("banner_btm_subtitle")."</h5>";
						echo "</div>";
						foreach (get_sub_field("banner_btm_img_text") as $bannerBtm) {
							if ($bannerBtm['banner_btm_pozycja_obrazka']==1){
								echo "<div class='col-6 col-left'>";
									echo "<div class='banner-btm-left'>";
										echo "<img src='".$bannerBtm['banner_btm_image']['sizes']['banner_btm_img']." 'alt='".$bannerBtm['banner_btm_image']['alt']."'>";
									echo "</div>";
									echo "<div class='banner-btm-right'>";
										echo "<h6>".$bannerBtm['banner_btm_tekst']."</h6>";
										echo "<a class='button' href='".$bannerBtm['banner_btm_btn_link']."' target='_blank'>";
										echo $bannerBtm['banner_btm_btn_text'];
										echo "</a>";
									echo "</div>";
								echo "</div>";
							}else{
								echo "<div class='col-6 col-right'>";
									echo "<div class='banner-btm-left'>";
										echo "<h6>".$bannerBtm['banner_btm_tekst']."</h6>";
										echo "<a class='button' href='".$bannerBtm['banner_btm_btn_link']."' target='_blank'>";
											echo $bannerBtm['banner_btm_btn_text'];
										echo "</a>";
									echo "</div>";
									echo "<div class='banner-btm-right'>";
										echo "<img src='".$bannerBtm['banner_btm_image']['sizes']['banner_btm_img']." 'alt='".$bannerBtm['banner_btm_image']['alt']."'>";
									echo "</div>";
								echo "</div>";
							}

					}
					echo "</div>";
		?>
	</div>
	</div>

<?php }?>
<?php }?>

<?php
endwhile;
endif;
?>
