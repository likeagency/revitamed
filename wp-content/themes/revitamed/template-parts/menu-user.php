<?php
$userRevita = new user;

foreach( getMaybeEndedDiet($userRevita->getId()) as $maybeEndedDiet)
	updateStateDiet($maybeEndedDiet);

$partialSurvey = getLastPartialSurvey($userRevita->getId(),'any');

$countMsg = $userRevita->getUnreadMsgCount();

?>
<h3 class="title">Na skróty</h3>
<ul class="icons-menu">
  <li class="el1"><a href="<?= get_the_permalink(get_id_after_template_filename('konto-komunikaty.php')); ?>">Mój dzienniczek</a></li>
  <?php
  if($userRevita->hasActiveSub()){

	 if($userRevita->hasEndedDiets()){ ?>
  <li class="el3"><a href="<?= get_the_permalink(get_id_after_template_filename('konto-historia-diety.php')); ?>">Historia Diety</a></li><?php } }?>
  <li class="el4"><a href="<?= get_the_permalink(get_id_after_template_filename('konto-platnosci.php')); ?>">Płatności</a></li>
  <?php if($userRevita->hasActiveSubPremium()){?>
  <li class="el5 <?= $countMsg!=0 ? '-new' : '' ?>"><a href="<?= get_the_permalink(get_id_after_template_filename('konto-wiadomosci.php')); ?>">Wiadomości z lekarzem <?= $countMsg!=0 ? "($countMsg)" : '' ?></a></li><?php  }?>

    <?php  if($partialSurvey!==null && $partialSurvey->post_status=='publish') {?>
  <li class="el6 ankieta-plus"><a href="<?= get_the_permalink($partialSurvey) ?>">Wypełnij dodatkową ankietę</a></li><?php  }?>
</ul><!-- ./iconsmenu --> 
