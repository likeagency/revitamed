<!-- INDYWIDUALNA DIAGNOSTYKA -->
<div class="individualDiagnostics">
    <form method="post" action="<?= get_the_permalink(get_id_after_template_filename('ankieta_1.php')) ?>" class="individualDiagnostics__form">
        <div class="individualDiagnostics__title">INDYWIDUALNA DIAGNOSTYKA</div>
        <div class="individualDiagnostics__group" data-group="purpose-of-visit">
            <div class="individualDiagnostics__category">Cel wizyty</div>
            <div class="individualDiagnostics__inputbox">
                <label class="individualDiagnostics__label">
                    <input class="individualDiagnostics__checkbox" name="user_purpose_of_visit" type="checkbox" data-item value="1"> odchudzający</label>
            </div>
            <div class="individualDiagnostics__inputbox">
                <label class="individualDiagnostics__label">
                    <input class="individualDiagnostics__checkbox" name="user_purpose_of_visit" type="checkbox" data-item value="2"> zdrowotny</label>
            </div>
            <div class="individualDiagnostics__inputbox">
                <label class="individualDiagnostics__label">
                    <input class="individualDiagnostics__checkbox" name="user_purpose_of_visit" type="checkbox" data-item value="3"> przyrost masy ciała</label>
            </div>
            <div class="individualDiagnostics__inputbox">
                <label class="individualDiagnostics__label">
                    <input class="individualDiagnostics__checkbox" name="user_purpose_of_visit" type="checkbox" data-item value="4"> regenerujący
                    budujący masę
                    mięśniową</label>
            </div>
            <div class="individualDiagnostics__inputbox">
                <label class="individualDiagnostics__label">
                    <input class="individualDiagnostics__checkbox" name="user_purpose_of_visit" type="checkbox" data-item value="5"> odmładzający,
                    rewitalizujący</label>
            </div>
            <div class="individualDiagnostics__inputbox">
                <label class="individualDiagnostics__label">
                    <input class="individualDiagnostics__checkbox" name="user_purpose_of_visit"  type="checkbox" data-item value="6"> inny, jaki?</label>
                <input class="individualDiagnostics__input" name="purpose-of-visit-other">
            </div>
        </div>
        <div class="individualDiagnostics__group" data-group="recognized-disease">
            <div class="individualDiagnostics__category">Rozpoznana choroba</div>
            <div class="individualDiagnostics__inputbox">
                <label class="individualDiagnostics__label">
                    <input type="checkbox" name="user_disease" class="individualDiagnostics__checkbox" data-item value="1"> Tak
                </label>
            </div>
            <div class="individualDiagnostics__inputbox">
                <label class="individualDiagnostics__label">
                    <input type="checkbox" name="user_disease" class="individualDiagnostics__checkbox" data-item value="2"> Nie
                </label>
            </div>
            <div class="individualDiagnostics__inputbox">
                <span class="individualDiagnostics__inputtext">Jakie? Kiedy?</span>
                <input class="individualDiagnostics__input" name="recognized-disease-other">
            </div>
        </div>
        <div class="individualDiagnostics__group">
            <div class="individualDiagnostics__category">Jaki masz problem?</div>
            <div class="individualDiagnostics__inputbox">
                <input class="individualDiagnostics__input" name="what-is-your-problem">
            </div>
        </div>
        <input type="hidden" name="action" value="individualDiagnostic">
        <div class="individualDiagnostics__navigation">
            <button class="individualDiagnostics__submit">
                PRZEJDŹ DALEJ <span class="fa fa-angle-double-right"></span>
            </button>
        </div>
    </form>
</div>