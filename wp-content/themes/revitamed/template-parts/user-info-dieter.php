<?php

function progressUserShow($userID){
	$lastMeasure = getLastUserMeasure($userID);
	$diet = getCurrentActiveDiet($userID);
	$dietID = $diet->ID;
	$weight_current = getUserWeigh($userID);
	$weight_target = get_post_meta($dietID,'sf_weight_target',true);
	if($lastMeasure==null) return;
	?>
  <div class="side-graph side-item">
    <h3 class="title">Postępy pacjenta</h3>
    <div class="chart graph1" data-weight='<?= json_encode(getUserWeights($userID)) ?>' data-target="<?= $weight_target ?>">
      <div class="chart__canvas"></div>
      <div class="chart__removeLabel"></div>
    </div>
    <div class="captions">
      <span class="weight actual">waga aktualna <?= $weight_current ?> kg</span>
		<?php if($weight_target!=''){?>
          <span class="weight target">waga docelowa <?= $weight_target ?> kg</span>
		<?php } ?>
    </div>
    <div class="chart graph2" data-waist='<?= json_encode(getUserWaist($userID)) ?>' data-stomach='<?= json_encode(getUserStomach($userID)) ?>' data-hips='<?= json_encode(getUserHips($userID)) ?>'>
      <div class="chart__canvas"></div>
      <div class="chart__removeLabel"></div>
    </div>
    <div class="captions">
      <span class="girth waist">Obwód talia: <?= get_post_meta($lastMeasure->ID,'sf_talia',true); ?> cm</span>
      <span class="girth stomach">Obwód brzuch: <?= get_post_meta($lastMeasure->ID,'sf_brzucha',true); ?> cm</span>
      <span class="girth hips">Obwód biodra: <?= get_post_meta($lastMeasure->ID,'sf_biodra',true); ?> cm</span>
    </div>
  </div><!-- ./sidegraph -->
	<?php
}

function showUserBasicData($userID){

$surveys = getUserSurvey($userID);
$survey = $surveys[0];
$age = get_post_meta($userID,'sf_age',true);
$weight = getUserWeigh($userID);
$height = get_post_meta($userID,'sf_height',true);
$data = getDataSurvey($survey->ID);
$partialSurvey = getLastPartialSurvey($userID);
$data['sf_purpose'] = maybe_unserialize($data['sf_purpose']);
$measure =  intval($data['sf_circuit_1']) .'-'. intval($data['sf_circuit_2']);
if($data['sf_circuit_3']!='')
	$measure .='-'. intval($data['sf_circuit_3']) ;
$purposeAr = [];
$purposes =  array(
  '1' => 'Odchudający',
  '2' => 'Zdrowotny',
  '3' => 'Przyrost masy ciała',
  '4' => 'Regenerujący budujący masę mięśniową',
  '5' => 'Odmładzający, rewitalizujący',
  '6' => 'Inny :'. esc_html($data['sf_user_other_purposes']),
  '7' => 'Podnoszący poziom energii',
);

if(is_array($data['sf_purpose']))
	foreach($data['sf_purpose'] as $single)
		$purposeAr[] = $purposes[$single];

if(!$data['sf_operation_yes_no'])
	$operations = 'Nie' ;
else
{
	$operations =  'Tak ' ;
	if($data['sf_operation_ww'])
		$operations .= '( ' . esc_html($data['sf_operation_ww']) . ' )';
}
if(!$data['sf_medicines_yes_no'])
	$medicines = 'Nie' ;
else
{
	$medicines =  'Tak ' ;
	if($data['sf_medicines_w'])
		$medicines .= '( ' . esc_html($data['sf_medicines_w']) . ' )';
}

	$percentFat = formatNumberToPolish(abs(calculateFatDegree($survey->ID)),0);

if(!$data['sf_disease_yes_no'])
	$diseases = 'Nie' ;
else
{
	$diseases =  'Tak ' ;
	if($data['sf_disease_w'])
		$diseases .= '( ' . esc_html($data['sf_disease_w']) . ' )';
}?>
<div class="patient">
	<h4>GŁÓWNE INFORMACJE O PACJENCIE</h4>
	<span><strong>Imię i Nazwisko:</strong> <?= getUserName($userID); ?></span>
	<span><strong>Wiek:</strong> <?= $age ?> lat</span>
	<span><strong>Waga:</strong> <?= $weight ?>kg</span>
	<span><strong>Wzrost:</strong> <?= $height ?>cm</span>
	<span><strong>Obwody:</strong> <?= $measure ?></span>
	<span><strong>Cel wizyty:</strong> <?= implode($purposeAr,', ') ?></span>
	<span><strong>Operacje:</strong> <?= $operations ?></span>
	<span><strong>Leki:</strong> <?=  $medicines?></span>
	<span><strong>Rozpoznane choroby:</strong> <?=  $diseases ?></span>
	<span><strong>Procent tkanki tłuszczowej:</strong> <?=  $percentFat ?>%</span>



  <?php if($partialSurvey !== null) { ?>
	<a href="<?= get_the_permalink( get_id_after_template_filename( 'ankieta-czastkowa.php' ) ) .'/'.($partialSurvey->ID) ?>" target="_blank">POKAŻ ANKIETĘ CZĄSTKOWĄ Z DNIA <?= date('d.m.Y',strtotime($partialSurvey->post_date)) ?></a>
  <?php } ?>
	<a href="<?= get_the_permalink($survey->ID); ?>" target="_blank">POKAŻ PEŁNĄ ANKIETĘ</a>
</div>

<?php }

function showUserFiles($userID){

	$files =  getFilesFromSection($userID,'research');  if(!empty($files)){ ?>
<div class="uploadFiles__container">
  <div class="uploadFiles__title">Badania<hr></div>
	<?php if(is_array($files)) foreach($files as $file){ ?>
      <a <?= isFileImage(get_post_meta($file->ID,'sf_path_var',true)) ? 'data-fancybox="group"' : '' ?>  download="<?= get_post_meta($file->ID,'sf_name',true) ?>" href="<?= home_url('/'). get_post_meta($file->ID,'sf_path_url',true); ?>" class="fileUploader__uploaded">
        <div class="fileUploader__uploadedIcon">
          <i class="fa fa-file-text-o"></i>
        </div>
        <div class="fileUploader__uploadedName"><?= $file->post_title ?></div>
      </a>
	<?php } ?>

</div>
  <?php }
	$files =  getFilesFromSection($userID,'research-fdt');  if(!empty($files)){ ?>
    <div class="uploadFiles__container">
      <div class="uploadFiles__title">Badania-FDT<hr></div>
		<?php if(is_array($files)) foreach($files as $file){ ?>
          <a <?= isFileImage(get_post_meta($file->ID,'sf_path_var',true)) ? 'data-fancybox="group"' : '' ?>  download="<?= get_post_meta($file->ID,'sf_name',true) ?>" href="<?= home_url('/'). get_post_meta($file->ID,'sf_path_url',true); ?>" class="fileUploader__uploaded">
            <div class="fileUploader__uploadedIcon">
              <i class="fa fa-file-text-o"></i>
            </div>
            <div class="fileUploader__uploadedName"><?= $file->post_title ?></div>
          </a>
		<?php } ?>

    </div>
	<?php }
	$files =  getFilesFromSection($userID,'research-beta');  if(!empty($files)){ ?>
    <div class="uploadFiles__container">
      <div class="uploadFiles__title">Badania BETA<hr></div>
		<?php if(is_array($files)) foreach($files as $file){ ?>
          <a <?= isFileImage(get_post_meta($file->ID,'sf_path_var',true)) ? 'data-fancybox="group"' : '' ?>  download="<?= get_post_meta($file->ID,'sf_name',true) ?>" href="<?= home_url('/'). get_post_meta($file->ID,'sf_path_url',true); ?>" class="fileUploader__uploaded">
            <div class="fileUploader__uploadedIcon">
              <i class="fa fa-file-text-o"></i>
            </div>
            <div class="fileUploader__uploadedName"><?= $file->post_title ?></div>
          </a>
		<?php } ?>

    </div>
	<?php }

}

if($_GET['userID']!='' || $_GET['dietID']!='')
{
  if($_GET['userID']=='')
	  $_GET['userID'] = get_post_meta($_GET['dietID'],'sf_user',true);
	showUserBasicData($_GET['userID']);
  progressUserShow($_GET['userID']);
	showUserFiles($_GET['userID']);
} ?>
