<?php
$image_object = get_field('badania_obrazek_top');
$image_size = 'badania_top_image';
$image_url = $image_object['sizes'][$image_size];
$alt = $image_object['alt'];
$caption = $image_object['caption'];
?>
<?php if( get_field('badania_obrazek_top') ): ?>
<div class="misja-top" style="background: url(<?php echo $image_url; ?>) no-repeat center center / cover">
	<div class="misja-top-overlay">
		<div class="container clear">
			<div class="col-12">
				<?php
				echo "<h1>";
				echo get_field("badania_top_tytul");
				echo "</h1>";
				echo "<h2>";
				echo get_field("badania_top_podtytul");
				echo "</h2>";
				echo get_field("badania_top_text");
				?>
			</div>
		</div>
	</div>
</div>
<?php else: ?>
	<div class="container">
		<div class="col-12">
	<div class="wrapper">
		<h2><?php the_title(); ?></h2>
		<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
			<?php if(function_exists('bcn_display'))
			{
				bcn_display();
			}?>
		</div>
	</div><!-- ./wrapper -->
	</div>
</div>
<?php endif; ?>
<?php
$tabsNumber=0;
$tabsContentNumber=0;
$boxID=0;
if( have_rows('badania_content') ):
	while ( have_rows('badania_content') ) : the_row();
?>
<?php if( get_row_layout() == 'badania_blok_tekstowy_z_obrazkiem' ){  ?>
	<div class="container">
		<?php
			if( have_rows('badania_photo_repeater') ):
				while ( have_rows('badania_photo_repeater') ) : the_row();
					if(get_sub_field("badania_pozycja_obrazka")==1){
						echo "<div id='boxID-".++$boxID."' class='container-img clear'>";
						if(get_sub_field("badania_video")){
							echo "<div class='col-6 pr'><a data-fancybox='group' href='".get_sub_field("badania_video")."'><img src='".get_sub_field("badania_obrazek")['sizes']['misja_repeater_image']."' alt='".get_sub_field("survey_repeater_image")['alt']."'></a></div>";
						}else{
							echo "<div class='col-6 pr'><img src='".get_sub_field("badania_obrazek")['sizes']['misja_repeater_image']."' alt='".get_sub_field("survey_repeater_image")['alt']."'></div>";
						}
							echo "<div class='col-6'>";
								echo "<div class='text-box'>";
									echo "<h6>";
									echo get_sub_field("badania_tytul");
									echo "</h6>";
									echo "<h5>";
									echo get_sub_field("badania_podtytul");
									echo "</h5>";
									echo get_sub_field("badania_tekst");
								echo "</div>";
							echo "</div>";
						echo "</div>";

					}else{
						echo "<div id='boxID-".++$boxID."' class='container-img clear'>";
							if(get_sub_field("badania_video")){
								echo "<div class='col-6 pl fr'><a data-fancybox='group' href='".get_sub_field("badania_video")."'><img src='".get_sub_field("badania_obrazek")['sizes']['misja_repeater_image']."' alt='".get_sub_field("survey_repeater_image")['alt']."'></a></div>";
							}else{
								echo "<div class='col-6 pl fr'><img src='".get_sub_field("badania_obrazek")['sizes']['misja_repeater_image']."' alt='".get_sub_field("survey_repeater_image")['alt']."'></div>";
							}
							echo "<div class='col-6 fl'>";
								echo "<div class='text-box'>";
									echo "<h6>";
									echo get_sub_field("badania_tytul");
									echo "</h6>";
									echo "<h5>";
									echo get_sub_field("badania_podtytul");
									echo "</h5>";
									echo get_sub_field("badania_tekst");
								echo "</div>";
							echo "</div>";
						echo "</div>";
					}

				endwhile;

			endif;
		?>
		</div>
<?php }?>


<?php if( get_row_layout() == 'badania_block_text' ){  ?>
	<div class="bc-container container-50 clear">
			<?php echo get_sub_field("badania_blok_50"); ?>
	</div>
<?php }?>

<?php if( get_row_layout() == 'badania_block_text_100' ){  ?>
	<div class="container clear" style="margin-bottom: 30px;">
		<div class="col-12">
			<?php echo get_sub_field("badania_blok_100p"); ?>
			</div>
	</div>
<?php }?>


<?php if( get_row_layout() == 'badania_info' ){  ?>
	<div class="container btm-margin clear">
	<?php
	while ( have_rows('badania_blok_25')) : the_row();
		echo "<div class='badania-blok-25 col-3'>";
			echo "<img src='".get_sub_field("badania_blok25_ikona")['sizes']['etapy-ikona']."' alt='".get_sub_field("badania_blok25_ikona")['alt']."'>";
			echo "<div class='top-text'>";
				echo get_sub_field("badania_blok25_top_tekst");
			echo "</div>";
		echo "</div>";
	endwhile;
	?>
	</div>
<?php }?>


<?php if( get_row_layout() == 'badania_tabs' ){  ?>
	<div class="container clear">
		<div class="col-12">
			<ul class="badania-tabs">
				<?php
				while ( have_rows('badania_zakladka')) : the_row(); ?>
						<li class="badania-tab-link" data-tab="tab-<?php echo $tabsNumber++ ?>"><?php echo get_sub_field("badania_zakladka_tytul"); ?></li>
				<?php endwhile;
				?>
			</ul>
		</div>
	</div>

<?php }?>

<?php if( get_row_layout() == 'badania_tabs' ){  ?>
	<div class="container btm-margin clear">
		<div class="col-12">
			<?php
			while ( have_rows('badania_zakladka')) : the_row(); ?>
				<div id="tab-<?php echo $tabsContentNumber++ ?>" class="badania-tab-content"><?php 	echo get_sub_field("badania_zakladka_tekst"); ?></div>
			<?php endwhile;
			?>
		</div>
	</div>
<?php }?>



<?php if( get_row_layout() == 'badania_titles' ){  ?>
	<div class="container">
		<div class="col-12">
				<div class="badania-title-box">
				<h2 class="badania-title"><?php echo get_sub_field("badania_title"); ?></h2>
				<h3 class="badania-subtitle"><?php echo get_sub_field("badania_subtitle"); ?></h3>
				</div>
		</div>
	</div>
<?php }?>

<?php if( get_row_layout() == 'badania_line' ){  ?>
			<div class="container">
				<div class="col-12"><hr class="survey-line"></div>
			</div>
<?php }?>



<?php
endwhile;
endif;
?>

<div class="bg-container">
	<?php
	if( have_rows('badania_content') ):
		while ( have_rows('badania_content') ) : the_row();
	?>
		<?php if( get_row_layout() == 'badania_block_text_image' ){  ?>
			<div class="mb container clear block-text-image">
				<div class='col-6 text-right'>
					<?php echo	"<img src='".get_sub_field("badania_blok_100_img")['sizes']['misja_image']."' alt='".get_sub_field("badania_blok_100_img")['alt']."'>";?>
				</div>
				<div class="col-6 pl ">
					<?php
					echo "<h5>";
					echo get_sub_field("badania_blok_100_title");
					echo "</h5>";
					echo get_sub_field("badania_blok_100_text");
					?>
				</div>
			</div>
		<?php }?>

	<?php
	endwhile;
	endif;
	?>
</div>

<?php
if( have_rows('banner_btm', 'option') ):
	while ( have_rows('banner_btm', 'option') ) : the_row();
?>

<?php if( get_row_layout() == 'survey_btm_text_bg' ){  ?>
	<?php
	$image_object = get_sub_field('banner_btm_text_image');
	$image_size = 'misja_btm_bg2';
	$image_url = $image_object['sizes'][$image_size];
	$alt = $image_object['alt'];
	$caption = $image_object['caption'];
	?>
	<?php
	if (get_sub_field('banner_btm_col')==1) {  ?>
		<div class="container-bg-btm clear" style="background: url(<?php echo $image_url; ?>) no-repeat center center / cover">
		<div class="container">
			<?php
				echo "<div class='col-8'>";
					echo get_sub_field("banner_btm_text_text");

						echo "<div class='btns-box'>";
							foreach (get_sub_field("banner_btm_btns") as $przycisk) {
							echo "<a class='button' href='".$przycisk['banner_btm_btn_link']."'>";
							echo $przycisk['banner_btm_btn_text'];
							echo "</a>";
						}
						echo "</div>";

				echo "</div>";

				echo "<div class='col-4'>";
					 echo	"<img src='".get_sub_field("banner_btm_text_img_right")['sizes']['misja_btm_img']."' alt='".get_sub_field("banner_btm_text_img_right")['alt']."'>";
				echo "</div>";
			?>
		</div>
		</div>

<?php } else {?>

	<div class="container-bg-btm clear" style="background: url(<?php echo $image_url; ?>) no-repeat center center / cover">
	<div class="container">
		<?php
					echo "<div class='banner-box'>";
						echo "<div class='col-12'>";
							echo "<h6>".get_sub_field("banner_btm_title")."</h6>";
						echo "<h5>".get_sub_field("banner_btm_subtitle")."</h5>";
						echo "</div>";
						foreach (get_sub_field("banner_btm_img_text") as $bannerBtm) {
							if ($bannerBtm['banner_btm_pozycja_obrazka']==1){
								echo "<div class='col-6 col-left'>";
									echo "<div class='banner-btm-left'>";
										echo "<img src='".$bannerBtm['banner_btm_image']['sizes']['banner_btm_img']." 'alt='".$bannerBtm['banner_btm_image']['alt']."'>";
									echo "</div>";
									echo "<div class='banner-btm-right'>";
										echo "<h6>".$bannerBtm['banner_btm_tekst']."</h6>";
										echo "<a class='button' href='".$bannerBtm['banner_btm_btn_link']."' target='_blank'>";
										echo $bannerBtm['banner_btm_btn_text'];
										echo "</a>";
									echo "</div>";
								echo "</div>";
							}else{
								echo "<div class='col-6 col-right'>";
									echo "<div class='banner-btm-left'>";
										echo "<h6>".$bannerBtm['banner_btm_tekst']."</h6>";
										echo "<a class='button' href='".$bannerBtm['banner_btm_btn_link']."' target='_blank'>";
											echo $bannerBtm['banner_btm_btn_text'];
										echo "</a>";
									echo "</div>";
									echo "<div class='banner-btm-right'>";
										echo "<img src='".$bannerBtm['banner_btm_image']['sizes']['banner_btm_img']." 'alt='".$bannerBtm['banner_btm_image']['alt']."'>";
									echo "</div>";
								echo "</div>";
							}

					}
					echo "</div>";
		?>
	</div>
	</div>

<?php }?>
<?php }?>

<?php
endwhile;
endif;
?>
