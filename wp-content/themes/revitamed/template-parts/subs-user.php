<?php
function showSubsToEnd(){
	$userRevitamed = new user();
	$userID = $userRevitamed->getId();
	$subEnd = $userRevitamed->getUserLastActiveDay();
	$subDocEnd = get_post_meta($userID,'sf_date_sub_doc',true);
	$dateDiff = $subEnd - strtotime('now');
	$dayBetweenSub =  floor($dateDiff / (60 * 60 * 24));
	if(get_post_meta($userRevitamed->getId(),'sf_date_sub',true)=='') return;
?>
<div class="side-subscription side-item">
	<h3 class="title">Abonament</h3>
	<div class="sbsp-info">
		<?php if($userRevitamed->hasActiveSub()) {?>
		Twoje konto jest opłacone do:
		<span class="expire"><?= date('d.m.Y',$subEnd) ?></span>
		Do końca pozostało: <strong><?= $dayBetweenSub ?> dni</strong>
		<?php } else { ?>
			Twój dostęp do serwisu zakończył się <strong><?= date('d.m.Y',get_post_meta($userRevitamed->getId(),'sf_date_sub',true) ) ?></strong>
		<?php } ?>
	</div><!-- ./sbspinfo -->
	<?php if($userRevitamed->hasActiveSub()) {?>
	<a href="<?= get_the_permalink(get_id_after_template_filename('konto-platnosci.php')) ?>" class="button-inw-red">Przedłuż abonament</a>
		<?php }else { ?>
		<a href="<?= get_the_permalink(get_id_after_template_filename('konto-platnosci.php')) ?>" class="button-inw-red">Wykup abonament</a>
		<?php } ?>
  <?php if($subDocEnd!='' && strtotime('now') < $subDocEnd ) {?>
  <div class="sbsp-info">
  Abonament na komunikacje z lekarzem do:
    <span class="expire"><?= date('d.m.Y',$subDocEnd) ?></span>
  </div>
  <?php } ?>
</div>
<?php } showSubsToEnd();
