<?php

function progressUserShow($userID){
	$lastMeasure = getLastUserMeasure($userID);
	$diet = getCurrentActiveDiet($userID);
	$surveys = getUserSurvey($userID);
	$dietID = $diet->ID;
	if($lastMeasure==null || empty($surveys)) return;
	$weight_current = getUserWeigh($userID);
	$weight_target = get_post_meta($surveys[0]->ID,'sf_nice_weight',true);
	?>
	<div class="side-graph side-item">
		<h3 class="title">Twoje postępy</h3>
		<div class="chart graph1" data-weight='<?= json_encode(getUserWeights($userID)) ?>' data-target="<?= $weight_target ?>">
			<div class="chart__canvas"></div>
			<div class="chart__removeLabel"></div>
		</div>
		<div class="captions">
			<span class="weight actual">waga aktualna <?= $weight_current ?> kg</span>
      <?php if($weight_target!=''){?>
			<span class="weight target">waga docelowa <?= $weight_target ?> kg</span>
        <?php } ?>
		</div>
		<div class="chart graph2" data-waist='<?= json_encode(getUserWaist($userID)) ?>' data-stomach='<?= json_encode(getUserStomach($userID)) ?>' data-hips='<?= json_encode(getUserHips($userID)) ?>'>
			<div class="chart__canvas"></div>
			<div class="chart__removeLabel"></div>
		</div>
		<div class="captions">
			<span class="girth waist">Obwód talia: <?= get_post_meta($lastMeasure->ID,'sf_talia',true); ?> cm</span>
			<span class="girth stomach">Obwód brzuch: <?= get_post_meta($lastMeasure->ID,'sf_brzucha',true); ?> cm</span>
			<span class="girth hips">Obwód biodra: <?= get_post_meta($lastMeasure->ID,'sf_biodra',true); ?> cm</span>
		</div>
	</div><!-- ./sidegraph -->
	<?php
}
$userRevita = new user();
$userID = $userRevita->getId();
progressUserShow($userID);