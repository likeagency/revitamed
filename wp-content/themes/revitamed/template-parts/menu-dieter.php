<?php
$userRevita = new userDoc;
$isDoc = get_post_meta($userRevita->getId(),'sf_doc',true) == 1;

$countMsg = count(getMsgUserDieterUnread( (!$isDoc) ? $userRevita->getId() : getAllDietaryForDoc($userRevita->getId())));


?>
<div class="side-menu side-item">
	<h3 class="title">Menu Dietetyka</h3>
	<ul class="icons-menu">
		<li class="el1"><a href="<?= get_the_permalink(get_id_after_template_filename('dietary-panel.php')) ?>?f[]=2&f[]=4">Nowe diety do ułożenia</a></li>
		<li class="el2"><a href="<?= get_the_permalink(get_id_after_template_filename('dietary-panel.php')) ?>?f[]=1">Diety w trakcie</a></li>
		<li class="el3 <?= $countMsg!=0 ? '-new' : '' ?>"><a href="<?= get_the_permalink(get_id_after_template_filename('dietary-message.php')) ?>">Wiadomości od pacjentów <?= $countMsg!=0 ? "($countMsg)" : '' ?></a></li>
    <li class="el4"><a href="<?= get_the_permalink(get_id_after_template_filename('dietery_edit_temp.php')); ?>">Dodawanie i edycja szablonów</a></li>
	</ul><!-- ./iconsmenu -->
</div><!-- ./sidemenu -->