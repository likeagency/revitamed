  <div id="owl-carousel-tm" class="owl-carousel">
    <?php while ( $posts_array->have_posts() ) { $posts_array->the_post(); $local_custom = get_post_custom(get_the_ID()); ?>
    <a href="/rekomendacje/#testimonialID_<?php echo get_the_ID(); ?>" class="item slide<?php the_ID(); ?>">
      <div class="txt-wrapper">
        <?php the_field('short_desc'); ?>
        <div class="who">
          <span class="r1"><?php echo $local_custom['ti_url'][0]; ?></span>
          <span class="r2"><?php echo $local_custom['ti_button_text'][0]; ?></span>
        </div>
      </div><!-- ./overlay -->
    </a><!-- ./item -->
    <?php } ?>
  </div><!-- ./carousel -->
