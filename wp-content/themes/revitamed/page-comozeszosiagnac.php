<?php
/**
 * The template for displaying all pages.
 * Template name: Co mozesz osiagnac
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */

get_header(); ?>
<div id="primary" class="content-area mb container">
	<div class="col-12">
	<div class="wrapper">
		<h2><?php the_title(); ?></h2>
		<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
			<?php if(function_exists('bcn_display'))
			{
				bcn_display();
			}?>
		</div>
	</div><!-- ./wrapper -->
	</div>
	<main id="main" class="site-main col-12" role="main">
		<?php the_content(); ?>

<div class="options-box clear">
		<?php
		if( have_rows('etapy_porada') ):
			while ( have_rows('etapy_porada') ) : the_row();
		?>
	<?php if( get_row_layout() == 'etap_box' ){  ?>
		<?php
			echo "<div class='etap-box'>";
				echo "<div class='etap-box-top'>";
					echo get_sub_field("etap_top_text");
					echo "<img src='".get_sub_field("etap_top_ikona")['sizes']['etapy-ikona']."' alt='".get_sub_field("etap_top_ikona")['alt']."'>";
				echo "</div>";
				echo "<div class='etap-box-middle'>";
					echo get_sub_field("etap_opis");
					echo "<div class='etap-lista'>";
					echo "<ul>";
					foreach (get_sub_field("etap_lista") as $etapLista) {
						echo "<li class='etap-lista'>";
						echo $etapLista['etap_wiersz_listy'];
						echo "</li>";
					}
					echo "</ul>";
					echo "</div>";
				echo "</div>";
				echo "<div class='etap-box-btm'>";
					echo get_sub_field('etap_koszyk');
				echo "</div>";
			echo "</div>";
		?>
	<?php }?>



		<?php
		endwhile;
		endif;
		?>
</div>
		<?php if( get_field('options_text') ): ?>
			<div class="single-btm-box clear">
			<?php the_field('options_text'); ?>
			</div>
		<?php endif; ?>

	</main><!-- #main -->

</div><!-- #primary -->

<?php get_footer();
