<?php
/**
 * The template for displaying all pages.
 * Template name: ankieta 3
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */
user_page();
saveDataFromFormToSession();
get_header(); ?>
<div id="primary" class="content-area  container">
  <div class="col-12">
  <div class="wrapper">
    <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
      <?php if (function_exists('bcn_display')) {
          bcn_display();
      } ?>
    </div>
  </div><!-- ./wrapper -->
</div>
  <main id="main" class="site-main bli" role="main">
    <div class="steps">
      <div class="steps__content">
        <a href="<?= get_permalink_template('ankieta_1.php'); ?>" class="steps__item ">
          <div class="steps__counter">Krok 1</div>
          <div class="steps__name">Wywiad ogólny</div>
        </a>
        <a href="<?= get_permalink_template('ankieta_2.php'); ?>" class="steps__item ">
          <div class="steps__counter">Krok 2</div>
          <div class="steps__name">Wywiad zdrowotny</div>
        </a>
        <a href="<?= get_permalink_template('ankieta_3.php'); ?>" class="steps__item -active">
          <div class="steps__counter">Krok 3</div>
          <div class="steps__name">Wywiad żywieniowy</div>
        </a>
        <a href="<?= get_permalink_template('ankieta_4.php'); ?>" class="steps__item">
          <div class="steps__counter">Krok 4</div>
          <div class="steps__name">Preferencje żywieniowe</div>
        </a>
        <div class="steps__item">
          <div class="steps__counter">Krok 5</div>
          <div class="steps__name">Podsumowanie</div>
        </div>
      </div>
    </div>
    <form method="post" class="form" action="<?= get_permalink_template('ankieta_4.php'); ?>" data-questionnaire-form>
      <div class="form__content">
        <div class="form__row -spaceBetween -marginBottom">
          <div class="form__col50">
            <div class="form__rowTitle -m-b-30" style="padding-top: 0;">PROSZE PODAĆ GODZINY POSIŁKÓW I PRZYKŁADOWE MENU <br>CZYLI  NAJCZĘSTSZE WARIANTY ŻYWIENIOWE:</div>

            <div class="form__row" data-show>
              <div class="form__rowSubtitle">I POSIŁEK - ŚNIADANIE</div>
              <div class="form__row" data-group>
                <div class="form__checkBoxHolder -width100">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_13_problem_0']==="0") ? 'checked' : '' ?> data-required="user_13_problem_0" name="user_13_problem_0" value="0" id="checkbox_2" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_2" class="form__label">Nie</label>
                </div>
                <div class="form__checkBoxHolder -width100">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_13_problem_0']==="1") ? 'checked' : '' ?> data-required="user_13_problem_0" name="user_13_problem_0" value="1" id="checkbox_1" type="checkbox" class="form__checkbox" data-toggle data-item>
                  <label for="checkbox_1" class="form__label">Tak</label>
                </div>

              </div>
              <div class="-hideOnNo subform -width100">
              <label class="form__label  -hideOnNo">Opis</label>
              <textarea name="user_13_problem_0_t" class="form__moreTextarea -hideOnNo -longInput"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_13_problem_0_t']) ?></textarea>
              <label class="form__label  -hideOnNo">Przybliżone godziny</label>
              <textarea name="user_13_problem_0_w" class="form__moreTextarea -hideOnNo"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_13_problem_0_w']) ?></textarea>
            </div>
            </div>

            <div class="form__row" data-show>
              <div class="form__rowSubtitle ">II POSIŁEK - ŚNIADANIE / LUNCH</div>
              <div class="form__row" data-group>
                <div class="form__checkBoxHolder -width100">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_13_problem_1']==="0") ? 'checked' : '' ?> data-required="user_13_problem_1"  name="user_13_problem_1" value="0"  id="checkbox_4" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_4" class="form__label">Nie</label>
                </div>
                <div class="form__checkBoxHolder -width100">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_13_problem_1']==="1") ? 'checked' : '' ?> data-required="user_13_problem_1"  name="user_13_problem_1" value="1" id="checkbox_3" type="checkbox" class="form__checkbox" data-toggle data-item>
                  <label for="checkbox_3" class="form__label">Tak</label>
                </div>

              </div>
              <div class="-hideOnNo subform -width100">
                <label class="form__label  -hideOnNo">Opis</label>
                <textarea name="user_13_problem_1_t" class="form__moreTextarea -hideOnNo -longInput"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_13_problem_1_t']) ?></textarea>
                <label class="form__label  -hideOnNo">Przybliżone godziny</label>
                <textarea name="user_13_problem_1_w" class="form__moreTextarea -hideOnNo"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_13_problem_1_w']) ?></textarea>
                </div>
            </div>

            <div class="form__row" data-show>
              <div class="form__rowSubtitle ">III POSIŁEK - OBIAD</div>
              <div class="form__row" data-group>
                <div class="form__checkBoxHolder -width100">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_13_problem_2']==="0") ? 'checked' : '' ?> data-required="user_13_problem_2"  name="user_13_problem_2" value="0" id="checkbox_6" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_6" class="form__label">Nie</label>
                </div>
                <div class="form__checkBoxHolder -width100">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_13_problem_2']==="1") ? 'checked' : '' ?> data-required="user_13_problem_2" name="user_13_problem_2" value="1" id="checkbox_5" type="checkbox" class="form__checkbox" data-toggle data-item>
                  <label for="checkbox_5" class="form__label">Tak</label>
                </div>

              </div>
              <div class="-hideOnNo subform -width100">
                <label class="form__label  -hideOnNo">Opis</label>
                <textarea name="user_13_problem_2_t"  class="form__moreTextarea -hideOnNo -longInput"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_13_problem_2_t']) ?></textarea>
                <label class="form__label  -hideOnNo">Przybliżone godziny</label>
                <textarea name="user_13_problem_2_w" class="form__moreTextarea -hideOnNo"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_13_problem_2_w']) ?></textarea>
              </div>
            </div>

            <div class="form__row" data-show>
              <div class="form__rowSubtitle ">IV POSIŁEK - KOLACJA</div>
              <div class="form__row" data-group>
                <div class="form__checkBoxHolder -width100">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_13_problem_4']==="0") ? 'checked' : '' ?> data-required="user_13_problem_4" name="user_13_problem_4" value="0" id="checkbox_10" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_10" class="form__label">Nie</label>
                </div>
                <div class="form__checkBoxHolder -width100">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_13_problem_4']==="1") ? 'checked' : '' ?> data-required="user_13_problem_4" name="user_13_problem_4" value="1" id="checkbox_9" type="checkbox" class="form__checkbox" data-toggle data-item>
                  <label for="checkbox_9"  class="form__label">Tak</label>
                </div>

              </div>
              <div class="-hideOnNo subform -width100">
                <label class="form__label  -hideOnNo">Opis</label>
                <textarea name="user_13_problem_4_t"  class="form__moreTextarea -hideOnNo -longInput"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_13_problem_4_t']) ?></textarea>
                <label class="form__label  -hideOnNo">Przybliżone godziny</label>
                <textarea name="user_13_problem_4_w" class="form__moreTextarea -hideOnNo"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_13_problem_4_w']) ?></textarea>
              </div>
            </div>

          </div>

          <div class="form__col50">
            <div class="form__rowTitle">PRZEKĄSKI</div>

            <div class="form__row" data-show>
              <div class="form__checkBoxHolder">
                <input <?= ($_SESSION['individualDiagnostic']['sf_14_problem_0']==="1") ? 'checked' : '' ?> name="user_14_problem_0" value="1" id="checkbox_11" type="checkbox" class="form__checkbox" data-toggle>
                <label for="checkbox_11" class="form__label">Owoce</label>
              </div>
              <div class="-hideOnNo subform -width100">
                <label class="form__label  -hideOnNo">Ile? Jakie? W jakiej porze dnia?</label>
                <textarea name="user_14_problem_0_t" class="form__moreTextarea -hideOnNo -longInput" ><?= esc_textarea($_SESSION['individualDiagnostic']['sf_14_problem_0_t']) ?></textarea>
              </div>
            </div>

            <div class="form__row" data-show>
              <div class="form__checkBoxHolder">
                <input <?= ($_SESSION['individualDiagnostic']['sf_14_problem_1']==="1") ? 'checked' : '' ?> name="user_14_problem_1" value="1" id="checkbox_12" type="checkbox" class="form__checkbox" data-toggle>
                <label for="checkbox_12" class="form__label">Orzeszki</label>
              </div>
              <div class="-hideOnNo subform -width100">
                <label class="form__label  -hideOnNo">Ile? Jakie? W jakiej porze dnia?</label>
                <textarea name="user_14_problem_1_t" class="form__moreTextarea -hideOnNo -longInput" ><?= esc_textarea($_SESSION['individualDiagnostic']['sf_14_problem_1_t']) ?></textarea>
              </div>
            </div>

            <div class="form__row" data-show>
              <div class="form__checkBoxHolder">
                <input <?= ($_SESSION['individualDiagnostic']['sf_14_problem_2']==="1") ? 'checked' : '' ?> name="user_14_problem_2" value="1" id="checkbox_13" type="checkbox" class="form__checkbox" data-toggle>
                <label for="checkbox_13" class="form__label">Pestki słonecznika, dyni</label>
              </div>
              <div class="-hideOnNo subform -width100">
                <label class="form__label  -hideOnNo">Ile? Jakie? W jakiej porze dnia?</label>
                <textarea name="user_14_problem_2_t" class="form__moreTextarea -hideOnNo -longInput" ><?= esc_textarea($_SESSION['individualDiagnostic']['sf_14_problem_2_t']) ?></textarea>
              </div>
            </div>

            <div class="form__row" data-show>
              <div class="form__checkBoxHolder">
                <input <?= ($_SESSION['individualDiagnostic']['sf_14_problem_3']==="1") ? 'checked' : '' ?> name="user_14_problem_3" value="1" id="checkbox_14" type="checkbox" class="form__checkbox" data-toggle>
                <label for="checkbox_14" class="form__label">Owoce suszone</label>
              </div>
              <div class="-hideOnNo subform -width100">
                <label class="form__label  -hideOnNo">Ile? Jakie? W jakiej porze dnia?</label>
                <textarea name="user_14_problem_3_t" class="form__moreTextarea -hideOnNo -longInput" ><?= esc_textarea($_SESSION['individualDiagnostic']['sf_14_problem_3_t']) ?></textarea>
              </div>
            </div>

            <div class="form__row -m-b-30" data-show>
              <div class="form__checkBoxHolder">
                <input <?= ($_SESSION['individualDiagnostic']['sf_14_problem_4']==="1") ? 'checked' : '' ?> name="user_14_problem_4" value="1" id="checkbox_15" type="checkbox" class="form__checkbox" data-toggle>
                <label for="checkbox_15" class="form__label">Słodycze, ciasta, batony</label>
              </div>
              <div class="-hideOnNo subform -width100">
                <label class="form__label  -hideOnNo">Ile? Jakie? W jakiej porze dnia?</label>
                <textarea name="user_14_problem_4_t" class="form__moreTextarea -hideOnNo -longInput" ><?= esc_textarea($_SESSION['individualDiagnostic']['sf_14_problem_4_t']) ?></textarea>
              </div>
            </div>

            <div class="form__hr"></div>
            <div class="form__rowTitle">NABIAŁ</div>
            <div class="form__row" data-show data-group>
              <div class="form__checkBoxHolder -width100">
                <input <?= ($_SESSION['individualDiagnostic']['sf_14_problem_5']==="0") ? 'checked' : '' ?> data-required="user_14_problem_5" name="user_14_problem_5" value="0" id="checkbox_15b" type="checkbox" class="form__checkbox" data-item>
                <label for="checkbox_15b" class="form__label">Nie</label>
              </div>
              <div class="form__checkBoxHolder -width100">
                <input <?= ($_SESSION['individualDiagnostic']['sf_14_problem_5']==="1") ? 'checked' : '' ?> data-required="user_14_problem_5" name="user_14_problem_5" value="1" id="checkbox_15z" type="checkbox" class="form__checkbox" data-toggle data-item>
                <label for="checkbox_15z" class="form__label">Tak</label>
              </div>
              <div class="-hideOnNo subform -width100">
                <div class="form__label -hideOnNo">Jak często w tygodniu? Ile? Jaki nabiał?</div>
                <textarea name="user_14_problem_5_t" class="form__moreTextarea -hideOnNo -longInput"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_14_problem_5_t']) ?></textarea>
              </div>
            </div>
            <div class="form__row" data-show data-group>
              <div class="form__rowTitle">CZY WYSTĘPUJĄ  PRODUKTY ŻYWIENIOWE, KTÓRYCH PAN/I ZDECYDOWNIE  NIE ŻYCZY SOBIE W DIECIE</div>
              <div class="form__checkBoxHolder -width100" >
                <input <?= ($_SESSION['individualDiagnostic']['sf_15_problem_0']==="0") ? 'checked' : '' ?> data-required="user_15_problem_0" name="user_15_problem_0" value="0" id="checkbox_15cd" type="checkbox" class="form__checkbox" data-item>
                <label for="checkbox_15cd" class="form__label">Nie</label>
              </div>
              <div class="form__checkBoxHolder -width100">
                <input <?= ($_SESSION['individualDiagnostic']['sf_15_problem_0']==="1") ? 'checked' : '' ?> data-required="user_15_problem_0" name="user_15_problem_0" value="1" id="checkbox_15de" type="checkbox" class="form__checkbox" data-toggle data-item>
                <label for="checkbox_15de" class="form__label">Tak</label>
              </div>
              <div class="-hideOnNo subform -width100">
              <div class="form__label -hideOnNo">Jakie?</div>
              <textarea name="user_15_problem_1" class="form__moreTextarea -longInput -hideOnNo"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_15_problem_1']) ?></textarea>
              </div>
            </div>

            <div class="form__row" data-show>
              <div class="form__rowTitle">Jakie  rodzaje olei /tłuszczów  występują w Pana/i  diecie? </div></div>
              <textarea name="user_15_problem_3" class="form__moreTextarea -longInput"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_15_problem_3']) ?></textarea>
            </div>

          </div>
        </div>
        <div class="form__row">
          <div class="form__col50">
            <div class="form__hr"></div>
            <div class="form__rowTitle">JAKIEGO RODZAJU POSIŁKI SPOŻYWA PAN/I?</div>
            <div class="form__row" data-show>
              <div class="form__checkBoxHolder" >
                <input <?= in_array(1,$_SESSION['individualDiagnostic']['sf_15_problem_2'])  ? 'checked' : '' ?> name="user_15_problem_2[]" value="1" id="checkbox_16" type="checkbox" class="form__checkbox" data-toggle>
                <label for="checkbox_16" class="form__label">Jedzenie domowe</label>
              </div>
              <div class="-hideOnNo subform -width100">
                <label class="form__label  -hideOnNo">Jak często?</label>
                <textarea  name="user_15_problem_2_0" class="form__moreTextarea -hideOnNo"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_15_problem_2_0']) ?></textarea>
              </div>
            </div>
            <div class="form__row" data-show>
              <div class="form__checkBoxHolder">
                <input <?= in_array(2,$_SESSION['individualDiagnostic']['sf_15_problem_2'])  ? 'checked' : '' ?> name="user_15_problem_2[]" value="2" id="checkbox_16a" type="checkbox" class="form__checkbox" data-toggle>
                <label for="checkbox_16a" class="form__label">Jedzenie przygotowane na stołówce</label>
              </div>
              <div class="-hideOnNo subform -width100">
                <label class="form__label  -hideOnNo">Jak często?</label>
                <textarea  name="user_15_problem_2_1" class="form__moreTextarea -hideOnNo"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_15_problem_2_1']) ?></textarea>
              </div>
            </div>
            <div class="form__row" data-show>
              <div class="form__checkBoxHolder">
                <input <?= in_array(3,$_SESSION['individualDiagnostic']['sf_15_problem_2'])  ? 'checked' : '' ?> name="user_15_problem_2[]" value="3" id="checkbox_16b" type="checkbox" class="form__checkbox" data-toggle>
                <label for="checkbox_16b" class="form__label">Jedzenie w barze, restauracji</label>
              </div>
              <div class="-hideOnNo subform -width100">
                <label class="form__label  -hideOnNo">Jak często?</label>
                <textarea  name="user_15_problem_2_2" class="form__moreTextarea -hideOnNo"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_15_problem_2_2']) ?></textarea>
              </div>
            </div>
            <div class="form__row" data-show>
              <div class="form__checkBoxHolder">
                <input <?= in_array(4,$_SESSION['individualDiagnostic']['sf_15_problem_2'])  ? 'checked' : '' ?> name="user_15_problem_2[]" value="4" id="checkbox_16c" type="checkbox" class="form__checkbox" data-toggle>
                <label for="checkbox_16c" class="form__label">Fast Food</label>
              </div>
              <div class="-hideOnNo subform -width100">
                <label class="form__label  -hideOnNo">Jak często?</label>
                <textarea  name="user_15_problem_2_3" class="form__moreTextarea -hideOnNo"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_15_problem_2_3']) ?></textarea>
              </div>
            </div>
          </div>
          <div class="form__col50">
            <div class="form__hr"></div>
            <div class="form__rowTitle">MĄCZNE PRODUKTY</div>
            <div data-show data-group>
              <div class="form__checkBoxHolder">
                <input <?= ($_SESSION['individualDiagnostic']['sf_16_problem_0s']==="0") ? 'checked' : '' ?> name="user_16_problem_0s" value="0" id="checkbox_16f" type="checkbox" class="form__checkbox" data-item>
                <label for="checkbox_16f" class="form__label">Nie</label>
              </div>

              <div class="form__checkBoxHolder">
                <input <?= ($_SESSION['individualDiagnostic']['sf_16_problem_0s']==="1") ? 'checked' : '' ?> name="user_16_problem_0s" value="1" id="checkbox_16g" type="checkbox" class="form__checkbox" data-toggle data-item>
                <label for="checkbox_16g" class="form__label">Tak</label>
              </div>
              <div class="-hideOnNo subform -width100">

              <label class="form__label  -hideOnNo">Jakie?</label>
              <div class="subform2 -hideOnNo" style="display: block;width: 100%;" >

                <div class="form__row" data-show>
                  <div class="form__checkBoxHolder -width100">
                    <input <?= ($_SESSION['individualDiagnostic']['sf_16_problem_0']==="1") ? 'checked' : '' ?> name="user_16_problem_0" value="1" id="checkbox_16d" type="checkbox" class="form__checkbox" data-toggle>
                    <label for="checkbox_16d" class="form__label">Makarony</label>
                  </div>
                  <div class="-hideOnNo subform3 -width100">
                    <label class="form__label  -hideOnNo">Jak często?</label>
                    <textarea name="user_16_problem_0_w" class="form__moreTextarea -hideOnNo" ><?= esc_textarea($_SESSION['individualDiagnostic']['sf_16_problem_0_w']) ?></textarea>
                  </div>
                </div>

                <div class="form__row"  data-show>
                  <div class="form__checkBoxHolder">
                    <input <?= ($_SESSION['individualDiagnostic']['sf_16_problem_1']==="1") ? 'checked' : '' ?> name="user_16_problem_1" value="1" id="checkbox_17" type="checkbox" class="form__checkbox" data-toggle>
                    <label for="checkbox_17" class="form__label">Pierogi</label>
                  </div>
                  <div class="-hideOnNo subform3 -width100">
                    <label class="form__label  -hideOnNo">Jak często?</label>
                    <textarea name="user_16_problem_1_w" class="form__moreTextarea -hideOnNo" ><?= esc_textarea($_SESSION['individualDiagnostic']['sf_16_problem_1_w']) ?></textarea>
                  </div>
                </div>

                <div class="form__row" data-show>
                  <div class="form__checkBoxHolder">
                    <input <?= ($_SESSION['individualDiagnostic']['sf_16_problem_2']==="1") ? 'checked' : '' ?> name="user_16_problem_2" value="1" id="checkbox_18a" type="checkbox" class="form__checkbox" data-toggle>
                    <label for="checkbox_18a" class="form__label">Naleśniki</label>
                  </div>
                  <div class="-hideOnNo subform3 -width100">
                    <label class="form__label  -hideOnNo">Jak często?</label>
                    <textarea name="user_16_problem_2_w" class="form__moreTextarea -hideOnNo"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_16_problem_2_w']) ?></textarea>
                  </div>

                </div>

                <div class="form__row"  data-show>
                  <div class="form__checkBoxHolder">
                    <input <?= ($_SESSION['individualDiagnostic']['sf_16_problem_3']==="1") ? 'checked' : '' ?> name="user_16_problem_3" value="1" id="checkbox_19" type="checkbox" class="form__checkbox" data-toggle>
                    <label for="checkbox_19" class="form__label">Pizza</label>
                  </div>
                  <div class="-hideOnNo subform3 -width100">
                    <label class="form__label  -hideOnNo">Jak często?</label>
                    <textarea name="user_16_problem_3_w" class="form__moreTextarea -hideOnNo" ><?= esc_textarea($_SESSION['individualDiagnostic']['sf_16_problem_3_w']) ?></textarea>
                  </div>
                </div>

                <div class="form__row" data-show>
                  <div class="form__checkBoxHolder">
                    <input <?= ($_SESSION['individualDiagnostic']['sf_16_problem_4']==="1") ? 'checked' : '' ?> name="user_16_problem_4" value="1" id="checkbox_20" type="checkbox" class="form__checkbox" data-toggle>
                    <label for="checkbox_20" class="form__label">Zapiekanki</label>
                  </div>
                  <div class="-hideOnNo subform3 -width100">
                    <label class="form__label  -hideOnNo">Jak często?</label>
                    <textarea name="user_16_problem_4_w" class="form__moreTextarea -hideOnNo"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_16_problem_4_w']) ?></textarea>
                  </div>
                </div>

                <div class="form__row-m-b-30 subform" data-show>
                  <div class="form__checkBoxHolder">
                    <input <?= ($_SESSION['individualDiagnostic']['sf_16_problem_5']==="1") ? 'checked' : '' ?> name="user_16_problem_5" value="1" id="checkbox_20a" type="checkbox" class="form__checkbox" data-toggle>
                    <label for="checkbox_20a" class="form__label" style="margin-left: 7px;">Inne</label>
                  </div>
                  <div class="-hideOnNo subform subform4 -width100">
                    <label class="form__label  -hideOnNo">Jakie?</label>
                    <textarea name="user_16_problem_5_w" class="form__moreTextarea -hideOnNo" ><?= esc_textarea($_SESSION['individualDiagnostic']['sf_16_problem_5_w']) ?></textarea>
                    <label class="form__label  -hideOnNo">Jak często?</label>
                    <textarea name="user_16_problem_5_d" class="form__moreTextarea -hideOnNo" ><?= esc_textarea($_SESSION['individualDiagnostic']['sf_16_problem_5_d']) ?></textarea>
                  </div>
                </div>

              </div>
            </div>
            </div>

          </div>
          <div class="form__col50 form-napoje">
            <div class="form__hr"></div>
            <div class="form__rowTitle">NAPOJE</div>
            <div class="form__label">Ile wody Pan/i  pije przez cały dzień, proszę podać nazwę wody  ? </div>
            <textarea name="user_19_problem_0" class="form__moreTextarea -longInput"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_19_problem_0']) ?></textarea>
            <div class="form__label">Herbaty (jakie, ile?)</div>
            <textarea name="user_19_problem_1" class="form__moreTextarea -longInput"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_19_problem_1']) ?></textarea>
            <div class="form__label">Kawy (jakie, ile?)</div>
            <textarea name="user_19_problem_2" class="form__moreTextarea -longInput"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_19_problem_2']) ?></textarea>
            <div class="form__label">Soki (jakie, ile?)</div>
            <textarea name="user_19_problem_3" class="form__moreTextarea -longInput"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_19_problem_3']) ?></textarea>
            <div class="form__label">Napoje gazowane (jakie, ile?)</div>
            <textarea name="user_19_problem_4" class="form__moreTextarea -longInput"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_19_problem_4']) ?></textarea>
            <div class="form__label">Energetyki (jakie, ile?)</div>
            <textarea name="user_19_problem_5" class="form__moreTextarea -longInput"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_19_problem_5']) ?></textarea>
          </div>
          <div class="form__col50">
            <div class="form__hr"></div>
            <div class="form__rowTitle">UŻYWKI</div>
            <div class="form__row" data-show>
              <div class="form__checkBoxHolder">
                <input <?= ($_SESSION['individualDiagnostic']['sf_17_problem_0']==="1") ? 'checked' : '' ?> name="user_17_problem_0" value="1" id="checkbox_16e" type="checkbox" class="form__checkbox" data-toggle>
                <label for="checkbox_16e" class="form__label">Alkohol</label>
              </div>
              <div class="-hideOnNo subform -width100">
                  <label class="form__label  -hideOnNo">Jaki?</label>
                  <textarea name="user_17_problem_0_w" class="form__moreTextarea -hideOnNo"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_17_problem_0_w']) ?></textarea>
                  <label class="form__label  -hideOnNo">Jak często?</label>
                  <textarea name="user_17_problem_0_t" class="form__moreTextarea -hideOnNo" ><?= esc_textarea($_SESSION['individualDiagnostic']['sf_17_problem_0_t']) ?></textarea>
                </div>
            </div>

            <div class="form__row" data-show>
              <div class="form__checkBoxHolder">
                <input <?= ($_SESSION['individualDiagnostic']['sf_17_problem_1']==="1") ? 'checked' : '' ?> name="user_17_problem_1" value="1" id="checkbox_17b" type="checkbox" class="form__checkbox" data-toggle>
                <label for="checkbox_17b" class="form__label">Papierosy</label>
              </div>
              <div class="-hideOnNo subform -width100">
                  <label class="form__label  -hideOnNo">Jak często?</label>
                  <textarea name="user_17_problem_1_t" class="form__moreTextarea -hideOnNo" ><?= esc_textarea($_SESSION['individualDiagnostic']['sf_17_problem_1_t']) ?></textarea>
              </div>
            </div>

            <div class="form__row" data-show>
              <div class="form__checkBoxHolder">
                <input <?= ($_SESSION['individualDiagnostic']['sf_17_problem_2']==="1") ? 'checked' : '' ?> name="user_17_problem_2" value="1" id="checkbox_18" type="checkbox" class="form__checkbox" data-toggle>
                <label for="checkbox_18" class="form__label">Inne używki</label>
              </div>
              <div class="-hideOnNo subform -width100">
                  <label class="form__label  -hideOnNo">Jakie?</label>
                  <textarea name="user_17_problem_2_w" class="form__moreTextarea -hideOnNo"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_17_problem_2_w']) ?></textarea>
                  <label class="form__label  -hideOnNo">Jak często?</label>
                  <textarea name="user_17_problem_2_t" class="form__moreTextarea -hideOnNo" ><?= esc_textarea($_SESSION['individualDiagnostic']['sf_17_problem_2_t']) ?></textarea>
              </div>
            </div>


          </div>
        </div>
        <div class="form__nav" data-questionnaire-navigation>
          <input type="hidden" value="ankieta_3" name="action">
          <input type="hidden" value="ankieta_3" name="step">
          <button class="btn form__saveDiet" data-questionnaire-save>Zapisz ankietę</button>
          <button class="form__button" onclick="window.onbeforeunload = null;">PRZEJDŹ DALEJ</button>
        </div>
      </div>
    </form>
  </main><!-- #main -->
</div><!-- #primary -->
  <script>
     var inputs = document.querySelectorAll('input, select, textarea');
     for(var i = 0; inputs && i < inputs.length; i++)
         inputs[i].addEventListener('change',function () {
             window.onbeforeunload = function () {
                 return true;
             };
         });
  //</script>
<?php get_footer();
