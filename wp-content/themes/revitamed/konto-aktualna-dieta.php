<?php
/**
 * The template for displaying all pages.
 * Template name: Konto - moja dieta
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */
user_page(false,true);
$userRevitamed = new user();
$userID = $userRevitamed->getId();

global $diet,$dietID;
$isArchiveDiet = true;
if($diet===NULL)
{
  $diet = getCurrentActiveDiet($userID);
  $diet = ($diet === null) ? getFutureDiet($userID) : $diet;
  $dietID = $diet->ID;
	$isArchiveDiet = false;
}

$measure = getTodayMeasure($userID);
$part = wp_get_post_terms($dietID,'etap_diety');

$dateStart = get_post_meta($dietID,'sf_date_from',true);
$dateStop = get_post_meta($dietID,'sf_date_to',true);


$userWeight = getUserWeigh($userID);
$amountOfMeals = get_post_meta($dietID,'sf_quantity_meal',true);
$dateDiff = $dateStop - strtotime('now');
$dayBetween =  floor($dateDiff / (60 * 60 * 24));
$durationOfDiet = get_post_meta($dietID,'sf_duration',true);
$subEnd = $userRevitamed->getUserLastActiveDay();
$dateDiff = $subEnd - strtotime('now');
$dayBetweenSub =  floor($dateDiff / (60 * 60 * 24));

$dietDurationTerms =  wp_get_post_terms($dietID, 'dl_diety');
$dietDuration =  (is_object($dietDurationTerms[0])) ? $dietDurationTerms[0]->name : '';
$forbiddenProducts = (is_array(get_post_meta($dietID,'sf_forbidden',true))) ? ''  : get_post_meta($dietID,'sf_forbidden',true);
$supplies = get_post_meta($dietID,'sf_supplements',true);
$warnings = get_post_meta($dietID,'sf_warnings',true);
$titan = TitanFramework::getInstance('revita');


  if($dayBetween>1)
    $textEndDiet = "(do końca diety $dayBetween dni)";
  elseif($dayBetween==1)
    $textEndDiet = "(pozostał jeden dzień)";
  else
    $textEndDiet = "(to jest ostatni dzień diety)";

$currentDay = ($_GET['date']=='' || strtotime($_GET['date'])==0) ?  date('d.m.Y') : $_GET['date'];

if(strtotime($currentDay) < $dateStart)
	$currentDay = date('d.m.Y',$dateStart);

$day = getDietInfoDay($dietID,strtotime($currentDay));
$dateStopView = strtotime('-1 day',$dateStop);

get_header(); ?>
<div id="primary" class="content-area konto-tpl">
  <div class="container">
    <div class="col-12">
    <div class="wrapper">
      <div class="title">Witaj, <span class="name"><?= getUserName($userID) ?>!</span> Zacznij z naszą pomocą dobry dzień</div>
      <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
        <?php if (function_exists('bcn_display')) {
            bcn_display();
        } ?>
      </div>
    </div><!-- ./wrapper -->
  </div>
  </div>
  <div class="container">
    <aside class="col-3 sidebar-konto">
      <div class="side-menu side-item">
	      <?php include 'template-parts/menu-user.php' ?>
      </div><!-- ./sidemenu -->
        <?php include 'template-parts/progress-user.php' ?>
        <?php include 'template-parts/subs-user.php' ?>
    </aside>
    <main id="main" class="site-main account-p col-9" role="main">
	    <?php if(isset($_GET['survey']) && $_GET['survey']=='1'){?>
          <div class="-notPrint" style="padding: 15px; margin-bottom: 20px; border: 1px solid #dff0d8;  border-radius: 4px; background-color: #dff0d8;"><?= $titan->getOption('post_survey_text_paid_account') ?></div>
	    <?php } ?>
      <?php
      $subEnd = $userRevitamed->getUserLastActiveDay();
      $dateDiff = $subEnd - strtotime('now');
      $dayBetweenSub =  ceil($dateDiff / (60.0 * 60 * 24));

      if($dayBetweenSub <= 7){ ?>
      <div class="red"><?= $titan->getOption('sub_end_is_coming_user_view') ?></div>
      <?php }


      $partialSurvey = getLastPartialSurvey($userRevitamed->getId(),'any');

	    if($partialSurvey!==null && $partialSurvey->post_status=='publish') {


			    ?>
              <section class="questionnaire -notPrint">
                <div class="questionnaire__content">
                  <div class="questionnaire__text">
	                  <?= $titan->getOption('part_survey_user'); ?>
                  </div>
                  <a href="<?= get_the_permalink($partialSurvey) ?>" class="btn questionnaire__button">Wypełnij ankietę &gt;</a>
                </div>
              </section>
		    <?php } ?>

      <?php if(isset($_GET['ac']) && $_GET['ac'] == 'sukces'){ ?>
      <section class="questionnaire -notPrint" style="padding: 15px;margin-bottom: 20px; border: 1px solid transparent;border-radius: 4px; color: #3c763d;background-color: #dff0d8;border-color: #dff0d8;">
	          <?=  !getCurrentUserPlan( $userID, strtotime( '+21 days' ))  ? $titan->getOption('partial_survey_accept_unpaid') : $titan->getOption('partial_survey_accept') ?>
      </section>
    <?php } ?>

      <section class="progressBar" <?php if($isArchiveDiet){ ?>style="display:none !important;"<?php } ?>>
        <div class="progressBar__content">
          <div class="progressBar__labels">
            <div class="progressBar__label -yourLvl" data-today="<?= date('d.m.Y') ?>">JESTEŚ TUTAJ</div>
            <div class="progressBar__label -access" data-access="<?= date('d.m.Y',$subEnd) ?>">OPŁACONY DOSTĘP</div>
          </div>
          <div class="progressBar__labels -bottom">
            <div class="progressBar__label -begin">DATA ROZPOCZĘCIA</div>
            <div class="progressBar__label -end">DATA ZAKOŃCZENIA</div>
          </div>
          <div class="progressBar__bar">
            <div class="progressBar__line -begin"></div>
            <div class="progressBar__progress"></div>
            <div class="progressBar__line -end"></div>
          </div>
          <div class="progressBar__dates">
            <div class="progressBar__date -register"><?= date('d.m.Y',$dateStart) ?></div>
            <div class="progressBar__date -end"><?= date('d.m.Y',$dateStopView) ?></div>
          </div>
        </div>
      </section>

      <section class="diet-header">
        <div class="left-info col-6">
          <h3>Informacje o Twojej diecie</h3>
          <p><strong>Data rozpoczęcia diety:</strong> <?= date('d.m.Y',$dateStart) ?></p>
          <p><strong>Etap diety:</strong> <?= $part[0]->name; ?> <?php if(!$isArchiveDiet){ ?> <span class="red"><?= $textEndDiet ?></span><?php } ?></p>
          <p><strong>Wskazany przez lekarza okres trwania diety:</strong> <?= $dietDuration ?></p>
        </div><!-- ./left-info -->
        <div class="col-6 msrmntHolder">
<?php if(!$isArchiveDiet){ ?>
          <div class="msrmnt">
            <h4>POMIARY - <?= date('d.m.y') ?></h4>
            <div class="js-alert-container"></div>
            <form class="js-submit-weight" method="post">
              <fieldset class="row1">
                <label for="waga">Twoja waga:</label>
                <span class="in-wrap kg">
								<input value="<?= $measure['user_weight'] ?>" required type="text" name="user_weight" pattern="[0-9,]+" id="waga">
							</span>
                <input name="action" type="hidden" value="weight_update">
                <input type="submit" value="ZAPISZ">
              </fieldset><!-- ./waga -->
            </form>
            <form class="js-submit-circuit" method="post">
              <fieldset class="row2">
                <div>
                  <label for="ob-talia">Obwód talii:</label>
                  <span class="in-wrap cm">
									<input value="<?= $measure['ob-talia'] ?>" type="text" required name="ob-talia" pattern="[0-9,]+">
								</span>
                </div>
                <div>
                  <label for="ob-brzuch">Obwód brzucha:</label>
                  <span class="in-wrap cm">
									<input value="<?= $measure['ob-brzuch'] ?>" type="text" required name="ob-brzuch" pattern="[0-9,]+">
								</span>
                </div>
                <div>
                  <label for="ob-biodra">Obwód bioder:</label>
                  <span class="in-wrap cm">
									<input value="<?= $measure['ob-biodra'] ?>" type="text" required name="ob-biodra" pattern="[0-9,]+" >
								</span>
                </div>
                <div>
                  <input name="action" type="hidden" value="circuit_update">
                  <input type="submit" value="ZAPISZ">
                </div>
              </fieldset>
            </form>
          </div>
<?php } ?>
        </div><!-- ./msrmnt -->
      </section><!-- ./diet-header -->
      <section class="diet-calendar">
        <div class="calendar">
          <div class="calendar__title">Kalendarz diety</div>
          <div class="calendar__slider">
            <?php
            foreach(getAllDietUser($userID,'ASC') as $diet){

	            $dateStartArchive = get_post_meta($diet->ID,'sf_date_from',true);
	            $dateStopArchive = get_post_meta($diet->ID,'sf_date_to',true);
	            $counter = 0;
	            if($dateStopArchive!=0)
	            while( strtotime("+$counter days",$dateStartArchive) <= $dateStopArchive)
	            {
	              $timestampToday = strtotime("+$counter days",$dateStartArchive);

	              if(getCurrentUserPlan($userID,$timestampToday) == 0)
                {
                  $counter++;
                  continue;
                }

		            $date = date('d.m.Y',$timestampToday);
		            if($diet->ID != getCurrentActiveDiet($userID)->ID)
			            $link = get_the_permalink(get_id_after_template_filename('konto-dieta-archiwalna.php')).'?dietID='.intval($diet->ID).'&date='.$date;
		            else
			            $link = get_the_permalink(get_id_after_template_filename('konto-aktualna-dieta.php')).'?date='.$date;

		            printf('<a href="%s" class="calendar__link %s"><span>%s</span>%s</a>',$link
			            ,
			            (date('d.m.Y',strtotime("+$counter days",$dateStartArchive))==$currentDay) ? '-active' : '',
			            mb_strtoupper(__(date('D',strtotime("+$counter days",$dateStartArchive)))),
			            $date);
		            $counter++;
	            }
            }

//            while( strtotime("+$counter days",$dateStart) < $dateStop)
//            {
//              $date = date('d.m.Y',strtotime("+$counter days",$dateStart));
//              if($isArchiveDiet)
//	              $link = get_the_permalink(get_id_after_template_filename('konto-dieta-archiwalna.php')).'?dietID='.intval($dietID).'&date='.$date;
//              else
//                $link = get_the_permalink(get_id_after_template_filename('konto-aktualna-dieta.php')).'?date='.$date;
//
//              printf('<a href="%s" class="calendar__link %s"><span>%s</span>%s</a>',$link
//                  ,
//                  (date('d.m.Y',strtotime("+$counter days",$dateStart))==$currentDay) ? '-active' : '',
//                  mb_strtoupper(__(date('D',strtotime("+$counter days",$dateStart)))),
//	              $date);
//	            $counter++;
//            }


            ?>
          </div>
          <div class="button calendar__button -printShopingList"><i class="fa fa-print" aria-hidden="true"></i> DRUKUJ LISTĘ ZAKUPÓW</div>
          <div class="button calendar__button -print"><i class="fa fa-print" aria-hidden="true"></i> DRUKUJ KALENDARZ</div>
        </div>
        <div class="calendarTable">
          <table class="diet-table calendarTable__content">
            <tr class="day-row unmarked -mainDay" data-day="<?= $currentDay ?>">
              <td class="day-col">
                <table>
                  <tr>
                    <th>DATA</th>
                  </tr>
                  <tr>
                    <td><strong><?= mb_strtoupper(__(date('l',strtotime($currentDay)))) ?></strong><br><?= $currentDay ?></td>
                  </tr>
                </table>

              </td>
              <td class="day-wrap" colspan="2">
                <table class="inner-day-table">
                  <tr>

                    <th colspan="2">POSIŁKI</th>
                    <th>WYKONANIE</th>
                  </tr>
                  <?php

                  $nthMeal = 0;

                  $day['meals'] = array_filter($day['meals'],function($single_meal){
                    return  !($single_meal['recipe']=='' || $single_meal['recipe']==0);
                  });
                  foreach($day['meals'] as $single_meal){
                    $mealEaten = getMealEaten($dietID,$nthMeal,$currentDay);
                    $nameMeal = ($nthMeal + 1) . ' Posiłek';


                    $recipe = get_post($single_meal['recipe']);
                    $ing = [];
                    foreach($single_meal['ing'] as $el)
	                    $ing []= $el['amount']. ' ' .get_unit_name(getUnitByIng($el['ing']),$el['amount']) . ' ' . get_the_title($el['ing'])  ;

                    $recipeImageID = get_post_thumbnail_id($recipe->ID);
                    ?>

                  <tr class="meal-row">
                    <td class="meal-col" data-meal="<?= $nthMeal ?>">
                      <div class="meal-title"><?= $nameMeal; ?></div>
                      <div class="meal-left">

                          <?php if(!empty($recipeImageID)){ ?>
                            <img src="<?= wp_get_attachment_image_url($recipeImageID,'recipe-diet-thumb') ?>" alt="<?= get_the_title($recipeImageID) ?>">
                          <?php } else { ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/diet-archive-img.jpg" alt="">
                          <?php } ?>
                      </div>
                      <div class="meal-center">
                        <div class="recipe-title"><?= $recipe->post_title ?></div>
                          <?php if(!empty($ing)) {?>
                        <div class="components-box"><span>Składniki:</span>

                        <?php
                          if(count($ing) > 0 ){
                            echo "<ul class='components-list'>";
                            foreach($ing as $ingSingle ){
                              echo "<li>".$ingSingle."</li>";
                            }
                            echo "</ul>";
                          }
                        ?>
                        </div>
                        <?php } ?>
                      </div>
                      <div class="meal-right recipe-link"><a target="_blank" href="<?= get_permalink($single_meal['recipe']) ?>">Zobacz przepis</a></div>
                    </td>
                    <td class="calendarTable__stats">

                    </td>
                    <td class="fun-col" style="<?php if($isArchiveDiet){ ?>display:none;<?php } ?>">
                      <div class="confirmed <?= (strtotime('now') < strtotime($currentDay) && date('d.m.Y')!=$currentDay) ? '-disabled' : '' ?> <?= ($mealEaten!==NULL && $mealEaten==1) ? '-active' : '' ?>" data-value="confirmed"><i class="fa fa-check" aria-hidden="true"></i>ZJADŁEM</div>
                      <div class="unconfirmed <?= (strtotime('now') < strtotime($currentDay) && date('d.m.Y')!=$currentDay) ? '-disabled' : '' ?> <?= ($mealEaten!==NULL && $mealEaten==0) ? '-active' : '' ?>" data-value="unconfirmed"><i class="fa fa-times" aria-hidden="true"></i>NIE ZJADŁEM</div>
                      <div class="existed <?= (strtotime('now') < strtotime($currentDay) && date('d.m.Y')!=$currentDay) ? '-disabled' : '' ?> <?= ($mealEaten!==NULL && $mealEaten==2) ? '-active' : '' ?>" data-value="existed"><i class="fa fa-times" aria-hidden="true"></i> ZJADŁEM COŚ INNEGO</div>
                    </td>
                  </tr><!-- ./meal-row -->
                <?php

	                  $nthMeal++;
                  } ?>
                </table><!-- ./innerdaytable -->
              </td><!-- ./daywrap -->
            </tr><!-- ./day-row unmarked -->
          </table><!-- ./diet-table -->
        </div>
        <?php if(!$isArchiveDiet){ ?>
        <div class="diet-summary"><i class="fa fa-line-chart" aria-hidden="true"></i><span id="current_plan_real" class="red"><?= (int)getCurrentPartOfPlan($currentDay,$dietID); ?>% </span>wykonanego planu</div><!-- ./diet-summary -->
    <?php } ?>
      </section><!-- ./diet-calendar -->


      <?php if($supplies!='' || $warnings!=''){?>
      <section class="diet-info">
        <div class="row">
        <?php if($supplies!=''){ ?>
        <div class="box-info col-6">
          <h3>Codzienna suplementacja</h3>
          <div class="inner-box">
            <?= apply_filters('the_content',$supplies); ?>
          </div><!-- ./inner-box -->
        </div><!-- ./boxinfo -->
        <?php } ?>
        <?php if($warnings!=''){?>
        <div class="box-info col-6">
          <h3>Uwagi</h3>
          <div class="inner-box">
	          <?= apply_filters('the_content',$warnings); ?>
          </div><!-- ./inner-box -->
        </div><!-- ./boxinfo -->
        <?php } ?>
        </div>
      </section><!-- ./diet-info -->
    <?php } ?>
      <?php if($forbiddenProducts!= '') {?>
      <section class="prohibitedProducts">
        <div class="prohibitedProducts__content">
          <div class="prohibitedProducts__title">Produkty zakazane</div>
          <div class="prohibitedProducts__item"><?= nl2br($forbiddenProducts) ?></div>
        </div>
      </section>
    <?php } ?>
      <div class="shopingList__list">
        <div class="shopingList__header">
          <div class="shopingList__title">Lista zakupów</div>
          <img class="shopingList__logo" src="<?php echo get_template_directory_uri(); ?>/images/logo.png">
          <div class="shopingList__dates">od <span class="shopingList__fromDate"></span> do <span class="shopingList__toDate"></span> </div>
        </div>
        <div class="shopingList__itemsHolder"></div>
      </div>
    </main><!-- #main -->
  </div><!-- ./container -->
</div><!-- #primary -->

<div class="shopingList" data-diet="<?= $dietID ?>">
  <div class="shopingList__background"></div>
  <div class="shopingList__content">
    <div class="shopingList__exit"><i class="fa fa-times" aria-hidden="true"></i></div>
    <div class="shopingList__title">Wydrukuj listę zakupów</div>
    <div class="shopingList__label">Wybierz zakres dat:</div>
    <div class="shopingList__datepickers">
      <input class="shopingList__input -from" id="from" type="text" data-min="<?= date('d.m.Y',$dateStart) ?>"  placeholder="Od:"> -
      <input class="shopingList__input -to" id="to" type="text" data-max="<?= date('d.m.Y',$dateStop) ?>" placeholder="Do:">
    </div>
    <div class="shopingList__button -pdf">DRUKUJ .PDF</div>
    <br>
    <div class="shopingList__button -mail">WYŚLIJ LISTĘ ZAKUPÓW NA MAILA</div>
    <div class="shopingList__success" style="display:none;">Dziękujemy! Formularz został wysłany poprawnie. Na twój adres email wysłaliśmy listę zakupów!</div>
  </div>
</div>
<div class="calendarList -notPrint" data-diet="<?= $dietID ?>">
  <div class="calendarList__background"></div>
  <div class="calendarList__content">
    <div class="calendarList__exit"><i class="fa fa-times" aria-hidden="true"></i></div>
    <div class="calendarList__title">Wydrukuj kalendarz</div>
    <div class="calendarList__label">Wybierz zakres dat:</div>
    <div class="calendarList__datepickers">
      <input id="from2" class="calendarList__input -from" value="<?= date('d.m.Y',$dateStart) ?>" data-min="<?= date('d.m.Y',$dateStart) ?>"  type="text" placeholder="Od:"> -
      <input id="to2" class="calendarList__input -to" value="<?= date('d.m.Y',$dateStop) ?>" data-max="<?= date('d.m.Y',$dateStop) ?>" type="text" placeholder="Do:">
    </div>
    <div class="calendarList__checkbox"><input type="checkbox" id="full_recipe"> <label for="full_recipe">Pokaż pełne przepis</label></div>
    <div class="calendarList__button -pdf" data-text="DRUKUJ .PDF">DRUKUJ .PDF</div>
  </div>
</div>

<?php get_footer();
