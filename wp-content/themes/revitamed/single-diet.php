<?php
/**
 * The template for displaying all pages.
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */

doc_page();
$currentDiet    = get_post( get_the_ID() );
$dietID         = $currentDiet->ID;
$_GET['userID'] = $userID = get_post_meta( $dietID, 'sf_user', true );

if ( $_POST['form'] == 'step_1_diet' ) {
	saveTemplateDiet();
}
$dietID = get_the_ID();
$days   = getDietInfo( $dietID , time() );

$forbidden     = get_post_meta( $dietID, 'sf_forbidden', true );
$forbidden     = ( is_array( $forbidden ) ) ? '' : $forbidden;
$supp          = get_post_meta( $dietID, 'sf_supplements', true );
$comments_diet = get_post_meta( $dietID, 'sf_warnings', true );

remove_theme_support( 'title-tag' );

$dateDietStart = date( 'd.m', get_post_meta( $dietID, 'sf_date_from', true ) );
$dateDietStop  = date( 'd.m', get_post_meta( $dietID, 'sf_date_to', true ) );
$weightStart   = getClosestWeight( $userID, get_post_meta( $dietID, 'sf_date_from', true ) );
$weightStop    = getClosestWeight( $userID, get_post_meta( $dietID, 'sf_date_to', true ) );
$part_a        = wp_get_post_terms( $dietID, 'etap_diety' );
$part          = $part_a[0]->name;
$length_a      = wp_get_post_terms( $dietID, 'dl_etapu' );
$length        = $length_a[0]->name;
$amountOfMeals = get_post_meta( $dietID, 'sf_quantity_meal', true );


$title = "$dateDietStart-$dateDietStop, $length, $part, $amountOfMeals dania, ";
if ( $weightStart != '' && $weightStop != '' ) {
	$title .= "Waga {$weightStart}kg > {$weightStop} kg";
}
add_action( 'wp_head', function () use ( $title ) {
	echo "<title>$title</title>";
}, 0 );
get_header();

?>
  <div id="primary" class="content-area konto-tpl">
    <div class="container">
      <div class="wrapper">
        <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
          <!-- Breadcrumb NavXT 5.7.0 -->
          <span property="itemListElement" typeof="ListItem">
            <a property="item" typeof="WebPage" href="<?= home_url( '/' ) ?>" class="home">
              <span property="name">Revitadiet</span>
            </a>
            <meta property="position" content="1">
          </span>
          &gt;
          <span property="itemListElement" typeof="ListItem">
            <a property="item" typeof="WebPage" class="taxonomy etap_diety">
              <span property="name">Dieta</span>
            </a>
            <meta property="position" content="2">
          </span> &gt;
          <span property="itemListElement" typeof="ListItem"><span property="name">
              <?= get_the_title(); ?></span><meta property="position" content="3"></span></div>
      </div><!-- ./wrapper -->
    </div>
    <div class="container">
      <aside class="col-3 sidebar-konto">
        <div class="side-menu side-item">
			<?php include 'template-parts/menu-dieter.php'; ?>
			<?php include 'template-parts/user-info-dieter.php'; ?>
        </div><!-- ./sidemenu -->
      </aside>
      <main id="main" class="site-main account-p col-9" role="main">
        <form data-diet-form>
          <h3>Dieta</h3>
          <a
                  href="<?= get_the_permalink( get_id_after_template_filename( 'diet-laying-step-1.php' ) ) ?>?dietID=<?= get_the_ID() ?>">EDYTUJ
              USTAWIENIA DIETY</a>
<div class="diet-top-box sticky-scroll-box sticky">
					<div class="days-buttons-box">
          <span class="all-btn" data-toggle="hide">Ukryj wszystkie</span>
          <span class="day-btn" data-day-show="0">Pn.</span>
          <span class="day-btn" data-day-show="1">Wt.</span>
          <span class="day-btn" data-day-show="2">Śr.</span>
          <span class="day-btn" data-day-show="3">Czw.</span>
          <span class="day-btn" data-day-show="4">Pt.</span>
          <span class="day-btn" data-day-show="5">Sob.</span>
          <span class="day-btn" data-day-show="6">Niedz.</span>
          <span class="btn-skladniki">Ukryj składniki</span>
          </div>
</div>
<style>
.fixed {
	position:fixed;
	top:0;
	z-index:99999;
}

</style>
          <div class="dietPicker js-dietPicker"
               data-autocomplete="<?= esc_attr( json_encode( getAllRecipies() ) ); ?>">
            <form class="dietPicker__form" method="post" action="#" data-diet-form>

				<?php displayDietPlanEdit( $days ); ?>

				<?php displayDietTextFields( $forbidden, $supp, $comments_diet ); ?>

              <div class="dietPicker__navigation" data-diet-navigation>
                <button class="btn dietPicker__link" data-diet-save>Zapisz zmiany w diecie</button>
              </div>
              <input type="hidden" value="sf_template" name="form">
              <input type="hidden" value="<?= get_the_ID(); ?>" name="sf_template">

            </form>

          </div>
        </form>

      </main><!-- #main -->
    </div><!-- ./container -->
  </div><!-- #primary -->
<?php get_footer();
