<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package like
 */

if ( ! is_active_sidebar( 'sidebar-main' ) ) {
	return;
}
?>
	<?php dynamic_sidebar( 'sidebar-main' ); ?>
