<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */

get_header(); ?>
<div id="primary" class="content-area mb container">
	<div class="col-12">
	<div class="wrapper">
		<h2><?php the_title(); ?></h2>
		<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
			<?php if(function_exists('bcn_display'))
			{
				bcn_display();
			}?>
		</div>
	</div><!-- ./wrapper -->
</div>
	<main id="main" class="site-main bli col-9" role="main">
		<?php the_content(); ?>

	</main><!-- #main -->
<aside id="secondary" class="col-3">
	<?php dynamic_sidebar('strona-sidebar'); ?>
</aside>
</div><!-- #primary -->

<?php get_footer();
