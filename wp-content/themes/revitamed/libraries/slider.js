var Slider = {
  init: function (target, args) {
    this.target = target;
    this.isSlick = args.slick || false;
    this.isLoader = args.loader || false;
    this.isImageGallery = args.popupImage || false;
    this.isVideoGallery = args.popupVideo || false;
    this.slickOptions = Object.assign({
      slide: '.' + $(target).children().attr('class'),
      infinite: true,
      dots: false,
      appendDots: target + '-dots',
      customPaging: function (slider, i) {
        return '<div class="-dot"></div>';
      },
      slidesToShow: 3,
      slidesToScroll: 3,
      adaptiveHeight: false,
      autoplay: true,
      autoplaySpeed: 5000,
      speed: 1000,
      arrows: false,
      prevArrow: $(target + '-arrow-prev'),
      nextArrow: $(target + '-arrow-next'),
      responsive: [
        {
          breakpoint: 969,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    }, args.slickOptions);

    this.catchDOM();
    if (this.isSlick) {
      this.generateSlick()
    }
    if (this.isLoader) {
      this.imagesLoader();
    }
    if (this.isImageGallery) {
      this.openImageGallery();
    }
    if (this.isVideoGallery) {
      this.openVideoGallery();
      this.generateThumbnail();
    }
  },
  catchDOM: function () {
    this.el = $(this.target)
  },
  generateSlick: function () {
    this.el.slick(this.slickOptions);
    this.el.css('display', 'block');
  },
  imagesLoader: function () {
    this.el.find('img').each(function (i, e) {
      this.self = $(e);
      this.self.waitForImages(function () {
          $(this).show();
          $(this).parent().find('.-loading').remove();
        }
      )
    })
  }
};

var SliderImageGallery = {
  openImageGallery: function () {
    this.el.find('.-popup-image').magnificPopup({
      type: 'image',
      gallery: {
        enabled: true
      }
    });
  }
};


var SliderVideoGallery = {
  openVideoGallery: function () {
    this.el.find('.-popup-video').magnificPopup({
      type: 'iframe',
      preloader: false,
      removalDelay: 160,
      disableOn: 700,
      gallery: {
        enabled: true
      },
      iframe: {
        markup: '<div class="mfp-iframe-scaler">'+
        '<div class="mfp-close"></div>'+
        '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
        '<div class="mfp-title">Some caption</div>'+
        '</div>'
      },
      callbacks: {
        markupParse: function(template, values, item) {
          values.title = item.el.attr('title');
        }
      }
    });
  },
  imagesLoader: function () {
    this.el.find('img').each(function (i, e) {
      this.self = $(e);
      this.self.waitForImages(function () {
          $(this).show();
          $(this).parent().find('.-videologo').show();
          $(this).parent().find('.-loading').remove();
        }
      )
    })
  },
  generateThumbnail: function () {
    this.el.find('.-popup-video').each(function (i, e) {
      $.when(UrlForMedia($(e).attr('href'))).then(function (data) {
        if (data) {
          if (data.type === "youtube") {
            $(e).find('img').get(0).src = data.image;
          } else if (data[0].thumbnail_medium) {
            $(e).find('img').get(0).src = data[0].thumbnail_medium;
          }
        }
      })
    })
  }
};