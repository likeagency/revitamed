function UrlForMedia(pastedData) {
  var success = false;
  var media = {};
  if (pastedData.match('https://(www.)?youtube|youtu\.be')) {
    if (pastedData.match('embed')) {
      youtube_id = pastedData.split(/embed\//)[1].split('"')[0];
    }
    else {
      youtube_id = pastedData.split(/v\/|v=|youtu\.be\//)[1].split(/[?&]/)[0];
    }
    media.type = "youtube";
    media.id = youtube_id;
    media.image = "http://img.youtube.com/vi/" + youtube_id + "/mqdefault.jpg";
    return media;
  }
  else if (pastedData.match('https://(player.)?vimeo\.com')) {
    vimeo_id = pastedData.split(/video\/|http:\/\/vimeo\.com\//)[1].split(/[?&]/)[0];
    media.type = "vimeo";
    media.id = vimeo_id;
    media.image = "";

    return $.ajax({
      async: false,
      type: 'GET',
      jsonp: 'callback',
      dataType: 'jsonp',
      url: 'http://vimeo.com/api/v2/video/' + media.id + '.json',
      success: function(data) {
        return data
      }
    });

  }
}