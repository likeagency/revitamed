<?php
/**
 * The template for displaying all pages.
 * Template name: Testimonials
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */

get_header(); ?>
<div id="primary" class="content-area mb container">
	<div class="col-12">
	<div class="wrapper">
		<h2><?php the_title(); ?></h2>
		<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
			<?php if(function_exists('bcn_display'))
			{
				bcn_display();
			}?>
		</div>
	</div><!-- ./wrapper -->
</div>
	<main id="main" class="site-main bli col-12" role="main">
		<?php the_content(); ?>
		<?php
		$args = array(
	    'orderby'		=> 'menu_order',
	    'order'			=> 'ASC',
	    'post_type'		=> 'testim',
	    'post_status'	=> 'publish',
	    'posts_per_page' => 12
	  );
		$posts_list = new WP_Query( $args );
		if (is_object($posts_list) && $posts_list->have_posts()) {
											while ($posts_list->have_posts()) {
												$posts_list->the_post(); $local_custom = get_post_custom(get_the_ID()); ?>
												<div id="testimonialID_<?php echo get_the_ID(); ?>" class="testimonials-box">
													<?php the_content(); ?>
													<div class="who">
													<span class="r1"><?php echo $local_custom['ti_url'][0]; ?></span>
													<span class="r2"><?php echo $local_custom['ti_button_text'][0]; ?></span>
													</div>
												</div>
												<?php } wp_reset_postdata(); } ?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer();
