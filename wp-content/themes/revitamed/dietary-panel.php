<?php
/**
 * The template for displaying all pages.
 * Template name: Panel dietetyka
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */


$userDoc = new userDoc();
$filters = $_GET['f'];
$filters = (is_array($filters)) ? $filters : [2,4];
doc_page();
get_header(); ?>
  <div id="primary" class="content-area konto-tpl">
    <div class="container">
      <div class="col-12">
      <div class="wrapper">
        <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
            <?php if (function_exists('bcn_display')) {
                bcn_display();
            } ?>
        </div>
      </div><!-- ./wrapper -->
    </div>
    </div>
    <div class="container">
      <aside class="col-3 sidebar-konto">
        <?php include 'template-parts/menu-dieter.php'; ?>
      </aside>
      <main id="main" class="site-main account-p col-9" role="main">
        <h3>Lista diet</h3>

        <div class="diet">
          <div class="diet__header">
            <div class="diet__sort">
              <span class="diet__sortItem" data-diet="[2,4]"><i class="fa fa-exclamation-circle"></i> DIETY DO
              UŁOŻENIA</span>
              <span class="diet__sortItem" data-diet=""><i class="fa fa-list"></i> WSZYSTKIE DIETY</span>
            </div>
          </div>
          <div class="diet__box">
            <?php
            $isDoc = $userDoc->isDoc();
            if(!$isDoc)
              $clients = getAllClientForDietary( $userDoc->getId());
            else
              $clients = getAllClientForDietary( getAllDietaryForDoc($userDoc->getId()));

            $dietStatus =
            [
              0 => 'Nieopłacona',
              1 => 'W trakcie',
              2 => 'Nowa do ułożenia',
              3 => 'Zakończona',
              4 => 'Kontynuacja',
              5 => 'Oczekuje na termin rozpoczęcia',
            ];

            $dietFlags =
            [
	            0 => '-paid',
	            1 => '-in-progress',
	            2 => '-paid',
	            3 => '-closed',
              4 => '-new',
              5 => '-in-progress',
            ];


            /**
             * NEW DIETS
             */
            foreach($clients as $client){

              $currentDiet = getCurrentActiveDiet($client->ID);
              if($currentDiet===null)
                $diet = getNewLastDiet($client->ID);
              else
	              $diet = getLastDiet($client->ID);

	            if($diet===null || $currentDiet->ID==$diet->ID ) continue;
	            updateStateDiet($diet);
	            $surveys = getUserSurvey($client->ID);
	            $survey = $surveys[0];
	            $weightLose = (get_post_meta($diet->ID,'sf_weight_start',true)!= '' && get_post_meta($diet->ID,'sf_weight_target',true)!='') ? ( get_post_meta($diet->ID,'sf_weight_target',true) - get_post_meta($diet->ID,'sf_weight_start',true) )  : null;
	            $dieter = get_post_meta($diet->ID,'sf_doc',true);
	            $status = get_post_meta($diet->ID,'sf_status',true);
	            $dateStart =  get_post_meta($diet->ID,'sf_date_from',true);
	            $dateStop  =  get_post_meta($diet->ID,'sf_date_to',true);
	            $dateOrder =  get_post_meta($diet->ID,'sf_date_order',true);
	            $partialSurvey = getPartialSurveyForDiet($diet->ID);
	            $isLocked = getPartialSurveyForDiet($diet->ID,'publish') === null;
	            $exists  =  !in_array($status,array(2,5));
	            if($status==='0')
		            continue;

	            ?>
              <div class="diet__item" data-id="<?= -$dateOrder ?>" data-status="<?= $status ?>" style="<?= ( is_array($filters) &&   !in_array($status,$filters)) ? 'display:none;' : '' ?>">

                <div class="diet-item">
                  <div class="diet-item__details">
		              <?php if($dateOrder!='') {?>
                        <span><i class="fa fa-calendar"></i> <strong>Data zgłoszenia:</strong> <?= date('d.m.Y',$dateOrder) ?></span><br>
		              <?php } ?>
                    <span><i class="fa fa-user"></i>
                      <strong>Dane pacjenta:</strong> <a href="<?= get_the_permalink(get_id_after_template_filename('history-user.php'))."/{$client->ID}/" ?>"><?= getUserName($client->ID); ?></a> /
                    <strong>Waga aktualna:</strong> <?= getUserWeigh($client->ID) ?> kg /
                    <strong>Wzrost:</strong> <?= getUserHeight($client->ID) ?> cm
                  </span><br>
		              <?php if($isDoc){?>
                        <span><i class="fa fa-user"></i>
                    <strong>Dane dietetyka:</strong> <?= (getDocName($dieter)); ?>
                    </span><br>
		              <?php } ?>

		              <?php if($dateStart!='' && $dateStop!=''){ ?>
                        <span><i class="fa fa-clock-o"></i> <strong>Okresy:</strong> <?= date('d.m.Y',$dateStart)?> - <?= date('d.m.Y',$dateStop)?></span><?php } ?>
                    <br>
                    <a class="diet-item__link" href="<?= get_the_permalink($survey) ?>">ZOBACZ ANKIETĘ</a>
                    <?php if($exists) { if($partialSurvey!==null){ ?>
                    <a class="diet-item__link" href="<?= get_the_permalink( get_id_after_template_filename( 'ankieta-czastkowa.php' ) ) .'/'.($partialSurvey->ID) ?>">ZOBACZ ANKIETĘ CZĄSTKOWĄ Z DNIA <?= date('d.m.Y',strtotime($partialSurvey->post_date)) ?></a>
                    <?php } elseif($isLocked) { ?>
                      <div class="diet-item__partialSurvey red">Ankieta cząstkowa nie wypełniona - zablokowana</div>
                    <?php } else { ?>
                      <div class="diet-item__partialSurvey">Ostatnia ankieta cząstkowa nie wypełniona</div>
                    <?php }} ?>
                  </div>
                  <div class="diet-item__navigation">
                    <span class="diet-item__status <?= $dietFlags[$status] ?>"><?= mb_strtoupper($dietStatus[$status]) ?></span>
                    <div class="diet-item__buttons">
			            <?php if(getCurrentUserPlan($client->ID)!=2){ ?>
                          <span class="diet-item__button -disabled">WIADOMOŚĆ <i class="fa fa-envelope"></i></span>
			            <?php }else { ?>
                          <a href="<?= get_the_permalink(get_id_after_template_filename('dietary-message.php')).'/'.$client->ID ?>/" class="diet-item__button">WIADOMOŚĆ <i class="fa fa-envelope"></i></a>
			            <?php } ?>
                          <a href="<?= get_the_permalink(get_id_after_template_filename('diet-laying-step-1.php')) ?>?userID=<?= $client->ID ?>" class="diet-item__button">UŁÓŻ DIETĘ <i class="fa fa-angle-double-right"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            <?php }

            /**
             * Active diets
             */
            foreach($clients as $client){

              $diet = getCurrentActiveDiet($client->ID);
              if($diet===null) continue;
	            updateStateDiet($diet);
	            $lastDiet = getLastDiet($client->ID);
	            $lastDietID = $lastDiet->ID;
	            $surveys = getUserSurvey($client->ID);
	            $survey = $surveys[0];
	            $weightLose = (get_post_meta($diet->ID,'sf_weight_start',true)!= '' && get_post_meta($diet->ID,'sf_weight_target',true)!='') ? ( get_post_meta($diet->ID,'sf_weight_target',true) - get_post_meta($diet->ID,'sf_weight_start',true) )  : null;
	            $dieter = get_post_meta($diet->ID,'sf_doc',true);
              $status = get_post_meta($diet->ID,'sf_status',true);
              $dateStart =  get_post_meta($diet->ID,'sf_date_from',true);
              $dateStop =  get_post_meta($diet->ID,'sf_date_to',true);
	            $dateOrder =  get_post_meta($diet->ID,'sf_date_order',true);
	            $partialSurvey = getPartialSurveyForThisDiet($diet->ID);
	            $isLocked = getPartialSurveyForThisDiet($diet->ID,'publish') === null;
	            $exists  =  time() > strtotime('-7 days',$dateStop);
	            if($status==='0')
		            continue;
              ?>
            <div class="diet__item" data-id="<?= -$dateOrder ?>" data-status="<?= $status ?>" style="<?= ( is_array($filters) &&   !in_array($status,$filters)) ? 'display:none;' : '' ?>">
              <div class="diet-item">
                <div class="diet-item__details">
                  <?php if($dateOrder!='') {?>
                  <span><i class="fa fa-calendar"></i> <strong>Data zgłoszenia:</strong> <?= date('d.m.Y',$dateOrder) ?></span><br>
                    <?php } ?>
                  <span><i class="fa fa-user"></i>
                    <strong>Dane pacjenta:</strong> <a href="<?= get_the_permalink(get_id_after_template_filename('history-user.php'))."/{$client->ID}/" ?>"><?= getUserName($client->ID); ?></a> /
                    <strong>Waga aktualna:</strong> <?= getUserWeigh($client->ID) ?> kg /
                    <strong>Wzrost:</strong> <?= getUserHeight($client->ID) ?> cm
                  </span><br>
                  <?php if($isDoc){?>
                  <span><i class="fa fa-user"></i>
                    <strong>Dane dietetyka:</strong> <?= (getDocName($dieter)); ?>
                    </span><br>
                    <?php } ?>
                  <?php if($dateStart!='' && $dateStop!=''){ ?>
                  <span><i class="fa fa-clock-o"></i> <strong>Okresy:</strong> <?= date('d.m.Y',$dateStart)?> - <?= date('d.m.Y',$dateStop)?></span><?php } ?>
                <br>
                  <a class="diet-item__link" href="<?= get_the_permalink($survey) ?>">ZOBACZ ANKIETĘ</a>
	                <?php if($exists) { if($partialSurvey!==null){ ?>
                      <a class="diet-item__link" href="<?= get_the_permalink( get_id_after_template_filename( 'ankieta-czastkowa.php' ) ) .'/'.($partialSurvey->ID) ?>">ZOBACZ ANKIETĘ CZĄSTKOWĄ Z DNIA <?= date('d.m.Y',strtotime($partialSurvey->post_date)) ?></a>
                  <?php } elseif($isLocked) { ?>
                    <div class="diet-item__partialSurvey red">Ankieta cząstkowa nie wypełniona - zablokowana</div>
                  <?php } else { ?>
                    <div class="diet-item__partialSurvey">Ostatnia ankieta cząstkowa nie wypełniona</div>
                  <?php } } ?>
                </div>
                <div class="diet-item__navigation">
                  <span class="diet-item__status <?= $dietFlags[$status] ?>"><?= mb_strtoupper($dietStatus[$status]) ?></span>
                  <div class="diet-item__buttons">
                    <?php if(getCurrentUserPlan($client->ID)!=2){ ?>
                    <span class="diet-item__button -disabled">WIADOMOŚĆ <i class="fa fa-envelope"></i></span>
                    <?php }else { ?>
                      <a href="<?= get_the_permalink(get_id_after_template_filename('dietary-message.php')).'/'.$client->ID ?>/" class="diet-item__button">WIADOMOŚĆ <i class="fa fa-envelope"></i></a>
                    <?php } ?>
                    <?php if($status==2){ ?>
                      <a href="<?= get_the_permalink(get_id_after_template_filename('diet-laying-step-1.php')) ?>?userID=<?= $client->ID ?>" class="diet-item__button">UŁÓŻ DIETĘ <i class="fa fa-angle-double-right"></i></a>
                    <?php }elseif($status==0){ //not payed?>
                      <span class="diet-item__button -disabled">UŁÓŻ DIETĘ <i class="fa fa-angle-double-right"></i></span>
                    <?php }elseif(
                    $status==4

                    ){ ?>
                      <a href="<?= get_the_permalink(get_id_after_template_filename('diet-laying-step-1.php')) ?>?userID=<?= $client->ID ?>" class="diet-item__button">UŁÓŻ NOWY ETAP<i class="fa fa-angle-double-right"></i></a>
                    <?php }else{ ?>
                      <a href="<?= get_the_permalink($diet->ID) ?>" class="diet-item__button">EDYTUJ DIETĘ <i class="fa fa-angle-double-right"></i></a>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
            <?php }

            /**
             * Ended diets
             */
              foreach($clients as $client) {

	              foreach( getMaybeEndedDiet($client->ID) as $maybeEndedDiet)
	                updateStateDiet($maybeEndedDiet);

              foreach(getEndedDiets($client) as $diet)
              {
	              $status = 3;
	              $surveys = getUserSurvey($client->ID);
	              $survey = $surveys[0];
	              $weightLose = (get_post_meta($diet->ID,'sf_weight_start',true)!= '' && get_post_meta($diet->ID,'sf_weight_target',true)!='') ? ( get_post_meta($diet->ID,'sf_weight_target',true) - get_post_meta($diet->ID,'sf_weight_start',true) )  : null;
	              $dieter = get_post_meta($diet->ID,'sf_doc',true);
	              $status = get_post_meta($diet->ID,'sf_status',true);
	              $dateStart =  get_post_meta($diet->ID,'sf_date_from',true);
	              $dateStop =  get_post_meta($diet->ID,'sf_date_to',true);
	              $partialSurvey = getPartialSurveyForDiet($diet->ID);
                ?>

                <div class="diet__item" data-id="<?= -$dateOrder ?>" data-status="<?= $status ?>" style="<?= ( is_array($filters) &&   !in_array($status,$filters)) ? 'display:none;' : '' ?>">
                  <div class="diet-item">
                    <div class="diet-item__details">
                      <span><i class="fa fa-calendar"></i> <strong>Data zgłoszenia:</strong> <?= date('d.m.Y',get_post_meta($diet->ID,'sf_date_order',true)) ?></span><br>
                      <span><i class="fa fa-user"></i>
                    <strong>Dane pacjenta:</strong> <a href="<?= get_the_permalink(get_id_after_template_filename('history-user.php'))."/{$client->ID}/" ?>"><?= getUserName($client->ID); ?></a> /
                    <strong>Waga aktualna:</strong> <?= getUserWeigh($client->ID) ?> kg /
                    <strong>Wzrost:</strong> <?= getUserHeight($client->ID) ?> cm
                  </span><br>
		                <?php if($isDoc){?>
                          <span><i class="fa fa-user"></i>
                    <strong>Dane dietetyka:</strong> <?= (getDocName($dieter)); ?>
                    </span><br>
		                <?php } ?>
		                <?php if($dateStart!='' && $dateStop!=''){ ?>
                          <span><i class="fa fa-clock-o"></i> <strong>Okresy:</strong> <?= date('d.m.Y',$dateStart)?> - <?= date('d.m.Y',$dateStop)?></span><?php } ?>
                      <br>
                      <a class="diet-item__link" href="<?= get_the_permalink($survey) ?>">ZOBACZ ANKIETĘ</a>
                    </div>
                    <div class="diet-item__navigation">
                      <span class="diet-item__status <?= $dietFlags[$status] ?>"><?= mb_strtoupper($dietStatus[$status]) ?></span>
                      <div class="diet-item__buttons">
			              <?php if(getCurrentUserPlan($client->ID)!=2){ ?>
                            <span class="diet-item__button -disabled" title="Użytkownik nie ma wykupionego abonamentu na wiadomości">WIADOMOŚĆ <i class="fa fa-envelope"></i></span>
			              <?php }else { ?>
                            <a href="<?= get_the_permalink(get_id_after_template_filename('dietary-message.php')).$client->ID ?>/" class="diet-item__button">WIADOMOŚĆ <i class="fa fa-envelope"></i></a>
			              <?php } ?>
                            <a href="<?= get_the_permalink($diet->ID) ?>" class="diet-item__button">ZOBACZ DIETĘ <i class="fa fa-angle-double-right"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
                  <?php
              }

              }
            ?>

          </div>
        </div>

      </main><!-- #main -->
    </div><!-- ./container -->
  </div><!-- #primary -->
<?php get_footer();
