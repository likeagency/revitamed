<?php
/**
 * The template for displaying all pages.
 * Template name: Kontakt
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */

get_header(); global $post; ?>
<div id="primary" class="content-area  container">
	<div class="col-12">
	<div class="wrapper">
		<h2><?php the_title();?></h2>
		<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
			<?php if (function_exists('bcn_display')) {
					bcn_display();
			} ?>
		</div>
	</div><!-- ./wrapper -->
</div>
	<main id="main" class="col-12" role="main">
		<div class="contact">
			<div class="contact__row">
				<div class="contact__column">
                <?= $post->post_content;  ?>
				</div>
				<div class="contact__column">
					<div class="contact__title">
						MAPA DOJAZDU
					</div>
					<div class="contact__map" id="contactMap" data-key="AIzaSyA_wwoeU9sjDtzk-4zmvtplPWmGYD9aeEo" data-lat="51.044723" data-lng="16.971681" data-zoom="17"></div>
				</div>
			</div>
		</div>
	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer();
