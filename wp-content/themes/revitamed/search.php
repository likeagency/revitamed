<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package like
 */
global $wp_query;
$wp_query->query['post_type'] = 'przepisy';
$wp_query->query['nopaging'] = true;

if($GLOBALS['wp']->query_vars['skladnik'] != '')
{
    $id = get_id_by_slug($GLOBALS['wp']->query_vars['skladnik'],'skladnik');
    $wp_query->query['meta_query'] = array(
        'relation' => 'AND',
        array(
            'key'     => 'rm_ing',
            'value'   => "\"$id\"",
            'compare' => 'LIKE'
        )
    );
    $wp_query->query['s'] = get_the_title($id);
}

if($_GET['ts']!='')
{
    $wp_query->query['tax_query'] = array(
        'relation' => 'AND',
        [
            'taxonomy' => 'rodzaj',
            'field' => 'slug',
            'terms' => $_GET['ts'],
            'operator' => 'IN'
        ],
    );
}

foreach(get_terms(array( 'taxonomy' => 'rodzaj',)) as $term) {
	if ( sanitize_title( $_GET['s'] ) == $term->slug ) {
		$wp_query->query['s']         = '';
		$wp_query->query['tax_query'] = array(
			'relation' => 'AND',
			[
				'taxonomy' => 'rodzaj',
				'field'    => 'slug',
				'terms'    => $term->slug,
				'operator' => 'IN'
			],
		);
		break;
	}


}
include 'archive-przepisy.php';