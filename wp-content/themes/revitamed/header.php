<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package like
 */
$userRevita = new user();
$docRevita = new userDoc();
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link href="//fonts.googleapis.com/css?family=Asap:400,500,700&amp;subset=latin-ext" rel="stylesheet">
    <?php wp_head(); ?>
  <style>
    .fixedHeight {
      font-size:10px;
      max-height: 200px;
      margin-bottom: 10px;
      overflow-x: auto;
      overflow-y: auto;
    }
  </style>

</head>
<body <?php body_class(); ?>>
    <div id="page" class="site">
        <a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'like' ); ?></a>
        <header id="masthead" class="site-header header-sub" role="banner">
            <div class="container">
                <div class="top-menu">
                    <?php if(!$userRevita->isActive() && !$docRevita->isActive() ){ ?>
                    <a href="<?= get_permalink( get_id_after_template_filename('konto-logowanie.php') ); ?>"><i class="fa fa-lock" aria-hidden="true"></i>Zaloguj się</a>
                    <a href="<?= get_permalink( get_id_after_template_filename('konto-logowanie.php') ); ?>"><i class="fa fa-user-plus" aria-hidden="true"></i>Zarejestruj się</a>
                    <?php } elseif($userRevita->isActive()) { ?>
                        <a href="<?= home_url('/moje-konto/') ?>"><i class="fa fa-user" aria-hidden="true"></i>Moje konto</a>
                        <a href="?logout=true"><i class="fa fa-lock" aria-hidden="true"></i>Wyloguj</a>
                    <?php }else{ ?>
                      <a href="<?= home_url('/panel-dietetyka/') ?>"><i class="fa fa-user" aria-hidden="true"></i>Panel dietetyka</a>
                      <a href="?logout=true"><i class="fa fa-lock" aria-hidden="true"></i>Wyloguj</a>
                  <?php } ?>
                      <a href="http://revitasklep.like.energy/" class="shop-link" target="_blank" ><i class="fa fa-shopping-cart" aria-hidden="true"></i>Nasz sklep</a>
                </div>
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="site-branding"><img src="<?php echo get_template_directory_uri(); ?>/images/logo-new.png" alt="<?php bloginfo( 'name' ); ?>" />
                <p class="site-branding__desc"><b>Portal dietetyczny prowadzony<br>przez lekarzy</b></p>
                <p class="site-branding__desc-mobile"><b>Portal dietetyczny prowadzony przez lekarzy</b></p>
                </a>
                <a href="#site-navigation" class="menu-toggler"><i class="fa fa-bars"></i><?php esc_html_e( 'Menu', 'like' ); ?></a>
                <nav id="site-navigation" class="main-navigation" role="navigation">
                    <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
                </nav><!-- #site-navigation -->
            </div>
        </header><!-- #masthead -->
        <div id="content" class="site-content">
