<?php
/**
 * The template for displaying all pages.
 * Template name: Konto - płatności
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */
user_page(false,false);
$userRevitamed = new user();
$name = $userRevitamed->getName();
$lastDay = $userRevitamed->getUserLastActiveDay();
$titan = TitanFramework::getInstance('revita');
get_header(); ?>
  <div id="primary" class="content-area konto-tpl">
    <div class="container">
      <div class="col-12">
      <div class="wrapper">
        <div class="title">Witaj, <span class="name"><?= $name ?>!</span> Zacznij z naszą pomocą dobry dzień</div>
        <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
            <?php if (function_exists('bcn_display')) {
                bcn_display();
            } ?>
        </div>
      </div><!-- ./wrapper -->
    </div>
    </div>
    <div class="container">
      <aside class="col-3 sidebar-konto">
        <div class="side-menu side-item">
	        <?php include 'template-parts/menu-user.php' ?>
        </div><!-- ./sidemenu -->
	      <?php include 'template-parts/progress-user.php' ?>
	      <?php include 'template-parts/subs-user.php' ?>
      </aside>
      <main id="main" class="site-main account-p col-9" role="main">
        <h3>Płatności</h3>
	      <?php if(isset($_GET['survey']) && $_GET['survey']=='1'){?>
            <div style="padding: 15px; margin-bottom: 20px; border: 1px solid #dff0d8;  border-radius: 4px; background-color: #dff0d8;"><?= $titan->getOption('post_survey_text_unpaid_account') ?></div>
	      <?php } ?>
        <?php if($_SERVER['REQUEST_METHOD']==='POST' && count(getAllDietUser($userRevitamed->getId())) < 2) { ?>
        <div style="padding: 15px; margin-bottom: 20px; border: 1px solid #dff0d8;  border-radius: 4px; background-color: #dff0d8;"><?= $titan->getOption((!empty(getUserSurvey($userRevitamed->getId()))) ?'post_pay_account_first_time' : 'post_pay_account_first_time_pre_survey') ?></div>
    <?php } ?>
	      <?php if($_SERVER['REQUEST_METHOD']==='POST' && count(getAllDietUser($userRevitamed->getId())) >= 2) { ?>
            <div style="padding: 15px; margin-bottom: 20px; border: 1px solid #dff0d8;  border-radius: 4px; background-color: #dff0d8;"><?= $titan->getOption('post_pay_account_text') ?></div>
	      <?php } ?>
        <div class="payment">

          <?php if(get_post_meta($userRevitamed->getId(),'sf_date_sub',true)!=''){?>
          <div class="payment__header">
            <?= ($userRevitamed->hasActiveSub()) ? 'Abonament opłacony do dnia' : 'Twój dostęp do serwisu zakończył się' ?>
            <?= date('d.m.Y',$lastDay ) ?>
          </div><?php } ?>
          <?php if(get_field('platnosci_opis')) { ?>
            <div class="platnosci-opis">
              <?php echo get_field('platnosci_opis'); ?>
            </div>
          <?php } ?>
          <form id="js-payment-form-acc" class="payment__form js-payment-form-acc" method="post">
            <div class="payment__content">
              <div class="payment__label">Wykup abonament</div>

              <?php $counter = 1;
              foreach(getAllCennik() as $cennik){ ?>
              <div class="payment__inputbox">
                <input class="payment__input" required data-price="<?= esc_attr(get_post_meta($cennik->ID,'sf_price_current',true)) ?>" id="payment-method-<?= $counter ?>" type="radio" name="id"  value="<?= $cennik->ID ?>"> <label
                  for="payment-method-<?= $counter++ ?>"><?= $cennik->post_title ?> (<?= get_post_meta($cennik->ID,'sf_price_current',true) ?> zł) </label>
              </div>
            <?php } ?>
              <?php if($userRevitamed->hasActiveSub()){ ?>
            <div class="payment__inputbox">
              <input class="payment__input" required data-price="0" id="payment-method-0" type="radio" name="id"  value="0"> <label
                for="payment-method-0">Nie</label>
            </div>
              <?php } ?>
            </div>
            <div class="payment__content" <?= $userRevitamed->hasActiveSub() ? '' : 'style="display:none;"' ?> >

              <div class="payment__label">Wykup dostęp do wiadomości z lekarzem</div>

	            <?php
	            foreach(getAllCennikDoc() as $cennik){ ?>
                  <div class="payment__inputbox">
                    <input class="payment__input" required  data-price="<?= esc_attr(get_post_meta($cennik->ID,'sf_price_current',true)) ?>" id="payment-method-<?= $counter ?>" type="radio" name="id_2"  value="<?= $cennik->ID ?>"> <label
                      for="payment-method-<?= $counter++ ?>"><?= $cennik->post_title ?>  (<?= get_post_meta($cennik->ID,'sf_price_current',true) ?> zł)</label>
                  </div>
	            <?php } ?>
              <div class="payment__inputbox">
                <input class="payment__input" checked data-price="0" required id="payment-method-<?= $counter ?>" type="radio" name="id_2"  value="0"> <label
                  for="payment-method-<?= $counter++ ?>">Nie</label>
              </div>
            </div>
            <div class="payment__navigation">
              <div class="payment-summary">
                Suma: <span id="overallPrice">99 zł</span>
              </div>
                <button class="payment__button">
                  <img src="<?php echo get_template_directory_uri(); ?>/components/payment/src/PayU.png" alt="PayU">
                </button>
            </div>
          </form>
          <form class="js-actual-payment-form" action="<?= home_url('/moje-konto/') ?>" method="post" style="display:none">
	          <?php foreach (createFormPayment() as $key => $item)
		          echo "<input name='$key' value='" . esc_attr($item) . "' type='hidden'>"; ?>
          </form>
        </div>

      </main><!-- #main -->
    </div><!-- ./container -->
  </div><!-- #primary -->
<?php get_footer();
