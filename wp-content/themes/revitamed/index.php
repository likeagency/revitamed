<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */

get_header(); ?>
<div id="primary" class="content-area mb container">
	<div class="col-12">
	<div class="wrapper">
		<h2>Porady</h2>
		<?php get_template_part('template-parts/breadcrumbs'); ?>
	</div><!-- ./wrapper -->
</div>
	<main id="main" class="site-main bli col-9" role="main">

		<?php
		if ( have_posts() ) :
			while ( have_posts() ) : the_post();
                if(get_post_type(get_the_ID())!='post') continue;
		?>
		<article class="spl">
			<div class="row">
				<div class="col-12">
				<h3><?php the_title(); ?></h3>
				</div>
			</div>
			<div class="row tips__item">
			<div class="col-12">
				<?php if ( has_post_thumbnail() ) : ?>
					<div class="thumb tips__imageHolder">
						<div class="tips__imageBox">
							<?php the_post_thumbnail('porady-thumb'); ?>
						</div>
					</div>
				<?php endif; ?>
				<div class="entry tips__text">
					<?php the_excerpt(); ?>
					<div class="meta">
						<?php if (get_field('opcjonalny_link')){ ?>
							<a href="<?php the_field('opcjonalny_link_wybor'); ?>" class="btn">Czytaj dalej</a>
						<?php }
						else{ ?>
							<a href="<?php the_permalink(); ?>" class="btn">Czytaj dalej</a>
						<?php }

						?>
						<div class="tags floatright">
							<?php the_tags( 'Tagi: ', ', ', '<br />' ); ?>
						</div>
					</div>
				</div>
			</div>
			</div>
		</article>
		<?php
		endwhile;
		wp_pagenavi();

		else :
			echo "<h2>Brak postów do wyświetlenia</h2>";

		endif; ?>

	</main><!-- #main -->

<aside id="secondary" class="col-3">
	<?php dynamic_sidebar('porady-sidebar'); ?>
</aside>
</div><!-- #primary -->

<?php
get_footer();
