<?php
/**
 * The template for displaying all pages.
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */
$titan = TitanFramework::getInstance( 'revita' );
$loginForm = ($_POST['action']=='login') ? login_user_doc() : null;
$forgotPasswordForm = ($_POST['action']=='reset_password') ? reset_password_for_doc() : null;
get_header(); ?>
	<div id="primary" class="content-area">
		<div class="container">
			<div class="col-12">
			<div class="wrapper">
				<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
					<?php if (function_exists('bcn_display')) {
						bcn_display();
					} ?>
				</div>
			</div><!-- ./wrapper -->
		</div>
		</div>
		<div class="container">
			<main id="main" class="site-main account-p col-12" role="main">
				<div class="login-form col-6">
					<div class="loginForm">
						<form class="loginForm__form" method="post" action="">
							<div class="loginForm__header">
								<div class="loginForm__title">Zaloguj się!</div>
							</div>
							<div class="loginForm__content">
								<?php if($forgotPasswordForm['result']=='success') { ?>
									<div class="registerForm__alert -success"><?= $forgotPasswordForm['text'] ?></div>
								<?php } ?>
								<?php if($forgotPasswordForm['result']=='error') { ?>
									<div class="registerForm__alert -fail"><?= $forgotPasswordForm['text'] ?></div>
								<?php } ?>

								<?php if($loginForm===false) { ?>
									<div class="registerForm__alert -fail"><?= $titan->getOption('login-wrong-data') ?></div>
								<?php } ?>
								<div class="loginForm__group">
									<div class="loginForm__inputbox">
										<label class="loginForm__label">Twój email <span>*</span></label>
										<input type="email" required name="user_email" class="loginForm__input">
									</div>
								</div>
								<div class="loginForm__group">
									<div class="loginForm__inputbox">
										<label class="loginForm__label">Twoje hasło <span>*</span></label>
										<input required name="user_pass" class="loginForm__input" type="password">
									</div>
								</div>

								<div class="loginForm__navigation">
									<input type="hidden" value="login" name="action">
									<?php wp_nonce_field( 'post_nonce', 'post_nonce_field' ); ?>
									<button class="loginForm__button">ZALOGUJ SIĘ</button>
									<span class="loginForm__forgottenText">Nie pamiętasz hasła? <span class="loginForm__forgotten js-forgottenForm">Zresetuj hasło.</span></span>
								</div>
							</div>
						</form>
					</div>

					<div class="forgottenForm">
						<form class="forgottenForm__form" method="post" action="">
							<div class="forgottenForm__header">
								<div class="forgottenForm__subtitle">Zapomniałeś hasła?</div>
								<div class="forgottenForm__title">Odzyskaj hasło!</div>
							</div>
							<div class="forgottenForm__content">
								<div class="forgottenForm__group">
									<div class="forgottenForm__inputbox">
										<label class="forgottenForm__label">Twój email <span>*</span></label>
										<input type="email" required name="user_email" class="forgottenForm__input">
									</div>
								</div>
								<div class="forgottenForm__navigation">
									<button class="forgottenForm__button">ODZYSKAJ HASŁO</button>
									<input type="hidden" name="action" value="reset_password">
									<?php wp_nonce_field( 'post_nonce', 'post_nonce_field' ); ?>
								</div>
							</div>
						</form>
					</div>
				</div><!-- ./loginform -->
			</main><!-- #main -->
		</div><!-- ./container -->
	</div><!-- #primary -->
<?php get_footer();
