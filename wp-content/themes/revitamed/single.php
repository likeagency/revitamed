<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package like
 */

get_header(); ?>

<div id="primary" class="content-area mb container">
	<div class="col-12">
	<div class="wrapper">
		<h2>Porady</h2>
		<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
			<?php get_template_part('template-parts/breadcrumbs'); ?>
		</div>
	</div><!-- ./wrapper -->

</div>
	<main id="main" class="site-main col-9" role="main">
		<div class="share">
			<h3>Podziel się:</h3>
			<a href="http://www.facebook.com/sharer.php?u=http://revitamed.like.gallery" target="_blank"><i class="fa fa-facebook-square"></i></a>
	  	<a href="https://twitter.com/share?url=http://revitamed.like.gallery&amp;text=Simple%20Share%20Buttons&amp;hashtags=revitadiet" target="_blank"><i class="fa fa-twitter-square"></i></a>
			<a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://revitamed.like.gallery" target="_blank"><i class="fa fa-linkedin-square"></i></a>
		</div>
		<article class="p-article">
			<header class="art">
				<h1 class="title"><?php the_title(); ?></h1>
			</header>

			<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'recipefull' );?>
			<section class="ft-img">
				<?php if ( has_post_thumbnail() ) : ?>
					<?php the_post_thumbnail('big-porada'); ?>
			<?php endif; ?>
		</section><!-- ./imagallery -->
		<section class="entry-content">
			<?php
			the_content();
			?>
			<div class="container-etap clear">

				<?php
				if( have_rows('etapy_porada') ):
					while ( have_rows('etapy_porada') ) : the_row();
				?>
			<?php if( get_row_layout() == 'etap_box' ){  ?>
				<?php
					echo "<div class='etap-box'>";
						echo "<div class='etap-box-top'>";
							echo get_sub_field("etap_top_text");
							echo "<img src='".get_sub_field("etap_top_ikona")['sizes']['etapy-ikona']."' alt='".get_sub_field("etap_top_ikona")['alt']."'>";
						echo "</div>";
						echo "<div class='etap-box-middle'>";
							echo get_sub_field("etap_opis");
							echo "<div class='etap-lista'>";
							echo "<ul>";
							foreach (get_sub_field("etap_lista") as $etapLista) {
								echo "<li class='etap-lista'>";
								echo $etapLista['etap_wiersz_listy'];
								echo "</li>";
							}
							echo "</ul>";
							echo "</div>";
						echo "</div>";
						echo "<div class='etap-box-btm'>";
							echo get_sub_field('etap_koszyk');
						echo "</div>";
					echo "</div>";
				?>
			<?php }?>
				<?php
				endwhile;
				endif;
				?>

			</div>
		</section>
		<section>
			<?php if( get_field('dodatkowy_tekst') ): ?>
				<div class="single-btm-box clear">
				<?php the_field('dodatkowy_tekst'); ?>
				</div>
			<?php endif; ?>
		</section>
		<section class="entry-meta">
			<div class="tags">
				<?php the_tags( 'Tagi: ', ', ', '<br />' ); ?>
			</div>
			<div class="date">
				<i class="fa fa-calendar" aria-hidden="true"></i>
                <?= date('d') ?> <?= __(date('F')) ?> <?= date('Y') ?>
			</div>
		</section>

		<section class="comments">
			<?php comments_template(); ?>
		</section>
	</article>
</main><!-- #main -->
<aside id="secondary" class="col-3">
	<?php dynamic_sidebar('porady-sidebar'); ?>
</aside>
</div><!-- #primary -->

<?php
get_footer();
