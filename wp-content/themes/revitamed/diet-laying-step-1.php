<?php
/**
 * The template for displaying all pages.
 * Template name: Układanie diety krok 1
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */

doc_page();
$userID = intval( $_GET['userID'] );
$dietID = intval( $_GET['dietID'] );
if ( get_post_type( $userID ) != 'user_client' && get_post_type( $dietID ) != 'diet' ) {
	wp_redirect( home_url( '/' ) );
}

if ( get_post_type( $dietID ) != 'diet' ) {
	$dietID  = getNewLastDiet( $_GET['userID'] )->ID;
	$created = false;

} else {
	$userID  = get_post_meta( $dietID, 'sf_user', true );
	$created = true;
}

$isFinished = get_post_meta($dietID,'sf_status',true)==3;

if ( $_POST['form'] == 'step_1_diet' ) {
	stepOneDietForm();
}

$previousDiets     = getEndedDiets( $userID );
$partialSurveys    = getUserPartialSurvey( $userID );
$previousDietsInfo = array_merge( $previousDiets, $partialSurveys );

usort( $previousDietsInfo, function ( $a, $b ) {
	return strtotime( $a->post_date ) < strtotime( $b->post_date );
} );


$allDiets          = getAllDietUser( $userID );
$period_full_diets = wp_get_post_terms( $dietID, 'dl_diety' );
$period_full_diet  = ( is_object( $period_full_diets[0] ) ) ? $period_full_diets[0]->slug : '';
if ( $period_full_diet == '' ) {
	$last_diet             = end( $allDiets );
	$period_full_last_diet = wp_get_post_terms( $last_diet->ID, 'dl_diety' );
	$period_full_diet      = ( is_object( $period_full_last_diet[0] ) ) ? $period_full_last_diet[0]->slug : '';

}

$current_stages = wp_get_post_terms( $dietID, 'etap_diety' );
$current_stage  = ( is_object( $current_stages[0] ) ) ? $current_stages[0]->slug : '';;

$surveys = getUserSurvey( $userID );
$survey  = $surveys[0];

$number_dishes = get_post_meta( $dietID, 'sf_quantity_meal', true );
$userWeight    = ( get_post_meta( $dietID, 'sf_weight_start', true ) == '' ) ? getUserWeigh( $userID ) : get_post_meta( $dietID, 'sf_weight_start', true );
$userWeightEnd = get_post_meta( $dietID, 'sf_weight_target', true );
$userWeightEnd = ( $userWeightEnd == '' ) ? get_post_meta( $survey->ID, 'sf_nice_weight', true ) : $userWeightEnd;
$userWeightEnd = ( $userWeightEnd == '' ) ? 65 : $userWeightEnd;

$data = getDataSurvey($survey->ID);
$data['sf_purpose'] = maybe_unserialize($data['sf_purpose']);
$purposeAr = [];
$purposes =  array(
	'1' => 'Odchudający',
	'2' => 'Zdrowotny',
	'3' => 'Przyrost masy ciała',
	'4' => 'Regenerujący budujący masę mięśniową',
	'5' => 'Odmładzający, rewitalizujący',
	'6' => 'Inny :'. esc_html($data['sf_user_other_purposes']),
	'7' => 'Podnoszący poziom energii',
);
if(is_array($data['sf_purpose']))
	foreach($data['sf_purpose'] as $single)
		$purposeAr[] = $purposes[$single];

$dateStop = ( get_post_meta( $dietID, 'sf_date_to', true ) != '' ) ? date( 'd-m-Y', get_post_meta( $dietID, 'sf_date_to', true ) ) : '';
$plan     = get_post_meta( $dietID, 'sf_plan', true );
$timeDoc  = get_post_meta( $dietID, 'sf_duration', true );


get_header(); ?>
  <div id="primary" class="content-area konto-tpl">
    <div class="container">
      <div class="col-12">
        <div class="wrapper">
          <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
						<?php if (function_exists('bcn_display')) {
								bcn_display();
						} ?>
          </div>
        </div><!-- ./wrapper -->
      </div>
    </div>
    <div class="container">
      <aside class="col-3 sidebar-konto">
        <div class="side-menu side-item">
			<?php include 'template-parts/menu-dieter.php'; ?>
			<?php include 'template-parts/user-info-dieter.php'; ?>
        </div><!-- ./sidemenu -->
      </aside>
      <main id="main" class="site-main account-p col-6" role="main">
        <h3>Układanie diety</h3>

        <div class="dietCreator">
          <form class="dietCreator__form"
                action="<?= ( $created ) ? get_the_permalink( get_id_after_template_filename( 'diet-laying-step-1.php' ) ) : get_the_permalink( get_id_after_template_filename( 'diet-laying-step-2.php' ) ) ?><?= ( $created ) ? ( '?dietID=' . $dietID ) : ( '?userID=' . $userID ) ?>"
                method="post">
            <div class="dietCreator__group">
              <label class="dietCreator__label" for="period-of-full-diet">Okres pełnej diety:</label>
              <select class="dietCreator__select" id="period-of-full-diet" name="period_full_diet">
				  <?php
          $terms = get_terms( [ 'hide_empty' => false, 'taxonomy' => 'dl_diety', ] );
          usort($terms,function($a,$b){
            return (int)$a->slug > (int)$b->slug ;
          });
          foreach ( $terms  as $term ) {
					  printf( '<option value="%s" %s>%s</option>', esc_attr( $term->slug ), ( ( $period_full_diet == $term->slug ) ? 'selected' : '' ), $term->name );
				  } ?>
              </select>
            </div>
            <div class="dietCreator__group">
              <label class="dietCreator__label" for="the-current-stage">Obecny etap:</label>
              <select class="dietCreator__select" id="the-current-stage" name="current_stage">
				  <?php foreach ( get_terms( [ 'hide_empty' => false, 'taxonomy' => 'etap_diety', ] ) as $term ) {
					  printf( '<option value="%s" %s>%s</option>', esc_attr( $term->slug ), ( ( $current_stage == $term->slug ) ? 'selected' : '' ), $term->name );
				  } ?>
              </select>
            </div>

            <div class="dietCreator__group">
              <label class="dietCreator__label">Ilość dań w ciagu dnia:</label>
              <div class="dietCreator__radiogroup">
                <div class="dietCreator__radioitem">
                  <input required id="dishes-2" value="2" <?= ( $number_dishes == 2 ) ? 'checked' : '' ?>
                         name="number_dishes" type="radio" class="dietCreator__radio">
                  <label for="dishes-3" class="dietCreator__radiolabel">2 dania</label>
                </div>
                <div class="dietCreator__radioitem">
                  <input required id="dishes-3" value="3" <?= ( $number_dishes == 3 ) ? 'checked' : '' ?>
                         name="number_dishes" type="radio" class="dietCreator__radio">
                  <label for="dishes-3" class="dietCreator__radiolabel">3 dania</label>
                </div>
                <br>
                <br>
                <div class="dietCreator__radioitem">
                  <input required id="dishes-4" value="4" <?= ( $number_dishes == 4 ) ? 'checked' : '' ?>
                         name="number_dishes" type="radio" class="dietCreator__radio">
                  <label for="dishes-4" class="dietCreator__radiolabel">4 dania</label>
                </div>
                <div class="dietCreator__radioitem">
                  <input required id="dishes-5" value="5" <?= ( $number_dishes == 5 ) ? 'checked' : '' ?>
                         name="number_dishes" type="radio" class="dietCreator__radio">
                  <label for="dishes-5" class="dietCreator__radiolabel">5 dań</label>
                </div>
              </div>
            </div>
            <div class="dietCreator__group">
              <label class="dietCreator__label" for="template_diet">Wybierz szablon :</label>
              <select class="dietCreator__select" id="template_diet" name="sf_template_diet">
                <option value="" selected>Poprzedni etap</option>
				  <?php if ( get_post_meta( $dietID, 'sf_meal_created', true ) ) { ?>
                    <option value="-1" selected>Kontynuuj wypełnianie</option>
				  <?php } ?>
                <option value="0">Zacznij od zera</option>
				  <?php foreach ( getDietsTemplates() as $template ) {
					  printf( '<option value="%s">%s</option>', $template->ID, esc_attr( $template->post_title ) );
				  } ?>
              </select>
            </div>
            <?php if(!$isFinished){ ?>
            <div class="dietCreator__navigation">
              <button class="btn dietCreator__button"><?= ( $created ) ? 'WYŚLIJ' : 'ZACZNIJ UKŁADAĆ DIETĘ' ?> <i
                  class="fa fa-angle-double-right"></i>
              </button>
            </div>
          <?php } ?>

            <input type="hidden" name="weight_start" value="<?= esc_attr( $userWeight ) ?>">
            <input type="hidden" name="purpose_of_diet" value="<?= esc_attr( implode($purposeAr,', ') ) ?>">
            <input type="hidden" name="weight_end" value="<?= esc_attr( $userWeightEnd ) ?>">
            <input type="hidden" value="step_1_diet" name="form">
            <input type="hidden" value="<?= $dietID; ?>" name="dietID">
          </form>

        </div>
      </main><!-- #main -->
      <div class="col-3 historia-diety">
		  <?php
		  if ( ! empty( $previousDietsInfo ) ) { ?>
            <h3>Historia diety</h3>
            <hr>
			  <?php foreach ( $previousDietsInfo as $info ) {
				  if ( $info->post_type == 'diet' ) {
					  $diet          = $info;
					  $dateDietStart = date( 'd.m', get_post_meta( $diet->ID, 'sf_date_from', true ) );
					  $dateDietStop  = date( 'd.m', get_post_meta( $diet->ID, 'sf_date_to', true ) );
					  $weightStart   = getClosestWeight( $userID, get_post_meta( $diet->ID, 'sf_date_from', true ) );
					  $weightStop    = getClosestWeight( $userID, get_post_meta( $diet->ID, 'sf_date_to', true ) );
					  $part_a        = wp_get_post_terms( $diet->ID, 'etap_diety' );
					  $part          = $part_a[0]->name;
					  $length_a      = wp_get_post_terms( $diet->ID, 'dl_etapu' );
					  $length        = $length_a[0]->name;
					  $amountOfMeals = get_post_meta( $diet->ID, 'sf_quantity_meal', true );
					  ?>
                    <div class="historia-diety-szczegoly">
						<?= $dateDietStart ?>-<?= $dateDietStop ?>,<br> <?= $length ?>, <br><?= $part ?>,
                      <br><?= $amountOfMeals ?> da<?= ( $amountOfMeals == 5 ) ? 'ń' : 'nia' ?>,
                      <br><?php if ( $weightStart != '' && $weightStop != '' ) { ?> Waga <?= $weightStart ?>kg > <?= $weightStop ?>kg <?php } ?>

                    </div>
                    <a href="<?= get_permalink( $diet->ID ) ?>" class="btn small-btn" target="_blank">więcej</a>
				  <?php } else {
					  ?>
                    <div class="historia-diety-szczegoly">
                      <a target="_blank"
                         href="<?= get_the_permalink( get_id_after_template_filename( 'ankieta-czastkowa.php' ) ) .'/'. $info->ID ?>/">Ankieta
                        cząstkowa <?= date( 'd.m.Y', strtotime( $info->post_date ) ) ?></a>
                    </div>
					  <?php

				  }

			  }

		  } ?>

      </div>
    </div><!-- ./container -->
  </div><!-- #primary -->
<?php get_footer();
