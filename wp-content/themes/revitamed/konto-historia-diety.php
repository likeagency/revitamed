<?php
/**
 * The template for displaying all pages.
 * Template name: Konto - historia diet
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */
user_page(false,true);
$userRevita = new user();
$currentDiet = getCurrentActiveDiet($userRevita->getId());
$previousDiets = getEndedDiets($userRevita->getId());
$name = $userRevita->getName();
get_header(); ?>
  <div id="primary" class="content-area konto-tpl">
    <div class="container">
      <div class="col-12">
      <div class="wrapper">
        <div class="title">Witaj, <span class="name"><?= $name?>!</span> Zacznij z naszą pomocą dobry dzień</div>
        <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
          <?php if (function_exists('bcn_display')) {
              bcn_display();
          } ?>
        </div>
      </div><!-- ./wrapper -->
    </div>
    </div>
    <div class="container">
      <aside class="col-3 sidebar-konto">
        <div class="side-menu side-item">
		    <?php include 'template-parts/menu-user.php' ?>
        </div><!-- ./sidemenu -->
	      <?php include 'template-parts/progress-user.php' ?>
	      <?php include 'template-parts/subs-user.php' ?>
      </aside>
      <main id="main" class="site-main account-p user-p col-9" role="main">


        <h3 class="acc-title">Historia diety</h3>


        <?php if($currentDiet!=null &&  get_post_meta($currentDiet->ID,'sf_status',true)==1){

	        $currentDietID = $currentDiet->ID;
          $target = get_post_meta($currentDietID,'sf_purpose_diet',true);
          $dateStart = date('d.m.Y',get_post_meta($currentDietID,'sf_date_from',true));
          $part = wp_get_post_terms($currentDietID,'etap_diety');
          $weightStart = get_post_meta($currentDietID,'sf_weight_start',true);
          $weightStop = get_post_meta($currentDietID,'sf_weight_target',true);
	        $dateDiff = get_post_meta($currentDietID,'sf_date_to',true) - strtotime('now');
          $dayBetween =  floor($dateDiff / (60 * 60 * 24));
          $durationOfDiet =  wp_get_post_terms($currentDietID,'dl_diety');
          ?>
        <section class="info-box grn">
          <div class="header">
            <h4>Aktualna dieta</h4>
          </div>
          <div class="body">
            <div class="col-6">
              <p>
                <strong>Cel diety:</strong> <?= $target ?> <br>
                <strong>Data rozpoczęcia diety:</strong> <?= $dateStart ?> <br>

                <strong>Wskazany przez lekarza okres trwania diety:</strong> <?= $durationOfDiet[0]->name ?>
              </p>
            </div>
            <div class="col-6">
              <p>
                <strong>Etap diety:</strong> <?= $part[0]->name ?> <span class="red">(do końca diety <?= $dayBetween ?> dni)</span> <br>
                <strong>Waga na początku diety: </strong> <?= $weightStart ?> kg<br>
                <strong>Waga na końcu diety:</strong> <?= $weightStop ?> kg
              </p>
            </div>
          </div>
        </section>
    <?php }?>


        <?php if(is_array($previousDiets)) foreach($previousDiets as $diet){
	        $currentDietID = $diet->ID;
	        $target = get_post_meta($currentDietID,'sf_purpose_diet',true);
	        $dateStart = date('d.m.Y',get_post_meta($currentDietID,'sf_date_from',true));
	        $part = wp_get_post_terms($currentDietID,'etap_diety');
	        $weightStart = get_post_meta($currentDietID,'sf_weight_start',true);
	        $weightStop = get_post_meta($currentDietID,'sf_weight_target',true);
	        $dateDiff = get_post_meta($currentDietID,'sf_date_to',true) - strtotime('now');
	        $dayBetween =  floor($dateDiff / (60 * 60 * 24));
	        $durationOfDiet =  wp_get_post_terms($currentDietID,'dl_diety');
          ?>
        <section class="info-box gra">
          <div class="header">
            <h4>Historia dieta</h4>
          </div>
          <div class="body">
            <div class="col-6">
              <p>
                <strong>Cel diety:</strong> <?= $target ?> <br>
                <strong>Data rozpoczęcia diety:</strong> <?= $dateStart ?> <br>
                <strong>Wskazany przez lekarza okres trwania diety:</strong> <?= $durationOfDiet[0]->name ?>
              </p>
            </div>
            <div class="col-6">
              <p>
                <strong>Etap diety:</strong> <?= $part[0]->name ?><br>
                <strong>Waga na początku diety: </strong> <?= $weightStart ?> kg<br>
                <strong>Waga na końcu diety:</strong> <?= $weightStop ?> kg
              </p>
            </div>
            <a href="<?= get_the_permalink(get_id_after_template_filename('konto-dieta-archiwalna.php')).'?dietID='.$currentDietID . '&date='.date('d.m.Y',get_post_meta($currentDietID,'sf_date_from',true)) ?>" class="btn">Więcej</a>
          </div>
        </section>
    <?php } ?>



      </main><!-- #main -->
    </div><!-- ./container -->
  </div><!-- #primary -->
<?php get_footer();
