<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package like
 */
global $activated_account;  $titan = TitanFramework::getInstance( 'revita' );
?>
</div><!-- #content -->

<!-- Modal HTML embedded directly into document -->
<div class="modal <?= ($activated_account===TRUE || $activated_account===FALSE) ? '-active' : '' ?>" style="display:none;">
  <?= ($activated_account===TRUE) ? $titan->getOption('activation_success') : $titan->getOption('activation_error') ?>
</div>

<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="container">
		<div class="ft-left col-5">
			<div class="row">
        <div class="ft-logo">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" ><img src="<?php echo get_template_directory_uri(); ?>/images/logo-new.png" alt="<?php bloginfo( 'name' ); ?>" /></a><br>
				<p class="site-branding__desc"><b>Portal dietetyczny prowadzony<br>przez lekarzy</b></p>
        Partnerzy: <br>
        <a href="http://revitabielany.pl/" target="_blank" ><img style="max-width: 145px;" src="<?php echo get_template_directory_uri(); ?>/images/revita-logo-main-1.png" alt="Revita" /></a>
        </div>
				<div class="ft-adress">
					<?php
					the_field('dane_teleadresowe', 'option');
					?>
				</div>
			</div>
		</div>
		<div class="ft-right col-7">
			<div class="row">
				<nav id="site--btm-navigation" class="btm-navigation" role="navigation">
					<?php wp_nav_menu( array( 'theme_location' => 'footer_menu', 'menu_id' => 'ft-menu' ) ); ?>
				</nav><!-- #site-navigation -->
				<div class="icons-wrapper">
					<?php
					if( have_rows('ikonki_spolecznosiowe', 'option') ):
						while ( have_rows('ikonki_spolecznosiowe', 'option') ) : the_row();
					?>
					<a href="<?php the_sub_field("is_adres_url_ikonki"); ?>" class="icon-item"><?php the_sub_field("id_ikonka"); ?></a>
					<?php
					endwhile;
					else :
						endif;
					?>
				</div>
			</div>
		</div>
		<div class="btm-info">
			<nav id="site-btm-navigation" class="ft-menu col-7" role="navigation">
				<div class="row">
					<?php wp_nav_menu( array( 'theme_location' => 'footer_menu_btm', 'menu_id' => 'footer-menu' ) ); ?>
				</div>
			</nav><!-- #site-navigation -->
			<div class="ft-copy col-5">
				<div class="row">
					&copy; REVITADIET <?php echo date('Y'); ?>.  Wszelkie prawa zastrzeżone. Projekt & Realizacja <a class="like-logo" href="https://like.agency/" target="_blank"><img src="<?= TEMP_URI ?>/images/logo.svg" alt="Like Angency"></a>
				</div>
			</div>

		</div><!-- .site-info -->
	</div><!-- ./cont -->
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<a href="#" class="scrollToTop -notPrint"><i class="fa fa-angle-up"></i></a>
</body>
</html>
