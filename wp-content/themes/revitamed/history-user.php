<?php
/**
 * The template for displaying all pages.
 * Template name: Historia diet użytkownika
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */
global $page;
doc_page();
$userID = $page;
$diets = getAllDietUserToShow($userID);
if(empty($userID) || get_post_type($userID)!='user_client')
	trigger404();
get_header(); ?>
	<div id="primary" class="content-area konto-tpl">
		<div class="container">
			<div class="col-12">
				<div class="wrapper">
					<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
						<?php if (function_exists('bcn_display')) {
							bcn_display();
						} ?>
					</div>
				</div><!-- ./wrapper -->
			</div>
		</div>
		<div class="container">
			<aside class="col-3 sidebar-konto">
				<?php include 'template-parts/menu-dieter.php'; ?>
			</aside>
			<main id="main" class="site-main account-p col-9" role="main">
				<h3><?php the_title(); ?> - <?= getUserName($userID) ?></h3>
				<ul>
				<?php foreach($diets as $diet)
				{
					$dateStart =  get_post_meta($diet->ID,'sf_date_from',true);
					$dateStart = $dateStart ? $dateStart : 0;
					$dateStop =  get_post_meta($diet->ID,'sf_date_to',true);
					$dateStop = $dateStop ? $dateStop : 0;
					$name = '';
					$name .= getUserName($userID) . ' ';
					$name .= date('d.m.Y',$dateStart) . ' - ' . date('d.m.Y',$dateStop);
					printf('<li style="list-style-type: none;"><a href="%s">%s</a></li>',get_the_permalink($diet->ID),$name);
				}
				?>
				</ul>
			</main><!-- #main -->
		</div><!-- ./container -->
	</div><!-- #primary -->

<?php get_footer();
