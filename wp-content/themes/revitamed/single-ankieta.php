<?php
doc_page(true);
$data = getDataSurvey(get_the_ID());
get_header();?>

<div id="primary" class="content-area  container">
	<div class="col-12">
	<div class="wrapper">
		<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
			<?php if(function_exists('bcn_display'))
			{
				bcn_display();
			}?>
		</div>
	</div><!-- ./wrapper -->
</div>
	<main id="main" class="site-main bli" role="main">
		<form class="form" method="post">
			<div class="form__content">
				<?php viewSurvey($data,true,get_post_meta(get_the_ID(),'sf_client_name',true)); ?>
			</div>
		</form>
	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer();
