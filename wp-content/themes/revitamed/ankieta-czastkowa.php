<?php
/**
 * The template for displaying all single posts.
 * Template name: Ankieta cząstkowa
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package like
 */
global $page;
$surveyID = $page;
doc_page();
if(get_post_type($surveyID)!='part_ankieta')
{
	trigger404();
}
remove_theme_support('title-tag');
$title = getUserName(get_post_meta($surveyID,'sf_client_name',true));

add_action('wp_head', function () use ($title) {
	echo "<title>Ankieta cząstkowa - $title</title>";
}, 0);
get_header();
$isFemale = get_post_meta(get_post_meta($surveyID,'sf_client_name',true),'sf_sex',true)==1;
?>

	<div id="primary" class="content-area mb container">
		<h2>Ankieta cząstkowa  - <?= $title ?></h2>
		<main id="main" class="site-main col-9" role="main">
			<form method="post" class="formPartialSurvey">
        <div class="formPartialSurvey__row">
          <div class="formPartialSurvey__row__inputBox">
            <div class="formPartialSurvey__row__label">Waga</div>
            <input required name="user_weight" disabled value="<?= esc_html(get_post_meta($surveyID,'sf_calc_1',true)) ?>" pattern="[0-9,.]*" class="formPartialSurvey__row__input"
                   type="text">
          </div>

          <div class="formPartialSurvey__row__inputBox">
            <div class="formPartialSurvey__row__label">obwód brzucha- pomiar  na wysokości pępka</div>
            <input required name="user_calc_1" disabled value="<?= esc_html(get_post_meta($surveyID,'sf_calc_2',true)) ?>" pattern="[0-9,.]*" class="formPartialSurvey__row__input"
                   type="text">
          </div>
			<?php if(get_post_meta($surveyID,'sf_calc_3',true)!=''){ ?>
              <div class="formPartialSurvey__row__inputBox">
                <div class="formPartialSurvey__row__label">obwód talii- pomiar pod żebrami</div>
                <input required name="user_calc_2" disabled value="<?= esc_html(get_post_meta($surveyID,'sf_calc_3',true)) ?>" pattern="[0-9,.]*" class="formPartialSurvey__row__input"
                       type="text">
              </div>
              <div class="formPartialSurvey__row__inputBox">
                <div class="formPartialSurvey__row__label">obwód bioder- pomiar na wysokości
                  kolców biodrowych</div>
                <input required name="user_calc_3" disabled value="<?= esc_html(get_post_meta($surveyID,'sf_calc_4',true)) ?>" pattern="[0-9,.]*" class="formPartialSurvey__row__input"
                       type="text">
              </div>

			<?php } ?>
        </div>
				<div class="formPartialSurvey__row">
					<label>Czy dobrze się Pan/-i czuje po zakończeniu obecnego etapu
            diety? Tak/ Nie (objawy)?</label>
          <p><?= (get_post_meta($surveyID,'sf_question_check_1',true)==='1') ? 'TAK' : 'NIE' ?></p>
					<p><?= (get_post_meta($surveyID,'sf_question_check_1',true)==='0') ? esc_html(get_post_meta($surveyID,'sf_question_1',true)) : '' ?></p>
				</div>
				<div class="formPartialSurvey__row">
					<label>Czy pojawiły się produkty, których nie chce Pan/-i kontynuować w kolejnym etapie diety (np. znudziły się )? NIE/TAK (Jakie?)</label>
          <p><?= (get_post_meta($surveyID,'sf_question_check_2',true)==='1') ? 'TAK' : 'NIE' ?></p>
          <p><?= (get_post_meta($surveyID,'sf_question_check_2',true)==='1') ? esc_html(get_post_meta($surveyID,'sf_question_2',true)) : '' ?></p>
				</div>
				<div class="formPartialSurvey__row">
					<label>Czy są produkty, których szczególnie Pan/-i brakuje w
            jadłospisie? NIE/TAK (Jakie?)</label>
          <p><?= (get_post_meta($surveyID,'sf_question_check_3',true)==='1') ? 'TAK' : 'NIE' ?></p>
          <p><?= (get_post_meta($surveyID,'sf_question_check_3',true)==='1') ? esc_html(get_post_meta($surveyID,'sf_question_3',true)) : '' ?></p>
				</div>
				<div class="formPartialSurvey__row">
					<label>Czy w tym okresie zostały spożyte produkty niedozwolone?
            Nie/ Tak (Jakie? Ile razy?)</label>
          <p><?= (get_post_meta($surveyID,'sf_question_check_4',true)==='1') ? 'TAK' : 'NIE' ?></p>
          <p><?= (get_post_meta($surveyID,'sf_question_check_4',true)==='1') ? esc_html(get_post_meta($surveyID,'sf_question_4',true)) : '' ?></p>
				</div>
				<div class="formPartialSurvey__row">
					<label>Czy podczas diety był odczuwalny głód? Nie/ Tak (W jakich
            porach dnia i po jakich posiłkach?)</label>
          <p><?= (get_post_meta($surveyID,'sf_question_check_5',true)==='1') ? 'TAK' : 'NIE' ?></p>
          <p><?= (get_post_meta($surveyID,'sf_question_check_5',true)==='1') ? esc_html(get_post_meta($surveyID,'sf_question_5',true)) : '' ?></p>
				</div>
				<div class="formPartialSurvey__row">
					<label> Czy została rozpoczęta aktywność fizyczna? NIE/TAK
            (jak często?)</label>
          <p><?= (get_post_meta($surveyID,'sf_question_check_6',true)==='1') ? 'TAK' : 'NIE' ?></p>
          <p><?= (get_post_meta($surveyID,'sf_question_check_6',true)==='1') ? esc_html(get_post_meta($surveyID,'sf_question_6',true)) : '' ?></p>
				</div>
				<div class="formPartialSurvey__row">
					<label> Czy ma Pan/-i dodatkowe uwagi dotyczące diety? Nie/Tak
            (jakie?)</label>
          <p><?= (get_post_meta($surveyID,'sf_question_check_7',true)==='1') ? 'TAK' : 'NIE' ?></p>
          <p><?= (get_post_meta($surveyID,'sf_question_check_7',true)==='1') ? esc_html(get_post_meta($surveyID,'sf_question_7',true)) : '' ?></p>
				</div>
			</form>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
