<?php
/**
 * like functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package like
 */
date_default_timezone_set('Europe/Warsaw');
if ( ! function_exists( 'like_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function like_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on like, use a find and replace
	 * to change 'like' to the name of your theme in all the template files.
	 */


	load_theme_textdomain( 'like', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'like' ),
		) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
		) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'like_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
		) ) );
}
endif;
add_action( 'after_setup_theme', 'like_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function like_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'like_content_width', 640 );
}
add_action( 'after_setup_theme', 'like_content_width', 0 );


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';




/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function like_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Homepage Sidebar', 'like' ),
		'id'            => 'sidebar-main',
		'description'   => esc_html__( 'Add widgets here.', 'like' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
		) );
	register_sidebar( array(
		'name'          => __( 'Przepisy-sidebar', 'like' ),
		'id'            => 'secondary-sidebar',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
		) );
	register_sidebar( array(
		'name'          => __( 'Porady-sidebar', 'like' ),
		'id'            => 'blog-sidebar',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s ">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
		) );
	register_sidebar( array(
		'name'          => __( 'Strona-sidebar', 'like' ),
		'id'            => 'strona-sidebar',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s ">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
		) );
}
add_action( 'widgets_init', 'like_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);
 // remove dashicons

/** disable jason http://www.wpbeginner.com/wp-tutorials/how-to-disable-json-rest-api-in-wordpress/ */
add_filter('json_enabled', '__return_false');
add_filter('json_jsonp_enabled', '__return_false');
//* shortcodes */
add_filter('widget_text', 'do_shortcode');


// REMOVE ORIGINAL WOO STYLES

// add_filter( 'woocommerce_enqueue_styles', 'jk_dequeue_styles' );
// function jk_dequeue_styles( $enqueue_styles ) {
// 	unset( $enqueue_styles['woocommerce-general'] );
// 	unset( $enqueue_styles['woocommerce-layout'] );
// 	unset( $enqueue_styles['woocommerce-smallscreen'] );
// 	return $enqueue_styles;
// }

// //ADD NEW CUSTOM STYLES

// function wp_enqueue_woocommerce_style(){
// 	wp_register_style( 'woocommerce-general', get_template_directory_uri() . '/css/woocommerce.css' );
// 	wp_register_style( 'woocommerce-layout', get_template_directory_uri() . '/css/woocommerce-layout.css' );
// 	wp_register_style( 'woocommerce-smallscreen', get_template_directory_uri() . '/css/woocommerce-smallscreen.css' );

// 	if ( class_exists( 'woocommerce' ) ) {
// 		wp_enqueue_style( 'woocommerce-general' );
// 		wp_enqueue_style( 'woocommerce-layout' );
// 		wp_enqueue_style( 'woocommerce-smallscreen' );
// 	}
// }
// add_action( 'wp_enqueue_scripts', 'wp_enqueue_woocommerce_style' );


function like_scripts() {
	wp_enqueue_style( 'like-style', get_stylesheet_uri() );
	if ( is_front_page() ) {
		wp_enqueue_style( 'homecss', get_template_directory_uri() . '/css/style-home.css', array(), '1.2', 'all');
	} else {
		wp_enqueue_style( 'subcss', get_template_directory_uri() . '/css/style-sub.css', array(), '1.3', 'all');
	}
	wp_enqueue_style( 'menucss', get_template_directory_uri() . '/css/jquery.mmenu.all.css', array(), '1.11', 'all');
	wp_enqueue_style( 'SfStyles', get_template_directory_uri() . '/css/main.css', array(), '1.0', 'all');
	wp_enqueue_style( 'FancySelectCss', get_template_directory_uri() . '/css/jquery.fancybox.css', array(), '1.11', 'all');
	wp_deregister_script( 'jquery' );
	$jquery_cdn = '//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js';
	wp_enqueue_script( 'jquery', $jquery_cdn, array(), '20130115', true );
	wp_enqueue_script( 'like-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	// wp_enqueue_script( 'lightbox', get_template_directory_uri() . '/js/lightbox.js', array(), '20151215', true );
	wp_enqueue_script( 'fancybox', get_template_directory_uri() . '/js/jquery.fancybox.min.js', array(), '20151215', true );
	wp_enqueue_script( 'height', get_template_directory_uri() . '/js/match-height.js', array(), '20151215', true );
	wp_enqueue_script( 'like-owl', get_template_directory_uri() . '/js/owl.carousel.js', array(), '20151215', true );
	wp_enqueue_script( 'mmenu', get_template_directory_uri() . '/js/jquery.mmenu.all.min.js', array(), '20151215', true );
	wp_enqueue_script( 'readmore', get_template_directory_uri() . '/js/readmore.js', array(), '20151215', true );
	wp_enqueue_script( 'scripts', get_template_directory_uri() . '/js/scripts.js', array(), '20151215', true );
	wp_enqueue_script( 'SfScripts', get_template_directory_uri() . '/scripts/main.min.js', array(), '20151215', true );
	wp_enqueue_script( 'customscrollbar', get_template_directory_uri() . '/js/jquery.scrollbar.min.js', array(), '20151215', true );
	wp_enqueue_script( 'comment-reply' );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
	}
}
add_action( 'wp_enqueue_scripts', 'like_scripts' );




//* ----------------------------------------------------------------THUMBNAILS*//
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size(1600, 460, true);
add_image_size( 'home-medic', 290, 433, true ); //hpmedic
add_image_size( 'big-porada', 909, 366, true ); //hpmedic
add_image_size( 'recipe-big', 922, 424, true ); //recipes
add_image_size( 'recipe-list', 228, 161, true ); //recipes
add_image_size( 'list-porada', 320, 129, true ); //r
add_image_size( 'recipe-widget', 280, 132, true ); //recipes
add_image_size( 'recipe-thumb', 120, 71, true ); //recipes
add_image_size( 'recipe-tiny', 59, 46, true ); //recipes
add_image_size( 'recipe-diet-thumb', 91 , 67, true );
add_image_size( 'porady-widget', 80, 80, true ); //recipes
add_image_size( 'misja_top_image', 1600, 738, true ); //recipes
add_image_size( 'badania_top_image', 1600, 738, true ); //recipes
add_image_size( 'misja_repeater_image', 767, 99999 ); //recipes
add_image_size( 'misja_image', 500, 99999 ); //recipes
add_image_size( 'etapy-ikona', 89, 89, true ); //recipes
add_image_size( 'misja_btm_bg', 767, 99999 ); //recipes
add_image_size( 'misja_btm_bg2', 1600, 99999 ); //recipes
add_image_size( 'misja_btm_img', 245, 99999 ); //recipes
add_image_size( 'banner_btm_img', 262, 99999 ); //recipes

//add_image_size( 'button_thumb_size', 600, 200, true ); //widgety z pluginem buttons


//* -------------------------------------------------------ADD LIGHTBOX*//
// add_filter( 'wp_get_attachment_link', 'add_gallery_attribute', 10, 2 );
// function add_gallery_attribute( $link ) {
//
// 	return str_replace( '>', ' data-fancybox="group">', $link );
// }
//
// add_filter('the_content', 'addlightboxrel_replace');
// function addlightboxrel_replace ($content)
// {	global $post;
// 	$pattern = "/<a(.*?)href=('|\")(.*?).(bmp|gif|jpeg|jpg|png)('|\")(.*?)>/i";
// 	$replacement = '<a$1data-fancybox="group" href=$2$3.$4$5$6</a>';
// 	$content = preg_replace($pattern, $replacement, $content);
// 	$content = str_replace("%LIGHTID%", $post->ID, $content);
// 	return $content;
// }

add_filter('the_content', 'addlightboxrel_replace');
function addlightboxrel_replace ($content)
{	global $post;
	$pattern = "/<a(.*?)href=('|\")(.*?).(bmp|gif|jpeg|jpg|png)('|\")(.*?)>/i";
	$replacement = '<a$1 data-fancybox href=$2$3.$4$5$6</a>';
	$content = preg_replace($pattern, $replacement, $content);
	$content = str_replace("%LIGHTID%", $post->ID, $content);
	return $content;
}

add_filter( 'wp_get_attachment_link', 'add_gallery_attribute', 10, 2 );
function add_gallery_attribute( $link ) {
	global $post;
	return str_replace('<a href', '<a data-fancybox="group" href', $link);
}

//* -------------------------------------------------------remove some not needed INCLUDES in header*//
//remove_action('wp_head', 'rsd_link'); //removes EditURI/RSD (Really Simple Discovery) link.
//remove_action('wp_head', 'wlwmanifest_link'); //removes wlwmanifest (Windows Live Writer) link.
//remove_action('wp_head', 'wp_generator'); //removes meta name generator.
//remove_action('wp_head', 'wp_shortlink_wp_head'); //removes shortlink.
//remove_action( 'wp_head', 'feed_links', 2 ); //removes feed links.
//remove_action('wp_head', 'feed_links_extra', 3 );  //removes comments feed.


//* -------------------------------------------------------REMOVE EMOTICONS*//
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');


//* ----------------------------------------------------------------DEREGISTER STYLES*//
add_action( 'wp_print_styles', 'my_deregister_styles', 100 );
function my_deregister_styles()    {
	if( !is_user_logged_in() ){
		wp_deregister_style( 'dashicons');
	}
}

register_nav_menus( array(
	'primary' => 'Primary navigation',
	'footer_menu' => 'Footer navigation',
	'footer_menu_btm' => 'Footer bottom navigation',
	) );

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page('Dodatkowe ustawienia');
}

function get_medic_posts() {
	global $paged;
	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

	$args = array(
		'post_type' => 'lekarz',
		'posts_per_page' => 1,
		'order' => 'DESC',
		'orderby' => 'date',
		'post_status' => 'publish',
		'paged' => $paged
		);

	$z_query = new Wp_Query ($args);
	query_posts($args);

	return $z_query;
}

/**
 * Get taxonomies terms links.
 *
 * @see get_object_taxonomies()
 */
function wpdocs_custom_taxonomies_terms_links() {
  global $post;
    // Get post by post ID.
	$post = get_post( $post->ID );

    // Get post type by post.
	$post_type = $post->post_type;

    // Get post type taxonomies.
	$taxonomies = get_object_taxonomies( $post_type, 'objects' );

	$out = array();

	foreach ( $taxonomies as $taxonomy_slug => $taxonomy ){

        // Get the terms related to post.
		$terms = get_the_terms( $post->ID, $taxonomy_slug );

		if ( ! empty( $terms ) ) {
			$out[] = "<h2>" . $taxonomy->label . "</h2>\n<ul>";
			foreach ( $terms as $term ) {
				$out[] = sprintf( '<li><a href="%1$s">%2$s</a></li>',
					esc_url( get_term_link( $term->slug, $taxonomy_slug ) ),
					esc_html( $term->name )
					);
			}
			$out[] = "\n</ul>\n";
		}
	}
	return implode( '', $out );
}

add_filter( 'manage_edit-przepisy_columns', 'my_columns_filter', 10, 1 );
function my_columns_filter( $columns ) {
	$column_featured = array( 'featured' => 'Polecane' );
	$columns = array_slice( $columns, 0, 3, true ) + $column_featured + array_slice( $columns, 3, NULL, true );

	return $columns;
}
add_action( 'manage_posts_custom_column', 'my_column_action', 10, 1 );
function my_column_action( $column ) {

	if($column == 'featured')
	{
		if(get_field('featured')) {
			echo 'Tak';
		} else {
			echo 'Nie';
		}
	}
}

//if(! defined( 'ABSPATH' )) return;

class ACF_widget_with_image extends WP_Widget {

    function __construct() {
        parent::__construct(
      'custom_widget', // Base ID
      __('LIKE widget strona główna', 'like'), // Name
      array( 'description' => __( 'This is a custom widget with image', 'like' ), ) // Args
      );
    }

    public function widget( $args, $instance ) {
        echo $args['before_widget'];
        if ( !empty($instance['title']) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
        }
        $btn_txt = get_field('text_in_widget_txt', 'widget_' . $args['widget_id']);
        $ext_dsc = get_field('opis_po', 'widget_' . $args['widget_id']);
        $image = get_field('photo_in_widget', 'widget_' . $args['widget_id']);
        $image_url = $image['sizes']['thumbnail'];
        echo "<img src='". $image_url ."' />";
        echo "<div class=btn-widget>". $btn_txt ."</div><div class=outertri><div class=innertri></div></div>";
        echo "<div class=ext-desc>". $ext_dsc ."</div>";
        echo $args['after_widget'];
    }

    public function form( $instance ) {
        if ( isset($instance['title']) ) {
            $title = $instance['title'];
        }
        else {
            $title = __( 'New title', 'like' );
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <?php
    }

    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

        return $instance;
    }

}
add_action( 'widgets_init', function(){
    register_widget( 'ACF_widget_with_image' );
});

class ACF_widget_with_image_and_link extends WP_Widget {

    function __construct() {
        parent::__construct(
      'custom_widget_link', // Base ID
      __('ACF widget with image and link', 'like'), // Name
      array( 'description' => __( 'This is a custom widget with image', 'like' ), ) // Args
      );
    }

    public function widget( $args, $instance ) {
        echo $args['before_widget'];
        $btn_url = get_field('link_in_widget_txt_l', 'widget_' . $args['widget_id']);
        echo "<a href='". $btn_url ."' /><div class='wleft ws'>";
        if ( !empty($instance['title']) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
        }
        $btn_txt = get_field('text_in_widget_l', 'widget_' . $args['widget_id']);
        $image = get_field('photo_in_widget_l', 'widget_' . $args['widget_id']);
        $image_url = $image['sizes']['thumbnail'];
        echo "<img src='". $image_url ."' />";
        echo "</div><div class='wright ws'><div class=btn-widget>". $btn_txt ."</div>";
        echo "</div></a>";
        echo $args['after_widget'];
    }

    public function form( $instance ) {
        if ( isset($instance['title']) ) {
            $title = $instance['title'];
        }
        else {
            $title = __( 'New title', 'like' );
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <?php
    }

    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

        return $instance;
    }

}
add_action( 'widgets_init', function(){
    register_widget( 'ACF_widget_with_image_and_link' );
});

require_once "functions/load.php";

/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
    return 15;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );


function modify_read_more_link() {
    return '<a class="more-link rm button-inw" href="' . get_permalink() . '">Więcej</a>';
}
add_filter( 'the_content_more_link', 'modify_read_more_link' );


// add lightbox
//[foobar]
function lightbox_opener( $atts ){
  $lightboxImage = get_field('lightbox_image', 'options')['url'];
  return "<a href='".$lightboxImage."' class='btn-how-it-works' data-id='open-lightbox-home'>
              <span class='button-how-it-works'>Zobacz jak to działa!</span>
          </a>";
}
add_shortcode( 'open-lightbox', 'lightbox_opener' );
