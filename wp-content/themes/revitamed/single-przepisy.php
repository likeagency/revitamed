<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package like
 */

get_header();
the_post();
$custom = get_post_custom(get_the_ID());
$meal = getRelatedMealDiet(get_the_ID());
?>

  <div id="primary" class="content-area mb container content-fix">
    <div class="col-12">
    <div class="wrapper">
      <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">

          <?php if (function_exists('bcn_display')) {
              bcn_display();
          } ?>
      </div>
    </div><!-- ./wrapper -->
  </div>
    <main id="main" class="site-main col-9" role="main">
      <article>
        <header class="art">
          <h1 class="title"><?php the_title(); ?></h1>
          <div class="meta">
              <?php
              $rows = get_the_terms(get_the_ID(), 'rodzaj');
              $row = $rows[0];
              $term_link = get_term_link($row);
              if ($row->name != '') {
                  ?>
                <div class="kategoria">
                  <span class="name">Kategoria: </span>
                  <span class="res">
							<?php echo $row->name; ?>
						</span>
                </div><!-- ./kategoria -->
              <?php } ?>
              <?php if ($custom['rm_czas_przyg'][0] != '') { ?>
                <div class="time">
                  <span class="name">Czas przygotowania: </span>
                  <span class="res"><?php echo $custom['rm_czas_przyg'][0]; ?></span>
                </div><!-- ./time -->
              <?php } ?>
              <?php if ($custom['trudnosc'][0]) { ?>
                <div class="level">
                  <span class="name">Poziom trudności: </span>
                  <span class="res">
							        <?php if ($custom['trudnosc'][0] == 1) { ?>łatwy <?php } ?>
                      <?php if ($custom['trudnosc'][0] == 2) { ?>średni<?php } ?>
                      <?php if ($custom['trudnosc'][0] == 3) { ?>trudny<?php } ?>
						</span>
                </div><!-- ./level -->
              <?php } ?>
            <div class="articleNav__item -printFix">
              <div class="articleNav__button -print"><i class="fa fa-print" aria-hidden="true"></i>Drukuj</div>
            </div>
          </div><!-- ./meta -->
        </header>
          <?php $thumb = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'recipefull'); ?>
          <?php if($thumb['0']){ ?>
          <section class="im-gallery" style="background-image: url('<?php echo $thumb['0']; ?>')">
          <?php   } else{ ?>
              <section class="im-gallery" style="background: url('<?= TEMP_URI ?>/components/vendor/src/placeholder-big-new.png') no-repeat center center / contain">
          <?php  } ?>
            <?php if ($thumb['0'] != '') { ?>
              <img class="imgToPrint" src="<?php echo $thumb['0']; ?>">
            <?php } ?>

              <?php if (is_array($custom['rm_gallery'])) {
                echo "<ul>";
                  foreach ($custom['rm_gallery'] as $item) { ?>

                    <li>
                      <a href="<?php echo wp_get_attachment_image_url($item, ''); ?>" data-fancybox="group">
                        <img
                             src="<?php echo wp_get_attachment_image_src($item, 'recipe-thumb', false)[0]; ?>"
                             alt="<?php echo esc_attr(get_the_title($item)); ?>">
                      </a>
                    </li>
                  <?php }
                  echo "</ul>";
              } ?>

        </section><!-- ./imagallery -->
        <div class="wrapper-p">
          <?php ?>
            <?php if (is_array(get_post_meta(get_the_ID(), 'rm_ing', true))) { ?>
          <aside class="ingredients col-4">
            <?php if(is_array($meal)){?>
            <h3>Ten przepis jest w Twojej diecie, twoje proporcje na jeden posiłek to:</h3>
            <ul>
              <?php foreach($meal as $ing)
              {
	              $unit = mb_strtolower(get_unit_name(getUnitByIng($ing['ing']),$ing['amount']));
	              printf('<li class="ingredients__item">%s %s %s</li>', get_the_title($ing['ing']),$ing['amount'], $unit );

              }

                ?>
            </ul>
            <?php } else { ?>
            <h3>Składniki</h3>
            <ul>
                <?php
                $ing = get_post_meta(get_the_ID(), 'rm_ing', true);

                foreach ($ing as $item)
                    if (get_post_type($item['ing']) == 'skladnik' && (isset($item['amount']) || isset($item['amount_show']))) {
                        $slug = (get_post($item['ing']));
                        $slug = $slug->post_name;
                        

                        if($item['amount_show']!='')
                        {
	                        $amount = formatNumberToPolish($item['amount_show'],null);
	                        $unit = mb_strtolower(get_unit_name(getUnitByIng($item['ing']),$item['amount_show']));
                        }
                        else
                        {
	                        $amount = formatNumberToPolish($item['amount'],null);
	                        $unit = 'g';
                        }


                        printf('<li class="ingredients__item"><a class="ingredients__link" href="%s">%s</a> %s %s</li>', home_url('/skladnik/'.$slug), get_the_title($item['ing']),$amount,$unit);


                    }

                }
                ?>
            </ul>
            <?php } ?>
          </aside>
          <section class="main-content col-8">
              <?php echo $custom['rm_opis'][0]; ?>
          </section>
        </div>
      </article>

      <div class="articleNav">
        <div class="articleNav__content">
          <div class="articleNav__item">
            <div class="articleNav__button -print"><i class="fa fa-print" aria-hidden="true"></i>Drukuj</div>
          </div>
          <div class="articleNav__item">
            <a href="<?= admin_url('admin-ajax.php') . '?action=recipe_pdf&id=' . get_the_ID() ?>"
               class="articleNav__button"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>Pobierz</a>
          </div>
          <div class="articleNav__item">
            <div class="articleNav__button"><i class="fa fa-share-alt" aria-hidden="true"></i>Poleć</div>
            <div class="articleNav__socialBox">
              <div class="articleNav__socialItem">
                <a href="http://www.facebook.com/share.php?u=<?php the_permalink(); ?>"
                   onclick="window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
                   class="articleNav__socialLink"><i class="fa fa-facebook" aria-hidden="true"></i></a>
              </div>
              <div class="articleNav__socialItem">
                <a href="https://plus.google.com/share?url=<?php the_permalink(); ?>"
                   onclick="window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
                   class="articleNav__socialLink"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
              </div>
              <div class="articleNav__socialItem">
                <a href="https://twitter.com/intent/tweet?text=<?php the_permalink(); ?>&source=webclient"
                   onclick="window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
                   class="articleNav__socialLink"><i class="fa fa-twitter" aria-hidden="true"></i></a>
              </div>
              <div class="articleNav__socialItem">
                <a
                  href="mailto:adres@odbiorcy?subject=Chcę się podzielić tym wpisem z Tobą!&body=<?php the_permalink() ?>"
                  class="articleNav__socialLink"><i class="fa fa-envelope" aria-hidden="true"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>

    </main><!-- #main -->
    <aside id="secondary" class="col-3">
			<?php echo do_shortcode('[open-lightbox]');?>
      <div class="filterit">
        <h2>Filtruj przepisy</h2>
          <?php
          $count_posts = wp_count_posts('przepisy')->publish;
          ?>
        <div class="av">Dostępne przepisy:<span class="no"><?php echo $count_posts; ?></span></div>
      </div>
        <?php dynamic_sidebar('secondary-sidebar'); ?>
    </aside>
  </div><!-- #primary -->

<?php
get_footer();
