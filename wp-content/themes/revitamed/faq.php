<?php
/**
 * The template for displaying all single posts.
 * Template name: FAQ
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package like
 */

get_header();
the_post();
$custom = get_post_custom(get_the_ID());
?>

<div id="primary" class="content-area container">
  <div class="col-12">
  <div class="wrapper">
    <h2>FAQ - Często zadawane pytania</h2>
    <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
      <?php if (function_exists('bcn_display')) {
          bcn_display();
      } ?>
    </div>
  </div><!-- ./wrapper -->
</div>

  <main id="main" class="site-main" role="main">
    <div class="cmsContainer">
    <?php the_content(); ?>
    </div>
    <div class="faq col-12">
      <div class="faq__content">
        <?php $ix = 1; ?>
        <?php foreach(getAllCatFAQ() as $cat){?>

        <div id="faq_<?= $ix; ?>" class="faq__item">
          <div class="faq__title"><?= $cat->name ?></div>
          <div class="accordion">
            <?php foreach(getFAQ($cat->slug) as $item) { ?>
            <div class="accordion__item">
              <div class="accordion__title"><?= $item->post_title ?></div>
              <div class="accordion__hidden"><?= apply_filters('the_content',$item->post_content); ?></div>
            </div>
    <?php } ?>
          </div>
        </div>
<?php $ix++; ?>
  <?php } ?>

      </div>
    </div>

  </main><!-- #main -->
</div><!-- #primary -->
<?php get_footer(); ?>
