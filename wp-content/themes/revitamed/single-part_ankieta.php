<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package like
 */
the_post();
user_page();
$user = new user();
remove_theme_support('title-tag');
if(get_post_meta(get_the_ID(),'sf_client_name',true) != $user->getId()){
	trigger404();
}
add_action('wp_head', function () {
	echo "<title>Ankieta cząstkowa</title>";
}, 0);
if($_POST['form'])
{
	partialSurveyForm();
}
$isFemale = $user->getIsFemale();

$id = 1;
$questions = array(
  array(
    'toggleOnYes' => false,
    'text' => 'Czy dobrze się Pan/-i czuje po zakończeniu obecnego etapu diety?',
    'textareaName' => 'sf_question_1',
    'inputName' => 'sf_question_check_1',
    'placeholder' => 'Objawy?',
  ),
    array(
    'toggleOnYes' => true,
    'text' => 'Czy pojawiły się produkty, których nie chce Pan/-i kontynuować w kolejnym etapie diety (np. znudziły się )?',
    'textareaName' => 'sf_question_2',
    'inputName' => 'sf_question_check_2',
    'placeholder' => 'Jakie?',
  ),
    array(
    'toggleOnYes' => true,
    'text' => 'Czy są produkty, których szczególnie Pan/-i brakuje w jadłospisie? ',
    'textareaName' => 'sf_question_3',
    'inputName' => 'sf_question_check_3',
    'placeholder' => 'Jakie?',
  ),
    array(
    'toggleOnYes' => true,
    'text' => 'Czy w tym okresie zostały spożyte produkty niedozwolone ?',
    'textareaName' => 'sf_question_4',
    'inputName' => 'sf_question_check_4',
    'placeholder' => 'Jakie? Ile razy?',
  ),
    array(
    'toggleOnYes' => true,
    'text' => 'Czy był Pan/-i głodny podczas diety?',
    'textareaName' => 'sf_question_5',
    'inputName' => 'sf_question_check_5',
    'placeholder' => 'W jakich porach dnia i po jakich posiłkach?',
  ),
    array(
    'toggleOnYes' => true,
    'text' => 'Czy rozpoczął Pan/-i dodatkową aktywność fizyczną?',
    'textareaName' => 'sf_question_6',
    'inputName' => 'sf_question_check_6',
    'placeholder' => 'Jak często?',
  ),
    array(
    'toggleOnYes' => true,
    'text' => 'Czy ma Pan/-i dodatkowe uwagi dotyczące diety?',
    'textareaName' => 'sf_question_7',
    'inputName' => 'sf_question_check_7',
    'placeholder' => 'Jakie?',
  ),
);
$isCompleted = get_post_status(get_the_ID()) == 'private';
$isBlocked   = (bool)get_post_meta(get_the_ID(),'sf_blocked',true);

get_header(); ?>

	<div id="primary" class="content-area mb container">
		<div class="col-12">
		<h2 class="mb-30">Ankieta cząstkowa</h2>
		</div>
		<main id="main" class="site-main col-12" role="main">
      <?php if($isCompleted){ ?>
        Twoja ankieta została już wypełniona - dziękujemy!
     <?php } elseif($isBlocked){ ?>
        Twoja ankieta została została zablokowana z powodu niewypełnienia jej w terminie.
      <?php } else { ?>
			<form method="post" class="formPartialSurvey">

        <div class="formPartialSurvey__row">
          <div class="formPartialSurvey__row__inputBox">
            <div class="formPartialSurvey__row__label">Waga</div>
            <input required maxlength="3" name="user_weight" value="<?= esc_html(get_post_meta(get_the_ID(),'sf_calc_1',true)) ?>" pattern="[0-9,.]+" class="formPartialSurvey__row__input"
                   type="text">
          </div>

          <div class="formPartialSurvey__row__inputBox">
            <div class="formPartialSurvey__row__label">Obwód brzucha: pomiar  na wysokości pępka</div>
            <input required maxlength="3" name="user_calc_1" value="<?= esc_html(get_post_meta(get_the_ID(),'sf_calc_2',true)) ?>" pattern="[0-9,.]+" class="formPartialSurvey__row__input"
                   type="text">
          </div>
	        <?php if($isFemale){ ?>
          <div class="formPartialSurvey__row__inputBox">
            <div class="formPartialSurvey__row__label">obwód talii: pomiar pod żebrami</div>
            <input required maxlength="3" name="user_calc_2" value="<?= esc_html(get_post_meta(get_the_ID(),'sf_calc_3',true)) ?>" pattern="[0-9,.]+" class="formPartialSurvey__row__input"
                   type="text">
          </div>
            <div class="formPartialSurvey__row__inputBox">
            <div class="formPartialSurvey__row__label">obwód bioder: pomiar na wysokości
              kolców biodrowych</div>
            <input required maxlength="3" name="user_calc_3" value="<?= esc_html(get_post_meta(get_the_ID(),'sf_calc_4',true)) ?>" pattern="[0-9,.]+" class="formPartialSurvey__row__input"
                   type="text">
          </div>

            <?php } ?>
        </div>

		  <?php foreach ( $questions as $question ) {
			  ?>
            <div class="formPartialSurvey__row" data-show>
              <label for="question_sf_part_survey_1"><?= $question['text'] ?></label>
              <div class="form_box" data-group>
								<div class="form-row">
                	<input type="checkbox"
                       id="checkbox_<?= $id + 1 ?>" value="0"
                       name="<?= $question['inputName'] ?>" <?= get_post_meta( get_the_ID(), $question['inputName'], true ) === '0' ? 'checked' : '' ?>
                       <?= ( ! $question['toggleOnYes'] ) ? 'data-toggle' : '' ?>
                       data-item><label for="checkbox_<?= $id + 1 ?>">Nie</label>
								</div>
								<div class="form-row">
									<input type="checkbox"
			                 id="checkbox_<?= $id ?>" value="1"
			                 name="<?= $question['inputName'] ?>" <?= get_post_meta( get_the_ID(), $question['inputName'], true ) === '1' ? 'checked' : '' ?>
			                 <?= ( $question['toggleOnYes'] ) ? 'data-toggle' : '' ?>
			                 data-item><label for="checkbox_<?= $id ?>">Tak</label>
								</div>
              </div>
							<div class="ankieta-subform -hideOnNo">
								<label for="<?= $question['textareaName'] ?>" > <?= $question['placeholder'] ?> </label>
	              <textarea name="<?= $question['textareaName'] ?>"
	                        id="question_sf_part_survey_<?= $question['text'] ?>"
	                        ><?= esc_html( get_post_meta( get_the_ID(), $question['textareaName'], true ) ) ?></textarea>
            	</div>
</div>
			  <?php
			  $id += 2;
		  } ?>

        <br>
				<input type="hidden" name="form" value="1">
				<button class="form__button">Wyślij</button>
			</form>
      <?php } ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
