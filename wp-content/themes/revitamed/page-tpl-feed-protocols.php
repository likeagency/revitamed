<?php
/**
 * The template for displaying all pages.
 * Template name: Protokoły żywieniowe
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */

get_header(); ?>
  <div id="primary" class="content-area mb container">
    <div class="col-12">
    <div class="wrapper">
      <h2><?php the_title(); ?></h2>
      <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
          <span property="itemListElement" typeof="ListItem">
              <a property="item" typeof="WebPage" title="Go to Revitadiet." href="<?= home_url('/'); ?>" class="home"><span property="name">Revitadiet</span>
              </a><meta property="position" content="1">
          </span>
          &gt;
          <span property="itemListElement" typeof="ListItem"><span property="name">Protokoły żywieniowe</span><meta property="position" content="2"></span>
      </div>
    </div><!-- ./wrapper -->
  </div>
    <main id="main" class="site-main col-12" role="main">
      <div class="mainText">
          <?php the_content(); ?>
      </div>
      <div class="basicBox">
          <?php foreach(getAllProtocols() as $item){ ?>

        <a class="basicBox__item" href="<?= get_the_permalink($item->ID) ?>">
            <?php if(has_post_thumbnail($item)){ ?>
          <img class="basicBox__image"
               src="<?= get_the_post_thumbnail_url($item,'diagnostyka-thumb') ?>" alt='<?= esc_attr(get_the_title(get_post_thumbnail_id($item))) ?>'/>
<?php } else { ?>
            <img class="basicBox__image"
                 src="<?php echo get_template_directory_uri().'/components/basicBox/src/image9.jpg'; ?>" alt='placeholder'/>
            <?php } ?>

          <div class="basicBox__title">
            <?= $item->post_title ?>
          </div>

          <span class="basicBox__link">WIĘCEJ INFORMACJI <span class="basicBox__linkIcon fa fa-chevron-right"></span></span>

        </a>

    <?php } ?>
      </div>

    </main><!-- #main -->
    <!-- <aside id="secondary" class="col-3">
        <?php //include 'template-parts/indywidualnaDiagnostykaForm.php';  ?>

    </aside> -->
  </div>

<?php get_footer();
