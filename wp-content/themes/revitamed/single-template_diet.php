<?php
/**
 * The template for displaying all pages.
 * Template name: Układanie diety krok 2
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */

if($_POST['form']=='sf_template')
  saveTemplateDiet();
$dietID = get_the_ID();
$days = getDietInfo($dietID);
get_header();
$forbidden = get_post_meta($dietID,'sf_forbidden',true);
$forbidden = (is_array($forbidden)) ? '' : $forbidden;
$supp = get_post_meta($dietID,'sf_supplements',true);
$comments_diet = get_post_meta($dietID,'sf_warnings',true);

?>
	<div id="primary" class="content-area konto-tpl">
		<div class="container">
			<div class="col-12">
				<div class="wrapper">
					<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
						<?php if (function_exists('bcn_display')) {
							bcn_display();
						} ?>
					</div>
				</div><!-- ./wrapper -->
			</div>
		</div>

		<div class="container">
			<aside class="col-3 sidebar-konto">
				<div class="side-menu side-item">
					<?php include 'template-parts/menu-dieter.php'; ?>
					<?php include 'template-parts/user-info-dieter.php'; ?>
				</div><!-- ./sidemenu -->
			</aside>
			<main id="main" class="site-main account-p col-9" role="main">
			<div id="sticky-box" class="diet-top-box sticky-scroll-box sticky"><h3>Dieta</h3>
        <div class="days-buttons-box">
        <span class="all-btn" data-toggle="hide">Ukryj wszystkie</span>
        <span class="day-btn" data-day-show="0">Pn.</span>
        <span class="day-btn" data-day-show="1">Wt.</span>
        <span class="day-btn" data-day-show="2">Śr.</span>
        <span class="day-btn" data-day-show="3">Czw.</span>
        <span class="day-btn" data-day-show="4">Pt.</span>
        <span class="day-btn" data-day-show="5">Sob.</span>
        <span class="day-btn" data-day-show="6">Niedz.</span>
        <span class="btn-skladniki">Ukryj składniki</span>
        </div>
      </div>

      <style>
      .fixed {
        position:fixed;
        top:0;
        z-index:99999;
      }
      </style>
				<div class="dietPicker js-dietPicker"
				     data-autocomplete="<?= esc_attr(json_encode(getAllRecipies())); ?>">
					<form class="dietPicker__form" method="post" action="#" data-diet-form>

			  <?php displayDietPlanEdit($days);  ?>

        <?php displayDietTextFields($forbidden,$supp,$comments_diet); ?>


            <div class="dietPicker__navigation" data-diet-navigation>
							<button class="btn dietPicker__link" data-diet-save>Zapisz szablon</button>
						</div>
						<input type="hidden" value="sf_template" name="form" >
						<input type="hidden" value="<?= get_the_ID(); ?>" name="sf_template">

					</form>

				</div>

			</main><!-- #main -->
		</div><!-- ./container -->



	</div><!-- #primary -->
<?php get_footer();
