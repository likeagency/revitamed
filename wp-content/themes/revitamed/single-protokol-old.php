<?php
/**
 * The template for displaying all pages.
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */
$prohibited_products = get_post_meta(get_the_ID(),'rm_zakazane',true);
$reduced_products = get_post_meta(get_the_ID(),'rm_ograniczone',true);
$allowed_products =  get_post_meta(get_the_ID(),'rm_wskazane',true);
$banners = get_post_meta(get_the_ID(),'rm_group',true);

if(!is_array($prohibited_products))
    $prohibited_products=[];
if(!is_array($reduced_products))
    $reduced_products=[];
if(!is_array($allowed_products))
    $allowed_products=[];

get_header(); ?>
  <div id="primary" class="content-area mb container">
    <div class="col-12">
    <div class="wrapper">
      <h2><?php the_title(); ?></h2>
      <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
          <span property="itemListElement" typeof="ListItem">
              <a property="item" typeof="WebPage" title="Go to Revitadiet." href="<?= home_url('/') ?>" class="home"><span property="name">Revitadiet</span>
              </a><meta property="position" content="1">
          </span>
          &gt;
          <span property="itemListElement" typeof="ListItem">
              <a property="item" typeof="WebPage" href="<?= get_the_permalink(get_id_after_template_filename('page-tpl-feed-protocols.php')) ?>" class="home"><span property="name"><?= get_the_title(get_id_after_template_filename('page-tpl-feed-protocols.php')); ?></span>
              </a><meta property="position" content="2">
          </span>
          &gt;
          <span property="itemListElement" typeof="ListItem"><span property="name"><?php the_title(); ?></span><meta property="position" content="3"></span>
      </div>
    </div><!-- ./wrapper -->
  </div>
    <main id="main" class="site-main col-9" role="main">
      <div class="mainText">
          <?php the_content(); ?>

        <section class="diet-basket">
          <h4>Twój koszyk żywieniowy</h4>

          <div class="piramida">
            <div class="row1 rowp">
              <div class="bg-top"></div>
              <div class="txt-top txtp">
                <div class="spacert">
              </div>
                <h4>Produkty zakazane:</h4>
                <?= implode(' ',array_map('format_item_protokol',$prohibited_products)) ?>
              </div>
            </div>
            <div class="row2 rowp">
              <div class="bg-top"></div>
              <div class="txt-top txtp">
                <h4>Produkty ograniczone:</h4>
                  <?= implode(' ',array_map('format_item_protokol',$reduced_products)) ?>
              </div>
            </div>
            <div class="row3 rowp">
              <div class="bg-top"></div>
              <div class="txt-top txtp">
                <h4>Produkty wskazane:</h4>
                  <?= implode(' ',array_map('format_item_protokol',$allowed_products)) ?>
              </div>
            </div>
          </div>
        </section>
      </div>

      <div class="encourage">
          <?php foreach($banners as $item){?>
        <div class="encourage__item">
          <div class="encourage__content">
            <div class="encourage__textbox">
              <div class="encourage__text">
                  <?= apply_filters('the_content',$item['content__sf']); ?>
              </div>
              <<?= ( $item['link']!='') ? 'a' : 'div' ?> <?= ( $item['link']!='' &&  $item['open']) ? 'target="_blank"' : '' ?> class="encourage__link" <?= ($item['link']!='') ? "href='{$item['link']}'" : '' ?> ><?= $item['text'] ?>
                  <span class="encourage__linkIcon fa fa-chevron-right"></span></<?= ( $item['link']!='') ? 'a' : 'div' ?>>
            </div>
            <?php if(wp_get_attachment_image_url($item['image'][0],'protokol-banner-thumb')!=''){ ?><div class="encourage__imagebox">
              <img class="encourage__image" src="<?= wp_get_attachment_image_url($item['image'][0],'protokol-banner-thumb') ?>" alt="<?= esc_attr(get_the_title($item['image'][0])) ?>">
            </div>
              <?php } ?>
          </div>
        </div>
    <?php } ?>


      </div>


    </main><!-- #main -->
    <aside id="secondary" class="col-3">
        <?php include 'template-parts/indywidualnaDiagnostykaForm.php';  ?>
    </aside>
  </div>

<?php get_footer();
