jQuery(function ($) {
    var wpbody = $("#wpbody");
    wpbody.on('click', '.sf-attachment-add-images', function (event) {
        event.preventDefault();
        var widget = $(this).closest('.widget');
        var frame = wp.media({
            title: 'Wybierz zdjęcie',
            library : { type : 'image' },
            multiple: false,
            button:{ text: 'Ustaw zdjęcie w widgecie' }
        });

        frame.on('select', function () {
            var attachment = frame.state().get('selection').first().toJSON();
            widget.find(".input_id_of_my_image").val(attachment.id);
            widget.find(".input_my_image_widget").attr("src",attachment.url);
            $('.sf-attachment-remove-images').css("display","inline-block");
        });

        frame.open();
        return false;
    });

    wpbody.on('click', '.sf-attachment-remove-images', function (event) {
        event.preventDefault();
        var widget = $(this).closest('.widget');
        widget.find(".input_id_of_my_image").val('');
        widget.find(".input_my_image_widget").attr("src",'');
    });

});