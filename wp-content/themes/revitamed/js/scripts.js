var $ = jQuery.noConflict();

var Project = {
  init: function () {
    $('html').removeClass('no-js');
    Project.mMenuInit();
    Project.matchHeightInit();
    Project.tabsInit();
    Project.widextInit();
    Project.expandInit();
    Project.rmInit();
    Project.owlInit();
		Project.swapSliderMobileBtnHref();
  },
  mMenuInit: function () {
    $("#site-navigation").mmenu({}, {
      clone: true
    });
    var API = $("#site-navigation").data("mmenu");
    var _PLUGIN_ = 'mmenu';
    $[_PLUGIN_].configuration.classNames.selected = 'current-menu-item';
    $("#wpadminbar")
      .css('position', 'fixed')
      .addClass('mm-slideout');
  },
  matchHeightInit: function () {
    $('.about .container .ain').matchHeight();
    $('.resport-wrapper article').matchHeight();
    $('.box-info div').matchHeight();
    $('.etap-box-top p:nth-child(2)').matchHeight();
    $('.etap-box-middle').matchHeight();
    $('.etap-box-middle p:nth-child(1)').matchHeight();
    $('.etap-box-middle p:nth-child(2)').matchHeight();
    $('.container-img .col-6').matchHeight();
    $('.survey-box .survey-box-text').matchHeight();
    $('.sciezka-box-text').matchHeight();
    $('.tab-top-text').matchHeight();
    $('.circle-container').matchHeight();
    $('.circle-container p').matchHeight();
    $('.form__label').matchHeight();
  },
  tabsInit: function () {
    $('.wdg-wrapper').each(function() {
      var topole = this;

    $(".tab_content", topole).hide();
    //$(".tab_content:first").show();

    /* if in tab mode */
    $("ul.tabs li", topole).click(function () {

      $(".tab_content", topole).hide();
      var activeTab = $(this).attr("rel");
      $("#" + activeTab, topole).fadeIn();

      $("ul.tabs li", topole).removeClass("active");
      $(this).addClass("active");
      if ($(this).hasClass('active') ) {
        $('.tab-inner-box', topole).addClass('border-btm');
        $('.arrow', topole).addClass('animated');

      }
      $(".tab_drawer_heading", topole).removeClass("d_active");
      $(".tab_drawer_heading[rel^='" + activeTab + "']", topole).addClass("d_active");

    });

    $(".tab_content .close", topole).click(function (e) {
      e.preventDefault();
      console.log("click");
      $("ul.tabs li", topole).removeClass("active");
      $(".tab_content", topole).fadeOut();
        $('.tab-inner-box', topole).removeClass('border-btm');
        $('.arrow', topole).removeClass('animated');
      // $("#"+activeTab).fadeOut();
      // $(this).parent().removeClass("active");
      // $(".tab_drawer_heading").removeClass("d_active");

    });
    /* if in drawer mode */
    $(".tab_drawer_heading", topole).click(function () {

      $(".tab_content", topole).hide();
      var d_activeTab = $(this).attr("rel");
      $("#" + d_activeTab, topole).fadeIn();

      $(".tab_drawer_heading", topole).removeClass("d_active");
      $(this, topole).addClass("d_active");

      $("ul.tabs li", topole).removeClass("active");
      $("ul.tabs li[rel^='" + d_activeTab + "']", topole).addClass("active");
    });


    /* Extra class "tab_last"
       to add border to right side
       of last tab */
    $('ul.tabs li', topole).last().addClass("tab_last");
    });
  },


  rmInit: function () {
    $('.row1 .txtp').readmore({
      speed: 175,
      collapsedHeight: 115,
      moreLink: '<a href="#" class=rmlink>Rozwiń <i class="fa fa-caret-right" aria-hidden="true"></i></a>',
      lessLink: '<a href="#" class=rslink>Zwiń <i class="fa fa-caret-down" aria-hidden="true"></i></a>'
    });
    $('.row2 .txtp').readmore({
      speed: 175,
      collapsedHeight: 76,
      moreLink: '<a href="#" class=rmlink>Rozwiń <i class="fa fa-caret-right" aria-hidden="true"></i></a>',
      lessLink: '<a href="#" class=rslink>Zwiń <i class="fa fa-caret-down" aria-hidden="true"></i></a>'
    });
    $('.row3 .txtp').readmore({
      speed: 175,
      collapsedHeight: 76,
      moreLink: '<a href="#" class=rmlink>Rozwiń <i class="fa fa-caret-right" aria-hidden="true"></i></a>',
      lessLink: '<a href="#" class=rslink>Zwiń <i class="fa fa-caret-down" aria-hidden="true"></i></a>'
    });
  },
  widextInit: function () {
    //  $('.widget_custom_widget').each(function(){
    //     var wtex = $(this).find('.ext-desc');
    //     var getwwh = $(this).height();
    //     var gethw = wtex.height();
    //     console.log(getwwh);
    //     console.log(gethw);
    //     $(this).height( getwwh + gethw );
    // });

    //  $('.widget_custom_widget').click(function(){
    //     console.log("click");
    //     var wtex = $(this).find('.ext-desc');
    //     wtex.slideToggle();
    // });
  },
  expandInit: function () {
    // $('.piramida .rowp .txtp').click(function(){
    // 	$(this).find('.more').slideToggle();
    // });
  },
  owlInit: function () {
    $('#owl-carousel-big').owlCarousel({
      loop: true,
      items: 1,
      autoplay: true,
      autoplayTimeout: 5000,
      autoplaySpeed: 700,
      nav: true,
      dots: true,
      navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],

    }),
      $('#owl-carousel-lekarz').owlCarousel({
        loop: true,
        items: 1,
        autoplay: true,
        nav: false,
        dots: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],

      }),
      $('#owl-carousel-tm').owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        autoplay: false,
        responsiveClass: true,
        autoplayHoverPause: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        responsive: {
          0: {
            items: 1,
            nav: false,
            dots: true
          },
          767: {
            items: 2,
            nav: true,
            dots: true
          },
          1246: {
            items: 3,
            nav: true,
            dots: false
          }
        },
      });
  },
	swapSliderMobileBtnHref() {
		
		
		const activeLink = $('.owl-item.active').find('.button');
		const activeLinkHref = activeLink.attr('href');
		const activeLinkInnerText = activeLink.text();
		
		const sliderMobileBtn = $('[data-id="slider-mobileBtn"]');
		sliderMobileBtn.attr('href', activeLinkHref);
		sliderMobileBtn.text(activeLinkInnerText);
	}
}

jQuery(document).ready(
  function () {
    Project.init();
  }
);


// open href swapping function when owl generates an event
$('#owl-carousel-big').on('translated.owl.carousel', function(event) {
	Project.swapSliderMobileBtnHref();
});

/* Thanks to CSS Tricks for pointing out this bit of jQuery
http://css-tricks.com/equal-height-blocks-in-rows/
It's been modified into a function called at page load and then each time the page is resized. One large modification was to remove the set height before each new calculation. */

// equalheight = function(container){

// 	var currentTallest = 0,
// 	currentRowStart = 0,
// 	rowDivs = new Array(),
// 	$el,
// 	topPosition = 0;
// 	$(container).each(function() {

// 		$el = $(this);
// 		$($el).height('auto')
// 		topPostion = $el.position().top;

// 		if (currentRowStart != topPostion) {
// 			for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
// 				rowDivs[currentDiv].height(currentTallest);
// 			}
//      rowDivs.length = 0; // empty the array
//      currentRowStart = topPostion;
//      currentTallest = $el.height();
//      rowDivs.push($el);
//  } else {
//  	rowDivs.push($el);
//  	currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
//  }
//  for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
//  	rowDivs[currentDiv].height(currentTallest);
//  }
// });
// }

// $(window).load(function() {
// 	//equalheight('.wdg-wrapper .widget');
// });

// $(window).resize(function(){
// 	//equalheight('.wdg-wrapper .widget');
// });

(function ($) {
  $.fn.extend({
    easyResponsiveTabs: function (options) {
      //Set the default values, use comma to separate the settings, example:
      var defaults = {
        type: 'default', //default, vertical, accordion;
        width: 'auto',
        fit: true,
        closed: false,
        tabidentify: '',
        activetab_bg: 'white',
        inactive_bg: '#F5F5F5',
        active_border_color: '#c1c1c1',
        active_content_border_color: '#c1c1c1',
        activate: function () {
        }
      }
      //Variables
      var options = $.extend(defaults, options);
      var opt = options, jtype = opt.type, jfit = opt.fit, jwidth = opt.width, vtabs = 'vertical', accord = 'accordion';
      var hash = window.location.hash;
      var historyApi = !!(window.history && history.replaceState);

      //Events
      $(this).bind('tabactivate', function (e, currentTab) {
        if (typeof options.activate === 'function') {
          options.activate.call(currentTab, e)
        }
      });

      //Main function
      this.each(function () {
        var $respTabs = $(this);
        var $respTabsList = $respTabs.find('ul.resp-tabs-list.' + options.tabidentify);
        var respTabsId = $respTabs.attr('id');
        $respTabs.find('ul.resp-tabs-list.' + options.tabidentify + ' li').addClass('resp-tab-item').addClass(options.tabidentify);
        $respTabs.css({
          'display': 'block',
          'width': jwidth
        });

        if (options.type == 'vertical')
          $respTabsList.css('margin-top', '3px');

        $respTabs.find('.resp-tabs-container.' + options.tabidentify).css('border-color', options.active_content_border_color);
        $respTabs.find('.resp-tabs-container.' + options.tabidentify + ' > div').addClass('resp-tab-content').addClass(options.tabidentify);
        jtab_options();

        //Properties Function
        function jtab_options() {
          if (jtype == vtabs) {
            $respTabs.addClass('resp-vtabs').addClass(options.tabidentify);
          }
          if (jfit == true) {
            $respTabs.css({width: '100%', margin: '0px'});
          }
          if (jtype == accord) {
            $respTabs.addClass('resp-easy-accordion').addClass(options.tabidentify);
            $respTabs.find('.resp-tabs-list').css('display', 'none');
          }
        }

        //Assigning the h2 markup to accordion title
        var $tabItemh2;
        $respTabs.find('.resp-tab-content.' + options.tabidentify).before("<h2 class='resp-accordion " + options.tabidentify + "' role='tab'><span class='resp-arrow'></span></h2>");

        $respTabs.find('.resp-tab-content.' + options.tabidentify).prev("h2").css({
          'background-color': options.inactive_bg,
          'border-color': options.active_border_color
        });

        var itemCount = 0;
        $respTabs.find('.resp-accordion').each(function () {
          $tabItemh2 = $(this);
          var $tabItem = $respTabs.find('.resp-tab-item:eq(' + itemCount + ')');
          var $accItem = $respTabs.find('.resp-accordion:eq(' + itemCount + ')');
          $accItem.append($tabItem.html());
          $accItem.data($tabItem.data());
          $tabItemh2.attr('aria-controls', options.tabidentify + '_tab_item-' + (itemCount));
          itemCount++;
        });

        //Assigning the 'aria-controls' to Tab items
        var count = 0,
          $tabContent;
        $respTabs.find('.resp-tab-item').each(function () {
          $tabItem = $(this);
          $tabItem.attr('aria-controls', options.tabidentify + '_tab_item-' + (count));
          $tabItem.attr('role', 'tab');
          $tabItem.css({
            'background-color': options.inactive_bg,
            'border-color': 'none'
          });

          //Assigning the 'aria-labelledby' attr to tab-content
          var tabcount = 0;
          $respTabs.find('.resp-tab-content.' + options.tabidentify).each(function () {
            $tabContent = $(this);
            $tabContent.attr('aria-labelledby', options.tabidentify + '_tab_item-' + (tabcount)).css({
              'border-color': options.active_border_color
            });
            tabcount++;
          });
          count++;
        });

        // Show correct content area
        var tabNum = 0;
        if (hash != '') {
          var matches = hash.match(new RegExp(respTabsId + "([0-9]+)"));
          if (matches !== null && matches.length === 2) {
            tabNum = parseInt(matches[1], 10) - 1;
            if (tabNum > count) {
              tabNum = 0;
            }
          }
        }

        //Active correct tab
        $($respTabs.find('.resp-tab-item.' + options.tabidentify)[tabNum]).addClass('resp-tab-active').css({
          'background-color': options.activetab_bg,
          'border-color': options.active_border_color
        });

        //keep closed if option = 'closed' or option is 'accordion' and the element is in accordion mode
        if (options.closed !== true && !(options.closed === 'accordion' && !$respTabsList.is(':visible')) && !(options.closed === 'tabs' && $respTabsList.is(':visible'))) {
          $($respTabs.find('.resp-accordion.' + options.tabidentify)[tabNum]).addClass('resp-tab-active').css({
            'background-color': options.activetab_bg + ' !important',
            'border-color': options.active_border_color,
            'background': 'none'
          });

          $($respTabs.find('.resp-tab-content.' + options.tabidentify)[tabNum]).addClass('resp-tab-content-active').addClass(options.tabidentify).attr('style', 'display:block');
        }
        //assign proper classes for when tabs mode is activated before making a selection in accordion mode
        else {
          // $($respTabs.find('.resp-tab-content.' + options.tabidentify)[tabNum]).addClass('resp-accordion-closed'); //removed resp-tab-content-active
        }

        //Tab Click action function
        $respTabs.find("[role=tab]").each(function () {

          var $currentTab = $(this);
          $currentTab.click(function () {

            var $currentTab = $(this);
            var $tabAria = $currentTab.attr('aria-controls');

            if ($currentTab.hasClass('resp-accordion') && $currentTab.hasClass('resp-tab-active')) {
              $respTabs.find('.resp-tab-content-active.' + options.tabidentify).slideUp('', function () {
                $(this).addClass('resp-accordion-closed');
              });
              $currentTab.removeClass('resp-tab-active').css({
                'background-color': options.inactive_bg,
                'border-color': 'none'
              });
              return false;
            }
            if (!$currentTab.hasClass('resp-tab-active') && $currentTab.hasClass('resp-accordion')) {
              $respTabs.find('.resp-tab-active.' + options.tabidentify).removeClass('resp-tab-active').css({
                'background-color': options.inactive_bg,
                'border-color': 'none'
              });
              $respTabs.find('.resp-tab-content-active.' + options.tabidentify).slideUp().removeClass('resp-tab-content-active resp-accordion-closed');
              $respTabs.find("[aria-controls=" + $tabAria + "]").addClass('resp-tab-active').css({
                'background-color': options.activetab_bg,
                'border-color': options.active_border_color
              });

              $respTabs.find('.resp-tab-content[aria-labelledby = ' + $tabAria + '].' + options.tabidentify).slideDown().addClass('resp-tab-content-active');
            } else {
              console.log('here');
              $respTabs.find('.resp-tab-active.' + options.tabidentify).removeClass('resp-tab-active').css({
                'background-color': options.inactive_bg,
                'border-color': 'none'
              });

              $respTabs.find('.resp-tab-content-active.' + options.tabidentify).removeAttr('style').removeClass('resp-tab-content-active').removeClass('resp-accordion-closed');

              $respTabs.find("[aria-controls=" + $tabAria + "]").addClass('resp-tab-active').css({
                'background-color': options.activetab_bg,
                'border-color': options.active_border_color
              });

              $respTabs.find('.resp-tab-content[aria-labelledby = ' + $tabAria + '].' + options.tabidentify).addClass('resp-tab-content-active').attr('style', 'display:block');
            }
            //Trigger tab activation event
            $currentTab.trigger('tabactivate', $currentTab);

            //Update Browser History
            if (historyApi) {
              var currentHash = window.location.hash;
              var tabAriaParts = $tabAria.split('tab_item-');
              // var newHash = respTabsId + (parseInt($tabAria.substring(9), 10) + 1).toString();
              var newHash = respTabsId + (parseInt(tabAriaParts[1], 10) + 1).toString();
              if (currentHash != "") {
                var re = new RegExp(respTabsId + "[0-9]+");
                if (currentHash.match(re) != null) {
                  newHash = currentHash.replace(re, newHash);
                }
                else {
                  newHash = currentHash + "|" + newHash;
                }
              }
              else {
                newHash = '#' + newHash;
              }

              history.replaceState(null, null, newHash);
            }
          });

        });

        //Window resize function
        $(window).resize(function () {
          $respTabs.find('.resp-accordion-closed').removeAttr('style');
        });
      });
    }
  });
})(jQuery);


$(document).ready(function(){

$( "ul.badania-tabs li:first-child" ).addClass( "current" );
$( ".badania-tab-content:first-child" ).addClass( "current" );

$('ul.badania-tabs li').click(function(){
	var tab_id = $(this).attr('data-tab');

	$('ul.badania-tabs li').removeClass('current');
	$('.badania-tab-content').removeClass('current');

	$(this).addClass('current');
	$("#"+tab_id).addClass('current');
})

})

$(document).ready(function(){

 if ($('.badania-blok-25').length == 5) {
   $(".badania-blok-25").addClass("badania-blok-20");
  } else {
   $(".badania-blok-25").removeClass("badania-blok-20");
 }

});

$(document).ready(function(){

   $(".home .box-ttm span.r1").before("<span class='ttm-more'>więcej <i class='fa fa-angle-right'></i></span>");

   $("section.widget_ultimate_posts li").not(":has(.upw-image)").prepend("<div class='upw-image holding-image'><img src='/wp-content/themes/revitamed/components/vendor/src/placeholder-new.png' style='padding-right:5px;'/></div>");
});

$(document).ready(function(){
  var daysToggler = $('.all-btn');
  var daysButtons = $(".day-btn");
  var dietOptions = $(".dietPicker .dietPicker__option");
  daysToggler.on('click', function () {
    if ($(this).attr('data-toggle') == 'hide') {
      $(this).attr('data-toggle', 'show').text("Pokaż wszystkie").addClass('to-show').removeClass('to-hide');
      dietOptions.removeClass("-active");
    } else {
      $(this).attr('data-toggle', 'hide').text("Ukryj wszystkie").removeClass('to-show').addClass('to-hide');
      dietOptions.addClass('-active');
    }
  });

  daysButtons.on('click', function () {
    var selectedDay = $(this).attr('data-day-show');
    dietOptions.removeClass('-active');
    daysButtons.removeClass('day-selected');
    $(this).addClass('day-selected');
    dietOptions.filter('[data-day-show="'+selectedDay+'"]').addClass('-active');
    daysToggler.attr('data-toggle', 'show').text("Pokaż wszystkie").addClass('to-show').removeClass('to-hide');
  });

  $(".btn-skladniki").click(function(){
    if($(this).text() == "Ukryj składniki") {
      $(this).text("Pokaż składniki");
    }else {
      $(this).text("Ukryj składniki");
    };

    $(".dietPicker-form__ingredients").toggleClass("hide-ingrediends");

  });

});


// // When the user scrolls the page, execute myFunction
// window.onscroll = function() {myFunction()};
//
// // Get the navbar
// var navbar1 = document.getElementById("sticky-box");
//
// // Get the offset position of the navbar
// var sticky = navbar1.offsetTop;
//
// // Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
// function myFunction() {
//   if (window.pageYOffset >= sticky) {
//     navbar1.classList.add("fixed")
//   } else {
//     navbar1.classList.remove("fixed");
//   }
// }


// STICKY SCROLL BOX MENU - NIE KOMENTOWAĆ TEGO
$(document).ready(function () {
  var $stickyBox = $('.sticky-scroll-box');

  if($stickyBox.length > 0){
    var top = $stickyBox.offset().top;

    $(window).scroll(function (event) {
      var formCustom = $('[data-diet-form]');
      var formCustomWidth = formCustom.width();
      var y = $(this).scrollTop();
      if (y >= top) {
          $stickyBox.addClass('fixed');
          $stickyBox.width(formCustomWidth -20);
      }
      else {
          $stickyBox.removeClass('fixed');
          $stickyBox.width(formCustomWidth);
      }
    });
  }
	
	

		
	var homeLightboxButton = $('[data-id="open-lightbox-home"]');
	

	homeLightboxButton.fancybox();
});
