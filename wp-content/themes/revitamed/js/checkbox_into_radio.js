(function () {
    "use strict";
    var $=jQuery,$metabox,$selects;
    $(document).ready(function () {
        init();
    });

     function init()
    {
        catchDOM();
        bindEvents();
        $.each($selects,replaceUnit);
    }

    function catchDOM()
    {
        $metabox = $('#standard');
        $selects = $metabox.find('select');
    }

    function bindEvents()
    {

        $metabox.on('change','select',replaceUnit);
    }

    function replaceUnit(){
         if($(this).val()=='')
             return;
        var that = this;
        try{

            $.getJSON(ajaxurl+'?action=getunit&id=' + $(this).val(),function (data) {
                $(that).parents('.rwmb-clone').find('input[type=text]').val(data.text);
            });
        }
        catch(e)
        {
            $(that).parents('.rwmb-clone').find('input[type=text]').val('Gram');
        }

    }


})();