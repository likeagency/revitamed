<?php
/**
 * The template for displaying all pages.
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */




get_header(); ?>
  <div id="primary" class="content-area mb container">
    <div class="col-12">
    <div class="wrapper">
      <h2><?php the_title(); ?></h2>
      <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
          <span property="itemListElement" typeof="ListItem">
              <a property="item" typeof="WebPage" title="Go to Revitadiet." href="<?= home_url('/') ?>" class="home"><span property="name">Revitadiet</span>
              </a><meta property="position" content="1">
          </span>
          &gt;
          <span property="itemListElement" typeof="ListItem">
              <a property="item" typeof="WebPage" href="<?= get_the_permalink(get_id_after_template_filename('page-tpl-feed-protocols.php')) ?>" class="home"><span property="name"><?= get_the_title(get_id_after_template_filename('page-tpl-feed-protocols.php')); ?></span>
              </a><meta property="position" content="2">
          </span>
          &gt;
          <span property="itemListElement" typeof="ListItem"><span property="name"><?php the_title(); ?></span><meta property="position" content="3"></span>
      </div>
    </div><!-- ./wrapper -->
  </div>
    <main id="main" class="site-main col-9" role="main">
      <div class="mainText">
          <?php the_content(); ?>


      </div>




    </main><!-- #main -->
    <aside id="secondary" class="col-3">
        <?php include 'template-parts/indywidualnaDiagnostykaForm.php';  ?>
    </aside>
  </div>

<?php get_footer();
