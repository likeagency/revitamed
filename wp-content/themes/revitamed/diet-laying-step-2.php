<?php
/**
 * The template for displaying all pages.
 * Template name: Układanie diety krok 2
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */

doc_page();
if ($_POST['form'] == 'step_1_diet') stepOneDietForm();
$userID = $_GET['userID'];
$currentDiet = getNewLastDiet($userID);
$dietID = $currentDiet->ID;

$days = getDietInfo($dietID);
get_header();

?>
  <div id="primary" class="content-area konto-tpl">
    <div class="container">
      <div class="col-12">
      <div class="wrapper">
        <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
          <?php if (function_exists('bcn_display')) {
              bcn_display();
          } ?>
        </div>
      </div><!-- ./wrapper -->
    </div>
    </div>
    <div class="container">
      <aside class="col-3 sidebar-konto">
        <div class="side-menu side-item">
            <?php include 'template-parts/menu-dieter.php'; ?>
            <?php include 'template-parts/user-info-dieter.php'; ?>
        </div><!-- ./sidemenu -->
      </aside>
        <main id="main" class="site-main account-p col-9" role="main">
        <div class="diet-top-box sticky-scroll-box sticky"><h3>Dieta</h3>
          <div class="days-buttons-box">
          <span class="all-btn" data-toggle="hide">Ukryj wszystkie</span>
          <span class="day-btn" data-day-show="0">Pn.</span>
          <span class="day-btn" data-day-show="1">Wt.</span>
          <span class="day-btn" data-day-show="2">Śr.</span>
          <span class="day-btn" data-day-show="3">Czw.</span>
          <span class="day-btn" data-day-show="4">Pt.</span>
          <span class="day-btn" data-day-show="5">Sob.</span>
          <span class="day-btn" data-day-show="6">Niedz.</span>
          <span class="btn-skladniki">Ukryj składniki</span>
          </div>
        </div>
        <style>
        .fixed {
          position:fixed;
          top:0;
          z-index:99999;
        }

        </style>
        <div class="dietPicker js-dietPicker"
             data-autocomplete="<?= esc_attr(json_encode(getAllRecipies())); ?>">
          <form class="dietPicker__form" method="post" action="<?= get_the_permalink(get_id_after_template_filename('diet-laying-step-3.php')) .'?userID='. $userID ?>" data-diet-form>


	          <?php displayDietPlanEdit($days);  ?>

            <!-- NAWIGACJA -->
            <div class="dietPicker__navigation" data-diet-navigation>
              <button class="btn dietPicker__link" data-diet-save>Zapisz dietę</button>
              <button class="btn dietPicker__button">Kontynuuj <i class="fa fa-angle-double-right"></i></button>
            </div>
            <input type="hidden" value="step_2_diet" name="form" >
            <input type="hidden" value="<?= $dietID; ?>" name="dietID">

          </form>

        </div>

      </main><!-- #main -->
    </div><!-- ./container -->
  </div><!-- #primary -->
<?php get_footer();
