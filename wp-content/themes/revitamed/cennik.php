<?php
/**
 * The template for displaying all pages.
 * Template name: Cennik
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */
$userRevitamed = new user();
get_header(); ?>
  <div id="primary" class="content-area">
    <div class="container">
      <div class="col-12">
      <div class="wrapper">
        <h2>Cennik</h2>
        <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
          <?php if (function_exists('bcn_display')) {
              bcn_display();
          } ?>
        </div>
      </div><!-- ./wrapper -->
    </div>
  </div>
    <div class="container">
      <main id="main" class="site-main col-12" role="main">

        <section class="priceList row">
					<div class="open-lightbox">
						<?php echo do_shortcode('[open-lightbox]'); ?>
					</div>
						
				
          <?php if($userRevitamed->isActive() && !$userRevitamed->hasActiveSub() && get_post_meta($userRevitamed->getId(),'sf_date_sub',true)!='') { ?>
          <div class="priceList__text col-12">Twój dostęp do serwisu zakończył się <strong><?= date('d.m.Y',get_post_meta($userRevitamed->getId(),'sf_date_sub',true) ) ?></strong></div>
          <?php } ?>
          <div class="priceList__box">
            <?php
              $lastCennik = getAllCennik()[1];
            $highest = get_post_meta($lastCennik->ID,'sf_days',true);
            foreach(getAllCennik() as $cennik){
              $rabat = formatNumberToPolish( formatNumberToPHP(get_post_meta($cennik->ID,'sf_price_previous',true)) - formatNumberToPHP(get_post_meta($cennik->ID,'sf_price_current',true)) ,0 );
              ?>
            <div class="priceList__item col-3">
              <div class="priceList-item">
                <?php if( get_post_meta($cennik->ID,'sf_days',true) == $highest) {?> <div class="priceList-item__headlight">NAJCZĘŚCIEJ WYBIERANY!</div>
                <?php } ?>
                <div class="priceList-item__headline"><?= get_the_title($cennik) ?></div>
                <div class="priceList-item__header">
                  <div class="priceList-item__old"><span><?= get_post_meta($cennik->ID,'sf_price_previous',true) ?></span> rabat <?= $rabat ?> zł</div>
                  <div class="priceList-item__cost"><?= get_post_meta($cennik->ID,'sf_price_current',true) ?> zł</div>
                  <div class="priceList-item__amount">DOSTĘP NA <?= strtoupper($cennik->post_title) ?></div>
                  <img class="priceList-item__deco"
                       src="<?php echo get_template_directory_uri(); ?>/components/priceList/priceList-item/src/element.png"
                       alt="">
                </div>
                <div class="priceList-item__content">
                  <div class="priceList-item__details">
                    <div class="priceList-item__action">
                      PRZEDŁUŻ ABONAMENT
                    </div>
                    <ul class="priceList-item__options">
                      <?php foreach(get_post_meta($cennik->ID,'sf_text',true) as $text) echo "<li>$text</li>" ?>
                    </ul>
                  </div>

                  <?php //TODO platnosci ?>
                  <form action="<?= home_url('/moje-konto/') ?>" method="post" class="priceList-item__navigation">
                    <?php foreach (createFormPayment() as $key => $item)
                      echo "<input name='$key' value='" . esc_attr($item) . "' type='hidden'>"; ?>
                    <button data-id="<?= $cennik->ID ?>" class="btn priceList-item__button js-buy-diet green-btn">KUP DIETĘ</button>
                  </form>
                </div>
              </div>
            </div>
  <?php } ?>
          <div class="col-3 cennik-right-box">
            <div class="cennik-right-col-box">
            <div class="cennik-right-col">
              <?php
            	if( have_rows('right_column') ):
            		while ( have_rows('right_column') ) : the_row();
            	?>

                <?php if( get_row_layout() == 'right_box_top' ){  ?>
            			<div class="righ-box-heading priceList-item__headline">
                    <?php echo get_sub_field("right_box_top_title"); ?>
                  </div>
            					<?php
                      while ( have_rows('right_box_top_repeater') ) : the_row();
                        echo "<div class='right-top-box'>";
                        echo "<div class='icon'>";
                      	echo "<img src='".get_sub_field("right_box_top_icon")['sizes']['etapy-ikona']."' alt='".get_sub_field("badania_blok25_ikona")['alt']."'>";
                        echo "</div>";
                        echo "<div class='text-right'>";
                        echo get_sub_field('right_box_top_repeater_title');
                        echo "</div>";
                        echo "<div class='btn-box'>";
                        foreach (get_sub_field("right_box_top_btns") as $boxBtns) {
                          if(!get_sub_field("link_w_nowym_oknie")){
                        		echo "<a class='button' href='".$boxBtns['right_box_top_btn_link']."' target='_blank'>";
                        		echo $boxBtns['right_box_top_btn_text'];
                        		echo "</a>";
                          }else{
                            echo "<a class='button' href='".$boxBtns['right_box_top_btn_link']."'>";
                        		echo $boxBtns['right_box_top_btn_text'];
                        		echo "</a>";
                          }
                    		}
                        echo "</div>";
                        echo "</div>";
                      endwhile;
                      ?>


            		<?php }?>

            	<?php
            	endwhile;
            	endif;
            	?>
              <div class="right-col-btm">
                <div class="righ-box-heading priceList-item__headline">
                  Wykup indywidualne <br>pytania do lekarza
                </div>
                <form id="js-payment-form-acc" class="payment__form js-payment-form-acc" method="post">

                  <div class="payment__content">

                    <?php $counter = 1;
                    foreach(getAllCennikDoc() as $cennik){ ?>
                        <div class="payment__inputbox">
                          <input class="payment__input" required id="payment-method-<?= $counter ?>" type="radio" name="id_2"  value="<?= $cennik->ID ?>"> <label
                            for="payment-method-<?= $counter++ ?>"><?= $cennik->post_title.'<br>' ?> (<?= get_post_meta($cennik->ID,'sf_price_current',true) ?> zł)&nbsp<span>(<?= get_post_meta($cennik->ID,'sf_price_previous',true) ?> zł)</span></label>
                        </div>
                    <?php } ?>
                  </div>
                  <div class="payment__navigation">
                      <button class="button-green payment__button">Wykup teraz</button>
                  </div>
                </form>
                <form class="js-actual-payment-form" action="<?= home_url('/moje-konto/') ?>" method="post" style="display:none">
                  <?php foreach (createFormPayment() as $key => $item)
                    echo "<input name='$key' value='" . esc_attr($item) . "' type='hidden'>"; ?>
                </form>
              </div>
            </div>
          </div>
          </div>
          </div>
        </section>




</div>













      </main><!-- #main -->
    </div><!-- ./container -->
  </div><!-- #primary -->
<?php get_footer();
