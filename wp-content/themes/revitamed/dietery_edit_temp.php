<?php
/**
 * The template for displaying all pages.
 * Template name: Panel edytowania szablonów
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */

$userDoc = new userDoc();
doc_page();
if($_POST['form']=='1')
{
	$title = $_POST['template_name'];
	$quantity = $_POST['quantity'];
	$id = createEmptyTemplate($title,$quantity);
  wp_redirect(get_the_permalink($id));
  exit();
}

get_header(); ?>
	<div id="primary" class="content-area konto-tpl">
		<div class="container">
			<div class="col-12">
				<div class="wrapper">
					<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
						<?php if (function_exists('bcn_display')) {
								bcn_display();
						} ?>
					</div>
				</div><!-- ./wrapper -->
			</div>
		</div>
		<div class="container">
			<aside class="col-3 sidebar-konto">
				<?php include 'template-parts/menu-dieter.php'; ?>
			</aside>
			<main id="main" class="site-main account-p col-9" role="main">
        <div class="dietTemplate__container">
      <?php $diets_templates = getDietsTemplates(); if(!empty($diets_templates)) { ?>
        <h3>Dostępne szablony:</h3>

	      <?php foreach ( $diets_templates as $diets_template ) {
		      printf( '<div class="dietTemplate__item"><strong>%s</strong> - <a class="edit-btn diet-item__link" href="%s">Edytuj</a>&nbsp; <span class="delete_item diet-item__link" data-id="%s">Usuń</span></div>', $diets_template->post_title, get_the_permalink( $diets_template ),$diets_template->ID );
	      }
      }
?>
          <br>
        <h3>Dodaj nowy szablon:</h3>
        <form action="#" method="post">
          <label for="template_name"><strong>Nazwa szablonu</strong></label>
          <input required id="template_name" name="template_name" type="text"><br><br>
          <label for="quantity"><strong>Liczba posiłków</strong></label>
          <input id="quantity" name="quantity" type="number" value="5" min="2" max="5" step="1">
          <input name="form" value="1" type="hidden"><br>

            <button type="submit">Stwórz</button>

        </form>
        </div>
			</main><!-- #main -->
		</div><!-- ./container -->
	</div><!-- #primary -->
<?php get_footer();
