<div id="owl-featured" class="owl-carousel owl-theme owl-products ">
  <?php while ( $sfs_loop->have_posts() ) : $sfs_loop->the_post(); global $product; ?>
    <div class="item slide">
      <a href="<?php the_permalink(); ?>">
        <?php if (has_post_thumbnail() ) {
          the_post_thumbnail('shop_thumbnail');
        }
        ?>
        <h4><?php the_title(); ?></h4>
        <div class="owl-price-box"><?php echo $product->get_price_html(); ?></div>
        <div class="add-to-cart-btn">Kup teraz</div>
      </a>
    </div>

  <?php endwhile; ?>

</div>