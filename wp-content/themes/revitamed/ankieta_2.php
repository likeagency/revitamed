<?php
/**
 * The template for displaying all pages.
 * Template name: ankieta 2
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */
user_page();
saveDataFromFormToSession();
get_header(); ?>
<div id="primary" class="content-area  container">
  <div class="col-12">
  <div class="wrapper">
    <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
      <?php if(function_exists('bcn_display'))
			{
				bcn_display();
			}?>
    </div>
  </div><!-- ./wrapper -->
  </div>
  <main id="main" class="site-main bli" role="main">
    <div class="cmsContainer">
    </div>
    <div class="steps">
      <div class="steps__content">
        <a href="<?= get_permalink_template('ankieta_1.php'); ?>" class="steps__item ">
          <div class="steps__counter">Krok 1</div>
          <div class="steps__name">Wywiad ogólny</div>
        </a>
        <a href="<?= get_permalink_template('ankieta_2.php'); ?>" class="steps__item -active">
          <div class="steps__counter">Krok 2</div>
          <div class="steps__name">Wywiad zdrowotny</div>
        </a>
        <a href="<?= get_permalink_template('ankieta_3.php'); ?>" class="steps__item">
          <div class="steps__counter">Krok 3</div>
          <div class="steps__name">Wywiad żywieniowy</div>
        </a>
        <a href="<?= get_permalink_template('ankieta_4.php'); ?>" class="steps__item">
          <div class="steps__counter">Krok 4</div>
          <div class="steps__name">Preferencje żywieniowe</div>
        </a>
        <div class="steps__item">
          <div class="steps__counter">Krok 5</div>
          <div class="steps__name">Podsumowanie</div>
        </div>
      </div>
    </div>
    <form class="form" method="post" action="<?= get_permalink_template('ankieta_3.php'); ?>" data-questionnaire-form>
      <div class="form__content">
        <div class="form__row -spaceBetween -marginBottom">
          <div class="form__col50">
            <div class="form__rowTitle">PRZEWÓD POKARMOWY</div>
              <div class="form__checkBoxHolder -width100">
                <input <?= in_array(1,$_SESSION['individualDiagnostic']['sf_1_problem_0']) ? 'checked' : '' ?>  data-required="problem_1_0" name="user_1_problem_0[]" value="1" id="checkbox_7" type="checkbox" class="form__checkbox" data-item>
                <label for="checkbox_7" class="form__label"> Bez dolegliwości</label>
              </div>

              <div class="form__checkBoxHolder -width100">
                <input <?= in_array(2,$_SESSION['individualDiagnostic']['sf_1_problem_0']) ? 'checked' : '' ?> data-required="problem_1_0" name="user_1_problem_0[]" value="2" id="checkbox_8" type="checkbox" class="form__checkbox" data-item>
                <label for="checkbox_8" class="form__label"> Zaparcia</label>
              </div>
            <div class="form__checkBoxHolder -width100">
                <input <?= in_array(3,$_SESSION['individualDiagnostic']['sf_1_problem_0']) ? 'checked' : '' ?> data-required="problem_1_0" name="user_1_problem_0[]" value="3" id="checkbox_8a" type="checkbox" class="form__checkbox" data-item>
                <label for="checkbox_8a" class="form__label"> Biegunki </label>
              </div>
            <div class="form__checkBoxHolder -width100">
                <input <?= in_array(4,$_SESSION['individualDiagnostic']['sf_1_problem_0']) ? 'checked' : '' ?> data-required="problem_1_0" name="user_1_problem_0[]" value="4" id="checkbox_8b" type="checkbox" class="form__checkbox" data-item>
                <label for="checkbox_8b" class="form__label"> Naprzemiennie, ale z przewagą zaparć</label>
              </div>
            <div class="form__checkBoxHolder -width100">
              <input <?= in_array(10,$_SESSION['individualDiagnostic']['sf_1_problem_0']) ? 'checked' : '' ?> data-required="problem_1_0" name="user_1_problem_0[]" value="4" id="checkbox_8c" type="checkbox" class="form__checkbox" data-item>
              <label for="checkbox_8c" class="form__label"> Naprzemiennie, ale z przewagą biegunek</label>
            </div>
            <div class="form__checkBoxHolder -width100">
              <input <?= in_array(5,$_SESSION['individualDiagnostic']['sf_1_problem_0']) ? 'checked' : '' ?> data-required="problem_1_0" name="user_1_problem_0[]" value="5" id="checkbox_13" type="checkbox" class="form__checkbox">
              <label for="checkbox_13" class="form__label"> Wzdęcia</label>
            </div>
            <div class="form__checkBoxHolder -width100">
              <input <?= in_array(6,$_SESSION['individualDiagnostic']['sf_1_problem_0']) ? 'checked' : '' ?> data-required="problem_1_0"  name="user_1_problem_0[]" value="6" id="checkbox_301" type="checkbox" class="form__checkbox">
              <label for="checkbox_301" class="form__label"> Zgaga</label>
            </div>
            <div class="form__checkBoxHolder -width100">
              <input <?= in_array(7,$_SESSION['individualDiagnostic']['sf_1_problem_0']) ? 'checked' : '' ?> data-required="problem_1_0"  name="user_1_problem_0[]" value="7" id="checkbox_14" type="checkbox" class="form__checkbox">
              <label for="checkbox_14" class="form__label">Refluks</label>
            </div>
            <div class="form__checkBoxHolder -width100">
              <input <?= in_array(8,$_SESSION['individualDiagnostic']['sf_1_problem_0']) ? 'checked' : '' ?> data-required="problem_1_0"  name="user_1_problem_0[]" value="8" id="checkbox_15" type="checkbox" class="form__checkbox">
              <label for="checkbox_15" class="form__label">Nadmierne gazy</label>
            </div>
            <div class="form__checkBoxHolder -width100">
              <input <?= in_array(9,$_SESSION['individualDiagnostic']['sf_1_problem_0']) ? 'checked' : '' ?> data-required="problem_1_0"  name="user_1_problem_0[]" value="9" id="checkbox_16z" type="checkbox" class="form__checkbox">
              <label for="checkbox_16z" class="form__label">Pieczenie odbytu lub świąd odbytu</label>
            </div>
          </div>
          <div class="form__col50">
            <div class="form__rowTitle">SKÓRA</div>
            <div class="form__checkBoxHolder -width100">
              <input <?= ($_SESSION['individualDiagnostic']['sf_2_problem_0']==="1") ? 'checked' : '' ?>   name="user_2_problem_0" value="1" id="checkbox_17" type="checkbox" class="form__checkbox">
              <label for="checkbox_17" class="form__label">Prawidłowa</label>
            </div>
            <div class="form__checkBoxHolder -width100">
              <input <?= ($_SESSION['individualDiagnostic']['sf_2_problem_1']==="1") ? 'checked' : '' ?> name="user_2_problem_1" value="1" id="checkbox_18" type="checkbox" class="form__checkbox">
              <label for="checkbox_18" class="form__label">Sucha</label>
            </div>
            <div class="form__checkBoxHolder -width100">
              <input <?= ($_SESSION['individualDiagnostic']['sf_2_problem_2']==="1") ? 'checked' : '' ?> name="user_2_problem_2" value="1" id="checkbox_19" type="checkbox" class="form__checkbox">
              <label for="checkbox_19" class="form__label">Nadmierne pocenie</label>
            </div>
            <div class="form__checkBoxHolder -width100">
              <input <?= ($_SESSION['individualDiagnostic']['sf_2_problem_3']==="1") ? 'checked' : '' ?> name="user_2_problem_3" value="1" id="checkbox_20" type="checkbox" class="form__checkbox">
              <label for="checkbox_20" class="form__label">Trądzik</label>
            </div>
            <div  class="form__checkBoxHolder -width100">
              <input <?= ($_SESSION['individualDiagnostic']['sf_2_problem_4']==="1") ? 'checked' : '' ?> name="user_2_problem_4" value="1" id="checkbox_21" type="checkbox" class="form__checkbox">
              <label for="checkbox_21" class="form__label">Świąd skóry</label>
            </div>
            <div class="form__checkBoxHolder -width100">
              <input <?= ($_SESSION['individualDiagnostic']['sf_2_problem_5']==="1") ? 'checked' : '' ?> name="user_2_problem_5" value="1" id="checkbox_22" type="checkbox" class="form__checkbox">
              <label for="checkbox_22" class="form__label">Pokrzywki, plamy, wypryski</label>
            </div>
            <div class="form__checkBoxHolder -width100">
              <input <?= ($_SESSION['individualDiagnostic']['sf_2_problem_6']==="1") ? 'checked' : '' ?> name="user_2_problem_6" value="1" id="checkbox_23" type="checkbox" class="form__checkbox">
              <label for="checkbox_23" class="form__label">Cellulit</label>
            </div>
            <div class="form__checkBoxHolder -width100" data-show>
              <input <?= ($_SESSION['individualDiagnostic']['sf_2_problem_7']==="1") ? 'checked' : '' ?> data-toggle name="user_2_problem_7" value="1" id="checkbox_23b" type="checkbox" class="form__checkbox">
              <label for="checkbox_23b" class="form__label">Inne</label>
              <textarea name="user_2_problem_7_w"
                        class="form__moreTextarea -hideOnNo"><?= $_SESSION['individualDiagnostic']['sf_2_problem_7_w'] ?></textarea>
            </div>
          </div>
        </div>
        <div class="form__row">
          <div class="form__col50">
            <div class="form__hr"></div>
            <div class="form__rowTitle">ENERGIA – uczucie zmęczenia</div>
            <div data-show>
              <div data-group="energy" style="display: contents;">
                <div class="form__checkBoxHolder -width100">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_3_problem_0']==="0") ? 'checked' : '' ?> data-required="problem_3_0" name="user_3_problem_0" value="0" id="checkbox_24" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_24"  class="form__label">Nie</label>
                </div>
                <div class="form__checkBoxHolder -width100">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_3_problem_0']==="1") ? 'checked' : '' ?> data-required="problem_3_0" name="user_3_problem_0"  value="1" id="checkbox_24b" type="checkbox" class="form__checkbox" data-item data-toggle>
                  <label for="checkbox_24b" class="form__label">Tak</label>
                </div>
              </div>
              <div data-group="energy_2" class="-hideOnNo subform" style="display: contents">
                <label class="form__label">Jakie?</label>
                <div class="form__checkBoxHolder -width100 -hideOnNo">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_3_problem_1']==="1") ? 'checked' : '' ?> name="user_3_problem_1" value="1" id="checkbox_24c" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_24c" class="form__label">Zmęczenie poranne (po nocy)</label>
                </div>
                <div class="form__checkBoxHolder -width100 -hideOnNo">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_3_problem_2']==="1") ? 'checked' : '' ?> name="user_3_problem_2" value="1" id="checkbox_24d" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_24d" class="form__label">Zmęczenie całodniowe</label>
                </div>
                <div class="form__checkBoxHolder -width100 -hideOnNo">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_3_problem_3']==="1") ? 'checked' : '' ?> name="user_3_problem_3" value="1" id="checkbox_24e" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_24e" class="form__label">Zmęczenie popołudniowe</label>
                </div>
              </div>
            </div>




            <div class="form__rowSubtitle">SEN - PRAWIDŁOWY</div>
            <div class="form__row" data-group="sleep" data-show>
              <div class="form__checkBoxHolder -width100">
                <input <?= ($_SESSION['individualDiagnostic']['sf_4_problem_0']==="0") ? 'checked' : '' ?> name="user_4_problem_0" value="0" id="checkbox_28" type="checkbox" class="form__checkbox" data-item>
                <label for="checkbox_28" class="form__label">Tak</label>
              </div>
              <div class="form__checkBoxHolder -width100">
                <input <?= ($_SESSION['individualDiagnostic']['sf_4_problem_0']==="1") ? 'checked' : '' ?> name="user_4_problem_0" value="1" id="checkbox_27" type="checkbox" class="form__checkbox" data-toggle data-item>
                <label for="checkbox_27"   class="form__label">Nie</label>
              </div>
              <div class="-hideOnNo subform" style="display: contents">
              <label class="form__label -hideOnNo ">Jakie problemy?</label>
              <div class="form__checkBoxHolder -width100 -hideOnNo">
                <input <?= in_array(1,$_SESSION['individualDiagnostic']['sf_4_problem_2']) ? 'checked' : '' ?> name="user_4_problem_2[]" value="1" id="checkbox_24_c" type="checkbox" class="form__checkbox">
                <label for="checkbox_24_c" class="form__label">trudności z zasypianiem </label>
              </div>
              <div class="form__checkBoxHolder -width100 -hideOnNo">
                <input <?= in_array(2,$_SESSION['individualDiagnostic']['sf_4_problem_2']) ? 'checked' : '' ?> name="user_4_problem_2[]" value="2" id="checkbox_24_d" type="checkbox" class="form__checkbox">
                <label for="checkbox_24_d" class="form__label">częste wybudzanie się w nocy</label>
              </div>
              <div class="form__checkBoxHolder -width100 -hideOnNo" data-show>
                <input <?= in_array(3,$_SESSION['individualDiagnostic']['sf_4_problem_2']) ? 'checked' : '' ?> name="user_4_problem_2[]" value="3" id="checkbox_24_e" type="checkbox" class="form__checkbox" data-toggle>
                <label for="checkbox_24_e" class="form__label">inne uwagi do snu np. poty nocne</label>
                <textarea name="user_4_problem_2_w"
                          class="form__moreTextarea -longInput -hideOnNo"><?= $_SESSION['individualDiagnostic']['sf_4_problem_2_w'] ?></textarea>
              </div>
            </div>
            </div>



          </div>
          <div class="form__col50" data-show>
            <div class="form__hr"></div>
            <div class="form__rowTitle">ALERGIE</div>
            <div class="form__row" data-group="allergy">
              <div class="form__checkBoxHolder -width100">
                <input <?= ($_SESSION['individualDiagnostic']['sf_5_problem_0']==="0") ? 'checked' : '' ?>  name="user_5_problem_0" value="0" id="checkbox_32" type="checkbox" class="form__checkbox" data-required="problem_5_0" data-item>
                <label for="checkbox_32" class="form__label">Nie</label>
              </div>
              <div class="form__checkBoxHolder -width100">
                <input <?= ($_SESSION['individualDiagnostic']['sf_5_problem_0']==="1") ? 'checked' : '' ?> name="user_5_problem_0" value="1" id="checkbox_31" type="checkbox" class="form__checkbox"  data-toggle data-required="problem_5_0" data-item>
                <label for="checkbox_31"  class="form__label">Tak</label>
              </div>

            </div>
            <div class="form__alergyType -hideOnNo subform" data-show>
              <div class="form__rowSubtitle special-subtitle">Typ alergii:</div>
              <div class="form__checkBoxHolder -width100">
                <input <?= ($_SESSION['individualDiagnostic']['sf_5_problem_1']==="1") ? 'checked' : '' ?> name="user_5_problem_1" value="1" id="checkbox_33" type="checkbox" class="form__checkbox">
                <label for="checkbox_33" class="form__label">Pokarmowe</label>
              </div>
              <div class="form__checkBoxHolder -width100">
                <input <?= ($_SESSION['individualDiagnostic']['sf_5_problem_2']==="1") ? 'checked' : '' ?> name="user_5_problem_2" value="1" id="checkbox_34" type="checkbox" class="form__checkbox">
                <label for="checkbox_34" class="form__label">Wziewne</label>
              </div>
              <div class="form__checkBoxHolder -width100">
                <input <?= ($_SESSION['individualDiagnostic']['sf_5_problem_3']==="1") ? 'checked' : '' ?> name="user_5_problem_3" value="1" id="checkbox_35" type="checkbox" class="form__checkbox">
                <label for="checkbox_35" class="form__label">Skórne</label>
              </div>
              <div class="form__checkBoxHolder -width100">
                <input <?= ($_SESSION['individualDiagnostic']['sf_5_problem_4']==="1") ? 'checked' : '' ?> name="user_5_problem_4" value="1" id="checkbox_36" type="checkbox" class="form__checkbox"  data-toggle>
                <label for="checkbox_36" class="form__label">Inne</label>
              </div>
              <div class="subform">
              <textarea name="user_5_problem_4_w"
                        class="form__moreTextarea -hideOnNo"><?= $_SESSION['individualDiagnostic']['sf_5_problem_4_w'] ?></textarea>
              </div>
            </div>
          </div>
          <div class="form__col50">
            <div class="form__hr"></div>
            <div class="form__rowTitle">ODPORNOŚĆ</div>
            <div  data-group>
              <div class="form__checkBoxHolder -width100">
                <input <?= ($_SESSION['individualDiagnostic']['sf_6_problem_0']==="1") ? 'checked' : '' ?> name="user_6_problem_0" value="1" id="checkbox_40" type="checkbox" class="form__checkbox" data-item>
                <label for="checkbox_40" class="form__label">Brak częstych infekcji</label>
              </div>
              <div class="form__checkBoxHolder -width100">
                <input <?= ($_SESSION['individualDiagnostic']['sf_6_problem_0']==="2") ? 'checked' : '' ?> name="user_6_problem_0" value="2" id="checkbox_41" type="checkbox" class="form__checkbox" data-item>
                <label for="checkbox_41" class="form__label">U dorosłych więcej niż 2 infekcje w roku </label>
              </div>
              <div class="form__checkBoxHolder -width100">
                <input <?= ($_SESSION['individualDiagnostic']['sf_6_problem_0']==="3") ? 'checked' : '' ?> name="user_6_problem_0" value="3" id="checkbox_42" type="checkbox" class="form__checkbox" data-item>
                <label for="checkbox_42" class="form__label">U dzieci więcej niż 4 infekcje w roku </label>
              </div>
            </div>
            <div data-show>
              <div data-group>
                <div class="form__rowTitle">CZY POJAWIAJĄ SIĘ U PANA/I CZĘSTE  INFEKCJE?</div>
                <div class="form__checkBoxHolder -width100">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_6_problem_0a']==="0") ? 'checked' : '' ?> name="user_6_problem_0a" value="0" id="checkbox_43" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_43" class="form__label">Nie</label>
                </div>
                <div class="form__checkBoxHolder -width100">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_6_problem_0a']==="1") ? 'checked' : '' ?> name="user_6_problem_0a" value="1" id="checkbox_43a" type="checkbox" class="form__checkbox" data-item data-toggle>
                  <label for="checkbox_43a" class="form__label">Tak</label>
                </div>
              </div>
              <div class="-hideOnNo subform">
              <div class="form__rowSubtitle special-subtitle">Jakie ?</div>
              <div class="form__checkBoxHolder -width100">
                <input <?= ($_SESSION['individualDiagnostic']['sf_6_problem_1']==="1") ? 'checked' : '' ?> name="user_6_problem_1" value="1" id="checkbox_43b" type="checkbox" class="form__checkbox">
                <label for="checkbox_43b" class="form__label">Zapalenia pęcherza</label>
              </div>
              <div class="form__checkBoxHolder -width100">
                <input <?= ($_SESSION['individualDiagnostic']['sf_6_problem_2']==="1") ? 'checked' : '' ?> name="user_6_problem_2" value="1" id="checkbox_44c" type="checkbox" class="form__checkbox">
                <label for="checkbox_44c" class="form__label">Zapalenia migdałków</label>
              </div>
              <div class="form__checkBoxHolder -width100">
                <input <?= ($_SESSION['individualDiagnostic']['sf_6_problem_3']==="1") ? 'checked' : '' ?> name="user_6_problem_3" value="1" id="checkbox_45d" type="checkbox" class="form__checkbox">
                <label for="checkbox_45d" class="form__label">Zapalenia uszu</label>
              </div>
              <div class="form__checkBoxHolder -width100">
                <input <?= ($_SESSION['individualDiagnostic']['sf_6_problem_4']==="1") ? 'checked' : '' ?> name="user_6_problem_4" value="1" id="checkbox_46e" type="checkbox" class="form__checkbox">
                <label for="checkbox_46e" class="form__label">Zapalenia górnych dróg oddechowych</label>
              </div>
              <div class="form__checkBoxHolder -width100">
                <input <?= ($_SESSION['individualDiagnostic']['sf_6_problem_5']==="1") ? 'checked' : '' ?> name="user_6_problem_5" value="1" id="checkbox_47f" type="checkbox" class="form__checkbox">
                <label for="checkbox_47f" class="form__label">Opryszczka</label>
              </div>
              <div class="form__checkBoxHolder -width100" data-show>
                <input <?= ($_SESSION['individualDiagnostic']['sf_6_problem_6']==="1") ? 'checked' : '' ?> name="user_6_problem_6" value="1" id="checkbox_48g" type="checkbox" class="form__checkbox" data-toggle>
                <label for="checkbox_48g" class="form__label">Inne</label>
                <textarea name="user_6_problem_6_w"
                          class="form__moreTextarea -longInput -hideOnNo"><?= esc_textarea( $_SESSION['individualDiagnostic']['sf_6_problem_6_w'] ) ?></textarea>
              </div>
              </div>
            </div>
            <div class="form__row" data-show>
              <div class="form__rowTitle">CZY BIERZESZ/BRAŁEŚ ANTYBIOTYKI</div>
              <div class="form__row -width100" data-group="antybiotyki">
                <div class="form__checkBoxHolder -width100">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_6_problem_7']==="0") ? 'checked' : '' ?> name="user_6_problem_7" value="0" id="checkbox_50" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_50" class="form__label">Nie</label>
                </div>
                <div class="form__checkBoxHolder -width100">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_6_problem_7']==="1") ? 'checked' : '' ?> name="user_6_problem_7" value="1" id="checkbox_49" type="checkbox" class="form__checkbox" data-item data-toggle>
                  <label for="checkbox_49" class="form__label">Tak</label>
                </div>
                  <div class="-hideOnNo subform -width100">
                    <label class="form__label -hideOnNo">Kiedy? Ile?</label>
                    <textarea name="user_6_problem_8" class="form__moreTextarea -hideOnNo -longInput"><?= esc_textarea( $_SESSION['individualDiagnostic']['sf_6_problem_8']) ?></textarea>
                  </div>
              </div>

            </div>




            <div data-show>
              <div data-group>
                <div class="form__rowTitle">OBRZĘKI?</div>
                <div class="form__checkBoxHolder -width100">
                  <input data-required="user_7_problem_1" <?= ($_SESSION['individualDiagnostic']['sf_7_problem_1']==="0") ? 'checked' : '' ?> name="user_7_problem_1" value="0" id="checkbox_54" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_54" class="form__label">Nie</label>
                </div>
                <div class="form__checkBoxHolder -width100">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_7_problem_1']==="1") ? 'checked' : '' ?> name="user_7_problem_1" data-required="user_7_problem_1"  value="1" id="checkbox_53" type="checkbox" class="form__checkbox" data-item data-toggle>
                  <label for="checkbox_53" class="form__label">Tak</label>
                </div>
              </div>
              <div class="-hideOnNo subform">
                <div class="form__rowSubtitle special-subtitle">Jakie ?</div>
                <div data-group>
                  <div class="form__checkBoxHolder -width100">
                    <input <?= ($_SESSION['individualDiagnostic']['sf_7_problem_1_s']==="1") ? 'checked' : '' ?> name="user_7_problem_1_s" value="1" id="checkbox_143b" type="checkbox" class="form__checkbox" data-item>
                    <label for="checkbox_143b" class="form__label">głównie nóg</label>
                  </div>
                  <div class="form__checkBoxHolder -width100">
                    <input <?= ($_SESSION['individualDiagnostic']['sf_7_problem_1_s']==="2") ? 'checked' : '' ?> name="user_7_problem_1_s" value="2" id="checkbox_144c" type="checkbox" class="form__checkbox" data-item>
                    <label for="checkbox_144c" class="form__label">głównie twarzy</label>
                  </div>
                  <div class="form__checkBoxHolder -width100">
                    <input <?= ($_SESSION['individualDiagnostic']['sf_7_problem_1_s']==="3") ? 'checked' : '' ?> name="user_7_problem_1_s" value="3" id="checkbox_145d" type="checkbox" class="form__checkbox" data-item>
                    <label for="checkbox_145d" class="form__label">całe ciało</label>
                  </div>
                  <div class="form__checkBoxHolder -width100">
                    <input <?= ($_SESSION['individualDiagnostic']['sf_7_problem_1_s']==="4") ? 'checked' : '' ?> name="user_7_problem_1_s" value="4" id="checkbox_146d" type="checkbox" class="form__checkbox" data-item>
                    <label for="checkbox_146d" class="form__label">nie umiem określić</label>
                  </div>
                </div>
                <div class="form__rowSubtitle special-subtitle">Kiedy ?</div>
                <div data-group>
                  <div class="form__checkBoxHolder -width100">
                    <input <?= ($_SESSION['individualDiagnostic']['sf_7_problem_1_w']==="1") ? 'checked' : '' ?> name="user_7_problem_1_w" value="1" id="checkbox_243b" type="checkbox" class="form__checkbox" data-item>
                    <label for="checkbox_243b" class="form__label">poranne</label>
                  </div>
                  <div class="form__checkBoxHolder -width100">
                    <input <?= ($_SESSION['individualDiagnostic']['sf_7_problem_1_w']==="2") ? 'checked' : '' ?> name="user_7_problem_1_w" value="2" id="checkbox_244c" type="checkbox" class="form__checkbox" data-item>
                    <label for="checkbox_244c" class="form__label">wieczorne</label>
                  </div>
                  <div class="form__checkBoxHolder -width100">
                    <input <?= ($_SESSION['individualDiagnostic']['sf_7_problem_1_w']==="3") ? 'checked' : '' ?> name="user_7_problem_1_w" value="3" id="checkbox_245d" type="checkbox" class="form__checkbox" data-item>
                    <label for="checkbox_245d" class="form__label">cały dzień</label>
                  </div>
                </div>
              </div>
            </div>

          </div>
          <div class="form__col50">
            <div class="form__hr"></div>
            <div class="form__rowTitle">STAWY</div>
            <div class="form__row">
              <div class="form__rowSubtitle -long">BÓLE</div>
              <div class="form__row" data-group="pain">
                <div class="form__checkBoxHolder">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_7_problem_0']==="0") ? 'checked' : '' ?> name="user_7_problem_0" value="0" id="checkbox_52" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_52" class="form__label">Nie</label>
                </div>
                <div class="form__checkBoxHolder">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_7_problem_0']==="1") ? 'checked' : '' ?> name="user_7_problem_0" value="1" id="checkbox_51" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_51" class="form__label">Tak</label>
                </div>

              </div>
            </div>
            <div class="form__row">
              <div class="form__rowSubtitle -long">ZWYRODNIENIA</div>
              <div class="form__row" data-group="spurs">
                <div class="form__checkBoxHolder">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_7_problem_2']==="0") ? 'checked' : '' ?> name="user_7_problem_2" value="0" id="checkbox_56" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_56" class="form__label">Nie</label>
                </div>
                <div class="form__checkBoxHolder">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_7_problem_2']==="1") ? 'checked' : '' ?> name="user_7_problem_2" value="1" id="checkbox_55" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_55" class="form__label">Tak</label>
                </div>

              </div>
            </div>
            <div class="form__row">
              <div class="form__rowSubtitle -long">OSTEOPOROZA</div>
              <div class="form__row" data-group="spurs">
                <div class="form__checkBoxHolder">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_7_problem_3']==="0") ? 'checked' : '' ?> name="user_7_problem_3" value="0" id="checkbox_56b" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_56b" class="form__label">Nie</label>
                </div>
                <div class="form__checkBoxHolder">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_7_problem_3']==="1") ? 'checked' : '' ?> name="user_7_problem_3" value="1" id="checkbox_55" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_55" class="form__label">Tak</label>
                </div>

              </div>
            </div>

          </div>
        </div>
        <div class="form__row">

          <div class="form__col50">
	          <?php if($_SESSION['individualDiagnostic']['sf_sex']==1){ ?>
            <div class="form__hr"></div>
            <div class="form__rowTitle">WYWIAD GINEKOLOGICZNY</div>
            <div class="form__row">
              <div class="form__rowSubtitle -long">MIESIĄCZKUJE</div>
              <div class="form__row" data-group="period">
                <div class="form__checkBoxHolder">
                  <input data-required="user_8_problem_0" <?= ($_SESSION['individualDiagnostic']['sf_8_problem_0']==="1") ? 'checked' : '' ?>  name="user_8_problem_0" value="1" id="checkbox_62" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_62" class="form__label">Tak</label>
                </div>
                <div class="form__checkBoxHolder">
                  <input data-required="user_8_problem_0" <?= ($_SESSION['individualDiagnostic']['sf_8_problem_0']==="0") ? 'checked' : '' ?> name="user_8_problem_0" value="0" id="checkbox_63" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_63" class="form__label">Nie</label>
                </div>
              </div>
            </div>
              <div class="form__row -periodRelated">
                <div class="form__rowSubtitle -long">MIESIĄCZKUJE REGULARNIE</div>
                <div class="form__row" data-group>
                  <div class="form__checkBoxHolder">
                    <input data-required="user_8_problem_5" <?= ($_SESSION['individualDiagnostic']['sf_8_problem_5']==="1") ? 'checked' : '' ?>   name="user_8_problem_5" value="1" id="checkbox_73" type="checkbox" class="form__checkbox" data-item>
                    <label for="checkbox_73" class="form__label">Tak</label>
                  </div>
                  <div class="form__checkBoxHolder">
                    <input data-required="user_8_problem_5" <?= ($_SESSION['individualDiagnostic']['sf_8_problem_5']==="0") ? 'checked' : '' ?> name="user_8_problem_5" value="0" id="checkbox_74" type="checkbox" class="form__checkbox" data-item>
                    <label for="checkbox_74" class="form__label">Nie</label>
                  </div>
                </div>
              </div>
            <div class="form__row -periodRelated">
              <div class="form__rowSubtitle -long">BOLESNE MIESIĄCZKI</div>
              <div class="form__row" data-group="painfulPeroid">
                <div class="form__checkBoxHolder">
                  <input data-required="user_8_problem_1" <?= ($_SESSION['individualDiagnostic']['sf_8_problem_1']==="1") ? 'checked' : '' ?>  name="user_8_problem_1" value="1" id="checkbox_64" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_64" class="form__label">Tak</label>
                </div>
                <div class="form__checkBoxHolder">
                  <input data-required="user_8_problem_1" <?= ($_SESSION['individualDiagnostic']['sf_8_problem_1']==="0") ? 'checked' : '' ?> name="user_8_problem_1" value="0" id="checkbox_65" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_65" class="form__label">Nie</label>
                </div>
              </div>
            </div>
            <div class="form__row">
              <div class="form__rowSubtitle -long">PORÓD</div>
              <div class="form__row" data-group="pregant">
                <div class="form__checkBoxHolder">
                  <input data-required="user_8_problem_2" <?= ($_SESSION['individualDiagnostic']['sf_8_problem_2']==="1") ? 'checked' : '' ?>   name="user_8_problem_2" value="1" id="checkbox_66" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_66" class="form__label">Tak</label>
                </div>
                <div class="form__checkBoxHolder">
                  <input data-required="user_8_problem_2" <?= ($_SESSION['individualDiagnostic']['sf_8_problem_2']==="0") ? 'checked' : '' ?> name="user_8_problem_2" value="0" id="checkbox_67" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_67" class="form__label">Nie</label>
                </div>
              </div>
            </div>
            <div class="form__row">
              <div class="form__rowSubtitle -long">PORONIENIA</div>
              <div class="form__row" data-group>
                <div class="form__checkBoxHolder">
                  <input data-required="user_8_problem_3" <?= ($_SESSION['individualDiagnostic']['sf_8_problem_3']==="1") ? 'checked' : '' ?>   name="user_8_problem_3" value="1" id="checkbox_68" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_68" class="form__label">Tak</label>
                </div>
                <div class="form__checkBoxHolder">
                  <input data-required="user_8_problem_3" <?= ($_SESSION['individualDiagnostic']['sf_8_problem_3']==="0") ? 'checked' : '' ?> name="user_8_problem_3" value="0" id="checkbox_69" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_69" class="form__label">Nie</label>
                </div>
              </div>
            </div>
            <div class="form__row -periodRelated">
              <div class="form__rowSubtitle -long">ZESPÓŁ NAPIĘCIA <br> PRZEDMIESIĄCZKOWEGO</div>
              <div class="form__row" data-group>
                <div class="form__checkBoxHolder">
                  <input data-required="user_8_problem_4" <?= ($_SESSION['individualDiagnostic']['sf_8_problem_4']==="1") ? 'checked' : '' ?>   name="user_8_problem_4" value="1" id="checkbox_71" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_71" class="form__label">Tak</label>
                </div>
                <div class="form__checkBoxHolder">
                  <input data-required="user_8_problem_4" <?= ($_SESSION['individualDiagnostic']['sf_8_problem_4']==="0") ? 'checked' : '' ?> name="user_8_problem_4" value="0" id="checkbox_72" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_72" class="form__label">Nie</label>
                </div>
              </div>
            </div>

            <div class="form__row">
              <div class="form__rowSubtitle -long">ANTYKONCEPCJA HORMONALNA</div>
              <div class="form__row" data-group>
                <div class="form__checkBoxHolder">
                  <input data-required="user_8_problem_6" <?= ($_SESSION['individualDiagnostic']['sf_8_problem_6']==="1") ? 'checked' : '' ?> name="user_8_problem_6" value="1"  id="checkbox_75" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_75" class="form__label">Tak</label>
                </div>
                <div class="form__checkBoxHolder">
                  <input data-required="user_8_problem_6" <?= ($_SESSION['individualDiagnostic']['sf_8_problem_6']==="0") ? 'checked' : '' ?> name="user_8_problem_6" value="0" id="checkbox_76" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_76" class="form__label">Nie</label>
                </div>
              </div>
            </div>
            <div class="form__row">
              <div class="form__rowSubtitle -long">HORMONALNE LECZENIE</div>
              <div class="form__row" data-group>
                <div class="form__checkBoxHolder">
                  <input data-required="user_8_problem_7" <?= ($_SESSION['individualDiagnostic']['sf_8_problem_7']==="1") ? 'checked' : '' ?> name="user_8_problem_7" value="1" id="checkbox_78" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_78" class="form__label">Tak</label>
                </div>
                <div class="form__checkBoxHolder">
                  <input data-required="user_8_problem_7" <?= ($_SESSION['individualDiagnostic']['sf_8_problem_7']==="0") ? 'checked' : '' ?> name="user_8_problem_7" value="0" id="checkbox_79" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_79" class="form__label">Nie</label>
                </div>
              </div>
            </div>
            <div class="form__row">
              <div class="form__rowSubtitle -long">MIĘŚNIAKI / TORBIELE</div>
              <div class="form__row" data-group>
                <div class="form__checkBoxHolder">
                  <input data-required="user_8_problem_8" <?= ($_SESSION['individualDiagnostic']['sf_8_problem_8']==="1") ? 'checked' : '' ?> name="user_8_problem_8" value="1" id="checkbox_80" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_80" class="form__label">Tak</label>
                </div>
                <div class="form__checkBoxHolder">
                  <input data-required="user_8_problem_8" <?= ($_SESSION['individualDiagnostic']['sf_8_problem_8']==="0") ? 'checked' : '' ?> name="user_8_problem_8" value="0" id="checkbox_81" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_81" class="form__label">Nie</label>
                </div>
              </div>
            </div>
	          <?php } ?>
            <div class="form__hr"></div>
            <div class="form__rowTitle">DIETY STOSOWANE W PRZESZŁOŚCI</div>

            <div class="form__row" data-show>
              <div class="form__row -width100"  data-group>
                <div class="form__checkBoxHolder -width100">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_12_problem_0']==="0") ? 'checked' : '' ?> data-required="user_12_problem_0" name="user_12_problem_0" value="0" id="checkbox_96" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_96" class="form__label">Nie</label>
                </div>
                <div class="form__checkBoxHolder -width100">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_12_problem_0']==="1") ? 'checked' : '' ?> data-required="user_12_problem_0" name="user_12_problem_0" value="1" id="checkbox_95" type="checkbox" class="form__checkbox" data-item data-toggle>
                  <label for="checkbox_95" class="form__label">Tak</label>
                </div>
                <div class="-hideOnNo subform -width100">
                <div class="form__label -hideOnNo">Kiedy? Jaki efekt? Jakie samopoczucie?</div>
                <textarea name="user_12_problem_1" class="form__moreTextarea -longInput -hideOnNo"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_12_problem_1']) ?></textarea>
              </div>
              </div>
            </div>

            <div class="form__row" data-show>
              <div class="form__rowTitle">CZY JESTEŚ LUB BYŁEŚ PACJENTEM REVITADIET? <br>(DR ARENDARCZYK, DR SOBKOWIAK LUB DR KRYNICKA)</div>
              <div class="form__row -width100"  data-group>
                <div class="form__checkBoxHolder -width100">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_12_problem_3']==="0") ? 'checked' : '' ?> data-required="user_12_problem_3" name="user_12_problem_3" value="0" id="checkbox_100" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_100" class="form__label">Nie</label>
                </div>
                <div class="form__checkBoxHolder -width100">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_12_problem_3']==="1") ? 'checked' : '' ?> data-required="user_12_problem_3" name="user_12_problem_3" value="1" id="checkbox_99" type="checkbox" class="form__checkbox" data-item data-toggle>
                  <label for="checkbox_99" class="form__label">Tak</label>
                </div>

                <div class="-hideOnNo subform -width100">
                  <label class="form__label  -hideOnNo">Kiedy? Którego lekarza</label>
                  <textarea name="user_12_problem_3_w"
                          class="form__moreTextarea -hideOnNo -longInput"><?= esc_textarea( $_SESSION['individualDiagnostic']['sf_12_problem_3_w'] ) ?></textarea>
                </div>
              </div>
            </div>
          </div>
          <div class="form__col50">
            <div class="form__hr"></div>
            <div class="form__rowTitle">SKURCZE</div>
            <div class="form__row">
              <div class="form__rowSubtitle -long">SKURCZE ŁYDEK</div>
              <div class="form__row" data-group>
                <div class="form__checkBoxHolder">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_9_problem_1']==="0") ? 'checked' : '' ?>  name="user_9_problem_1" value="0" id="checkbox_85" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_85" class="form__label">Nie</label>
                </div>
                <div class="form__checkBoxHolder">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_9_problem_1']==="1") ? 'checked' : '' ?>  name="user_9_problem_1" value="1" id="checkbox_84" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_84" class="form__label">Tak</label>
                </div>

              </div>
            </div>
            <div class="form__row">
              <div class="form__rowSubtitle -long">SKURCZE POWIEK</div>
              <div class="form__row" data-group>
                <div class="form__checkBoxHolder">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_9_problem_2']==="0") ? 'checked' : '' ?>  name="user_9_problem_2" value="0" id="checkbox_87" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_87" class="form__label">Nie</label>
                </div>
                <div class="form__checkBoxHolder">
                  <input <?= ($_SESSION['individualDiagnostic']['sf_9_problem_2']==="1") ? 'checked' : '' ?>  name="user_9_problem_2" value="1" id="checkbox_86" type="checkbox" class="form__checkbox" data-item>
                  <label for="checkbox_86" class="form__label">Tak</label>
                </div>

              </div>
            </div>
            <div class="form__row" data-show>
              <div class="form__rowSubtitle -long">SKURCZE INNE</div>
                <textarea name="user_9_problem_3"
                          class="form__moreTextarea -hideOnNo"><?= esc_textarea( $_SESSION['individualDiagnostic']['sf_9_problem_3'] ) ?></textarea>
            </div>

            <div class="form__hr"></div>
            <div class="form__rowTitle">WYWIAD RODZINNY</div>
            <div data-show>
            <div class="form__checkBoxHolder -width100">
              <input <?= (in_array(1,$_SESSION['individualDiagnostic']['sf_10_problem_0'])) ? 'checked' : '' ?> name="user_10_problem_0[]" value="1" id="checkbox_88" type="checkbox" class="form__checkbox">
              <label for="checkbox_88" class="form__label">Choroby tarczycy</label>
            </div>
            <div class="form__checkBoxHolder -width100">
              <input <?= (in_array(2,$_SESSION['individualDiagnostic']['sf_10_problem_0'])) ? 'checked' : '' ?> name="user_10_problem_0[]" value="2" id="checkbox_89" type="checkbox" class="form__checkbox">
              <label for="checkbox_89" class="form__label">Nadciśnienie</label>
            </div>
            <div class="form__checkBoxHolder -width100">
              <input <?= (in_array(3,$_SESSION['individualDiagnostic']['sf_10_problem_0'])) ? 'checked' : '' ?> name="user_10_problem_0[]" value="3" id="checkbox_90" type="checkbox" class="form__checkbox">
              <label for="checkbox_90" class="form__label">Astma</label>
            </div>
            <div class="form__checkBoxHolder -width100">
              <input <?= (in_array(4,$_SESSION['individualDiagnostic']['sf_10_problem_0'])) ? 'checked' : '' ?> name="user_10_problem_0[]" value="4" id="checkbox_91" type="checkbox" class="form__checkbox">
              <label for="checkbox_91" class="form__label">Cukrzyca</label>
            </div>
            <div class="form__checkBoxHolder -width100">
              <input <?= (in_array(5,$_SESSION['individualDiagnostic']['sf_10_problem_0'])) ? 'checked' : '' ?> name="user_10_problem_0[]" value="5" id="checkbox_92" type="checkbox" class="form__checkbox">
              <label for="checkbox_92" class="form__label">Zawały</label>
            </div>
            <div class="form__checkBoxHolder -width100">
              <input <?= (in_array(6,$_SESSION['individualDiagnostic']['sf_10_problem_0'])) ? 'checked' : '' ?> name="user_10_problem_0[]" value="6" id="checkbox_93" type="checkbox" class="form__checkbox">
              <label for="checkbox_93" class="form__label">Udary</label>
            </div>
            <div class="form__checkBoxHolder -width100">
              <input <?= (in_array(7,$_SESSION['individualDiagnostic']['sf_10_problem_0'])) ? 'checked' : '' ?> name="user_10_problem_0[]" value="7" id="checkbox_94" type="checkbox" class="form__checkbox">
              <label for="checkbox_94" class="form__label">Nowotwory</label>
            </div>
            <div class="form__checkBoxHolder -width100">
              <input <?= (in_array(8,$_SESSION['individualDiagnostic']['sf_10_problem_0'])) ? 'checked' : '' ?> name="user_10_problem_0[]" value="8" id="checkbox_94z" type="checkbox" class="form__checkbox" data-toggle>
              <label for="checkbox_94z" class="form__label">Inne</label>
            </div>
            <div class="-hideOnNo subform -width100">
              <label class="form__label -hideOnNo">Jakie?</label>
              <textarea name="user_10_problem_1" class="form__moreTextarea -hideOnNo -longInput"><?= esc_textarea($_SESSION['individualDiagnostic']['sf_10_problem_1']) ?></textarea>
            </div>
            </div>
          </div>
        </div>

        <div class="form__nav" data-questionnaire-navigation>
          <button class="btn form__saveDiet" data-questionnaire-save>Zapisz ankietę</button>
          <button onclick="window.onbeforeunload = null;" class="form__button">PRZEJDŹ DALEJ</button>
          <input type="hidden" value="ankieta_2" name="action">
          <input type="hidden" name="step" value="ankieta_2">
        </div>
      </div>
    </form>
  </main><!-- #main -->
</div><!-- #primary -->
  <script>
      var inputs = document.querySelectorAll('input, select, textarea');
      for(var i = 0; inputs && i < inputs.length; i++)
          inputs[i].addEventListener('change',function () {
              window.onbeforeunload = function () {
                  return true;
              };
          });
  </script>
<?php get_footer();
