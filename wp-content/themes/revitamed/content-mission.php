<?php
$image_object = get_field('misja_top_image');
$image_size = 'misja_top_image';
$image_url = $image_object['sizes'][$image_size];
$alt = $image_object['alt'];
$caption = $image_object['caption'];

?>
<div class="misja-top" style="background: url(<?php echo $image_url; ?>) no-repeat center center / cover">
	<div class="misja-top-overlay">
		<div class="container clear">
			<div class="col-12">
				<?php
				echo "<h1>";
				echo get_field("misja_top_title");
				echo "</h1>";
				echo "<h2>";
				echo get_field("misja_top_subtitle");
				echo "</h2>";
				echo get_field("misja_top_text");
				?>
			</div>
		</div>
	</div>
</div>


<div class="mb container">
<?php

if( have_rows('misja_content') ):
	while ( have_rows('misja_content') ) : the_row();
?>
<?php if( get_row_layout() == 'misja_block_image' ){  ?>
		<?php
			if( have_rows('misja_photo_repeater') ):
				while ( have_rows('misja_photo_repeater') ) : the_row();
					if(get_sub_field("misja_pozycja_obrazka")==1){

						echo "<div class='container-img clear'>";
						echo "<div class='col-6 pr'><img src='".get_sub_field("misja_repeater_image")['sizes']['misja_repeater_image']."' alt='".get_sub_field("misja_repeater_image")['alt']."'></div>";
						echo "<div class='col-6'>";
						echo "<div class='text-box'>";
						echo "<h6>";
						echo get_sub_field("misja_repeater_title");
						echo "</h6>";
						echo "<h5>";
						echo get_sub_field("misja_repeater_subtitle");
						echo "</h5>";
						echo get_sub_field("misja_repeater_text");
						echo "</div>";
						echo "</div>";
						echo "</div>";

					}else{
						echo "<div class='container-img clear'>";
						echo "<div class='col-6 pl fr'><img src='".get_sub_field("misja_repeater_image")['sizes']['misja_repeater_image']."' alt='".get_sub_field("misja_repeater_image")['alt']."'></div>";
						echo "<div class='col-6 fl'>";
						echo "<div class='text-box'>";
						echo "<h6>";
						echo get_sub_field("misja_repeater_title");
						echo "</h6>";
						echo "<h5>";
						echo get_sub_field("misja_repeater_subtitle");
						echo "</h5>";
						echo get_sub_field("misja_repeater_text");
						echo "</div>";
						echo "</div>";
						echo "</div>";
					}

				endwhile;
			endif;
		?>
<?php }?>
<?php if( get_row_layout() == 'misja_block_text' ){  ?>
	<div class="bc-container container-50 clear">
			<?php echo get_sub_field("misja_blok_50"); ?>
	</div>
<?php }?>

<?php
endwhile;
endif;
?>
</div>


<div class="bg-container">
<?php

if( have_rows('misja_content') ):
	while ( have_rows('misja_content') ) : the_row();
?>

<?php if( get_row_layout() == 'misja_block_text_image' ){  ?>

<div class="mb container clear">
		<div class='col-6'>
		<?php echo	"<img src='".get_sub_field("misja_blok_100_img")['sizes']['misja_image']."' alt='".get_sub_field("misja_blok_100_img")['alt']."'>";?>
		</div>
		<div class="col-6 pl">
			<?php
			echo "<h5>";
			echo get_sub_field("misja_blok_100_title");
			echo "</h5>";
			echo get_sub_field("misja_blok_100_text");
			echo "<a class='button' href='".get_sub_field('misja_blok_100_link')."'>";
			echo get_sub_field('misja_blok_100_link_text');
			echo "</a>";
			?>
	</div>
	</div>
<?php }?>

<?php
endwhile;
endif;
?>
</div>
<div class="container clear etapy-top-title">
	<div class="col-12">
		<h6><?php echo get_field("etapy_top_title"); ?></h6>
	</div>
</div>

<div class="container container-etap clear">
	<div class="col-12">
	<?php
	if( have_rows('etapy') ):
		while ( have_rows('etapy') ) : the_row();
	?>
<?php if( get_row_layout() == 'etap_box' ){  ?>
	<?php
		echo "<div class='etap-box'>";
			echo "<div class='etap-box-top'>";
				echo get_sub_field("etap_top_text");
				echo "<img src='".get_sub_field("etap_top_ikona")['sizes']['etapy-ikona']."' alt='".get_sub_field("etap_top_ikona")['alt']."'>";
			echo "</div>";
			echo "<div class='etap-box-middle'>";
				echo get_sub_field("etap_opis");
				echo "<div class='etap-btn-box'>";
					echo "<a class='button' href='".get_sub_field('etap_link')."'>";
					echo get_sub_field('etap_link_text');
					echo "</a>";
				echo "</div>";
			echo "</div>";
			echo "<div class='etap-box-btm'>";
				echo get_sub_field('etap_koszyk');
			echo "</div>";
		echo "</div>";
	?>
<?php }?>
	<?php
	endwhile;
	endif;
	?>
</div>
</div>



	<?php
	if( have_rows('misja_btm') ):
		while ( have_rows('misja_btm') ) : the_row();
	?>
<?php if( get_row_layout() == 'misja_btm_text_image_left' ){  ?>

		<?php
		$image_object = get_sub_field('misja_btm_image');
		$image_size = 'misja_btm_bg';
		$image_url = $image_object['sizes'][$image_size];
		$alt = $image_object['alt'];
		$caption = $image_object['caption'];

		?>
	<div class="container-misja-btm" style="background: url(<?php echo $image_url; ?>) no-repeat ">
		<div class="container clear">
			<div class="right">
			<?php
				echo "<h6>";
				echo get_sub_field("misja_btm_title");
				echo "</h6>";
				echo "<h5>";
				echo get_sub_field("misja_btm_subtitle");
				echo "</h5>";
			echo "<div class='btns-box'>";
				foreach (get_sub_field("misja_btm_btns") as $przycisk) {
				echo "<a class='button' href='".$przycisk['misja_btm_btn_link']."'>";
				echo $przycisk['misja_btm_btn_text'];
				echo "</a>";
			}
			echo "</div>";
			?>
		</div>
		</div>
	</div>
<?php }?>
<?php if( get_row_layout() == 'misja_btm_text_image' ){  ?>
	<div class="container container-img2 clear">
		<?php
			echo "<div class='col-6'>";
				echo "<h6>";
				echo get_sub_field("misja_btm_title");
				echo "</h6>";
				echo "<h5>";
				echo get_sub_field("misja_btm_subtitle");
				echo "</h5>";
				echo get_sub_field("misja_btm_text");
					echo "<div class='btns-box clear'>";
						foreach (get_sub_field("misja_btm_btns") as $przycisk) {
						echo "<a class='button' href='".$przycisk['misja_btm_btn_link']."'>";
						echo $przycisk['misja_btm_btn_text'];
						echo "</a>";
					}
					echo "</div>";

			echo "</div>";

			echo "<div class='col-6 mobile-hidden'>";
				 echo	"<img src='".get_sub_field("misja_btm_image")['sizes']['misja_image']."' alt='".get_sub_field("misja_btm_image")['alt']."'>";
			echo "</div>";
		?>
	</div>
<?php }?>

<?php if( get_row_layout() == 'misja_btm_text_bg' ){  ?>
	<?php
	$image_object = get_sub_field('misja_btm_text_image');
	$image_size = 'misja_btm_bg2';
	$image_url = $image_object['sizes'][$image_size];
	$alt = $image_object['alt'];
	$caption = $image_object['caption'];

	?>
	<div class="container-bg-btm clear" style="background: url(<?php echo $image_url; ?>) no-repeat center center / cover">
	<div class="container">
		<?php
			echo "<div class='col-8'>";
				echo get_sub_field("misja_btm_text_text");

					echo "<div class='btns-box'>";
						foreach (get_sub_field("misja_btm_text_btns") as $przycisk) {
						echo "<a class='button' href='".$przycisk['misja_btm_btn_link']."'>";
						echo $przycisk['misja_btm_btn_text'];
						echo "</a>";
					}
					echo "</div>";

			echo "</div>";

			echo "<div class='col-4'>";
				 echo	"<img src='".get_sub_field("misja_btm_text_img_right")['sizes']['misja_btm_img']."' alt='".get_sub_field("misja_btm_text_img_right")['alt']."'>";
			echo "</div>";
		?>
	</div>
</div>
<?php }?>

	<?php
	endwhile;
	endif;
	?>
