<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */
global $wp_query;
$wp_query->query['posts_per_page'] = 16;
$wp_query = new WP_Query($wp_query->query);
remove_theme_support('title-tag');
add_action('wp_head', function () {
    echo "<title>Nasi lekarze - Revitadiet</title>";
}, 0);
get_header(); ?>

  <div id="primary" class="content-area mb container content-fix">
    <div class="col-12">
    <div class="wrapper">
      <h2>Nasi lekarze</h2>
  		<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
        <?php if (function_exists('bcn_display')) {
            bcn_display();
        } ?>
  		</div>
    </div><!-- ./wrapper -->
  </div>
    <main id="main" class="site-main col-12" role="main">
      <?php


      $the_query_map = new WP_Query( array(
        'post_type' => 'lekarz',
        'posts_per_page' => -1
        ) );
      if($the_query_map->have_posts()) :
        $integer=0;
        while($the_query_map->have_posts()):
          $the_query_map->the_post();
        ?>
        <?php if (++$integer % 2 != 0) { ?>
        <div id="lekarz-<?php the_ID(); ?>" class="lekarz-row">
          <div class="lekarz-text text-left">
            <h5><?php the_title(); ?></h5>
            <div class="desc1">
              <?php the_content(); ?>
            </div>
          </div>
          <div class="lekarz-image image-right">
            <?php if ( has_post_thumbnail() ) : ?>
              <?php the_post_thumbnail('home-medic'); ?>
            <?php endif; ?>
          </div><!-- ./thumb -->
        </div>
      <?php } else { ?>
        <div id="lekarz-<?php the_ID(); ?>" class="lekarz-row">
          <div class="lekarz-image image-left">
            <?php if ( has_post_thumbnail() ) : ?>
              <?php the_post_thumbnail('home-medic'); ?>
            <?php endif; ?>
          </div><!-- ./thumb -->
          <div class="lekarz-text text-right">
            <h5><?php the_title(); ?></h5>
            <div class="desc1">
              <?php the_content(); ?>
            </div>
          </div>

        </div>
      <?php } ?>
        <?php
        endwhile; endif;
        wp_reset_postdata();
        ?>
    </main><!-- #main -->


  </div><!-- #primary -->

<?php
get_footer();
