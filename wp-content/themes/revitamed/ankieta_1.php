<?php
/**
 * The template for displaying all pages.
 * Template name: ankieta 1
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */
user_page();
saveDataFromFormToSession();
$userRevita = new user;
$titan = TitanFramework::getInstance('revita');
get_header(); ?>
	<div id="primary" class="content-area  container">
		<div class="col-12">
			<div class="wrapper">
				<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
					<?php if ( function_exists( 'bcn_display' ) ) {
						bcn_display();
					} ?>
				</div>
			</div><!-- ./wrapper -->
		</div>
		<main id="main" class="site-main bli survey-1 col-12" role="main">
			<div class="steps">
				<div class="steps__content">
					<a href="<?= get_permalink_template( 'ankieta_1.php' ); ?>" class="steps__item -active">
						<div class="steps__counter">Krok 1</div>
						<div class="steps__name">Wywiad ogólny</div>
					</a>
					<a href="<?= get_permalink_template( 'ankieta_2.php' ); ?>" class="steps__item">
						<div class="steps__counter">Krok 2</div>
						<div class="steps__name">Wywiad zdrowotny</div>
					</a>
					<a href="<?= get_permalink_template( 'ankieta_3.php' ); ?>" class="steps__item">
						<div class="steps__counter">Krok 3</div>
						<div class="steps__name">Wywiad żywieniowy</div>
					</a>
					<a href="<?= get_permalink_template( 'ankieta_4.php' ); ?>" class="steps__item">
						<div class="steps__counter">Krok 4</div>
						<div class="steps__name">Preferencje żywieniowe</div>
					</a>
					<div class="steps__item">
						<div class="steps__counter">Krok 5</div>
						<div class="steps__name">Podsumowanie</div>
					</div>
				</div>
			</div>
			<form class="form" method="post" action="<?= get_permalink_template( 'ankieta_2.php' ); ?>"
			      data-questionnaire-form>
				<div class="form__row js-file-research"  data-show>
					<?php
					$badaniaBeta = getFilesFromSection( $userRevita->getId(), 'research-beta' );
					$badaniafdt = getFilesFromSection( $userRevita->getId(), 'research-fdt' ); ?>
					<div class="form__rowTitle">Czy wykupił/a Pan/i przez portal badania z krwi:  Food Test i/lub BETA </div>
					<div class="form__row" data-group>
						<div class="form__checkBoxHolder">
							<input name="sf_files"
							       value="1" <?=  !empty($badaniaBeta) || !empty($badaniafdt)  ? 'checked' : '' ?>
							       data-required="sf_files" id="checkbox_13dd" type="checkbox" data-toggle class="form__checkbox" data-item>
							<label for="checkbox_13dd" class="form__label"> TAK</label>
						</div>
						<div class="form__checkBoxHolder">
							<input name="sf_files"
							       value="0" <?=  empty($badaniaBeta) && empty($badaniafdt)   ? 'checked' : '' ?>
							       data-required="sf_files" id="checkbox_14dd" type="checkbox" class="form__checkbox" data-item>
							<label for="checkbox_14dd" class="form__label"> NIE</label>
						</div>

					</div>
					<div class="form__rowTitle -hideOnNo">BADANIE Food Test</div>
					<div class="fileUploader -hideOnNo">
						<p class="fileUploader__text">Jeśli wykonałeś badanie Food Test - wgraj jego wyniki tutaj, jeśli chcesz je
							wykonać - zakup zestaw w naszym <a href="#">sklepie</a>. Dopuszczalne rozszerzenia: jpg, png, pdf, doc, odt. Maksymalny rozmiar pojedyńczego pliku to 2mb.</p>
						<div class="fileUploader__inputbox">
							<input class="fileUploader__file" id="file-research-fdt" type="file" data-section="research-fdt" accept="image/jpg, image/jpeg,image/gif,image/png,	application/pdf,text/plain, application/msword,	application/vnd.oasis.opendocument.text"/>
							<label class="btn fileUploader__upload" for="file-research-fdt"><span>Dodaj badanie</span><i
									class="fa fa-circle-o-notch fa-spin fa-fw"></i></label>
						</div>
						<div class="fileUploader__filebox" data-section="research-fdt">
							<?php foreach(getFilesFromSection($userRevita->getId(),'research-fdt') as $image){ ?>
								<div class="fileUploader__uploaded">
									<div class="fileUploader__uploadedIcon">
										<i class="fa fa-file-text-o"></i>
									</div>
									<div class="fileUploader__uploadedName"><?= $image->post_title ?></div>
									<div class="fileUploader__uploadedDelete" data-delete="<?= $image->ID ?>"><i class="fa fa-trash-o"></i></div>
								</div>
							<?php } ?>
						</div>
					</div>

					<div class="form__rowTitle -hideOnNo">WYNIKI BADANIA BETA</div>
					<div class="fileUploader -hideOnNo">
						<p class="fileUploader__text">Jeśli wykonałeś badanie BETA - wgraj tu jego wyniki. Jeśli chcesz je wykonać
							w jednym z naszych gabinetów we Wrocławiu lub Warszawie - zakup je w naszym sklepie. Dopuszczalne rozszerzenia: jpg, png, pdf, doc, odt. Maksymalny rozmiar pojedyńczego pliku to 2mb.</p>
						<div class="fileUploader__inputbox">
							<input class="fileUploader__file" id="file-research-beta" type="file" data-section="research-beta" accept="image/jpg, image/jpeg,image/gif,image/png,	application/pdf,text/plain, application/msword,	application/vnd.oasis.opendocument.text" />
							<label class="btn fileUploader__upload" for="file-research-beta"><span>Dodaj badanie</span><i
									class="fa fa-circle-o-notch fa-spin fa-fw"></i></label>
						</div>
						<div class="fileUploader__filebox" data-section="research-beta">
							<?php foreach(getFilesFromSection($userRevita->getId(),'research-beta') as $image){ ?>
								<div class="fileUploader__uploaded">
									<div class="fileUploader__uploadedIcon">
										<i class="fa fa-file-text-o"></i>
									</div>
									<div class="fileUploader__uploadedName"><?= $image->post_title ?></div>
									<div class="fileUploader__uploadedDelete" data-delete="<?= $image->ID ?>"><i class="fa fa-trash-o"></i></div>
								</div>
							<?php } ?>
						</div>

					</div>
				</div>

				<div class="form__content">
					<div class="form__start">
						<div class="form__rowTitle">PŁEĆ</div>
						<div class="form__row -marginBottom" data-group="purpose">
							<div class="form__checkBoxHolder">
								<input name="user_sex" data-required="user_sex"
								       value="2"  <?= ( $_SESSION['individualDiagnostic']['sf_sex'] == 2 ) ? 'checked' : '' ?>
								       id="checkbox_men" type="checkbox" class="form__checkbox" data-item >
								<label for="checkbox_men" class="form__label">Mężczyzna</label>
							</div>
							<div class="form__checkBoxHolder">
								<input name="user_sex" data-required="user_sex"
								       value="1"  <?= ( $_SESSION['individualDiagnostic']['sf_sex'] == 1 ) ? 'checked' : '' ?>
								       type="checkbox" id="checkbox_woman" class="form__checkbox" data-item>
								<label for="checkbox_woman" class="form__label">Kobieta</label>
							</div>
						</div>
						<div class="form__row form__inputsRow -marginBottom form-ankieta-1" data-id="form-row-sex">
							<div class="form-box">
								<div class="form__inputBox">
									<div class="form__label">Wiek</div>
									<input required name="user_age" pattern="[0-9,.]*" maxlength="2"
									       value="<?= esc_html( $_SESSION['individualDiagnostic']['sf_age'] ) ?>" class="form__input"
									       type="text" data-id="input-age">
								</div>
								<div class="form__inputBox">
									<div class="form__label">Wzrost</div>
									<input required name="user_height" pattern="[0-9,.]+" maxlength="3"
									       value="<?= esc_html( $_SESSION['individualDiagnostic']['sf_height'] ) ?>" class="form__input"
									       placeholder="cm" type="text">
								</div>
								<div class="form__inputBox">
									<div class="form__label">Waga</div>
									<input required name="user_weight" pattern="[0-9,.]+" maxlength="3"
									       value="<?= esc_html( $_SESSION['individualDiagnostic']['sf_weight'] ) ?>" class="form__input"
									       placeholder="kg" type="text">
								</div>

								<div class="form__inputBox">
									<div class="form__label">Obwód talii</div>
									<input required data-name="user_circuit_1" data-type="k, m" pattern="[0-9,.]+" maxlength="3"
									       value="<?= esc_html( $_SESSION['individualDiagnostic']['sf_circuit_1'] ) ?>" class="form__input"
									       placeholder="cm" type="text">
								</div>
								<div class="form__inputBox">
									<div class="form__label">Obwód brzucha</div>
									<input required data-name="user_circuit_2" data-type="k,m,d" pattern="[0-9,.]+" maxlength="3"
									       value="<?= esc_html( $_SESSION['individualDiagnostic']['sf_circuit_2'] ) ?>" class="form__input"
									       placeholder="cm" type="text">
								</div>
								<div class="form__inputBox">
									<div class="form__label">Obwód bioder</div>
									<input required data-name="user_circuit_3" data-type="k" pattern="[0-9,.]+" maxlength="3"
									       value="<?= esc_html( $_SESSION['individualDiagnostic']['sf_circuit_3'] ) ?>" class="form__input"
									       placeholder="cm" type="text">
								</div>
								<div class="form__inputBox">
									<div class="form__label">Obwód nadgarstka</div>
									<input required data-name="user_circuit_4" data-type="m" pattern="[0-9,.]+" maxlength="2"
									       value="<?= esc_html( $_SESSION['individualDiagnostic']['sf_circuit_4'] ) ?>" class="form__input"
									       placeholder="cm" type="text">
								</div>
							</div>

							<div class="form-box">
								<div class="form__inputBox">
									<div class="form__label">Waga przy której Pan/i czuje się najlepiej</div>
									<input required data-name="user_nice_weight" data-type="k,m" pattern="[0-9,.]+" maxlength="3"
									       value="<?= esc_html( $_SESSION['individualDiagnostic']['sf_nice_weight'] ) ?>" class="form__input"
									       placeholder="kg" type="text">
								</div>
							</div>

							<div class="form-box">
								<div class="form__inputBox">
									<div class="form__label">Najwyższa waga w dorosłym życiu (u kobiet bez ciąży)</div>
									<input required data-name="user_highest_weight" data-type="k,m" pattern="[0-9,.]+" maxlength="3"
									       value="<?= esc_html( $_SESSION['individualDiagnostic']['sf_highest_weight'] ) ?>"
									       class="form__input"
									       placeholder="kg" type="text">
								</div>
							</div>
						</div>

						<div class="form__help"><strong>WSKAZÓWKI DO POMIARÓW: </strong>
							<div class="form__help-text"><div data-type="m"><?= $titan->getOption('text_measure_m') ?></div> </div>
							<div class="form__help-text"><div data-type="k"><?= $titan->getOption('text_measure_k') ?></div> </div>
							<div class="form__help-text"><div data-type="d"><?= $titan->getOption('text_measure_d') ?></div> </div>
							<div>
								<?php ($titan->getOption( 'image_measure_m' )); ?>
								<img data-type="m" src="<?= wp_get_attachment_image_url( $titan->getOption( 'image_measure_m' ), '' ) ?>"
								     style="display: block;">
							</div>
							<div>
								<img data-type="k" src="<?= wp_get_attachment_image_url( $titan->getOption( 'image_measure_k' ), '' ) ?>"
								     style="display: block;">
							</div>
							<div>
								<img data-type="d" src="<?= wp_get_attachment_image_url( $titan->getOption( 'image_measure_d' ), '' ) ?>"
								     style="display: block;">
							</div>
						</div>
						<div class="form__inputsRow"><div class="forms__inputBox" style="width: 100%; padding-right: 30px;">
								<div class="form__label">Proszę opisać historię wagi dziecka</div>
								<textarea style="height:100px;width: 100%;" data-type="d" data-name="user_history_weight"
								          class="form__moreTextarea"><?= esc_html( $_SESSION['individualDiagnostic']['sf_history_weight'] ) ?></textarea>
							</div>
						</div>

						<div class="form__rowTitle">CEL WIZYTY</div>
						<div class="form__row">
							<div class="form__checkBoxHolder">
								<input name="user_purpose[]" data-required="user_purpose"
								       value="1" <?= in_array( 1, $_SESSION['individualDiagnostic']['sf_purpose'] ) ? 'checked' : '' ?>
								       type="checkbox" id="checkbox_1" class="form__checkbox">
								<label for="checkbox_1" class="form__label">odchudzający</label>
							</div>
							<div class="form__checkBoxHolder">
								<input name="user_purpose[]" data-required="user_purpose"
								       value="2" <?= in_array( 2, $_SESSION['individualDiagnostic']['sf_purpose'] ) ? 'checked' : '' ?>
								       id="checkbox_2" type="checkbox" class="form__checkbox">
								<label for="checkbox_2" class="form__label"> zdrowotny</label>
							</div>
							<div class="form__checkBoxHolder">
								<input name="user_purpose[]" data-required="user_purpose"
								       value="3" <?= in_array( 3, $_SESSION['individualDiagnostic']['sf_purpose'] ) ? 'checked' : '' ?>
								       id="checkbox_3" type="checkbox" class="form__checkbox">
								<label for="checkbox_3" class="form__label">przyrost masy ciała</label>
							</div>
							<div class="form__checkBoxHolder">
								<input name="user_purpose[]" data-required="user_purpose"
								       value="7" <?= in_array( 7, $_SESSION['individualDiagnostic']['sf_purpose'] ) ? 'checked' : '' ?>
								       id="checkbox_5a" type="checkbox" class="form__checkbox">
								<label for="checkbox_5a" class="form__label">podnoszący poziom energii</label>
							</div>
							<div class="form__checkBoxHolder">
								<input name="user_purpose[]" data-required="user_purpose"
								       value="5" <?= in_array( 5, $_SESSION['individualDiagnostic']['sf_purpose'] ) ? 'checked' : '' ?>
								       id="checkbox_5" type="checkbox" class="form__checkbox">
								<label for="checkbox_5" class="form__label">odmładzający, rewitalizujący</label>
							</div>
							<div class="form__checkBoxHolder">
								<input name="user_purpose[]" data-required="user_purpose"
								       value="4" <?= in_array( 4, $_SESSION['individualDiagnostic']['sf_purpose'] ) ? 'checked' : '' ?>
								       id="checkbox_4" type="checkbox" class="form__checkbox">
								<label for="checkbox_4" class="form__label">budujący masę mięśniową</label>
							</div>
							<div class="form__checkBoxHolder">
								<input class="form__checkbox -other" name="user_purpose[]" data-required="user_purpose"
								       value="6" <?= in_array( 6, $_SESSION['individualDiagnostic']['sf_purpose'] ) ? 'checked' : '' ?>
								       id="checkbox_6" type="checkbox" class="form__checkbox">
								<label for="checkbox_6" class="form__label">inny, jaki?</label>
							</div>

							<textarea name="user_other_purposes"
							          class="form__moreTextarea -longInput -otherInput -m-b-30"><?= esc_textarea( $_SESSION['individualDiagnostic']['sf_user_other_purposes'] ); ?></textarea>
						</div>

						<div class="form__row -spaceBetween">
							<div class="form__col50">
								<div class="form__hr"></div>
								<div class="form__rowTitle">OPERACJE OD OKRESU DZIECIĘGO</div>
								<div class="form__row" data-group="operation" data-show>
									<div class="form__checkBoxHolder -width100">
										<input <?= ( $_SESSION['individualDiagnostic']['sf_operation_yes_no'] === "0" ) ? 'checked' : '' ?>
											name="user_operation_yes_no" value="0" id="checkbox_8" type="checkbox" class="form__checkbox" data-required="user_operation_yes_no"
											data-item>
										<label for="checkbox_8" class="form__label"> NIE</label>
									</div>
									<div class="form__checkBoxHolder -width100">
										<input <?= ( $_SESSION['individualDiagnostic']['sf_operation_yes_no'] === "1" ) ? 'checked' : '' ?>
											name="user_operation_yes_no" data-toggle value="1" id="checkbox_7" type="checkbox"
											class="form__checkbox" data-required="user_operation_yes_no"
											data-item>
										<label for="checkbox_7" class="form__label"> TAK</label>
									</div>

									<div class="-hideOnNo subform -width100">
										<label class="form__label -hideOnNo">Jakie? Kiedy?</label>
										<textarea name="user_operation_ww"
										          class="form__moreTextarea -longInput -hideOnNo"><?= $_SESSION['individualDiagnostic']['sf_operation_ww'] ?></textarea>
									</div>
								</div>
								<div class="form__hr"></div>
								<div class="form__rowTitle">Zdiagnozowane choroby obecne i istotne przebyte w przeszłości</div>
								<div class="form__row" data-group="diseases" data-show>

									<div class="form__checkBoxHolder -width100">
										<input name="user_disease_yes_no"
										       value="0" <?= ( $_SESSION['individualDiagnostic']['sf_disease_yes_no'] === "0" ) ? 'checked' : '' ?>
										       data-required="user_disease_yes_no" id="checkbox_14" type="checkbox" class="form__checkbox" data-item>
										<label for="checkbox_14" class="form__label"> NIE</label>
									</div>
									<div class="form__checkBoxHolder -width100">
										<input name="user_disease_yes_no"
										       value="1" <?= ( $_SESSION['individualDiagnostic']['sf_disease_yes_no'] === "1" ) ? 'checked' : '' ?>
										       data-required="user_disease_yes_no" id="checkbox_13" type="checkbox" data-required="user_operation_yes_no" data-toggle class="form__checkbox" data-item>
										<label for="checkbox_13" class="form__label"> TAK</label>
									</div>
									<div class="-hideOnNo subform -width100">
										<label class="form__label  -hideOnNo">Jakie? Od kiedy?</label>
										<textarea name="user_disease_w"
										          class="form__moreTextarea -longInput -hideOnNo"><?= esc_textarea( $_SESSION['individualDiagnostic']['sf_disease_w'] ) ?></textarea>
									</div>
								</div>
								<div class="form__rowTitle">JAKIE CIŚNIENIE TĘTNICZE MA PAN/I </div>
								<div data-group="pressure" style="display: contents">
									<div class="form__checkBoxHolder">
										<input name="user_pressure" data-required="user_pressure"
										       value="1" <?= ( $_SESSION['individualDiagnostic']['sf_pressure'] === "1" ) ? 'checked' : '' ?>
										       id="checkbox_213" type="checkbox" class="form__checkbox" data-item>
										<label for="checkbox_213" class="form__label"> Prawidłowe</label>
									</div>
									<div class="form__checkBoxHolder">
										<input name="user_pressure" data-required="user_pressure"
										       value="2" <?= ( $_SESSION['individualDiagnostic']['sf_pressure'] === "2" ) ? 'checked' : '' ?>
										       id="checkbox_13a" type="checkbox" class="form__checkbox" data-item>
										<label for="checkbox_13a" class="form__label"> Niskie</label>
									</div>
									<div class="form__checkBoxHolder">
										<input name="user_pressure" data-required="user_pressure"
										       value="3" <?= ( $_SESSION['individualDiagnostic']['sf_pressure'] === "3" ) ? 'checked' : '' ?>
										       id="checkbox_13b" type="checkbox" class="form__checkbox" data-item>
										<label for="checkbox_13b" class="form__label"> Wysokie</label>
									</div>
									<div class="form__checkBoxHolder">
										<input name="user_pressure" data-required="user_pressure"
										       value="4" <?= ( $_SESSION['individualDiagnostic']['sf_pressure'] === "4" ) ? 'checked' : '' ?>
										       id="checkbox_13c" type="checkbox"  class="form__checkbox" data-item>
										<label for="checkbox_13c" class="form__label"> Nie wiem</label>
									</div>
								</div>
							</div>

							<div class="form__col50">
								<div class="form__hr"></div>
								<div class="form__rowTitle">LEKI</div>
								<div class="form__row">
									<div class="form__row" data-show>
										<div data-group="medical" >
											<div class="form__checkBoxHolder -width100">
												<input <?= ( $_SESSION['individualDiagnostic']['sf_medicines_yes_no'] === "0" ) ? 'checked' : '' ?>
													data-required="user_medicines_yes_no" name="user_medicines_yes_no" value="0" id="checkbox_10" type="checkbox" class="form__checkbox"
													data-item>
												<label for="checkbox_10" class="form__label"> NIE</label>
											</div>
											<div class="form__checkBoxHolder -width100">
												<input
													name="user_medicines_yes_no" <?= ( $_SESSION['individualDiagnostic']['sf_medicines_yes_no'] === "1" ) ? 'checked' : '' ?>
													data-required="user_medicines_yes_no" value="1" id="checkbox_9" data-toggle type="checkbox" class="form__checkbox" data-item>
												<label for="checkbox_9" class="form__label"> TAK</label>
											</div>

										</div>
										<div class="-hideOnNo subform -width100">
											<label class="form__rowTitle  -hideOnNo">Przewlekle brane</label>
											<div class="-hideOnNo" style="display: contents" data-show >
												<div class="medical_long_term" data-group="medical_long_term">
													<div class="form__checkBoxHolder -width100">
														<input name="user_medicines_chronic" <?= ( $_SESSION['individualDiagnostic']['sf_medicines_chronic'] === "0" ) ? 'checked' : '' ?>
														       value="0" id="checkbox_912" type="checkbox" class="form__checkbox"
														       data-item>
														<label for="checkbox_912" class="form__label"> NIE</label>
													</div>
													<div class="form__checkBoxHolder -width100">
														<input name="user_medicines_chronic" <?= ( $_SESSION['individualDiagnostic']['sf_medicines_chronic'] === "1" ) ? 'checked' : '' ?>
														       value="1" id="checkbox_1z1" type="checkbox" class="form__checkbox" data-toggle data-item>
														<label for="checkbox_1z1" class="form__label"> TAK</label>
													</div>

												</div>
												<div class="subform" style="display: contents">
													<label class="form__label  -hideOnNo">Jakie? Kiedy?</label>
													<textarea name="user_medicines_chronic_w"
													          class="form__moreTextarea -longInput -hideOnNo"><?= esc_textarea( $_SESSION['individualDiagnostic']['sf_medicines_chronic_w'] ) ?></textarea>
												</div>
											</div>
										</div>
										<div class="-hideOnNo subform -width100">

											<label class="form__rowTitle  -hideOnNo">Okresowo/doraźnie np. antyalergicznie</label>
											<div class="-hideOnNo" style="display: contents" data-show >
												<div class="medical_long_term" data-group="medical_long_term" >
													<div class="form__checkBoxHolder -width100">
														<input <?= ( $_SESSION['individualDiagnostic']['sf_medicines_season'] === "0" ) ? 'checked' : '' ?>
															name="user_medicines_season" value="0" id="checkbox_114" type="checkbox" class="form__checkbox"
															data-item>
														<label for="checkbox_114" class="form__label"> NIE</label>
													</div>
													<div class="form__checkBoxHolder -width100">
														<input
															name="user_medicines_season" <?= ( $_SESSION['individualDiagnostic']['sf_medicines_season'] === "1" ) ? 'checked' : '' ?>
															value="1" id="checkbox_113" type="checkbox" class="form__checkbox" data-toggle data-item>
														<label for="checkbox_113" class="form__label"> TAK</label>
													</div>

												</div>
												<div class="subform" style="display: contents">
													<label class="form__label  -hideOnNo">Jakie? Kiedy?</label>
													<textarea name="user_medicines_season_w"
													          class="form__moreTextarea -longInput -hideOnNo"><?= esc_textarea( $_SESSION['individualDiagnostic']['sf_medicines_season_w'] ) ?></textarea>
												</div>
											</div>

										</div>


									</div>
								</div>
							</div>
						</div>
						<div class="form__row js-file-research">
							<div id="file-research-modal" class="modal" style="display: none"></div>
							<div class="form__rowTitle">BADANIA</div>
							<div class="fileUploader">
								<p class="fileUploader__text">Jeśli posiadasz wykonane badania, wczytaj skany tutaj. Dopuszczalne
									rozszerzenia: jpg, png, pdf, doc, odt. Maksymalny rozmiar pojedyńczego pliku to 2mb.</p>
								<div class="fileUploader__inputbox">
									<input class="fileUploader__file" id="file-research" type="file" data-section="research"
									       accept="image/jpg, image/jpeg,image/gif,image/png,	application/pdf,text/plain, application/msword,	application/vnd.oasis.opendocument.text"/>
									<label class="btn fileUploader__upload" for="file-research"><span>Dodaj badanie</span><i
											class="fa fa-circle-o-notch fa-spin fa-fw"></i></label>
								</div>
								<div class="fileUploader__filebox">
									<?php foreach ( getFilesFromSection( $userRevita->getId(), 'research' ) as $image ) { ?>
										<div class="fileUploader__uploaded">
											<div class="fileUploader__uploadedIcon">
												<i class="fa fa-file-text-o"></i>
											</div>
											<div class="fileUploader__uploadedName"><?= $image->post_title ?></div>
											<div class="fileUploader__uploadedDelete" data-delete="<?= $image->ID ?>'"><i
													class="fa fa-trash-o"></i></div>
										</div>
									<?php } ?>
								</div>

								<p class="fileUploader__infobox"><strong>Co może być pomocne dla naszych lekarzy?</strong><br> Załącz nam
									badania krwi, tarczycy i usg klatki jeśli takie posiadasz. Jeśli nie - nie przejmuj się - nie są one
									obowiązkowe.</p>
							</div>

						</div>


						<input type="hidden" name="action" value="ankieta_1">
						<input type="hidden" name="step" value="ankieta_1">
						<div class="form__nav" data-questionnaire-navigation>
							<button class="btn form__saveDiet" data-questionnaire-save>Zapisz ankietę</button>
							<button onclick="window.onbeforeunload = null;" class="form__button">PRZEJDŹ DALEJ</button>
						</div>
					</div>

				</div>
			</form>
		</main><!-- #main -->
	</div><!-- #primary -->

	<script>
        (function(){
            var inputs = document.querySelectorAll('input, select, textarea');
            for(var i = 0; inputs && i < inputs.length; i++)
                inputs[i].addEventListener('change',function () {
                    window.onbeforeunload = function () {
                        return true;
                    };
                });
        })();
	</script>
<?php get_footer();