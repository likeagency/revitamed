<?php
/**
 * The template for displaying all pages.
 * Template name: Moje konto
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package like
 */


user_page(false,false);
$userRevitamed = new user();
if( ($userRevitamed->hasActiveDiet() || $userRevitamed->hasFutureDiet()) && $userRevitamed->hasActiveSub()>0)
{
  include 'konto-aktualna-dieta.php';
  die();
}
$titan = TitanFramework::getInstance('revita');
$name = $userRevitamed->getName();
$measure = getTodayMeasure($userRevitamed->getId());
get_header(); ?>
  <div id="primary" class="content-area konto-tpl">
    <div class="container">
      <div class="col-12">
      <div class="wrapper">
        <div class="title">Witaj, <span class="name"><?= $name ?>!</span> Zacznij z naszą pomocą dobry dzień</div>
        <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
            <?php if (function_exists('bcn_display')) {
                bcn_display();
            } ?>
        </div>
      </div><!-- ./wrapper -->
    </div>
    </div>
    <div class="container">
      <aside class="col-3 sidebar-konto">
        <div class="side-menu side-item">
		      <?php include 'template-parts/menu-user.php' ?>
        </div><!-- ./sidemenu -->
	      <?php include 'template-parts/progress-user.php' ?>
	      <?php include 'template-parts/subs-user.php' ?>
      </aside>
      <main id="main" class="site-main account-p col-9" role="main">
	      <?php if(isset($_GET['survey']) && $_GET['survey']=='1'){?>
            <div style="padding: 15px; margin-bottom: 20px; border: 1px solid #dff0d8;  border-radius: 4px; background-color: #dff0d8;"><?= $titan->getOption('post_survey_text_paid_account') ?></div>
	      <?php } ?>
        <?php if(!$userRevitamed->hasSurvey()){ ?>
        <section class="questionnaire">
          <div class="questionnaire__content">
            <div class="questionnaire__text">
              <div class="questionnaire__icon">
                <img src="<?php echo get_template_directory_uri(); ?>/images/ico-quest.svg"
                     alt="<?php bloginfo('name'); ?>"/>
              </div>
              Wypełnij ankietę aby otrzymać zindywidualizowaną dietę
            </div>
            <a href="<?= get_permalink_template('ankieta_1.php') ?>" class="btn questionnaire__button">Wypełnij ankietę &gt;</a>
          </div>
        </section>
        <?php } ?>

        <?php if(!$userRevitamed->hasActiveSub() && $userRevitamed->hasSurvey()){ ?>
        <section class="questionnaire">
          <div class="questionnaire__content">
            <div class="questionnaire__text">
              <div class="questionnaire__icon">
                <img src="<?php echo get_template_directory_uri(); ?>/images/ico-hand.svg"
                     alt="<?php bloginfo('name'); ?>"/>
              </div>
              Opłać abonament aby otrzymać spersonalizowaną dietę
            </div>
            <a href="<?= get_permalink_template('konto-platnosci.php') ?>" class="btn questionnaire__button">OPŁAĆ ABONAMENT &gt;</a>
          </div>
        </section>
        <?php } ?>

        <?php

        if(get_post_meta($userRevitamed->getId(),'sf_survey_part',true)) {
	        $partialSurvey = get_post_meta($userRevitamed->getId(),'sf_survey_part',true);

	        if(get_post_status($partialSurvey)!='publish')
          {
            delete_post_meta($userRevita->getId(),'sf_survey_part');
          }
          else{
          ?>
            <section class="questionnaire">
              <div class="questionnaire__content">
                <div class="questionnaire__text">
                  Nie było cię chwilę! Mamy kilka pytań
                </div>
                <a href="<?= get_the_permalink($partialSurvey) ?>" class="btn questionnaire__button">Wypełnij ankietę &gt;</a>
              </div>
            </section>
        <?php }} ?>

        <section class="row konto-tpl__box">
          <div class="col-6">
        <?php if(!$userRevitamed->hasActiveDiet() && $userRevitamed->hasSurvey() && $userRevitamed->hasActiveSub()){
          if(getCurrentUserPlan( $userID,strtotime('
	+21 days') ) != 0){
          ?>
            <section class="questionnaire">
              <div class="questionnaire__content">
                <div class="questionnaire__text">
                  <div class="questionnaire__icon">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/ico-alarm.svg"
                         alt="<?php bloginfo('name'); ?>"/>
                  </div>
                  Twoja dieta będzie gotowa w ciągu 3 dni roboczych
                </div>
                <a href="<?= get_the_permalink(get_option('page_for_posts')) ?>" class="btn questionnaire__button">CZYTAJ PORADY &gt;</a>
              </div>
            </section>
            <?php }else{ ?>
            <section class="questionnaire">
              <div class="questionnaire__content">
                <div class="questionnaire__text">
                  <div class="questionnaire__icon">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/ico-hand.svg"
                         alt="<?php bloginfo('name'); ?>"/>
                  </div>
                 <?= $titan->getOption('text-no-diet-21-days'); ?>
                </div>
                <a href="<?= get_permalink_template('konto-platnosci.php') ?>" class="btn questionnaire__button">OPŁAĆ ABONAMENT &gt;</a>
              </div>
            </section>

        <?php }
        } ?>
          </div>
          <?php if($userRevitamed->hasActiveSub()){ ?>
          <div class="col-6">
            <div class="msrmnt">
              <h4>POMIARY - <?= date('d.m.y') ?></h4>
              <div class="js-alert-container"></div>
              <form class="js-submit-weight" method="post">
                <fieldset class="row1">
                  <label for="waga">Twoja waga:</label>
                  <span class="in-wrap kg">
								<input value="<?= $measure['user_weight'] ?>" required type="text" name="user_weight" pattern="[0-9,]+" id="waga">
							</span>
                  <input name="action" type="hidden" value="weight_update">
                  <input type="submit" value="ZAPISZ">
                </fieldset><!-- ./waga -->
              </form>
              <form class="js-submit-circuit" method="post">
                <fieldset class="row2">
                  <div>
                    <label for="ob-talia">Obwód talii:</label>
                    <span class="in-wrap cm">
									<input value="<?= $measure['ob-talia'] ?>" type="text" required name="ob-talia" pattern="[0-9,]+">
								</span>
                  </div>
                  <div>
                    <label for="ob-brzuch">Obwód brzucha:</label>
                    <span class="in-wrap cm">
									<input value="<?= $measure['ob-brzuch'] ?>" type="text" required name="ob-brzuch" pattern="[0-9,]+">
								</span>
                  </div>
                  <div>
                    <label for="ob-biodra">Obwód bioder:</label>
                    <span class="in-wrap cm">
									<input value="<?= $measure['ob-biodra'] ?>" type="text" required name="ob-biodra" pattern="[0-9,]+" >
								</span>
                  </div>
                  <div>
                    <input name="action" type="hidden" value="circuit_update">
                    <input type="submit" value="ZAPISZ">
                  </div>
                </fieldset>
              </form>
            </div>
          </div><!-- ./msrmnt -->
        <?php } ?>
        </section><!-- ./diet-header -->

      </main><!-- #main -->
    </div><!-- ./container -->
  </div><!-- #primary -->
<?php get_footer();
